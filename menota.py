#   
# WARNING: This is an automatically generated file.  
#   
# Created from /home/bluche/src/kaldiscripts/tbkaldi/lang/menota-entities.txt  
# Creation time: Wed Jun  4 11:18:48 2014  
#   
#   
#   


class MenotaEntities:
  
  
  #  This list of entities is identical to the one defined in the MUFI character recommendation v. 3.0 
  #  Each entity is assigned to a unique code point, and vice versa 
  #  Saturday 20 June 2009, Odd Einar Haugen 
  
  #  Entity added Tuesday 24 January 2012, Odd Einar Haugen 
  space = u'\u0020'	#  SPACE 
  
  #  (I) Characters in the Unicode Standard version 5.1 
  
  #  (1): Basic Latin 
  
  #  Characters in Basic Latin which should be designed by entities to avoid misrepresentation (specified by XML, but included here anyhow) 
  
  quot = u'\u0022'	#  QUOTATION MARK 
  amp = u'\u0026'	#  AMPERSAND 
  apos = u'\u0027'	#  APOSTROPHE 
  lt = u'\u003C'	#  LESS-THAN SIGN 
  gt = u'\u003E'	#  GREATER-THAN SIGN 
  
  #  Characters in Basic Latin which sometimes are designed by entities 
  
  excl = u'\u0021'	#  EXCLAMATION MARK 
  dollar = u'\u0024'	#  DOLLAR SIGN 
  percnt = u'\u0025'	#  PERCENT SIGN 
  lpar = u'\u0028'	#  LEFT PARENTHESIS 
  rpar = u'\u0029'	#  RIGHT PARENTHESIS 
  ast = u'\u002A'	#  ASTERISK 
  plus = u'\u002B'	#  PLUS SIGN 
  comma = u'\u002C'	#  COMMA 
  hyphen = u'\u002D'	#  HYPHEN-MINUS 
  period = u'\u002E'	#  FULL STOP 
  sol = u'\u002F'	#  SOLIDUS 
  colon = u'\u003A'	#  COLON 
  semi = u'\u003B'	#  SEMICOLON 
  equals = u'\u003D'	#  EQUALS SIGN 
  quest = u'\u003F'	#  QUESTION MARK 
  commat = u'\u0040'	#  COMMERCIAL AT 
  lsqb = u'\u005B'	#  LEFT SQUARE BRACKET 
  bsol = u'\u005C'	#  REVERSE SOLIDUS 
  rsqb = u'\u005D'	#  RIGHT SQUARE BRACKET 
  circ = u'\u005E'	#  CIRCUMFLEX ACCENT 
  lowbar = u'\u005F'	#  LOW LINE 
  grave = u'\u0060'	#  GRAVE ACCENT 
  lcub = u'\u007B'	#  LEFT CURLY BRACKET 
  verbar = u'\u007C'	#  VERTICAL LINE 
  rcub = u'\u007D'	#  RIGHT CURLY BRACKET 
  tld = u'\u007E'	#  TILDE 
  
  Basic_Latin = u''
  Basic_Latin += quot + amp + apos + lt + gt
  Basic_Latin += excl + dollar + percnt + lpar + rpar
  Basic_Latin += ast + plus + comma + hyphen + period
  Basic_Latin += sol + colon + semi + equals + quest
  Basic_Latin += commat + lsqb + bsol + rsqb + circ
  Basic_Latin += lowbar + grave + lcub + verbar + rcub
  Basic_Latin += tld
  
  
  #  (2): Latin-1 Supplement 
  
  nbsp = u'\u00A0'	#  NO-BREAK SPACE 
  iexcl = u'\u00A1'	#  INVERTED EXCLAMATION MARK 
  cent = u'\u00A2'	#  CENT SIGN 
  pound = u'\u00A3'	#  POUND SIGN 
  curren = u'\u00A4'	#  CURRENCY SIGN 
  yen = u'\u00A5'	#  YEN SIGN 
  brvbar = u'\u00A6'	#  BROKEN BAR 
  sect = u'\u00A7'	#  SECTION SIGN 
  uml = u'\u00A8'	#  DIAERESIS 
  copy = u'\u00A9'	#  COPYRIGHT SIGN 
  ordf = u'\u00AA'	#  FEMININE ORDINAL INDICATOR 
  laquo = u'\u00AB'	#  LEFT-POINTING DOUBLE ANGLE QUOTATION MARK 
  not_ = u'\u00AC'	#  NOT SIGN 
  shy = u'\u00AD'	#  SOFT HYPHEN 
  reg = u'\u00AE'	#  REGISTERED SIGN 
  macr = u'\u00AF'	#  MACRON 
  deg = u'\u00B0'	#  DEGREE SIGN 
  plusmn = u'\u00B1'	#  PLUS-MINUS SIGN 
  sup2 = u'\u00B2'	#  SUPERSCRIPT TWO 
  sup3 = u'\u00B3'	#  SUPERSCRIPT THREE 
  acute = u'\u00B4'	#  ACUTE ACCENT 
  micro = u'\u00B5'	#  MICRO SIGN 
  para = u'\u00B6'	#  PILCROW SIGN 
  middot = u'\u00B7'	#  MIDDLE DOT 
  cedil = u'\u00B8'	#  CEDILLA 
  sup1 = u'\u00B9'	#  SUPERSCRIPT ONE 
  ordm = u'\u00BA'	#  MASCULINE ORDINAL INDICATOR 
  raquo = u'\u00BB'	#  RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK 
  frac14 = u'\u00BC'	#  VULGAR FRACTION ONE QUARTER 
  frac12 = u'\u00BD'	#  VULGAR FRACTION ONE HALF 
  frac34 = u'\u00BE'	#  VULGAR FRACTION THREE QUARTERS 
  iquest = u'\u00BF'	#  INVERTED QUESTION MARK 
  Agrave = u'\u00C0'	#  LATIN CAPITAL LETTER A WITH GRAVE 
  Aacute = u'\u00C1'	#  LATIN CAPITAL LETTER A WITH ACUTE 
  Acirc = u'\u00C2'	#  LATIN CAPITAL LETTER A WITH CIRCUMFLEX 
  Atilde = u'\u00C3'	#  LATIN CAPITAL LETTER A WITH TILDE 
  Auml = u'\u00C4'	#  LATIN CAPITAL LETTER A WITH DIAERESIS 
  Aring = u'\u00C5'	#  LATIN CAPITAL LETTER A WITH RING ABOVE 
  AElig = u'\u00C6'	#  LATIN CAPITAL LETTER AE 
  Ccedil = u'\u00C7'	#  LATIN CAPITAL LETTER C WITH CEDILLA 
  Egrave = u'\u00C8'	#  LATIN CAPITAL LETTER E WITH GRAVE 
  Eacute = u'\u00C9'	#  LATIN CAPITAL LETTER E WITH ACUTE 
  Ecirc = u'\u00CA'	#  LATIN CAPITAL LETTER E WITH CIRCUMFLEX 
  Euml = u'\u00CB'	#  LATIN CAPITAL LETTER E WITH DIAERESIS 
  Igrave = u'\u00CC'	#  LATIN CAPITAL LETTER I WITH GRAVE 
  Iacute = u'\u00CD'	#  LATIN CAPITAL LETTER I WITH ACUTE 
  Icirc = u'\u00CE'	#  LATIN CAPITAL LETTER I WITH CIRCUMFLEX 
  Iuml = u'\u00CF'	#  LATIN CAPITAL LETTER I WITH DIAERESIS 
  ETH = u'\u00D0'	#  LATIN CAPITAL LETTER ETH 
  Ntilde = u'\u00D1'	#  LATIN CAPITAL LETTER N WITH TILDE 
  Ograve = u'\u00D2'	#  LATIN CAPITAL LETTER O WITH GRAVE 
  Oacute = u'\u00D3'	#  LATIN CAPITAL LETTER O WITH ACUTE 
  Ocirc = u'\u00D4'	#  LATIN CAPITAL LETTER O WITH CIRCUMFLEX 
  Otilde = u'\u00D5'	#  LATIN CAPITAL LETTER O WITH TILDE 
  Ouml = u'\u00D6'	#  LATIN CAPITAL LETTER O WITH DIAERESIS 
  times = u'\u00D7'	#  MULTIPLICATION SIGN 
  Oslash = u'\u00D8'	#  LATIN CAPITAL LETTER O WITH STROKE 
  Ugrave = u'\u00D9'	#  LATIN CAPITAL LETTER U WITH GRAVE 
  Uacute = u'\u00DA'	#  LATIN CAPITAL LETTER U WITH ACUTE 
  Ucirc = u'\u00DB'	#  LATIN CAPITAL LETTER U WITH CIRCUMFLEX 
  Uuml = u'\u00DC'	#  LATIN CAPITAL LETTER U WITH DIAERESIS 
  Yacute = u'\u00DD'	#  LATIN CAPITAL LETTER Y WITH ACUTE 
  THORN = u'\u00DE'	#  LATIN CAPITAL LETTER THORN 
  szlig = u'\u00DF'	#  LATIN SMALL LETTER SHARP S 
  agrave = u'\u00E0'	#  LATIN SMALL LETTER A WITH GRAVE 
  aacute = u'\u00E1'	#  LATIN SMALL LETTER A WITH ACUTE 
  acirc = u'\u00E2'	#  LATIN SMALL LETTER A WITH CIRCUMFLEX 
  atilde = u'\u00E3'	#  LATIN SMALL LETTER A WITH TILDE 
  auml = u'\u00E4'	#  LATIN SMALL LETTER A WITH DIAERESIS 
  aring = u'\u00E5'	#  LATIN SMALL LETTER A WITH RING ABOVE 
  aelig = u'\u00E6'	#  LATIN SMALL LETTER AE 
  ccedil = u'\u00E7'	#  LATIN SMALL LETTER C WITH CEDILLA 
  egrave = u'\u00E8'	#  LATIN SMALL LETTER E WITH GRAVE 
  eacute = u'\u00E9'	#  LATIN SMALL LETTER E WITH ACUTE 
  ecirc = u'\u00EA'	#  LATIN SMALL LETTER E WITH CIRCUMFLEX 
  euml = u'\u00EB'	#  LATIN SMALL LETTER E WITH DIAERESIS 
  igrave = u'\u00EC'	#  LATIN SMALL LETTER I WITH GRAVE 
  iacute = u'\u00ED'	#  LATIN SMALL LETTER I WITH ACUTE 
  icirc = u'\u00EE'	#  LATIN SMALL LETTER I WITH CIRCUMFLEX 
  iuml = u'\u00EF'	#  LATIN SMALL LETTER I WITH DIAERESIS 
  eth = u'\u00F0'	#  LATIN SMALL LETTER ETH 
  ntilde = u'\u00F1'	#  LATIN SMALL LETTER N WITH TILDE 
  ograve = u'\u00F2'	#  LATIN SMALL LETTER O WITH GRAVE 
  oacute = u'\u00F3'	#  LATIN SMALL LETTER O WITH ACUTE 
  ocirc = u'\u00F4'	#  LATIN SMALL LETTER O WITH CIRCUMFLEX 
  otilde = u'\u00F5'	#  LATIN SMALL LETTER O WITH TILDE 
  ouml = u'\u00F6'	#  LATIN SMALL LETTER O WITH DIAERESIS 
  divide = u'\u00F7'	#  DIVISION SIGN 
  oslash = u'\u00F8'	#  LATIN SMALL LETTER O WITH STROKE 
  ugrave = u'\u00F9'	#  LATIN SMALL LETTER U WITH GRAVE 
  uacute = u'\u00FA'	#  LATIN SMALL LETTER U WITH ACUTE 
  ucirc = u'\u00FB'	#  LATIN SMALL LETTER U WITH CIRCUMFLEX 
  uuml = u'\u00FC'	#  LATIN SMALL LETTER U WITH DIAERESIS 
  yacute = u'\u00FD'	#  LATIN SMALL LETTER Y WITH ACUTE 
  thorn = u'\u00FE'	#  LATIN SMALL LETTER THORN 
  yuml = u'\u00FF'	#  LATIN SMALL LETTER Y WITH DIAERESIS 
  
  Latin1_Supplement = u''
  Latin1_Supplement += nbsp + iexcl + cent + pound + curren
  Latin1_Supplement += yen + brvbar + sect + uml + copy
  Latin1_Supplement += ordf + laquo + not_ + shy + reg
  Latin1_Supplement += macr + deg + plusmn + sup2 + sup3
  Latin1_Supplement += acute + micro + para + middot + cedil
  Latin1_Supplement += sup1 + ordm + raquo + frac14 + frac12
  Latin1_Supplement += frac34 + iquest + Agrave + Aacute + Acirc
  Latin1_Supplement += Atilde + Auml + Aring + AElig + Ccedil
  Latin1_Supplement += Egrave + Eacute + Ecirc + Euml + Igrave
  Latin1_Supplement += Iacute + Icirc + Iuml + ETH + Ntilde
  Latin1_Supplement += Ograve + Oacute + Ocirc + Otilde + Ouml
  Latin1_Supplement += times + Oslash + Ugrave + Uacute + Ucirc
  Latin1_Supplement += Uuml + Yacute + THORN + szlig + agrave
  Latin1_Supplement += aacute + acirc + atilde + auml + aring
  Latin1_Supplement += aelig + ccedil + egrave + eacute + ecirc
  Latin1_Supplement += euml + igrave + iacute + icirc + iuml
  Latin1_Supplement += eth + ntilde + ograve + oacute + ocirc
  Latin1_Supplement += otilde + ouml + divide + oslash + ugrave
  Latin1_Supplement += uacute + ucirc + uuml + yacute + thorn
  Latin1_Supplement += yuml
  
  
  #  (3): Latin Extended-A 
  
  Amacr = u'\u0100'	#  LATIN CAPITAL LETTER A WITH MACRON 
  amacr = u'\u0101'	#  LATIN SMALL LETTER A WITH MACRON 
  Abreve = u'\u0102'	#  LATIN CAPITAL LETTER A WITH BREVE 
  abreve = u'\u0103'	#  LATIN SMALL LETTER A WITH BREVE 
  Aogon = u'\u0104'	#  LATIN CAPITAL LETTER A WITH OGONEK 
  aogon = u'\u0105'	#  LATIN SMALL LETTER A WITH OGONEK 
  Cacute = u'\u0106'	#  LATIN CAPITAL LETTER C WITH ACUTE 
  cacute = u'\u0107'	#  LATIN SMALL LETTER C WITH ACUTE 
  Cdot = u'\u010A'	#  LATIN CAPITAL LETTER C WITH DOT ABOVE 
  cdot = u'\u010B'	#  LATIN SMALL LETTER C WITH DOT ABOVE 
  Dstrok = u'\u0110'	#  LATIN CAPITAL LETTER D WITH STROKE 
  dstrok = u'\u0111'	#  LATIN SMALL LETTER D WITH STROKE 
  Emacr = u'\u0112'	#  LATIN CAPITAL LETTER E WITH MACRON 
  emacr = u'\u0113'	#  LATIN SMALL LETTER E WITH MACRON 
  Ebreve = u'\u0114'	#  LATIN CAPITAL LETTER E WITH BREVE 
  ebreve = u'\u0115'	#  LATIN SMALL LETTER E WITH BREVE 
  Edot = u'\u0116'	#  LATIN CAPITAL LETTER E WITH DOT ABOVE 
  edot = u'\u0117'	#  LATIN SMALL LETTER E WITH DOT ABOVE 
  Eogon = u'\u0118'	#  LATIN CAPITAL LETTER E WITH OGONEK 
  eogon = u'\u0119'	#  LATIN SMALL LETTER E WITH OGONEK 
  Gdot = u'\u0120'	#  LATIN CAPITAL LETTER G WITH DOT ABOVE 
  gdot = u'\u0121'	#  LATIN SMALL LETTER G WITH DOT ABOVE 
  hstrok = u'\u0127'	#  LATIN SMALL LETTER H WITH STROKE 
  Imacr = u'\u012A'	#  LATIN CAPITAL LETTER I WITH MACRON 
  imacr = u'\u012B'	#  LATIN SMALL LETTER I WITH MACRON 
  Ibreve = u'\u012C'	#  LATIN CAPITAL LETTER I WITH BREVE 
  ibreve = u'\u012D'	#  LATIN SMALL LETTER I WITH BREVE 
  Iogon = u'\u012E'	#  LATIN CAPITAL LETTER I WITH OGONEK 
  iogon = u'\u012F'	#  LATIN SMALL LETTER I WITH OGONEK 
  Idot = u'\u0130'	#  LATIN CAPITAL LETTER I WITH DOT ABOVE 
  inodot = u'\u0131'	#  LATIN SMALL LETTER DOTLESS I 
  IJlig = u'\u0132'	#  LATIN CAPITAL LIGATURE IJ 
  ijlig = u'\u0133'	#  LATIN SMALL LIGATURE IJ 
  Lacute = u'\u0139'	#  LATIN CAPITAL LETTER L WITH ACUTE 
  lacute = u'\u013A'	#  LATIN SMALL LETTER L WITH ACUTE 
  Lstrok = u'\u0141'	#  LATIN CAPITAL LETTER L WITH STROKE 
  lstrok = u'\u0142'	#  LATIN SMALL LETTER L WITH STROKE 
  Nacute = u'\u0143'	#  LATIN CAPITAL LETTER N WITH ACUTE 
  nacute = u'\u0144'	#  LATIN SMALL LETTER N WITH ACUTE 
  ENG = u'\u014A'	#  LATIN CAPITAL LETTER ENG 
  eng = u'\u014B'	#  LATIN SMALL LETTER ENG 
  Omacr = u'\u014C'	#  LATIN CAPITAL LETTER O WITH MACRON 
  omacr = u'\u014D'	#  LATIN SMALL LETTER O WITH MACRON 
  Obreve = u'\u014E'	#  LATIN CAPITAL LETTER O WITH BREVE 
  obreve = u'\u014F'	#  LATIN SMALL LETTER O WITH BREVE 
  Odblac = u'\u0150'	#  LATIN CAPITAL LETTER O WITH DOUBLE ACUTE 
  odblac = u'\u0151'	#  LATIN SMALL LETTER O WITH DOUBLE ACUTE 
  OElig = u'\u0152'	#  LATIN CAPITAL LIGATURE OE 
  oelig = u'\u0153'	#  LATIN SMALL LIGATURE OE 
  Racute = u'\u0154'	#  LATIN CAPITAL LETTER R WITH ACUTE 
  racute = u'\u0155'	#  LATIN SMALL LETTER R WITH ACUTE 
  Sacute = u'\u015A'	#  LATIN CAPITAL LETTER S WITH ACUTE 
  sacute = u'\u015B'	#  LATIN SMALL LETTER S WITH ACUTE 
  Umacr = u'\u016A'	#  LATIN CAPITAL LETTER U WITH MACRON 
  umacr = u'\u016B'	#  LATIN SMALL LETTER U WITH MACRON 
  Ubreve = u'\u016C'	#  LATIN CAPITAL LETTER U WITH BREVE 
  ubreve = u'\u016D'	#  LATIN SMALL LETTER U WITH BREVE 
  Uring = u'\u016E'	#  LATIN CAPITAL LETTER U WITH RING ABOVE 
  uring = u'\u016F'	#  LATIN SMALL LETTER U WITH RING ABOVE 
  Udblac = u'\u0170'	#  LATIN CAPITAL LETTER U WITH DOUBLE ACUTE 
  udblac = u'\u0171'	#  LATIN SMALL LETTER U WITH DOUBLE ACUTE 
  Uogon = u'\u0172'	#  LATIN CAPITAL LETTER U WITH OGONEK 
  uogon = u'\u0173'	#  LATIN SMALL LETTER U WITH OGONEK 
  Wcirc = u'\u0174'	#  LATIN CAPITAL LETTER W WITH CIRCUMFLEX 
  wcirc = u'\u0175'	#  LATIN SMALL LETTER W WITH CIRCUMFLEX 
  Ycirc = u'\u0176'	#  LATIN CAPITAL LETTER Y WITH CIRCUMFLEX 
  ycirc = u'\u0177'	#  LATIN SMALL LETTER Y WITH CIRCUMFLEX 
  Yuml = u'\u0178'	#  LATIN CAPITAL LETTER Y WITH DIAERESIS 
  Zdot = u'\u017B'	#  LATIN CAPITAL LETTER Z WITH DOT ABOVE 
  zdot = u'\u017C'	#  LATIN SMALL LETTER Z WITH DOT ABOVE 
  slong = u'\u017F'	#  LATIN SMALL LETTER LONG S 
  
  Latin_ExtendedA = u''
  Latin_ExtendedA += Amacr + amacr + Abreve + abreve + Aogon
  Latin_ExtendedA += aogon + Cacute + cacute + Cdot + cdot
  Latin_ExtendedA += Dstrok + dstrok + Emacr + emacr + Ebreve
  Latin_ExtendedA += ebreve + Edot + edot + Eogon + eogon
  Latin_ExtendedA += Gdot + gdot + hstrok + Imacr + imacr
  Latin_ExtendedA += Ibreve + ibreve + Iogon + iogon + Idot
  Latin_ExtendedA += inodot + IJlig + ijlig + Lacute + lacute
  Latin_ExtendedA += Lstrok + lstrok + Nacute + nacute + ENG
  Latin_ExtendedA += eng + Omacr + omacr + Obreve + obreve
  Latin_ExtendedA += Odblac + odblac + OElig + oelig + Racute
  Latin_ExtendedA += racute + Sacute + sacute + Umacr + umacr
  Latin_ExtendedA += Ubreve + ubreve + Uring + uring + Udblac
  Latin_ExtendedA += udblac + Uogon + uogon + Wcirc + wcirc
  Latin_ExtendedA += Ycirc + ycirc + Yuml + Zdot + zdot
  Latin_ExtendedA += slong
  
  
  #  (4): Latin Extended-B 
  
  bstrok = u'\u0180'	#  LATIN SMALL LETTER B WITH STROKE 
  hwair = u'\u0195'	#  LATIN SMALL LETTER HV 
  khook = u'\u0199'	#  LATIN SMALL LETTER K WITH HOOK 
  lbar = u'\u019A'	#  LATIN SMALL LETTER L WITH BAR 
  nlrleg = u'\u019E'	#  LATIN SMALL LETTER N WITH LONG RIGHT LEG 
  YR = u'\u01A6'	#  LATIN LETTER YR 
  Zstrok = u'\u01B5'	#  LATIN CAPITAL LETTER Z WITH STROKE 
  zstrok = u'\u01B6'	#  LATIN SMALL LETTER Z WITH STROKE 
  EZH = u'\u01B7'	#  LATIN CAPITAL LETTER EZH 
  wynn = u'\u01BF'	#  LATIN LETTER WYNN 
  Ocar = u'\u01D1'	#  LATIN CAPITAL LETTER O WITH CARON 
  ocar = u'\u01D2'	#  LATIN SMALL LETTER O WITH CARON 
  Ucar = u'\u01D3'	#  LATIN CAPITAL LETTER U WITH CARON 
  ucar = u'\u01D4'	#  LATIN SMALL LETTER U WITH CARON 
  Uumlmacr = u'\u01D5'	#  LATIN CAPITAL LETTER U WITH DIAERESIS AND MACRON 
  uumlmacr = u'\u01D6'	#  LATIN SMALL LETTER U WITH DIAERESIS AND MACRON 
  AEligmacr = u'\u01E2'	#  LATIN CAPITAL LETTER AE WITH MACRON 
  aeligmacr = u'\u01E3'	#  LATIN SMALL LETTER AE WITH MACRON 
  Gstrok = u'\u01E4'	#  LATIN CAPITAL LETTER G WITH STROKE 
  gstrok = u'\u01E5'	#  LATIN SMALL LETTER G WITH STROKE 
  Oogon = u'\u01EA'	#  LATIN CAPITAL LETTER O WITH OGONEK 
  oogon = u'\u01EB'	#  LATIN SMALL LETTER O WITH OGONEK 
  Oogonmacr = u'\u01EC'	#  LATIN CAPITAL LETTER O WITH OGONEK AND MACRON 
  oogonmacr = u'\u01ED'	#  LATIN SMALL LETTER O WITH OGONEK AND MACRON 
  Gacute = u'\u01F4'	#  LATIN CAPITAL LETTER G WITH ACUTE 
  gacute = u'\u01F5'	#  LATIN SMALL LETTER G WITH ACUTE 
  HWAIR = u'\u01F6'	#  LATIN CAPITAL LETTER HWAIR 
  WYNN = u'\u01F7'	#  LATIN CAPITAL LETTER WYNN 
  AEligacute = u'\u01FC'	#  LATIN CAPITAL LETTER AE WITH ACUTE 
  aeligacute = u'\u01FD'	#  LATIN SMALL LETTER AE WITH ACUTE 
  Oslashacute = u'\u01FE'	#  LATIN CAPITAL LETTER O WITH STROKE AND ACUTE
  oslashacute = u'\u01FF'	#  LATIN SMALL LETTER O WITH STROKE AND ACUTE 
  YOGH = u'\u021C'	#  LATIN CAPITAL LETTER YOGH 
  yogh = u'\u021D'	#  LATIN SMALL LETTER YOGH 
  Adot = u'\u0226'	#  LATIN CAPITAL LETTER A WITH DOT ABOVE 
  adot = u'\u0227'	#  LATIN SMALL LETTER A WITH DOT ABOVE 
  Oumlmacr = u'\u022A'	#  LATIN CAPITAL LETTER O WITH DIAERESIS AND MACRON 
  oumlmacr = u'\u022B'	#  LATIN SMALL LETTER O WITH DIAERESIS AND MACRON 
  Odot = u'\u022E'	#  LATIN CAPITAL LETTER O WITH DOT ABOVE 
  odot = u'\u022F'	#  LATIN SMALL LETTER O WITH DOT ABOVE 
  Ymacr = u'\u0232'	#  LATIN CAPITAL LETTER Y WITH MACRON 
  ymacr = u'\u0233'	#  LATIN SMALL LETTER Y WITH MACRON 
  jnodot = u'\u0237'	#  LATIN SMALL LETTER DOTLESS J 
  Jbar = u'\u0248'	#  LATIN CAPITAL LETTER J WITH STROKE 
  jbar = u'\u0249'	#  LATIN SMALL LETTER J WITH STROKE 
  
  Latin_ExtendedB = u''
  Latin_ExtendedB += bstrok + hwair + khook + lbar + nlrleg
  Latin_ExtendedB += YR + Zstrok + zstrok + EZH + wynn
  Latin_ExtendedB += Ocar + ocar + Ucar + ucar + Uumlmacr
  Latin_ExtendedB += uumlmacr + AEligmacr + aeligmacr + Gstrok + gstrok
  Latin_ExtendedB += Oogon + oogon + Oogonmacr + oogonmacr + Gacute
  Latin_ExtendedB += gacute + HWAIR + WYNN + AEligacute + aeligacute
  Latin_ExtendedB += Oslashacute + oslashacute + YOGH + yogh + Adot
  Latin_ExtendedB += adot + Oumlmacr + oumlmacr + Odot + odot
  Latin_ExtendedB += Ymacr + ymacr + jnodot + Jbar + jbar
  
  
  #  (5): IPA Extensions 
  
  oopen = u'\u0254'	#  LATIN SMALL LETTER OPEN O 
  dtail = u'\u0256'	#  LATIN SMALL LETTER D WITH TAIL 
  schwa = u'\u0259'	#  LATIN SMALL LETTER SCHWA 
  jnodotstrok = u'\u025F'	#  LATIN SMALL LETTER DOTLESS J WITH STROKE 
  gopen = u'\u0261'	#  LATIN SMALL LETTER SCRIPT G 
  gscap = u'\u0262'	#  LATIN LETTER SMALL CAPITAL G 
  hhook = u'\u0266'	#  LATIN SMALL LETTER H WITH HOOK 
  istrok = u'\u0268'	#  LATIN SMALL LETTER I WITH STROKE 
  iscap = u'\u026A'	#  LATIN LETTER SMALL CAPITAL I 
  nlfhook = u'\u0272'	#  LATIN SMALL LETTER N WITH LEFT HOOK 
  nscap = u'\u0274'	#  LATIN LETTER SMALL CAPITAL N 
  oeligscap = u'\u0276'	#  LATIN LETTER SMALL CAPITAL OE 
  rdes = u'\u027C'	#  LATIN SMALL LETTER R WITH LONG LEG 
  rscap = u'\u0280'	#  LATIN LETTER SMALL CAPITAL R 
  ubar = u'\u0289'	#  LATIN SMALL LETTER U BAR 
  yscap = u'\u028F'	#  LATIN LETTER SMALL CAPITAL Y 
  ezh = u'\u0292'	#  LATIN SMALL LETTER EZH 
  bscap = u'\u0299'	#  LATIN LETTER SMALL CAPITAL B 
  hscap = u'\u029C'	#  LATIN LETTER SMALL CAPITAL H 
  lscap = u'\u029F'	#  LATIN LETTER SMALL CAPITAL L 
  
  IPA_Extensions = u''
  IPA_Extensions += oopen + dtail + schwa + jnodotstrok + gopen
  IPA_Extensions += gscap + hhook + istrok + iscap + nlfhook
  IPA_Extensions += nscap + oeligscap + rdes + rscap + ubar
  IPA_Extensions += yscap + ezh + bscap + hscap + lscap
  
  
  #  (6): Spacing Modifying Letters 
  
  apomod = u'\u02BC'	#  MODIFIER LETTER APOSTROPHE 
  verbarup = u'\u02C8'	#  MODIFIER LETTER VERTICAL LINE 
  breve = u'\u02D8'	#  BREVE 
  dot = u'\u02D9'	#  DOT ABOVE 
  ring = u'\u02DA'	#  RING ABOVE 
  ogon = u'\u02DB'	#  OGONEK 
  tilde = u'\u02DC'	#  SMALL TILDE 
  dblac = u'\u02DD'	#  DOUBLE ACUTE ACCENT 
  xmod = u'\u02E3'	#  MODIFIER LETTER SMALL X 
  
  Spacing_Modifying_Letters = u''
  Spacing_Modifying_Letters += apomod + verbarup + breve + dot + ring
  Spacing_Modifying_Letters += ogon + tilde + dblac + xmod
  
  
  #  (7): Combining Diacritical Marks 
  
  combgrave = u'\u0300'	#  COMBINING GRAVE ACCENT 
  combacute = u'\u0301'	#  COMBINING ACUTE ACCENT 
  combcirc = u'\u0302'	#  COMBINING CIRCUMFLEX ACCENT 
  combtilde = u'\u0303'	#  COMBINING TILDE 
  combmacr = u'\u0304'	#  COMBINING MACRON 
  bar = u'\u0305'	#  COMBINING ABBREVIATION MARK BAR ABOVE 
  combbreve = u'\u0306'	#  COMBINING BREVE 
  combdot = u'\u0307'	#  COMBINING DOT ABOVE 
  combuml = u'\u0308'	#  COMBINING DIAERESIS 
  combhook = u'\u0309'	#  COMBINING HOOK ABOVE 
  combring = u'\u030A'	#  COMBINING RING ABOVE 
  combdblac = u'\u030B'	#  COMBINING DOUBLE ACUTE ACCENT 
  combsgvertl = u'\u030D'	#  COMBINING VERTICAL LINE ABOVE 
  combdbvertl = u'\u030E'	#  COMBINING DOUBLE VERTICAL LINE ABOVE 
  combcomma = u'\u0315'	#  COMBINING COMMA ABOVE 
  combdotbl = u'\u0323'	#  COMBINING DOT BELOW 
  combced = u'\u0327'	#  COMBINING CEDILLA 
  combogon = u'\u0328'	#  COMBINING OGONEK 
  barbl = u'\u0332'	#  COMBINING LOW LINE 
  dblbarbl = u'\u0333'	#  COMBINING DOUBLE LOW LINE 
  baracr = u'\u0336'	#  COMBINING LONG STROKE OVERLAY 
  combtildevert = u'\u033E'	#  COMBINING VERTICAL TILDE 
  dblovl = u'\u033F'	#  COMBINING DOUBLE OVERLINE 
  combastbl = u'\u0359'	#  COMBINING ASTERISK BELOW 
  er = u'\u035B'	#  COMBINING ZIGZAG ABOVE 
  combdblbrevebl = u'\u035C'	#  COMBINING DOUBLE BREVE BELOW 
  asup = u'\u0363'	#  COMBINING LATIN SMALL LETTER A 
  esup = u'\u0364'	#  COMBINING LATIN SMALL LETTER E 
  isup = u'\u0365'	#  COMBINING LATIN SMALL LETTER I 
  osup = u'\u0366'	#  COMBINING LATIN SMALL LETTER O 
  usup = u'\u0367'	#  COMBINING LATIN SMALL LETTER U 
  csup = u'\u0368'	#  COMBINING LATIN SMALL LETTER C 
  dsup = u'\u0369'	#  COMBINING LATIN SMALL LETTER D 
  hsup = u'\u036A'	#  COMBINING LATIN SMALL LETTER H 
  msup = u'\u036B'	#  COMBINING LATIN SMALL LETTER M 
  rsup = u'\u036C'	#  COMBINING LATIN SMALL LETTER R 
  tsup = u'\u036D'	#  COMBINING LATIN SMALL LETTER T 
  vsup = u'\u036E'	#  COMBINING LATIN SMALL LETTER V 
  xsup = u'\u036F'	#  COMBINING LATIN SMALL LETTER X 
  
  Combining_Diacritical_Marks = u''
  Combining_Diacritical_Marks += combgrave + combacute + combcirc + combtilde + combmacr
  Combining_Diacritical_Marks += bar + combbreve + combdot + combuml + combhook
  Combining_Diacritical_Marks += combring + combdblac + combsgvertl + combdbvertl + combcomma
  Combining_Diacritical_Marks += combdotbl + combced + combogon + barbl + dblbarbl
  Combining_Diacritical_Marks += baracr + combtildevert + dblovl + combastbl + er
  Combining_Diacritical_Marks += combdblbrevebl + asup + esup + isup + osup
  Combining_Diacritical_Marks += usup + csup + dsup + hsup + msup
  Combining_Diacritical_Marks += rsup + tsup + vsup + xsup
  
  
  #  (8): Greek and Coptic 
  #  The Greek characters have not been included in the MUFI recommendation 3.0 (except the two theta characters). 
  #  However, since they sometimes are used in manuscript sigla the small letters have been included here. 
  
  alpha = u'\u03B1'	#  GREEK SMALL LETTER ALPHA 
  beta = u'\u03B2'	#  GREEK SMALL LETTER BETA 
  gamma = u'\u03B3'	#  GREEK SMALL LETTER GAMMA 
  delta = u'\u03B4'	#  GREEK SMALL LETTER DELTA 
  epsilon = u'\u03B5'	#  GREEK SMALL LETTER EPSILON 
  zeta = u'\u03B6'	#  GREEK SMALL LETTER ZETA 
  eta = u'\u03B7'	#  GREEK SMALL LETTER ETA 
  Theta = u'\u0398'	#  LATIN CAPITAL LETTER THETA 
  theta = u'\u03B8'	#  GREEK SMALL LETTER THETA 
  iota = u'\u03B9'	#  GREEK SMALL LETTER IOTA 
  kappa = u'\u03BA'	#  GREEK SMALL LETTER KAPPA 
  lambda_ = u'\u03BB'	#  GREEK SMALL LETTER LAMDA 
  mu = u'\u03BC'	#  GREEK SMALL LETTER MU 
  nu = u'\u03BD'	#  GREEK SMALL LETTER NU 
  xi = u'\u03BE'	#  GREEK SMALL LETTER XI 
  omicron = u'\u03BF'	#  GREEK SMALL LETTER OMICRON 
  pi = u'\u03C0'	#  GREEK SMALL LETTER PI 
  rho = u'\u03C1'	#  GREEK SMALL LETTER RHO 
  sigmaf = u'\u03C2'	#  GREEK SMALL LETTER FINAL SIGMA 
  sigma = u'\u03C3'	#  GREEK SMALL LETTER SIGMA 
  tau = u'\u03C4'	#  GREEK SMALL LETTER TAU 
  upsilon = u'\u03C5'	#  GREEK SMALL LETTER UPSILON 
  phi = u'\u03C6'	#  GREEK SMALL LETTER PHI 
  chi = u'\u03C7'	#  GREEK SMALL LETTER CHI 
  psi = u'\u03C8'	#  GREEK SMALL LETTER PSI 
  omega = u'\u03C9'	#  GREEK SMALL LETTER OMEGA 
  
  Greek_and_Coptic = u''
  Greek_and_Coptic += alpha + beta + gamma + delta + epsilon
  Greek_and_Coptic += zeta + eta + Theta + theta + iota
  Greek_and_Coptic += kappa + lambda_ + mu + nu + xi
  Greek_and_Coptic += omicron + pi + rho + sigmaf + sigma
  Greek_and_Coptic += tau + upsilon + phi + chi + psi
  Greek_and_Coptic += omega
  
  
  #  (9): Georgian 
  
  tridotright = u'\u10FB'	#  PUNCTUATION MARK RIGHT-POINTING TRIANGULAR DOTS 
  
  Georgian = u''
  Georgian += tridotright
  
  
  #  (10): Runic 
  
  fMedrun = u'\u16A0'	#  RUNIC LETTER FEHU FEOH FE F 
  mMedrun = u'\u16D8'	#  RUNIC LETTER LONG-BRANCH-MADR M 
  
  Runic = u''
  Runic += fMedrun + mMedrun
  
  
  #  (11): Phonetic Extensions 
  
  ascap = u'\u1D00'	#  LATIN LETTER SMALL CAPITAL A 
  aeligscap = u'\u1D01'	#  LATIN LETTER SMALL CAPITAL AE 
  cscap = u'\u1D04'	#  LATIN LETTER SMALL CAPITAL C 
  dscap = u'\u1D05'	#  LATIN LETTER SMALL CAPITAL D 
  ethscap = u'\u1D06'	#  LATIN LETTER SMALL CAPITAL ETH 
  escap = u'\u1D07'	#  LATIN LETTER SMALL CAPITAL E 
  jscap = u'\u1D0A'	#  LATIN LETTER SMALL CAPITAL J 
  kscap = u'\u1D0B'	#  LATIN LETTER SMALL CAPITAL K 
  mscap = u'\u1D0D'	#  LATIN LETTER SMALL CAPITAL M 
  oscap = u'\u1D0F'	#  LATIN LETTER SMALL CAPITAL O 
  pscap = u'\u1D18'	#  LATIN LETTER SMALL CAPITAL P 
  tscap = u'\u1D1B'	#  LATIN LETTER SMALL CAPITAL T 
  uscap = u'\u1D1C'	#  LATIN LETTER SMALL CAPITAL U 
  vscap = u'\u1D20'	#  LATIN LETTER SMALL CAPITAL V 
  wscap = u'\u1D21'	#  LATIN LETTER SMALL CAPITAL W 
  zscap = u'\u1D22'	#  LATIN LETTER SMALL CAPITAL Z 
  Imod = u'\u1D35'	#  MODIFIER LETTER CAPITAL I 
  gins = u'\u1D79'	#  LATIN SMALL LETTER INSULAR G 
  
  Phonetic_Extensions = u''
  Phonetic_Extensions += ascap + aeligscap + cscap + dscap + ethscap
  Phonetic_Extensions += escap + jscap + kscap + mscap + oscap
  Phonetic_Extensions += pscap + tscap + uscap + vscap + wscap
  Phonetic_Extensions += zscap + Imod + gins
  
  
  #  (12): Combining Diacritical Marks Supplement 
  
  combcircdbl = u'\u1DCD'	#  COMBINING DOUBLE CIRCUMFLEX ABOVE 
  combcurl = u'\u1DCE'	#  COMBINING OGONEK ABOVE 
  ersub = u'\u1DCF'	#  COMBINING ZIGZAG BELOW 
  combisbelow = u'\u1DD0'	#  COMBINING IS BELOW 
  ur = u'\u1DD1'	#  COMBINING UR ABOVE 
  us = u'\u1DD2'	#  COMBINING US ABOVE 
  ra = u'\u1DD3'	#  COMBINING LATIN SMALL LETTER FLATTENED OPEN A ABOVE 
  aeligsup = u'\u1DD4'	#  COMBINING LATIN SMALL LETTER AE 
  aoligsup = u'\u1DD5'	#  COMBINING LATIN SMALL LETTER AO 
  avligsup = u'\u1DD6'	#  COMBINING LATIN SMALL LETTER AV 
  ccedilsup = u'\u1DD7'	#  COMBINING LATIN SMALL LETTER C CEDILLA 
  drotsup = u'\u1DD8'	#  COMBINING LATIN SMALL LETTER INSULAR D 
  ethsup = u'\u1DD9'	#  COMBINING LATIN SMALL LETTER ETH 
  gsup = u'\u1DDA'	#  COMBINING LATIN SMALL LETTER G 
  gscapsup = u'\u1DDB'	#  COMBINING LATIN LETTER SMALL CAPITAL G 
  ksup = u'\u1DDC'	#  COMBINING LATIN SMALL LETTER K 
  lsup = u'\u1DDD'	#  COMBINING LATIN SMALL LETTER L 
  lscapsup = u'\u1DDE'	#  COMBINING LATIN LETTER SMALL CAPITAL L 
  mscapsup = u'\u1DDF'	#  COMBINING LATIN LETTER SMALL CAPITAL M 
  nsup = u'\u1DE0'	#  COMBINING LATIN SMALL LETTER N 
  nscapsup = u'\u1DE1'	#  COMBINING LATIN LETTER SMALL CAPITAL N 
  rrotsup = u'\u1DE3'	#  COMBINING LATIN SMALL LETTER R ROTUNDA 
  ssup = u'\u1DE4'	#  COMBINING LATIN SMALL LETTER S 
  slongsup = u'\u1DE5'	#  COMBINING LATIN SMALL LETTER LONG S 
  zsup = u'\u1DE6'	#  COMBINING LATIN SMALL LETTER Z 
  
  Combining_Diacritical_Marks_Supplement = u''
  Combining_Diacritical_Marks_Supplement += combcircdbl + combcurl + ersub + combisbelow + ur
  Combining_Diacritical_Marks_Supplement += us + ra + aeligsup + aoligsup + avligsup
  Combining_Diacritical_Marks_Supplement += ccedilsup + drotsup + ethsup + gsup + gscapsup
  Combining_Diacritical_Marks_Supplement += ksup + lsup + lscapsup + mscapsup + nsup
  Combining_Diacritical_Marks_Supplement += nscapsup + rrotsup + ssup + slongsup + zsup
  
  
  #  (13): Latin Extended Additional 
  
  Bdot = u'\u1E02'	#  LATIN CAPITAL LETTER B WITH DOT ABOVE 
  bdot = u'\u1E03'	#  LATIN SMALL LETTER B WITH DOT ABOVE 
  Bdotbl = u'\u1E04'	#  LATIN CAPITAL LETTER B WITH DOT BELOW 
  bdotbl = u'\u1E05'	#  LATIN SMALL LETTER B WITH DOT BELOW 
  Ddot = u'\u1E0A'	#  LATIN CAPITAL LETTER D WITH DOT ABOVE 
  ddot = u'\u1E0B'	#  LATIN SMALL LETTER D WITH DOT ABOVE 
  Ddotbl = u'\u1E0C'	#  LATIN CAPITAL LETTER D WITH DOT BELOW 
  ddotbl = u'\u1E0D'	#  LATIN SMALL LETTER D WITH DOT BELOW 
  Emacracute = u'\u1E16'	#  LATIN CAPITAL LETTER E WITH MACRON AND ACUTE 
  emacracute = u'\u1E17'	#  LATIN SMALL LETTER E WITH MACRON AND ACUTE 
  Fdot = u'\u1E1E'	#  LATIN CAPITAL LETTER F WITH DOT ABOVE 
  fdot = u'\u1E1F'	#  LATIN SMALL LETTER F WITH DOT ABOVE 
  Hdot = u'\u1E22'	#  LATIN CAPITAL LETTER H WITH DOT ABOVE 
  hdot = u'\u1E23'	#  LATIN SMALL LETTER H WITH DOT ABOVE 
  Hdotbl = u'\u1E24'	#  LATIN CAPITAL LETTER H WITH DOT BELOW 
  hdotbl = u'\u1E25'	#  LATIN SMALL LETTER H WITH DOT BELOW 
  Kacute = u'\u1E30'	#  LATIN CAPITAL LETTER K WITH ACUTE 
  kacute = u'\u1E31'	#  LATIN SMALL LETTER K WITH ACUTE 
  Kdotbl = u'\u1E32'	#  LATIN CAPITAL LETTER K WITH DOT BELOW 
  kdotbl = u'\u1E33'	#  LATIN SMALL LETTER K WITH DOT BELOW 
  Ldotbl = u'\u1E36'	#  LATIN CAPITAL LETTER L WITH DOT BELOW 
  ldotbl = u'\u1E37'	#  LATIN SMALL LETTER L WITH DOT BELOW 
  Macute = u'\u1E3E'	#  LATIN CAPITAL LETTER M WITH ACUTE 
  macute = u'\u1E3F'	#  LATIN SMALL LETTER M WITH ACUTE 
  Mdot = u'\u1E40'	#  LATIN CAPITAL LETTER M WITH DOT ABOVE 
  mdot = u'\u1E41'	#  LATIN SMALL LETTER M WITH DOT ABOVE 
  Mdotbl = u'\u1E42'	#  LATIN CAPITAL LETTER M WITH DOT BELOW 
  mdotbl = u'\u1E43'	#  LATIN SMALL LETTER M WITH DOT BELOW 
  Ndot = u'\u1E44'	#  LATIN CAPITAL LETTER N WITH DOT ABOVE 
  ndot = u'\u1E45'	#  LATIN SMALL LETTER N WITH DOT ABOVE 
  Ndotbl = u'\u1E46'	#  LATIN CAPITAL LETTER N WITH DOT BELOW 
  ndotbl = u'\u1E47'	#  LATIN SMALL LETTER N WITH DOT BELOW 
  Omacracute = u'\u1E52'	#  LATIN CAPITAL LETTER O WITH MACRON AND ACUTE 
  omacracute = u'\u1E53'	#  LATIN SMALL LETTER O WITH MACRON AND ACUTE 
  Pacute = u'\u1E54'	#  LATIN CAPITAL LETTER P WITH ACUTE 
  pacute = u'\u1E55'	#  LATIN SMALL LETTER P WITH ACUTE 
  Pdot = u'\u1E56'	#  LATIN CAPITAL LETTER P WITH DOT ABOVE 
  pdot = u'\u1E57'	#  LATIN SMALL LETTER P WITH DOT ABOVE 
  Rdot = u'\u1E58'	#  LATIN CAPITAL LETTER R WITH DOT ABOVE 
  rdot = u'\u1E59'	#  LATIN SMALL LETTER R WITH DOT ABOVE 
  Rdotbl = u'\u1E5A'	#  LATIN CAPITAL LETTER R WITH DOT BELOW 
  rdotbl = u'\u1E5B'	#  LATIN SMALL LETTER R WITH DOT BELOW 
  Sdot = u'\u1E60'	#  LATIN CAPITAL LETTER S WITH DOT ABOVE 
  sdot = u'\u1E61'	#  LATIN SMALL LETTER S WITH DOT ABOVE 
  Sdotbl = u'\u1E62'	#  LATIN CAPITAL LETTER S WITH DOT BELOW 
  sdotbl = u'\u1E63'	#  LATIN SMALL LETTER S WITH DOT BELOW 
  Tdot = u'\u1E6A'	#  LATIN CAPITAL LETTER T WITH DOT ABOVE 
  tdot = u'\u1E6B'	#  LATIN SMALL LETTER T WITH DOT ABOVE 
  Tdotbl = u'\u1E6C'	#  LATIN CAPITAL LETTER T WITH DOT BELOW 
  tdotbl = u'\u1E6D'	#  LATIN SMALL LETTER T WITH DOT BELOW 
  Vdotbl = u'\u1E7E'	#  LATIN CAPITAL LETTER V WITH DOT BELOW 
  vdotbl = u'\u1E7F'	#  LATIN SMALL LETTER V WITH DOT BELOW 
  Wgrave = u'\u1E80'	#  LATIN CAPITAL LETTER W WITH GRAVE 
  wgrave = u'\u1E81'	#  LATIN SMALL LETTER W WITH GRAVE 
  Wacute = u'\u1E82'	#  LATIN CAPITAL LETTER W WITH ACUTE 
  wacute = u'\u1E83'	#  LATIN SMALL LETTER W WITH ACUTE 
  Wuml = u'\u1E84'	#  LATIN CAPITAL LETTER W WITH DIAERESIS 
  wuml = u'\u1E85'	#  LATIN SMALL LETTER W WITH DIAERESIS 
  Wdot = u'\u1E86'	#  LATIN CAPITAL LETTER W WITH DOT ABOVE 
  wdot = u'\u1E87'	#  LATIN SMALL LETTER W WITH DOT ABOVE 
  Wdotbl = u'\u1E88'	#  LATIN CAPITAL LETTER W WITH DOT BELOW 
  wdotbl = u'\u1E89'	#  LATIN SMALL LETTER W WITH DOT BELOW 
  Ydot = u'\u1E8E'	#  LATIN CAPITAL LETTER Y WITH DOT ABOVE 
  ydot = u'\u1E8F'	#  LATIN SMALL LETTER Y WITH DOT ABOVE 
  Zdotbl = u'\u1E92'	#  LATIN CAPITAL LETTER Z WITH DOT BELOW 
  zdotbl = u'\u1E93'	#  LATIN SMALL LETTER Z WITH DOT BELOW 
  wring = u'\u1E98'	#  LATIN SMALL LETTER W WITH RING ABOVE 
  yring = u'\u1E99'	#  LATIN SMALL LETTER Y WITH RING ABOVE 
  slongbarslash = u'\u1E9C'	#  LATIN SMALL LETTER S WITH DIAGONAL STROKE 
  slongbar = u'\u1E9D'	#  LATIN SMALL LETTER S WITH HIGH STROKE 
  SZlig = u'\u1E9E'	#  LATIN CAPITAL LETTER SHARP S 
  dscript = u'\u1E9F'	#  LATIN SMALL LETTER DELTA 
  Adotbl = u'\u1EA0'	#  LATIN CAPITAL LETTER A WITH DOT BELOW 
  adotbl = u'\u1EA1'	#  LATIN SMALL LETTER A WITH DOT BELOW 
  Ahook = u'\u1EA2'	#  LATIN CAPITAL LETTER A WITH HOOK ABOVE 
  ahook = u'\u1EA3'	#  LATIN SMALL LETTER A WITH HOOK ABOVE 
  Abreveacute = u'\u1EAE'	#  LATIN CAPITAL LETTER A WITH BREVE AND ACUTE 
  abreveacute = u'\u1EAF'	#  LATIN SMALL LETTER A WITH BREVE AND ACUTE 
  Edotbl = u'\u1EB8'	#  LATIN CAPITAL LETTER E WITH DOT BELOW 
  edotbl = u'\u1EB9'	#  LATIN SMALL LETTER E WITH DOT BELOW 
  Ihook = u'\u1EC8'	#  LATIN CAPITAL LETTER I WITH HOOK ABOVE 
  ihook = u'\u1EC9'	#  LATIN SMALL LETTER I WITH HOOK ABOVE 
  Idotbl = u'\u1ECA'	#  LATIN CAPITAL LETTER I WITH DOT BELOW 
  idotbl = u'\u1ECB'	#  LATIN SMALL LETTER I WITH DOT BELOW 
  Odotbl = u'\u1ECC'	#  LATIN CAPITAL LETTER O WITH DOT BELOW 
  odotbl = u'\u1ECD'	#  LATIN SMALL LETTER O WITH DOT BELOW 
  Ohook = u'\u1ECE'	#  LATIN CAPITAL LETTER O WITH HOOK ABOVE 
  ohook = u'\u1ECF'	#  LATIN SMALL LETTER O WITH HOOK ABOVE 
  Udotbl = u'\u1EE4'	#  LATIN CAPITAL LETTER U WITH DOT BELOW 
  udotbl = u'\u1EE5'	#  LATIN SMALL LETTER U WITH DOT BELOW 
  Uhook = u'\u1EE6'	#  LATIN CAPITAL LETTER U WITH HOOK ABOVE 
  uhook = u'\u1EE7'	#  LATIN SMALL LETTER U WITH HOOK ABOVE 
  Ygrave = u'\u1EF2'	#  LATIN CAPITAL LETTER Y WITH GRAVE 
  ygrave = u'\u1EF3'	#  LATIN SMALL LETTER Y WITH GRAVE 
  Ydotbl = u'\u1EF4'	#  LATIN CAPITAL LETTER Y WITH DOT BELOW 
  ydotbl = u'\u1EF5'	#  LATIN SMALL LETTER Y WITH DOT BELOW 
  Yhook = u'\u1EF6'	#  LATIN CAPITAL LETTER Y WITH HOOK ABOVE 
  yhook = u'\u1EF7'	#  LATIN SMALL LETTER Y WITH HOOK ABOVE 
  LLwelsh = u'\u1EFA'	#  LATIN CAPITAL LETTER MIDDLE-WELSH LL 
  llwelsh = u'\u1EFB'	#  LATIN SMALL LETTER MIDDLE-WELSH LL 
  Vwelsh = u'\u1EFC'	#  LATIN CAPITAL LETTER MIDDLE-WELSH V 
  vwelsh = u'\u1EFD'	#  LATIN SMALL LETTER MIDDLE-WELSH V 
  Yloop = u'\u1EFE'	#  LATIN CAPITAL LETTER Y WITH LOOP 
  yloop = u'\u1EFF'	#  LATIN SMALL LETTER Y WITH LOOP 
  
  Latin_Extended_Additional = u''
  Latin_Extended_Additional += Bdot + bdot + Bdotbl + bdotbl + Ddot
  Latin_Extended_Additional += ddot + Ddotbl + ddotbl + Emacracute + emacracute
  Latin_Extended_Additional += Fdot + fdot + Hdot + hdot + Hdotbl
  Latin_Extended_Additional += hdotbl + Kacute + kacute + Kdotbl + kdotbl
  Latin_Extended_Additional += Ldotbl + ldotbl + Macute + macute + Mdot
  Latin_Extended_Additional += mdot + Mdotbl + mdotbl + Ndot + ndot
  Latin_Extended_Additional += Ndotbl + ndotbl + Omacracute + omacracute + Pacute
  Latin_Extended_Additional += pacute + Pdot + pdot + Rdot + rdot
  Latin_Extended_Additional += Rdotbl + rdotbl + Sdot + sdot + Sdotbl
  Latin_Extended_Additional += sdotbl + Tdot + tdot + Tdotbl + tdotbl
  Latin_Extended_Additional += Vdotbl + vdotbl + Wgrave + wgrave + Wacute
  Latin_Extended_Additional += wacute + Wuml + wuml + Wdot + wdot
  Latin_Extended_Additional += Wdotbl + wdotbl + Ydot + ydot + Zdotbl
  Latin_Extended_Additional += zdotbl + wring + yring + slongbarslash + slongbar
  Latin_Extended_Additional += SZlig + dscript + Adotbl + adotbl + Ahook
  Latin_Extended_Additional += ahook + Abreveacute + abreveacute + Edotbl + edotbl
  Latin_Extended_Additional += Ihook + ihook + Idotbl + idotbl + Odotbl
  Latin_Extended_Additional += odotbl + Ohook + ohook + Udotbl + udotbl
  Latin_Extended_Additional += Uhook + uhook + Ygrave + ygrave + Ydotbl
  Latin_Extended_Additional += ydotbl + Yhook + yhook + LLwelsh + llwelsh
  Latin_Extended_Additional += Vwelsh + vwelsh + Yloop + yloop
  
  
  #  (14): General Punctuation 
  
  enqd = u'\u2000'	#  EN QUAD 
  emqd = u'\u2001'	#  EM QUAD 
  ensp = u'\u2002'	#  EN SPACE 
  emsp = u'\u2003'	#  EM SPACE 
  emsp13 = u'\u2004'	#  THREE-PER-EM SPACE 
  emsp14 = u'\u2005'	#  FOUR-PER-EM SPAC 
  emsp16 = u'\u2006'	#  SIX-PER-EM SPAC 
  numsp = u'\u2007'	#  FIGURE SPACE 
  puncsp = u'\u2008'	#  PUNCTUATION SPACE 
  thinsp = u'\u2009'	#  THIN SPACE 
  hairsp = u'\u200A'	#  HAIR SPACE 
  zerosp = u'\u200B'	#  ZERO WIDTH SPACE 
  dash = u'\u2010'	#  HYPHEN 
  nbhy = u'\u2011'	#  NON-BREAKING HYPHEN 
  numdash = u'\u2012'	#  FIGURE DASH 
  ndash = u'\u2013'	#  EN DASH 
  mdash = u'\u2014'	#  EM DASH 
  horbar = u'\u2015'	#  HORIZONTAL BAR 
  Verbar = u'\u2016'	#  DOUBLE VERTICAL LINE 
  lsquo = u'\u2018'	#  LEFT SINGLE QUOTATION MARK 
  rsquo = u'\u2019'	#  RIGHT SINGLE QUOTATION MARK 
  lsquolow = u'\u201A'	#  SINGLE LOW-9 QUOTATION MARK 
  rsquorev = u'\u201B'	#  SINGLE HIGH-REVERSED-9 QUOTATION MARK 
  ldquo = u'\u201C'	#  LEFT DOUBLE QUOTATION MARK 
  rdquo = u'\u201D'	#  RIGHT DOUBLE QUOTATION MARK 
  ldquolow = u'\u201E'	#  DOUBLE LOW-9 QUOTATION MARK 
  rdquorev = u'\u201F'	#  DOUBLE HIGH-REVERSED-9 QUOTATION MARK 
  dagger = u'\u2020'	#  DAGGER 
  Dagger = u'\u2021'	#  DOUBLE DAGGER 
  bull = u'\u2022'	#  BULLET 
  tribull = u'\u2023'	#  TRIANGULAR BULLET 
  sgldr = u'\u2024'	#  ONE DOT LEADER 
  dblldr = u'\u2025'	#  TWO DOT LEADER 
  hellip = u'\u2026'	#  HORIZONTAL ELLIPSIS 
  hyphpoint = u'\u2027'	#  HYPHENATION POINT 
  nnbsp = u'\u202F'	#  NARROW NO-BREAK SPACE 
  permil = u'\u2030'	#  PER MILLE SIGN 
  prime = u'\u2032'	#  PRIME 
  Prime = u'\u2033'	#  DOUBLE PRIME 
  lsaquo = u'\u2039'	#  SINGLE LEFT-POINTING ANGLE QUOTATION MARK 
  rsaquo = u'\u203A'	#  SINGLE RIGHT-POINTING ANGLE QUOTATION MARK 
  refmark = u'\u203B'	#  REFERENCE MARK 
  triast = u'\u2042'	#  ASTERISM 
  fracsol = u'\u2044'	#  FRACTION SLASH 
  lsqbqu = u'\u2045'	#  LEFT SQUARE BRACKET WITH QUILL 
  rsqbqu = u'\u2046'	#  RIGHT SQUARE BRACKET WITH QUILL 
  et = u'\u204A'	#  TIRONIAN SIGN ET 
  revpara = u'\u204B'	#  REVERSED PILCROW SIGN 
  lozengedot = u'\u2058'	#  FOUR DOT PUNCTUATION 
  dotcross = u'\u205C'	#  DOTTED CROSS 
  
  General_Punctuation = u''
  General_Punctuation += enqd + emqd + ensp + emsp + emsp13
  General_Punctuation += emsp14 + emsp16 + numsp + puncsp + thinsp
  General_Punctuation += hairsp + zerosp + dash + nbhy + numdash
  General_Punctuation += ndash + mdash + horbar + Verbar + lsquo
  General_Punctuation += rsquo + lsquolow + rsquorev + ldquo + rdquo
  General_Punctuation += ldquolow + rdquorev + dagger + Dagger + bull
  General_Punctuation += tribull + sgldr + dblldr + hellip + hyphpoint
  General_Punctuation += nnbsp + permil + prime + Prime + lsaquo
  General_Punctuation += rsaquo + refmark + triast + fracsol + lsqbqu
  General_Punctuation += rsqbqu + et + revpara + lozengedot + dotcross
  
  
  #  (15): Superscripts and subscripts
  
  sup0 = u'\u2070'	#  SUPERSCRIPT ZERO 
  sup4 = u'\u2074'	#  SUPERSCRIPT FOUR 
  sup5 = u'\u2075'	#  SUPERSCRIPT FIVE 
  sup6 = u'\u2076'	#  SUPERSCRIPT SIX 
  sup7 = u'\u2077'	#  SUPERSCRIPT SEVEN 
  sup8 = u'\u2078'	#  SUPERSCRIPT EIGHT 
  sup9 = u'\u2079'	#  SUPERSCRIPT NINE 
  sub0 = u'\u2080'	#  SUBSCRIPT ZERO 
  sub1 = u'\u2081'	#  SUBSCRIPT ONE 
  sub2 = u'\u2082'	#  SUBSCRIPT TWO 
  sub3 = u'\u2083'	#  SUBSCRIPT THREE 
  sub4 = u'\u2084'	#  SUBSCRIPT FOUR 
  sub5 = u'\u2085'	#  SUBSCRIPT FIVE 
  sub6 = u'\u2086'	#  SUBSCRIPT SIX 
  sub7 = u'\u2087'	#  SUBSCRIPT SEVEN 
  sub8 = u'\u2088'	#  SUBSCRIPT EIGHT 
  sub9 = u'\u2089'	#  SUBSCRIPT NINE 
  
  Superscripts_and_subscripts = u''
  Superscripts_and_subscripts += sup0 + sup4 + sup5 + sup6 + sup7
  Superscripts_and_subscripts += sup8 + sup9 + sub0 + sub1 + sub2
  Superscripts_and_subscripts += sub3 + sub4 + sub5 + sub6 + sub7
  Superscripts_and_subscripts += sub8 + sub9
  
  
  #  (16): Currency Symbols 
  
  pennygerm = u'\u20B0'	#  GERMAN PENNY SIGN 
  
  Currency_Symbols = u''
  Currency_Symbols += pennygerm
  
  
  #  (17): Letterlike Symbols 
  
  scruple = u'\u2108'	#  SCRUPLE 
  lbbar = u'\u2114'	#  L B BAR SYMBOL 
  Rtailstrok = u'\u211E'	#  PRESCRIPTION TAKE 
  Rslstrok = u'\u211F'	#  RESPONSE 
  Vslstrok = u'\u2123'	#  VERSICLE 
  ounce = u'\u2125'	#  OUNCE SIGN 
  Fturn = u'\u2132'	#  TURNED CAPITAL F 
  fturn = u'\u214E'	#  TURNED SMALL F 
  
  Letterlike_Symbols = u''
  Letterlike_Symbols += scruple + lbbar + Rtailstrok + Rslstrok + Vslstrok
  Letterlike_Symbols += ounce + Fturn + fturn
  
  
  #  (18): Number forms 
  
  romnumCDlig = u'\u2180'	#  ROMAN NUMERAL ONE THOUSAND C D 
  romnumDDlig = u'\u2181'	#  ROMAN NUMERAL FIVE THOUSAND 
  romnumDDdbllig = u'\u2182'	#  ROMAN NUMERAL TEN THOUSAND 
  CONbase = u'\u2183'	#  ROMAN NUMERAL REVERSED ONE HUNDRED 
  conbase = u'\u2184'	#  LATIN SMALL LETTER REVERSED C 
  
  Number_forms = u''
  Number_forms += romnumCDlig + romnumDDlig + romnumDDdbllig + CONbase + conbase
  
  
  #  (19): Arrows 
  
  arrsgllw = u'\u2190'	#  LEFTWARDS ARROW 
  arrsglupw = u'\u2191'	#  UPWARDS ARROW 
  arrsglrw = u'\u2192'	#  RIGHTWARDS ARROW 
  arrsgldw = u'\u2193'	#  DOWNWARDS ARROW 
  
  Arrows = u''
  Arrows += arrsgllw + arrsglupw + arrsglrw + arrsgldw
  
  
  #  (20): Mathematical Operators 
  
  minus = u'\u2212'	#  MINUS 
  infin = u'\u221E'	#  INFINITY 
  logand = u'\u2227'	#  LOGICAL AND 
  tridotupw = u'\u2234'	#  THEREFORE 
  tridotdw = u'\u2235'	#  BECAUSE 
  quaddot = u'\u2237'	#  PROPORTION 
  est = u'\u223B'	#  HOMOTHETIC 
  esse = u'\u2248'	#  ALMOST EQUAL TO 
  notequals = u'\u2260'	#  NOT EQUAL TO 
  dipledot = u'\u22D7'	#  GREATER-THAN WITH DOT 
  
  Mathematical_Operators = u''
  Mathematical_Operators += minus + infin + logand + tridotupw + tridotdw
  Mathematical_Operators += quaddot + est + esse + notequals + dipledot
  
  
  #  (21): Miscellaneous Technical 
  
  metrshort = u'\u23D1'	#  METRICAL BREVE 
  metrshortlong = u'\u23D2'	#  METRICAL LONG OVER SHORT 
  metrlongshort = u'\u23D3'	#  METRICAL SHORT OVER LONG 
  metrdblshortlong = u'\u23D4'	#  METRICAL LONG OVER TWO SHORTS 
  
  Miscellaneous_Technical = u''
  Miscellaneous_Technical += metrshort + metrshortlong + metrlongshort + metrdblshortlong
  
  
  #  (22): Geometric Shapes 
  
  squareblsm = u'\u25AA'	#  BLACK SMALL SQUARE 
  squarewhsm = u'\u25AB'	#  WHITE SMALL SQUARE 
  trirightwh = u'\u25B9'	#  WHITE RIGHT-POINTING SMALL TRIANGLE 
  trileftwh = u'\u25C3'	#  WHITE LEFT-POINTING SMALL TRIANGLE 
  circledot = u'\u25CC'	#  DOTTED CIRCLE 
  
  Geometric_Shapes = u''
  Geometric_Shapes += squareblsm + squarewhsm + trirightwh + trileftwh + circledot
  
  
  #  (23): Dingbats 
  
  cross = u'\u271D'	#  LATIN CROSS 
  hedera = u'\u2766'	#  FLORAL HEART 
  hederarot = u'\u2767'	#  ROTATED FLORAL HEART BULLET 
  
  Dingbats = u''
  Dingbats += cross + hedera + hederarot
  
  
  #  (24): Miscellaneous Mathematical Symbols-A 
  
  lwhsqb = u'\u27E6'	#  MATHEMATICAL LEFT WHITE SQUARE BRACKET 
  rwhsqb = u'\u27E7'	#  MATHEMATICAL RIGHT WHITE SQUARE BRACKET 
  langb = u'\u27E8'	#  MATHEMATICAL LEFT ANGLE BRACKET 
  rangb = u'\u27E9'	#  MATHEMATICAL RIGHT ANGLE BRACKET 
  
  Miscellaneous_Mathematical_SymbolsA = u''
  Miscellaneous_Mathematical_SymbolsA += lwhsqb + rwhsqb + langb + rangb
  
  
  #  (25): Supplemental Mathematical Operators 
  
  dblsol = u'\u2AFD'	#  DOUBLE SOLIDUS OPERATOR 
  
  Supplemental_Mathematical_Operators = u''
  Supplemental_Mathematical_Operators += dblsol
  
  
  #  (26): Latin Extended-C 
  
  Hhalf = u'\u2C75'	#  LATIN CAPITAL LETTER HALF H 
  hhalf = u'\u2C76'	#  LATIN SMALL LETTER HALF H 
  
  Latin_ExtendedC = u''
  Latin_ExtendedC += Hhalf + hhalf
  
  
  #  (27): Supplemental Punctuation 
  
  luhsqbNT = u'\u2E00'	#  RIGHT ANGLE SUBSTITUTION MARKER 
  luslst = u'\u2E0C'	#  LEFT RAISED OMISSION BRACKET 
  ruslst = u'\u2E0D'	#  RIGHT RAISED OMISSION BRACKET 
  dbloblhyph = u'\u2E17'	#  DOUBLE OBLIQUE HYPHEN 
  rlslst = u'\u2E1C'	#  LEFT LOW PARAPHRASE BRACKET 
  llslst = u'\u2E1D'	#  RIGHT LOW PARAPHRASE BRACKET 
  verbarql = u'\u2E20'	#  LEFT VERTICAL BAR WITH QUILL 
  verbarqr = u'\u2E21'	#  RIGHT VERTICAL BAR WITH QUILL 
  luhsqb = u'\u2E22'	#  TOP LEFT HALF BRACKET 
  ruhsqb = u'\u2E23'	#  TOP RIGHT HALF BRACKET 
  llhsqb = u'\u2E24'	#  BOTTOM LEFT HALF BRACKET 
  rlhsqb = u'\u2E25'	#  BOTTOM RIGHT HALF BRACKET 
  lUbrack = u'\u2E26'	#  LEFT SIDEWAYS U BRACKET 
  rUbrack = u'\u2E27'	#  RIGHT SIDEWAYS U BRACKET 
  ldblpar = u'\u2E28'	#  LEFT DOUBLE PARENTHESIS 
  rdblpar = u'\u2E29'	#  RIGHT DOUBLE PARENTHESIS 
  tridotsdownw = u'\u2E2A'	#  TWO DOTS OVER ONE DOT PUNCTUATION 
  tridotsupw = u'\u2E2B'	#  ONE DOT OVER TWO DOTS PUNCTUATION 
  quaddots = u'\u2E2C'	#  SQUARED FOUR DOT PUNCTUATION 
  fivedots = u'\u2E2D'	#  FIVE DOT MARK 
  punctpercont = u'\u2E2E'	#  REVERSED QUESTION MARK 
  
  Supplemental_Punctuation = u''
  Supplemental_Punctuation += luhsqbNT + luslst + ruslst + dbloblhyph + rlslst
  Supplemental_Punctuation += llslst + verbarql + verbarqr + luhsqb + ruhsqb
  Supplemental_Punctuation += llhsqb + rlhsqb + lUbrack + rUbrack + ldblpar
  Supplemental_Punctuation += rdblpar + tridotsdownw + tridotsupw + quaddots + fivedots
  Supplemental_Punctuation += punctpercont
  
  
  #  (28): Latin Extended-D 
  
  fscap = u'\uA730'	#  LATIN LETTER SMALL CAPITAL F 
  sscap = u'\uA731'	#  LATIN LETTER SMALL CAPITAL S 
  AAlig = u'\uA732'	#  LATIN CAPITAL LETTER AA 
  aalig = u'\uA733'	#  LATIN SMALL LETTER AA 
  AOlig = u'\uA734'	#  LATIN CAPITAL LETTER AO 
  aolig = u'\uA735'	#  LATIN SMALL LETTER AO 
  AUlig = u'\uA736'	#  LATIN CAPITAL LETTER AU 
  aulig = u'\uA737'	#  LATIN SMALL LETTER AU 
  AVlig = u'\uA738'	#  LATIN CAPITAL LETTER AV 
  avlig = u'\uA739'	#  LATIN SMALL LETTER AV 
  AVligslash = u'\uA73A'	#  LATIN CAPITAL LETTER AV WITH HORIZONTAL BAR 
  avligslash = u'\uA73B'	#  LATIN SMALL LETTER AV WITH HORIZONTAL BAR 
  AYlig = u'\uA73C'	#  LATIN CAPITAL LETTER AY 
  aylig = u'\uA73D'	#  LATIN SMALL LETTER AY 
  CONdot = u'\uA73E'	#  LATIN CAPITAL LETTER REVERSED C WITH DOT 
  condot = u'\uA73F'	#  LATIN SMALL LETTER REVERSED C WITH DOT 
  Kbar = u'\uA740'	#  LATIN CAPITAL LETTER K WITH STROKE 
  kbar = u'\uA741'	#  LATIN SMALL LETTER K WITH STROKE 
  Kstrleg = u'\uA742'	#  LATIN CAPITAL LETTER K WITH DIAGONAL STROKE 
  kstrleg = u'\uA743'	#  LATIN SMALL LETTER K WITH DIAGONAL STROKE 
  Kstrascleg = u'\uA744'	#  LATIN CAPITAL LETTER K WITH STROKE AND DIAGONAL STROKE 
  kstrascleg = u'\uA745'	#  LATIN SMALL LETTER K WITH STROKE AND DIAGONAL STROKE 
  Lbrk = u'\uA746'	#  LATIN CAPITAL LETTER BROKEN L 
  lbrk = u'\uA747'	#  LATIN SMALL LETTER BROKEN L 
  Lhighstrok = u'\uA748'	#  LATIN CAPITAL LETTER L WITH HIGH STROKE 
  lhighstrok = u'\uA749'	#  LATIN SMALL LETTER L WITH HIGH STROKE 
  OBIIT = u'\uA74A'	#  LATIN CAPITAL LETTER O WITH LONG STROKE OVERLAY 
  obiit = u'\uA74B'	#  LATIN SMALL LETTER O WITH LONG STROKE OVERLAY 
  Oloop = u'\uA74C'	#  LATIN CAPITAL LETTER O WITH LOOP 
  oloop = u'\uA74D'	#  LATIN SMALL LETTER O WITH LOOP 
  OOlig = u'\uA74E'	#  LATIN CAPITAL LETTER OO 
  oolig = u'\uA74F'	#  LATIN SMALL LETTER OO 
  Pbardes = u'\uA750'	#  LATIN CAPITAL LETTER P WITH STROKE THROUGH DESCENDER 
  pbardes = u'\uA751'	#  LATIN SMALL LETTER P WITH STROKE THROUGH DESCENDER 
  Pflour = u'\uA752'	#  LATIN CAPITAL LETTER P WITH FLOURISH 
  pflour = u'\uA753'	#  LATIN SMALL LETTER P WITH FLOURISH 
  Psquirrel = u'\uA754'	#  LATIN CAPITAL LETTER P WITH SQUIRREL TAIL 
  psquirrel = u'\uA755'	#  LATIN SMALL LETTER P WITH SQUIRREL TAIL 
  Qbardes = u'\uA756'	#  LATIN CAPITAL LETTER Q WITH STROKE THROUGH DESCENDER 
  qbardes = u'\uA757'	#  LATIN SMALL LETTER Q WITH STROKE THROUGH DESCENDER 
  Qslstrok = u'\uA758'	#  LATIN CAPITAL LETTER Q WITH DIAGONAL STROKE 
  qslstrok = u'\uA759'	#  LATIN SMALL LETTER Q WITH DIAGONAL STROKE 
  Rrot = u'\uA75A'	#  LATIN CAPITAL LETTER R ROTUNDA 
  rrot = u'\uA75B'	#  LATIN SMALL LETTER R ROTUNDA 
  RUM = u'\uA75C'	#  LATIN CAPITAL LETTER RUM ROTUNDA 
  rum = u'\uA75D'	#  LATIN SMALL LETTER RUM ROTUNDA 
  Vdiagstrok = u'\uA75E'	#  LATIN CAPITAL LETTER V WITH DIAGONAL STROKE 
  vdiagstrok = u'\uA75F'	#  LATIN SMALL LETTER V WITH DIAGONAL STROKE 
  YYlig = u'\uA760'	#  LATIN CAPITAL LETTER VY 
  yylig = u'\uA761'	#  LATIN SMALL LETTER VY 
  Vvisigot = u'\uA762'	#  LATIN CAPITAL LETTER VISIGOTHIC Z 
  vvisigot = u'\uA763'	#  LATIN SMALL LETTER VISIGOTHIC Z 
  THORNbar = u'\uA764'	#  LATIN CAPITAL LETTER THORN WITH STROKE 
  thornbar = u'\uA765'	#  LATIN SMALL LETTER THORN WITH STROKE 
  THORNbardes = u'\uA766'	#  LATIN CAPITAL LETTER THORN WITH STROKE THROUGH DESCENDER 
  thornbardes = u'\uA767'	#  LATIN SMALL LETTER THORN WITH STROKE THROUGH DESCENDER 
  Vins = u'\uA768'	#  LATIN CAPITAL LETTER VEND 
  vins = u'\uA769'	#  LATIN SMALL LETTER VEND 
  ETfin = u'\uA76A'	#  LATIN CAPITAL LETTER ET 
  etfin = u'\uA76B'	#  LATIN SMALL LETTER ET 
  IS = u'\uA76C'	#  LATIN CAPITAL LETTER IS 
  is_ = u'\uA76D'	#  LATIN SMALL LETTER IS 
  CONdes = u'\uA76E'	#  LATIN CAPITAL LETTER CON 
  condes = u'\uA76F'	#  LATIN SMALL LETTER CON 
  usmod = u'\uA770'	#  MODIFIER LETTER US 
  dtailstrok = u'\uA771'	#  LATIN SMALL LETTER DUM 
  ltailstrok = u'\uA772'	#  LATIN SMALL LETTER LUM 
  mtailstrok = u'\uA773'	#  LATIN SMALL LETTER MUM 
  ntailstrok = u'\uA774'	#  LATIN SMALL LETTER NUM 
  rtailstrok = u'\uA775'	#  LATIN SMALL LETTER RUM 
  rscaptailstrok = u'\uA776'	#  LATIN LETTER SMALL CAPITAL RUM 
  ttailstrok = u'\uA777'	#  LATIN SMALL LETTER TUM 
  sstrok = u'\uA778'	#  LATIN SMALL LETTER UM 
  Drot = u'\uA779'	#  LATIN CAPITAL LETTER INSULAR D 
  drot = u'\uA77A'	#  LATIN SMALL LETTER INSULAR D 
  Fins = u'\uA77B'	#  LATIN CAPITAL LETTER INSULAR F 
  fins = u'\uA77C'	#  LATIN SMALL LETTER INSULAR F 
  Gins = u'\uA77D'	#  LATIN CAPITAL LETTER INSULAR G 
  Ginsturn = u'\uA77E'	#  LATIN CAPITAL LETTER TURNED INSULAR G 
  ginsturn = u'\uA77F'	#  LATIN SMALL LETTER TURNED INSULAR G 
  Lturn = u'\uA780'	#  LATIN CAPITAL LETTER TURNED L 
  lturn = u'\uA781'	#  LATIN SMALL LETTER TURNED L 
  Rins = u'\uA782'	#  LATIN CAPITAL LETTER INSULAR R 
  rins = u'\uA783'	#  LATIN SMALL LETTER INSULAR R 
  Sins = u'\uA784'	#  LATIN CAPITAL LETTER INSULAR S 
  sins = u'\uA785'	#  LATIN SMALL LETTER INSULAR S 
  Trot = u'\uA786'	#  LATIN CAPITAL LETTER INSULAR T 
  trot = u'\uA787'	#  LATIN SMALL LETTER INSULAR T 
  Frev = u'\uA7FB'	#  LATIN EPIGRAPHIC LETTER REVERSED F 
  Prev = u'\uA7FC'	#  LATIN EPIGRAPHIC LETTER REVERSED P 
  Minv = u'\uA7FD'	#  LATIN EPIGRAPHIC LETTER INVERTED M 
  Ilong = u'\uA7FE'	#  LATIN EPIGRAPHIC LETTER I LONGA 
  M5leg = u'\uA7FF'	#  LATIN EPIGRAPHIC LETTER ARCHAIC M 
  
  Latin_ExtendedD = u''
  Latin_ExtendedD += fscap + sscap + AAlig + aalig + AOlig
  Latin_ExtendedD += aolig + AUlig + aulig + AVlig + avlig
  Latin_ExtendedD += AVligslash + avligslash + AYlig + aylig + CONdot
  Latin_ExtendedD += condot + Kbar + kbar + Kstrleg + kstrleg
  Latin_ExtendedD += Kstrascleg + kstrascleg + Lbrk + lbrk + Lhighstrok
  Latin_ExtendedD += lhighstrok + OBIIT + obiit + Oloop + oloop
  Latin_ExtendedD += OOlig + oolig + Pbardes + pbardes + Pflour
  Latin_ExtendedD += pflour + Psquirrel + psquirrel + Qbardes + qbardes
  Latin_ExtendedD += Qslstrok + qslstrok + Rrot + rrot + RUM
  Latin_ExtendedD += rum + Vdiagstrok + vdiagstrok + YYlig + yylig
  Latin_ExtendedD += Vvisigot + vvisigot + THORNbar + thornbar + THORNbardes
  Latin_ExtendedD += thornbardes + Vins + vins + ETfin + etfin
  Latin_ExtendedD += IS + is_ + CONdes + condes + usmod
  Latin_ExtendedD += dtailstrok + ltailstrok + mtailstrok + ntailstrok + rtailstrok
  Latin_ExtendedD += rscaptailstrok + ttailstrok + sstrok + Drot + drot
  Latin_ExtendedD += Fins + fins + Gins + Ginsturn + ginsturn
  Latin_ExtendedD += Lturn + lturn + Rins + rins + Sins
  Latin_ExtendedD += sins + Trot + trot + Frev + Prev
  Latin_ExtendedD += Minv + Ilong + M5leg
  
  
  #  (29): Alphabetic Presentation Forms 
  
  fflig = u'\uFB00'	#  LATIN SMALL LIGATURE FF 
  filig = u'\uFB01'	#  LATIN SMALL LIGATURE FI 
  fllig = u'\uFB02'	#  LATIN SMALL LIGATURE FL 
  ffilig = u'\uFB03'	#  LATIN SMALL LIGATURE FFI 
  ffllig = u'\uFB04'	#  LATIN SMALL LIGATURE FFL 
  slongtlig = u'\uFB05'	#  LATIN SMALL LIGATURE LONG S T 
  stlig = u'\uFB06'	#  LATIN SMALL LIGATURE ST 
  
  # <!-- (30): Ancient Symbols
  
  # The codepoints in this section are outside the Basic Multilingual Plane.
  # Until further notice, the old codepoints in the Private Use Area subrange 12 should be used.
  
  # -->
  
  #  (II) Characters in the Private Use Area - MUFI v. 3.0 allocation 
  #  (A) Base characters 
  Alphabetic_Presentation_Forms = u''
  Alphabetic_Presentation_Forms += fflig + filig + fllig + ffilig + ffllig
  Alphabetic_Presentation_Forms += slongtlig + stlig
  
  
  #  (1): Ligatures 
  #  (a): Structural ligatures 
  
  aacloselig = u'\uEFA0'	#  LATIN SMALL LIGATURE AA CLOSED FORM 
  aeligred = u'\uF204'	#  LATIN SMALL LETTER AE WITH RIGHT UPPER LOOP 
  AnecklessElig = u'\uEFAE'	#  LATIN CAPITAL LIGATURE NECKLESS A E 
  anecklesselig = u'\uEFA1'	#  LATIN SMALL LIGATURE NECKLESS A E 
  anecklessvlig = u'\uEFA2'	#  LATIN SMALL LIGATURE NECKLESS A V 
  AOligred = u'\uF205'	#  LATIN CAPITAL LIGATURE AO NECKLESS 
  aoligred = u'\uF206'	#  LATIN SMALL LIGATURE AO NECKLESS 
  
  #  (b): Non-structural ligatures 
  
  aflig = u'\uEFA3'	#  LATIN SMALL LIGATURE AF 
  afinslig = u'\uEFA4'	#  LATIN SMALL LIGATURE A INSULAR F 
  aglig = u'\uEFA5'	#  LATIN SMALL LIGATURE AG 
  allig = u'\uEFA6'	#  LATIN SMALL LIGATURE AL 
  anlig = u'\uEFA7'	#  LATIN SMALL LIGATURE AN 
  anscaplig = u'\uEFA8'	#  LATIN SMALL LIGATURE A SMALL CAPITAL N 
  aplig = u'\uEFA9'	#  LATIN SMALL LIGATURE AP 
  arlig = u'\uEFAA'	#  LATIN SMALL LIGATURE AR 
  arscaplig = u'\uEFAB'	#  LATIN SMALL LIGATURE A SMALL CAPITAL R 
  athornlig = u'\uEFAC'	#  LATIN SMALL LIGATURE A THORN 
  bblig = u'\uEEC2'	#  LATIN SMALL LIGATURE BB 
  bglig = u'\uEEC3'	#  LATIN SMALL LIGATURE BG 
  cklig = u'\uEEC4'	#  LATIN SMALL LIGATURE CK 
  ctlig = u'\uEEC5'	#  LATIN SMALL LIGATURE CT 
  drotdrotlig = u'\uEEC6'	#  LATIN SMALL LIGATURE D ROTUNDA D ROTUNDA 
  eylig = u'\uEEC7'	#  LATIN SMALL LIGATURE EY 
  faumllig = u'\uEEC8'	#  LATIN SMALL LIGATURE F A WITH DIAERESIS 
  fjlig = u'\uEEC9'	#  LATIN SMALL LIGATURE FJ 
  foumllig = u'\uF1BC'	#  LATIN SMALL LIGATURE F O WITH DIAERESIS 
  frlig = u'\uEECA'	#  LATIN SMALL LIGATURE FR 
  ftlig = u'\uEECB'	#  LATIN SMALL LIGATURE FT 
  fuumllig = u'\uEECC'	#  LATIN SMALL LIGATURE F U WITH DIAERESIS 
  fylig = u'\uEECD'	#  LATIN SMALL LIGATURE FY 
  fftlig = u'\uEECE'	#  LATIN SMALL LIGATURE FFT 
  ffylig = u'\uEECF'	#  LATIN SMALL LIGATURE FFY 
  ftylig = u'\uEED0'	#  LATIN SMALL LIGATURE FTY 
  gglig = u'\uEED1'	#  LATIN SMALL LIGATURE GG 
  gdlig = u'\uEED2'	#  LATIN SMALL LIGATURE GD 
  gdrotlig = u'\uEED3'	#  LATIN SMALL LIGATURE G D ROTUNDA 
  gethlig = u'\uEED4'	#  LATIN SMALL LIGATURE G ETH 
  golig = u'\uEEDE'	#  LATIN SMALL LIGATURE GO 
  gplig = u'\uEAD2'	#  LATIN SMALL LIGATURE GP 
  grlig = u'\uEAD0'	#  LATIN SMALL LIGATURE GR 
  qvinslig = u'\uEAD1'	#  LATIN SMALL LIGATURE Q INSULAR V 
  hrarmlig = u'\uE8C3'	#  LATIN SMALL LETTER H LIGATED WITH ARM OF LATIN SMALL LETTER R 
  Hrarmlig = u'\uE8C2'	#  LATIN CAPITAL LETTER H LIGATED WITH ARM OF LATIN SMALL LETTER R 
  krarmlig = u'\uE8C5'	#  LATIN SMALL LETTER K LIGATED WITH ARM OF LATIN SMALL LETTER R 
  lllig = u'\uF4F9'	#  LATIN SMALL LIGATURE LL 
  nscapslonglig = u'\uEED5'	#  LATIN SMALL LIGATURE SMALL CAPITAL N LONG S 
  oclig = u'\uEFAD'	#  LATIN SMALL LIGATURE OC 
  PPlig = u'\uEEDD'	#  LATIN CAPITAL LIGATURE PP 
  pplig = u'\uEED6'	#  LATIN SMALL LIGATURE PP 
  ppflourlig = u'\uEED7'	#  LATIN SMALL LIGATURE PP WITH FLOURISH 
  slongaumllig = u'\uEBA0'	#  LATIN SMALL LIGATURE LONG S A WITH DIAERESIS 
  slongchlig = u'\uF4FA'	#  LATIN SMALL LIGATURE LONG S CH 
  slonghlig = u'\uEBA1'	#  LATIN SMALL LIGATURE LONG S H 
  slongilig = u'\uEBA2'	#  LATIN SMALL LIGATURE LONG S I 
  slongjlig = u'\uF4FB'	#  LATIN SMALL LIGATURE LONG S J 
  slongklig = u'\uF4FC'	#  LATIN SMALL LIGATURE LONG S K 
  slongllig = u'\uEBA3'	#  LATIN SMALL LIGATURE LONG S L 
  slongoumllig = u'\uEBA4'	#  LATIN SMALL LIGATURE LONG S O WITH DIAERESIS 
  slongplig = u'\uEBA5'	#  LATIN SMALL LIGATURE LONG S P 
  slongslig = u'\uF4FD'	#  LATIN SMALL LIGATURE LONG S S 
  slongslonglig = u'\uEBA6'	#  LATIN SMALL LIGATURE LONG S LONG S 
  slongslongilig = u'\uEBA7'	#  LATIN SMALL LIGATURE LONG S LONG S I 
  slongslongklig = u'\uF4FE'	#  LATIN SMALL LIGATURE LONG S LONG S K 
  slongslongllig = u'\uEBA8'	#  LATIN SMALL LIGATURE LONG S LONG S L 
  slongslongtlig = u'\uF4FF'	#  LATIN SMALL LIGATURE LONG S LONG S T 
  slongtilig = u'\uEBA9'	#  LATIN SMALL LIGATURE LONG S TI 
  slongtrlig = u'\uEBAA'	#  LATIN SMALL LIGATURE LONG S TR 
  slonguumllig = u'\uEBAB'	#  LATIN SMALL LIGATURE LONG S U WITH DIAERESIS 
  slongvinslig = u'\uEBAC'	#  LATIN SMALL LIGATURE LONG S INSULAR V 
  slongdestlig = u'\uEADA'	#  LATIN SMALL LIGATURE LONG S DESCENDING T 
  trlig = u'\uEED8'	#  LATIN SMALL LIGATURE TR 
  ttlig = u'\uEED9'	#  LATIN SMALL LIGATURE TT 
  trottrotlig = u'\uEEDA'	#  LATIN SMALL LIGATURE T ROTUNDA T ROTUNDA 
  tylig = u'\uEEDB'	#  LATIN SMALL LIGATURE TY 
  tzlig = u'\uEEDC'	#  LATIN SMALL LIGATURE TZ 
  thornrarmlig = u'\uE8C1'	#  LATIN SMALL LETTER THORN LIGATED WITH ARM OF LATIN SMALL LETTER R 
  
  Ligatures = u''
  Ligatures += aacloselig + aeligred + AnecklessElig + anecklesselig + anecklessvlig
  Ligatures += AOligred + aoligred + aflig + afinslig + aglig
  Ligatures += allig + anlig + anscaplig + aplig + arlig
  Ligatures += arscaplig + athornlig + bblig + bglig + cklig
  Ligatures += ctlig + drotdrotlig + eylig + faumllig + fjlig
  Ligatures += foumllig + frlig + ftlig + fuumllig + fylig
  Ligatures += fftlig + ffylig + ftylig + gglig + gdlig
  Ligatures += gdrotlig + gethlig + golig + gplig + grlig
  Ligatures += qvinslig + hrarmlig + Hrarmlig + krarmlig + lllig
  Ligatures += nscapslonglig + oclig + PPlig + pplig + ppflourlig
  Ligatures += slongaumllig + slongchlig + slonghlig + slongilig + slongjlig
  Ligatures += slongklig + slongllig + slongoumllig + slongplig + slongslig
  Ligatures += slongslonglig + slongslongilig + slongslongklig + slongslongllig + slongslongtlig
  Ligatures += slongtilig + slongtrlig + slonguumllig + slongvinslig + slongdestlig
  Ligatures += trlig + ttlig + trottrotlig + tylig + tzlig
  Ligatures += thornrarmlig
  
  
  #  (2): Small capitals 
  
  qscap = u'\uEF0C'	#  LATIN LETTER SMALL CAPITAL Q 
  xscap = u'\uEF11'	#  LATIN LETTER SMALL CAPITAL X 
  thornscap = u'\uEF15'	#  LATIN LETTER SMALL CAPITAL THORN 
  
  Small_capitals = u''
  Small_capitals += qscap + xscap + thornscap
  
  
  #  (3): Enlarged minuscules 
  
  aenl = u'\uEEE0'	#  LATIN ENLARGED LETTER SMALL A 
  aenlacute = u'\uEAF0'	#  LATIN ENLARGED LETTER SMALL A WITH ACUTE 
  aaligenl = u'\uEFDF'	#  LATIN ENLARGED LETTER SMALL LIGATURE AA 
  aeligenl = u'\uEAF1'	#  LATIN ENLARGED LETTER SMALL LIGATURE AE 
  aoligenl = u'\uEFDE'	#  LATIN ENLARGED LETTER SMALL LIGATURE A0 
  aenlosmalllig = u'\uEAF2'	#  LATIN LIGATURE ENLARGED LETTER SMALL A AND LATIN SMALL LETTER O 
  benl = u'\uEEE1'	#  LATIN ENLARGED LETTER SMALL B 
  cenl = u'\uEEE2'	#  LATIN ENLARGED LETTER SMALL C 
  denl = u'\uEEE3'	#  LATIN ENLARGED LETTER SMALL D 
  drotenl = u'\uEEE4'	#  LATIN ENLARGED LETTER D ROTUNDA 
  ethenl = u'\uEEE5'	#  LATIN ENLARGED LETTER SMALL ETH 
  eenl = u'\uEEE6'	#  LATIN ENLARGED LETTER SMALL E 
  eogonenl = u'\uEAF3'	#  LATIN ENLARGED LETTER SMALL E WITH OGONEK 
  fenl = u'\uEEE7'	#  LATIN ENLARGED LETTER SMALL F 
  finsenl = u'\uEEFF'	#  LATIN ENLARGED LETTER SMALL INSULAR F 
  genl = u'\uEEE8'	#  LATIN ENLARGED LETTER SMALL G 
  henl = u'\uEEE9'	#  LATIN ENLARGED LETTER SMALL H 
  ienl = u'\uEEEA'	#  LATIN ENLARGED LETTER SMALL I 
  inodotenl = u'\uEEFD'	#  LATIN ENLARGED LETTER SMALL DOTLESS I 
  jenl = u'\uEEEB'	#  LATIN ENLARGED LETTER SMALL J 
  jnodotenl = u'\uEEFE'	#  LATIN ENLARGED LETTER SMALL DOTLESS J 
  kenl = u'\uEEEC'	#  LATIN ENLARGED LETTER SMALL K 
  lenl = u'\uEEED'	#  LATIN ENLARGED LETTER SMALL L 
  menl = u'\uEEEE'	#  LATIN ENLARGED LETTER SMALL M 
  nenl = u'\uEEEF'	#  LATIN ENLARGED LETTER SMALL N 
  oenl = u'\uEEF0'	#  LATIN ENLARGED LETTER SMALL O 
  oeligenl = u'\uEFDD'	#  LATIN ENLARGED LETTER SMALL LIGATURE OE 
  penl = u'\uEEF1'	#  LATIN ENLARGED LETTER SMALL P 
  qenl = u'\uEEF2'	#  LATIN ENLARGED LETTER SMALL Q 
  renl = u'\uEEF3'	#  LATIN ENLARGED LETTER SMALL R 
  senl = u'\uEEF4'	#  LATIN ENLARGED LETTER SMALL S 
  slongenl = u'\uEEDF'	#  LATIN ENLARGED LETTER SMALL LONG S 
  tenl = u'\uEEF5'	#  LATIN ENLARGED LETTER SMALL T 
  uenl = u'\uEEF7'	#  LATIN ENLARGED LETTER SMALL U 
  venl = u'\uEEF8'	#  LATIN ENLARGED LETTER SMALL V 
  wenl = u'\uEEF9'	#  LATIN ENLARGED LETTER SMALL W 
  xenl = u'\uEEFA'	#  LATIN ENLARGED LETTER SMALL X 
  yenl = u'\uEEFB'	#  LATIN ENLARGED LETTER SMALL Y 
  zenl = u'\uEEFC'	#  LATIN ENLARGED LETTER SMALL Z 
  thornenl = u'\uEEF6'	#  LATIN ENLARGED LETTER SMALL THORN 
  
  Enlarged_minuscules = u''
  Enlarged_minuscules += aenl + aenlacute + aaligenl + aeligenl + aoligenl
  Enlarged_minuscules += aenlosmalllig + benl + cenl + denl + drotenl
  Enlarged_minuscules += ethenl + eenl + eogonenl + fenl + finsenl
  Enlarged_minuscules += genl + henl + ienl + inodotenl + jenl
  Enlarged_minuscules += jnodotenl + kenl + lenl + menl + nenl
  Enlarged_minuscules += oenl + oeligenl + penl + qenl + renl
  Enlarged_minuscules += senl + slongenl + tenl + uenl + venl
  Enlarged_minuscules += wenl + xenl + yenl + zenl + thornenl
  
  
  #  (4): Base-line abbreviation characters 
  
  USbase = u'\uF1A5'	#  LATIN ABBREVIATION SIGN SPACING BASE-LINE CAPITAL US 
  usbase = u'\uF1A6'	#  LATIN ABBREVIATION SIGN SPACING BASE-LINE SMALL US 
  ET = u'\uF142'	#  LATIN ABBREVIATION SIGN CAPITAL ET 
  ETslash = u'\uF1A7'	#  LATIN ABBREVIATION SIGN CAPITAL ET WITH STROKE 
  etslash = u'\uF158'	#  LATIN ABBREVIATION SIGN SMALL ET WITH STROKE 
  de = u'\uF159'	#  LATIN ABBREVIATION SIGN SMALL DE 
  sem = u'\uF1AC'	#  LATIN ABBREVIATION SIGN SEMICOLON 
  
  Baseline_abbreviation_characters = u''
  Baseline_abbreviation_characters += USbase + usbase + ET + ETslash + etslash
  Baseline_abbreviation_characters += de + sem
  
  
  #  (5): Modified base-line abbreviation characters 
  
  hslonglig = u'\uEBAD'	#  LATIN SMALL LIGATURE H AND LONG S 
  hslongligbar = u'\uE7C7'	#  LATIN SMALL LIGATURE H AND LONG S WITH STROKE
  kslonglig = u'\uEBAE'	#  LATIN SMALL LIGATURE K AND LONG S 
  kslongligbar = u'\uE7C8'	#  LATIN SMALL LIGATURE K AND LONG S WITH STROKE 
  q2app = u'\uE8B3'	#  LATIN SMALL LETTER Q LIGATED WITH R ROTUNDA 
  q3app = u'\uE8BF'	#  LATIN SMALL LETTER Q LIGATED WITH FINAL ET 
  qcentrslstrok = u'\uE8B4'	#  LATIN SMALL LETTER Q WITH CENTRAL SLANTED STROKE 
  rdesstrok = u'\uE7E4'	#  LATIN SMALL LETTER R WITH LONG LEG AND STROKE THROUGH DESCENDER 
  slongflour = u'\uE8B7'	#  LATIN SMALL LETTER LONG S WITH FLOURISH 
  slongslstrok = u'\uE8B8'	#  LATIN SMALL LETTER LONG S WITH SLANTED DESCENDING STROKE 
  vslash = u'\uE8BA'	#  LATIN SMALL LETTER V WITH SHORT SLASH 
  xslashula = u'\uE8BD'	#  LATIN SMALL LETTER X WITH SHORT SLASH ABOVE 
  xslashlra = u'\uE8BE'	#  LATIN SMALL LETTER X WITH SHORT SLASH BELOW 
  THORNbarslash = u'\uE337'	#  LATIN CAPITAL LETTER THORN WITH DIAGONAL STROKE 
  thornbarslash = u'\uF149'	#  LATIN SMALL LETTER THORN WITH DIAGONAL STROKE 
  thornslonglig = u'\uE734'	#  LATIN SMALL LIGATURE THORN AND LONG S 
  thornslongligbar = u'\uE735'	#  LATIN SMALL LIGATURE THORN AND LONG S WITH STROKE 
  
  Modified_baseline_abbreviation_characters = u''
  Modified_baseline_abbreviation_characters += hslonglig + hslongligbar + kslonglig + kslongligbar + q2app
  Modified_baseline_abbreviation_characters += q3app + qcentrslstrok + rdesstrok + slongflour + slongslstrok
  Modified_baseline_abbreviation_characters += vslash + xslashula + xslashlra + THORNbarslash + thornbarslash
  Modified_baseline_abbreviation_characters += thornslonglig + thornslongligbar
  
  
  #  (6): Combining marks 
  
  arbar = u'\uF1C0'	#  COMBINING ABBREVIATION MARK BAR ABOVE WITH DOT 
  erang = u'\uF1C7'	#  COMBINING ABBREVIATION MARK ZIGZAG ABOVE ANGLE FORM 
  ercurl = u'\uF1C8'	#  COMBINING ABBREVIATION MARK ZIGZAG ABOVE CURLY FORM 
  rabar = u'\uF1C1'	#  COMBINING ABBREVIATION MARK SUPERSCRIPT RA OPEN A FORM WITH BAR ABOVE 
  urrot = u'\uF153'	#  COMBINING ABBREVIATION MARK SUPERSCRIPT UR ROUND R FORM 
  urlemn = u'\uF1C2'	#  COMBINING ABBREVIATION MARK SUPERSCRIPT UR LEMNISKATE FORM 
  combcurlhigh = u'\uF1C5'	#  COMBINING CURL HIGH POSITION 
  combdothigh = u'\uF1CA'	#  COMBINING DOT ABOVE HIGH POSITION 
  combcurlbar = u'\uF1CC'	#  COMBINING CURLY BAR ABOVE 
  combtripbrevebl = u'\uF1FC'	#  COMBINING TRIPLE BREVE BELOW 
  
  Combining_marks = u''
  Combining_marks += arbar + erang + ercurl + rabar + urrot
  Combining_marks += urlemn + combcurlhigh + combdothigh + combcurlbar + combtripbrevebl
  
  
  #  (7): Combining superscript characters 
  
  anligsup = u'\uF036'	#  COMBINING LATIN SMALL LIGATURE AN 
  anscapligsup = u'\uF03A'	#  COMBINING LATIN SMALL LIGATURE A SMALL CAPITAL N 
  arligsup = u'\uF038'	#  COMBINING LATIN SMALL LIGATURE AR 
  arscapligsup = u'\uF130'	#  COMBINING LATIN SMALL LIGATURE A SMALL CAPITAL R 
  bsup = u'\uF012'	#  COMBINING LATIN SMALL LETTER B 
  bscapsup = u'\uF013'	#  COMBINING LATIN LETTER SMALL CAPITAL B 
  dscapsup = u'\uF016'	#  COMBINING LATIN LETTER SMALL CAPITAL D 
  eogonsup = u'\uF135'	#  COMBINING LATIN SMALL LETTER E WITH OGONEK 
  emacrsup = u'\uF136'	#  COMBINING LATIN SMALL LETTER E WITH MACRON 
  fsup = u'\uF017'	#  COMBINING LATIN SMALL LETTER F 
  inodotsup = u'\uF02F'	#  COMBINING LATIN SMALL LETTER DOTLESS I 
  jsup = u'\uF030'	#  COMBINING LATIN SMALL LETTER J 
  jnodotsup = u'\uF031'	#  COMBINING LATIN SMALL LETTER DOTLESS J 
  kscapsup = u'\uF01C'	#  COMBINING LATIN LETTER SMALL CAPITAL K 
  omacrsup = u'\uF13F'	#  COMBINING LATIN SMALL LETTER O WITH MACRON 
  oogonsup = u'\uF13E'	#  COMBINING LATIN SMALL LETTER O WITH OGONEK 
  oslashsup = u'\uF032'	#  COMBINING LATIN SMALL LETTER O WITH STROKE 
  orrotsup = u'\uF03E'	#  COMBINING LATIN SMALL LETTER O R ROTUNDA 
  orumsup = u'\uF03F'	#  COMBINING LATIN SMALL LETTER O RUM 
  psup = u'\uF025'	#  COMBINING LATIN SMALL LETTER P 
  qsup = u'\uF033'	#  COMBINING LATIN SMALL LETTER Q 
  rumsup = u'\uF040'	#  COMBINING LATIN SMALL LETTER RUM 
  tscapsup = u'\uF02A'	#  COMBINING LATIN LETTER SMALL CAPITAL T 
  trotsup = u'\uF03B'	#  COMBINING LATIN SMALL LETTER T ROTUNDA 
  wsup = u'\uF03C'	#  COMBINING LATIN SMALL LETTER W 
  ysup = u'\uF02B'	#  COMBINING LATIN SMALL LETTER Y 
  thornsup = u'\uF03D'	#  COMBINING LATIN SMALL LETTER THORN 
  
  Combining_superscript_characters = u''
  Combining_superscript_characters += anligsup + anscapligsup + arligsup + arscapligsup + bsup
  Combining_superscript_characters += bscapsup + dscapsup + eogonsup + emacrsup + fsup
  Combining_superscript_characters += inodotsup + jsup + jnodotsup + kscapsup + omacrsup
  Combining_superscript_characters += oogonsup + oslashsup + orrotsup + orumsup + psup
  Combining_superscript_characters += qsup + rumsup + tscapsup + trotsup + wsup
  Combining_superscript_characters += ysup + thornsup
  
  
  #  (8): Punctuation marks 
  
  hidot = u'\uF1F8'	#  HIGH DOT 
  posit = u'\uF1E2'	#  COMMA POSITURA 
  ductsimpl = u'\uF1E3'	#  HIGH COMMA POSITURA (SIMPLEX DUCTUS) 
  punctvers = u'\uF1EA'	#  PUNCTUS VERSUS 
  colelevposit = u'\uF1E4'	#  PUNCTUS WITH COMMA POSITURA 
  colmidcomposit = u'\uF1E5'	#  COLON WITH MIDDLE COMMA POSITURA 
  bidotscomposit = u'\uF1F2'	#  TWO DOTS OVER COMMA POSITURA 
  tridotscomposit = u'\uF1E6'	#  THREE DOTS WITH COMMA POSITURA 
  punctelev = u'\uF161'	#  PUNCTUS ELEVATUS 
  punctelevdiag = u'\uF1F0'	#  PUNCTUS ELEVATUS DIAGONAL STROKE 
  punctelevhiback = u'\uF1FA'	#  PUNCTUS ELEVATUS WITH HIGH BACK 
  punctelevhack = u'\uF1FB'	#  PUNCTUS ELEVATUS WITH HACKLE 
  punctflex = u'\uF1F5'	#  PUNCTUS FLEXUS 
  punctexclam = u'\uF1E7'	#  PUNCTUS EXCLAMATIVUS 
  punctinter = u'\uF160'	#  PUNCTUS INTERROGATIVUS 
  punctintertilde = u'\uF1E8'	#  PUNCTUS INTERROGATIVUS HORIZONTAL TILDE 
  punctinterlemn = u'\uF1F1'	#  PUNCTUS INTERROGATIVUS LEMNISKATE FORM 
  wavylin = u'\uF1F9'	#  WAVY LINE 
  medcom = u'\uF1E0'	#  MEDIEVAL COMMA 
  parag = u'\uF1E1'	#  PARAGRAPHUS 
  renvoi = u'\uF1EC'	#  SIGNE DE RENVOI 
  virgsusp = u'\uF1F4'	#  VIRGULA SUSPENSIVA 
  virgmin = u'\uF1F7'	#  SHORT VIRGULA 
  
  Punctuation_marks = u''
  Punctuation_marks += hidot + posit + ductsimpl + punctvers + colelevposit
  Punctuation_marks += colmidcomposit + bidotscomposit + tridotscomposit + punctelev + punctelevdiag
  Punctuation_marks += punctelevhiback + punctelevhack + punctflex + punctexclam + punctinter
  Punctuation_marks += punctintertilde + punctinterlemn + wavylin + medcom + parag
  Punctuation_marks += renvoi + virgsusp + virgmin
  
  
  #  (9): Critical and epigraphical signs 
  
  midring = u'\uF1DA'	#  MIDDLE RING 
  ramus = u'\uF1DB'	#  PALM BRANCH 
  
  Critical_and_epigraphical_signs = u''
  Critical_and_epigraphical_signs += midring + ramus
  
  
  #  (10): Metrical symbols 
  
  metranc = u'\uF70A'	#  METRICAL SYMBOL ANCEPS 
  metrancacute = u'\uF70B'	#  METRICAL SYMBOL ANCEPS WITH ACUTE (PRIMARY STRESS) 
  metrancdblac = u'\uF719'	#  METRICAL SYMBOL ANCEPS WITH DOUBLE ACUTE (PRIMARY STRESS AND ALLITERATION) 
  metrancgrave = u'\uF70C'	#  METRICAL SYMBOL ANCEPS WITH GRAVE (SECONDARY STRESS) 
  metrancdblgrave = u'\uF71A'	#  METRICAL SYMBOL ANCEPS WITH DOUBLE GRAVE (SECONDARY STRESS AND ALLITERATION) 
  metrbreve = u'\uF701'	#  METRICAL SYMBOL BREVE 
  metrbreveacute = u'\uF706'	#  METRICAL SYMBOL BREVE WITH ACUTE (PRIMARY STRESS) 
  metrbrevedblac = u'\uF717'	#  METRICAL SYMBOL BREVE WITH DOUBLE ACUTE (PRIMARY STRESS AND ALLITERATION) 
  metrbrevegrave = u'\uF707'	#  METRICAL SYMBOL BREVE WITH GRAVE (SECONDARY STRESS) 
  metrbrevedblgrave = u'\uF718'	#  METRICAL SYMBOL BREVE WITH DOUBLE GRAVE (SECONDARY STRESS AND ALLITERATION) 
  metrmacr = u'\uF700'	#  METRICAL SYMBOL LONGUM 
  metrmacracute = u'\uF704'	#  METRICAL SYMBOL LONGUM WITH ACUTE (PRIMARY STRESS) 
  metrmacrdblac = u'\uF715'	#  METRICAL SYMBOL LONGUM WITH DOUBLE ACUTE (PRIMARY STRESS AND ALLITERATION) 
  metrmacrgrave = u'\uF705'	#  METRICAL SYMBOL LONGUM WITH GRAVE (SECONDARY STRESS) 
  metrmacrdblgrave = u'\uF716'	#  METRICAL SYMBOL LONGUM WITH DOUBLE GRAVE (SECONDARY STRESS AND ALLITERATION) 
  metrmacrbreve = u'\uF702'	#  METRICAL SYMBOL BREVE ABOVE LONGUM (SHORT OR LONG SYLLABLE) 
  metrbrevemacr = u'\uF703'	#  METRICAL SYMBOL LONGUM ABOVE BREVE (SHORT OR LONG SYLLABLE) 
  metrmacrbreveacute = u'\uF708'	#  METRICAL SYMBOL BREVE ABOVE LONGUM WITH ACUTE (SHORT OR LONG SYLLABLE WITH PRIMARY STRESS) 
  metrmacrbrevegrave = u'\uF709'	#  METRICAL SYMBOL BREVE ABOVE LONGUM WITH GRAVE (SHORT OR LONG SYLLABLE WITH SECONDARY STRESS) 
  metrdblbrevemacr = u'\uF72E'	#  METRICAL SYMBOL RESOLVED LIFT 
  
  metrdblbrevemacracute = u'\uF71B'	#  METRICAL SYMBOL RESOLVED LIFT WITH ACUTE (PRIMARY STRESS) 
  metrdblbrevemacrdblac = u'\uF71C'	#  METRICAL SYMBOL RESOLVED LIFT WITH DOUBLE ACUTE (PRIMARY STRESS AND ALLITERATION) 
  metrpause = u'\uF714'	#  METRICAL SYMBOL PAUSE 
  
  Metrical_symbols = u''
  Metrical_symbols += metranc + metrancacute + metrancdblac + metrancgrave + metrancdblgrave
  Metrical_symbols += metrbreve + metrbreveacute + metrbrevedblac + metrbrevegrave + metrbrevedblgrave
  Metrical_symbols += metrmacr + metrmacracute + metrmacrdblac + metrmacrgrave + metrmacrdblgrave
  Metrical_symbols += metrmacrbreve + metrbrevemacr + metrmacrbreveacute + metrmacrbrevegrave + metrdblbrevemacr
  Metrical_symbols += metrdblbrevemacracute + metrdblbrevemacrdblac + metrpause
  
  
  #  (11): Additional number forms 
  
  smallzero = u'\uF1BD'	#  SMALL BASE LINE ZERO SIGN 
  Vmod = u'\uF1BE'	#  MODIFIER CAPITAL LETTER V 
  Xmod = u'\uF1BF'	#  MODIFIER CAPITAL LETTER X 
  
  Additional_number_forms = u''
  Additional_number_forms += smallzero + Vmod + Xmod
  
  
  #  (12): Weight, currency and measurement 
  
  romaslibr = u'\uF2E0'	#  ROMAN AS LIBRALIS SIGN 
  romXbar = u'\uF2E1'	#  LATIN CAPITAL LETTER X WITH BAR (DENARIUS SIGN) 
  romscapxbar = u'\uF2E2'	#  LATIN SMALL CAPITAL LETTER X WITH BAR (DENARIUS SIGN) 
  romscapybar = u'\uF2E3'	#  LATIN SMALL CAPITAL LETTER Y WITH BAR 
  romscapdslash = u'\uF2E4'	#  LATIN SMALL CAPITAL LETTER D WITH SLASH 
  dram = u'\uF2E6'	#  PHARMACEUTICAL DRAM SIGN 
  ecu = u'\uF2E7'	#  ECU SIGN 
  florloop = u'\uF2E8'	#  FLOREN SIGN WITH LOOP 
  grosch = u'\uF2E9'	#  GROSCHEN SIGN 
  libradut = u'\uF2EA'	#  DUTCH LIBRA SIGN 
  librafren = u'\uF2EB'	#  FRENCH LIBRA SIGN 
  libraital = u'\uF2EC'	#  ITALIAN LIBRA SIGN 
  libraflem = u'\uF2ED'	#  FLEMISH LIBRA SIGN 
  liranuov = u'\uF2EE'	#  LIRA NUOVA SIGN 
  lirasterl = u'\uF2EF'	#  LIRA STERLINA SIGN 
  markold = u'\uF2F0'	#  OLD MARK SIGN 
  markflour = u'\uF2F1'	#  OLD FLOURISH MARK SIGN 
  msign = u'\uF2F2'	#  MARKED SMALL LETTER M SIGN 
  msignflour = u'\uF2F3'	#  FLOURISHED SMALL LETTER M SIGN 
  obol = u'\uF2F4'	#  PHARMACEUTICAL OBOLUS SIGN 
  penningar = u'\uF2F5'	#  PENNING SIGN 
  reichtalold = u'\uF2F6'	#  OLD REICHSTALER SIGN 
  schillgerm = u'\uF2F7'	#  GERMAN SCHILLING SIGN 
  schillgermscript = u'\uF2F8'	#  GERMAN SCRIPT SCHILLING SIGN 
  scudi = u'\uF2F9'	#  SCUDI SIGN 
  sestert = u'\uF2FA'	#  SESTERTIUS SIGN 
  sextans = u'\uF2FB'	#  PHARMACEUTICAL SEXTANS SIGN 
  ouncescript = u'\uF2FD'	#  SCRIPT OUNCE SIGN 
  romas = u'\uF2D8'	#  ROMAN AS SIGN 
  romunc = u'\uF2D9'	#  ROMAN UNCIA SIGN 
  romsemunc = u'\uF2DA'	#  ROMAN SEMIUNCIA SIGN 
  romsext = u'\uF2DB'	#  ROMAN SEXTULA SIGN 
  romdimsext = u'\uF2DC'	#  ROMAN DIMIDIA SEXTULA SIGN 
  romsiliq = u'\uF2DD'	#  ROMAN SILIQUA SIGN 
  romquin = u'\uF2DE'	#  ROMAN QUINARIUS SIGN 
  romdupond = u'\uF2DF'	#  ROMAN DUPONDIUS SIGN 
  
  Weight_currency_and_measurement = u''
  Weight_currency_and_measurement += romaslibr + romXbar + romscapxbar + romscapybar + romscapdslash
  Weight_currency_and_measurement += dram + ecu + florloop + grosch + libradut
  Weight_currency_and_measurement += librafren + libraital + libraflem + liranuov + lirasterl
  Weight_currency_and_measurement += markold + markflour + msign + msignflour + obol
  Weight_currency_and_measurement += penningar + reichtalold + schillgerm + schillgermscript + scudi
  Weight_currency_and_measurement += sestert + sextans + ouncescript + romas + romunc
  Weight_currency_and_measurement += romsemunc + romsext + romdimsext + romsiliq + romquin
  Weight_currency_and_measurement += romdupond
  
  
  #  (13): Modified base-line characters 
  
  nbar = u'\uE7B2'	#  LATIN SMALL LETTER N WITH BAR 
  vbar = u'\uE74E'	#  LATIN SMALL LETTER V WITH BAR 
  ybar = u'\uE77B'	#  LATIN SMALL LETTER Y WITH BAR 
  
  Modified_baseline_characters = u''
  Modified_baseline_characters += nbar + vbar + ybar
  
  
  #  (14): For future additions 
  
  #  (B) Precomposed characters 
  For_future_additions = u''
  
  
  #  (15): Characters with macron or overline 
  
  macrhigh = u'\uF00A'	#  COMBINING HIGH MACRON WITH FIXED HEIGHT (PART-WIDTH) 
  macrmed = u'\uF00B'	#  COMBINING MEDIUM-HIGH MACRON WITH FIXED HEIGHT (PART-WIDTH) 
  ovlhigh = u'\uF00C'	#  COMBINING HIGH OVERLINE WITH FIXED HEIGHT (FULL WIDTH) 
  ovlmed = u'\uF00D'	#  COMBINING MEDIUM-HIGH OVERLINE WITH FIXED HEIGHT (FULL WIDTH) 
  bovlmed = u'\uE44D'	#  LATIN SMALL LETTER B WITH MEDIUM-HIGH OVERLINE (ACROSS ASCENDER) 
  Covlhigh = u'\uF7B5'	#  LATIN CAPITAL LETTER C WITH HIGH OVERLINE (ABOVE CHARACTER) 
  romnumCrevovl = u'\uF23F'	#  ROMAN NUMERAL REVERSED ONE HUNDRED WITH OVERLINE 
  Dovlhigh = u'\uF7B6'	#  LATIN CAPITAL LETTER D WITH HIGH OVERLINE (ABOVE CHARACTER) 
  dovlmed = u'\uE491'	#  LATIN SMALL LETTER D WITH MEDIUM-HIGH OVERLINE (ACROSS ASCENDER) 
  Eogonmacr = u'\uE0BC'	#  LATIN CAPITAL LETTER E WITH OGONEK AND MACRON 
  eogonmacr = u'\uE4BC'	#  LATIN SMALL LETTER E WITH OGONEK AND MACRON 
  hovlmed = u'\uE517'	#  LATIN SMALL LETTER H WITH MEDIUM-HIGH OVERLINE (ACROSS ASCENDER) 
  Iovlhigh = u'\uE150'	#  LATIN CAPITAL LETTER I WITH HIGH OVERLINE (ABOVE CHARACTER) 
  iovlmed = u'\uE550'	#  LATIN SMALL LETTER I WITH MEDIUM-HIGH OVERLINE (ABOVE CHARACTER) 
  Jmacrhigh = u'\uE154'	#  LATIN CAPITAL LETTER J WITH HIGH MACRON (ABOVE CHARACTER) 
  Jovlhigh = u'\uE152'	#  LATIN CAPITAL LETTER J WITH HIGH OVERLINE (ABOVE CHARACTER) 
  jmacrmed = u'\uE554'	#  LATIN SMALL LETTER J WITH MEDIUM-HIGH MACRON (ABOVE CHARACTER) 
  jovlmed = u'\uE552'	#  LATIN SMALL LETTER J WITH MEDIUM-HIGH OVERLINE (ABOVE CHARACTER) 
  kovlmed = u'\uE7C3'	#  LATIN SMALL LETTER K WITH MEDIUM-HIGH OVERLINE (ACROSS ASCENDER) 
  lovlmed = u'\uE5B1'	#  LATIN SMALL LETTER L WITH MEDIUM-HIGH OVERLINE (ACROSS ASCENDER) 
  Lovlhigh = u'\uF7B4'	#  LATIN CAPITAL LETTER L WITH HIGH OVERLINE (ABOVE CHARACTER) 
  lmacrhigh = u'\uE596'	#  LATIN SMALL LETTER L WITH HIGH MACRON (ABOVE CHARACTER) 
  lovlhigh = u'\uE58C'	#  LATIN SMALL LETTER L WITH HIGH OVERLINE (ABOVE CHARACTER) 
  Mmacrhigh = u'\uE1B8'	#  LATIN CAPITAL LETTER M WITH HIGH MACRON (ABOVE CHARACTER) 
  Movlhigh = u'\uE1D2'	#  LATIN CAPITAL LETTER M WITH HIGH OVERLINE (ABOVE CHARACTER) 
  mmacrmed = u'\uE5B8'	#  LATIN SMALL LETTER M WITH MEDIUM-HIGH MACRON (ABOVE CHARACTER) 
  movlmed = u'\uE5D2'	#  LATIN SMALL LETTER M WITH MEDIUM-HIGH OVERLINE (ABOVE CHARACTER) 
  Nmacrhigh = u'\uE1DC'	#  LATIN CAPITAL LETTER N WITH HIGH MACRON (ABOVE CHARACTER) 
  nmacrmed = u'\uE5DC'	#  LATIN SMALL LETTER N WITH MEDIUM-HIGH MACRON (ABOVE CHARACTER) 
  Oslashmacr = u'\uE252'	#  LATIN CAPITAL LETTER O WITH STROKE AND MACRON 
  oslashmacr = u'\uE652'	#  LATIN SMALL LETTER O WITH STROKE AND MACRON 
  OEligmacr = u'\uE25D'	#  LATIN CAPITAL LIGATURE OE WITH MACRON 
  oeligmacr = u'\uE65D'	#  LATIN SMALL LIGATURE OE WITH MACRON 
  oopenmacr = u'\uE7CC'	#  LATIN SMALL LETTER OPEN O WITH MACRON 
  pmacr = u'\uE665'	#  LATIN SMALL LETTER P WITH MACRON 
  qmacr = u'\uE681'	#  LATIN SMALL LETTER Q WITH MACRON 
  slongovlmed = u'\uE79E'	#  LATIN SMALL LETTER LONG S WITH MEDIUM-HIGH OVERLINE (ACROSS ASCENDER) 
  Vmacr = u'\uE34D'	#  LATIN CAPITAL LETTER V WITH MACRON 
  Vovlhigh = u'\uF7B2'	#  LATIN CAPITAL LETTER V WITH HIGH OVERLINE (ABOVE CHARACTER) 
  vmacr = u'\uE74D'	#  LATIN SMALL LETTER V WITH MACRON 
  Wmacr = u'\uE357'	#  LATIN CAPITAL LETTER W WITH MACRON 
  wmacr = u'\uE757'	#  LATIN SMALL LETTER W WITH MACRON 
  Xovlhigh = u'\uF7B3'	#  LATIN CAPITAL LETTER X WITH HIGH OVERLINE (ABOVE CHARACTER) 
  thornovlmed = u'\uE7A2'	#  LATIN SMALL LETTER THORN WITH MEDIUM-HIGH OVERLINE (ACROSS ASCENDER) 
  
  Characters_with_macron_or_overline = u''
  Characters_with_macron_or_overline += macrhigh + macrmed + ovlhigh + ovlmed + bovlmed
  Characters_with_macron_or_overline += Covlhigh + romnumCrevovl + Dovlhigh + dovlmed + Eogonmacr
  Characters_with_macron_or_overline += eogonmacr + hovlmed + Iovlhigh + iovlmed + Jmacrhigh
  Characters_with_macron_or_overline += Jovlhigh + jmacrmed + jovlmed + kovlmed + lovlmed
  Characters_with_macron_or_overline += Lovlhigh + lmacrhigh + lovlhigh + Mmacrhigh + Movlhigh
  Characters_with_macron_or_overline += mmacrmed + movlmed + Nmacrhigh + nmacrmed + Oslashmacr
  Characters_with_macron_or_overline += oslashmacr + OEligmacr + oeligmacr + oopenmacr + pmacr
  Characters_with_macron_or_overline += qmacr + slongovlmed + Vmacr + Vovlhigh + vmacr
  Characters_with_macron_or_overline += Wmacr + wmacr + Xovlhigh + thornovlmed
  
  
  #  (16): Characters with acute accent 
  
  AAligacute = u'\uEFE0'	#  LATIN CAPITAL LIGATURE AA WITH ACUTE 
  aaligacute = u'\uEFE1'	#  LATIN SMALL LIGATURE AA WITH ACUTE 
  AOligacute = u'\uEFE2'	#  LATIN CAPITAL LIGATURE AO WITH ACUTE 
  aoligacute = u'\uEFE3'	#  LATIN SMALL LIGATURE AO WITH ACUTE 
  AUligacute = u'\uEFE4'	#  LATIN CAPITAL LIGATURE AU WITH ACUTE 
  auligacute = u'\uEFE5'	#  LATIN SMALL LIGATURE AU WITH ACUTE 
  AVligacute = u'\uEFE6'	#  LATIN CAPITAL LIGATURE AV WITH ACUTE 
  avligacute = u'\uEFE7'	#  LATIN SMALL LIGATURE AV WITH ACUTE 
  AVligslashacute = u'\uEBB0'	#  LATIN CAPITAL LIGATURE AV WITH STROKE AND ACUTE 
  avligslashacute = u'\uEBB1'	#  LATIN SMALL LIGATURE AV WITH STROKE AND ACUTE 
  Bacute = u'\uE044'	#  LATIN CAPITAL LETTER B WITH ACUTE 
  bacute = u'\uE444'	#  LATIN SMALL LETTER B WITH ACUTE 
  Dacute = u'\uE077'	#  LATIN CAPITAL LETTER D WITH ACUTE 
  dacute = u'\uE477'	#  LATIN SMALL LETTER D WITH ACUTE 
  drotacute = u'\uEBB2'	#  LATIN SMALL LETTER D ROTUNDA WITH ACUTE 
  Facute = u'\uE0F0'	#  LATIN CAPITAL LETTER F WITH ACUTE 
  facute = u'\uE4F0'	#  LATIN SMALL LETTER F WITH ACUTE 
  Finsacute = u'\uEBB3'	#  LATIN CAPITAL LETTER INSULAR F WITH ACUTE 
  finsacute = u'\uEBB4'	#  LATIN SMALL LETTER INSULAR F WITH ACUTE 
  Hacute = u'\uE116'	#  LATIN CAPITAL LETTER H WITH ACUTE 
  hacute = u'\uE516'	#  LATIN SMALL LETTER H WITH ACUTE 
  Jacute = u'\uE153'	#  LATIN CAPITAL LETTER J WITH ACUTE 
  jacute = u'\uE553'	#  LATIN SMALL LETTER J WITH ACUTE 
  Muncacute = u'\uEBB5'	#  LATIN CAPITAL LETTER UNCIAL M WITH ACUTE 
  muncacute = u'\uEBB6'	#  LATIN SMALL LETTER UNCIAL M WITH ACUTE 
  OEligacute = u'\uE259'	#  LATIN CAPITAL LIGATURE OE WITH ACUTE 
  oeligacute = u'\uE659'	#  LATIN SMALL LIGATURE OE WITH ACUTE 
  OOligacute = u'\uEFE8'	#  LATIN CAPITAL LIGATURE OO WITH ACUTE 
  ooligacute = u'\uEFE9'	#  LATIN SMALL LIGATURE OO WITH ACUTE 
  rrotacute = u'\uEBB9'	#  LATIN SMALL LETTER R ROTUNDA WITH ACUTE 
  slongacute = u'\uEBAF'	#  LATIN SMALL LETTER LONG S WITH ACUTE 
  Tacute = u'\uE2E2'	#  LATIN CAPITAL LETTER T WITH ACUTE 
  tacute = u'\uE6E2'	#  LATIN SMALL LETTER T WITH ACUTE 
  Vacute = u'\uE33A'	#  LATIN CAPITAL LETTER V WITH ACUTE 
  vacute = u'\uE73A'	#  LATIN SMALL LETTER V WITH ACUTE 
  Vinsacute = u'\uEBBA'	#  LATIN CAPITAL LETTER INSULAR V (VEND) WITH ACUTE 
  vinsacute = u'\uEBBB'	#  LATIN SMALL LETTER INSULAR V (VEND) WITH ACUTE 
  thornacute = u'\uE737'	#  LATIN SMALL LETTER THORN WITH ACUTE 
  
  Characters_with_acute_accent = u''
  Characters_with_acute_accent += AAligacute + aaligacute + AOligacute + aoligacute + AUligacute
  Characters_with_acute_accent += auligacute + AVligacute + avligacute + AVligslashacute + avligslashacute
  Characters_with_acute_accent += Bacute + bacute + Dacute + dacute + drotacute
  Characters_with_acute_accent += Facute + facute + Finsacute + finsacute + Hacute
  Characters_with_acute_accent += hacute + Jacute + jacute + Muncacute + muncacute
  Characters_with_acute_accent += OEligacute + oeligacute + OOligacute + ooligacute + rrotacute
  Characters_with_acute_accent += slongacute + Tacute + tacute + Vacute + vacute
  Characters_with_acute_accent += Vinsacute + vinsacute + thornacute
  
  
  #  (17): Characters with double acute accent 
  
  Adblac = u'\uE025'	#  LATIN CAPITAL LETTER A WITH DOUBLE ACUTE 
  adblac = u'\uE425'	#  LATIN SMALL LETTER A WITH DOUBLE ACUTE 
  AAligdblac = u'\uEFEA'	#  LATIN CAPITAL LIGATURE AA WITH DOUBLE ACUTE 
  aaligdblac = u'\uEFEB'	#  LATIN SMALL LIGATURE AA WITH DOUBLE ACUTE 
  AEligdblac = u'\uE041'	#  LATIN CAPITAL LETTER AE WITH DOUBLE ACUTE 
  aeligdblac = u'\uE441'	#  LATIN SMALL LETTER AE WITH DOUBLE ACUTE 
  AOligdblac = u'\uEBC0'	#  LATIN CAPITAL LIGATURE AO WITH DOUBLE ACUTE 
  aoligdblac = u'\uEBC1'	#  LATIN SMALL LIGATURE AO WITH DOUBLE ACUTE 
  AVligdblac = u'\uEBC2'	#  LATIN CAPITAL LIGATURE AV WITH DOUBLE ACUTE 
  avligdblac = u'\uEBC3'	#  LATIN SMALL LIGATURE AV WITH DOUBLE ACUTE 
  Edblac = u'\uE0D1'	#  LATIN CAPITAL LETTER E WITH DOUBLE ACUTE 
  edblac = u'\uE4D1'	#  LATIN SMALL LETTER E WITH DOUBLE ACUTE 
  Idblac = u'\uE143'	#  LATIN CAPITAL LETTER I WITH DOUBLE ACUTE 
  idblac = u'\uE543'	#  LATIN SMALL LETTER I WITH DOUBLE ACUTE 
  Jdblac = u'\uE162'	#  LATIN CAPITAL LETTER J WITH DOUBLE ACUTE 
  jdblac = u'\uE562'	#  LATIN SMALL LETTER J WITH DOUBLE ACUTE 
  Oslashdblac = u'\uEBC6'	#  LATIN CAPITAL LETTER O WITH STROKE AND DOUBLE ACUTE 
  oslashdblac = u'\uEBC7'	#  LATIN SMALL LETTER O WITH STROKE AND DOUBLE ACUTE 
  OEligdblac = u'\uEBC8'	#  LATIN CAPITAL LIGATURE OE WITH DOUBLE ACUTE 
  oeligdblac = u'\uEBC9'	#  LATIN SMALL LIGATURE OE WITH DOUBLE ACUTE 
  OOligdblac = u'\uEFEC'	#  LATIN CAPITAL LIGATURE OO WITH DOUBLE ACUTE 
  ooligdblac = u'\uEFED'	#  LATIN SMALL LIGATURE OO WITH DOUBLE ACUTE 
  Vdblac = u'\uE34B'	#  LATIN CAPITAL LETTER V WITH DOUBLE ACUTE 
  vdblac = u'\uE74B'	#  LATIN SMALL LETTER V WITH DOUBLE ACUTE 
  Wdblac = u'\uE350'	#  LATIN CAPITAL LETTER W WITH DOUBLE ACUTE 
  wdblac = u'\uE750'	#  LATIN SMALL LETTER W WITH DOUBLE ACUTE 
  Ydblac = u'\uE37C'	#  LATIN CAPITAL LETTER Y WITH DOUBLE ACUTE 
  ydblac = u'\uE77C'	#  LATIN SMALL LETTER Y WITH DOUBLE ACUTE 
  YYligdblac = u'\uEBCA'	#  LATIN CAPITAL LIGATURE YY WITH DOUBLE ACUTE 
  yyligdblac = u'\uEBCB'	#  LATIN SMALL LIGATURE YY WITH DOUBLE ACUTE 
  
  Characters_with_double_acute_accent = u''
  Characters_with_double_acute_accent += Adblac + adblac + AAligdblac + aaligdblac + AEligdblac
  Characters_with_double_acute_accent += aeligdblac + AOligdblac + aoligdblac + AVligdblac + avligdblac
  Characters_with_double_acute_accent += Edblac + edblac + Idblac + idblac + Jdblac
  Characters_with_double_acute_accent += jdblac + Oslashdblac + oslashdblac + OEligdblac + oeligdblac
  Characters_with_double_acute_accent += OOligdblac + ooligdblac + Vdblac + vdblac + Wdblac
  Characters_with_double_acute_accent += wdblac + Ydblac + ydblac + YYligdblac + yyligdblac
  
  
  #  (18): Characters with dot above 
  
  AAligdot = u'\uEFEE'	#  LATIN CAPITAL LIGATURE AA WITH DOT ABOVE 
  aaligdot = u'\uEFEF'	#  LATIN SMALL LIGATURE AA WITH DOT ABOVE 
  AEligdot = u'\uE043'	#  LATIN CAPITAL LETTER AE WITH DOT ABOVE 
  aeligdot = u'\uE443'	#  LATIN SMALL LETTER AE WITH DOT ABOVE 
  AYligdot = u'\uEFF0'	#  LATIN CAPITAL LIGATURE AY WITH DOT ABOVE 
  ayligdot = u'\uEFF1'	#  LATIN SMALL LIGATURE AY WITH DOT ABOVE 
  bscapdot = u'\uEBD0'	#  LATIN LETTER SMALL CAPITAL B WITH DOT ABOVE 
  drotdot = u'\uEBD1'	#  LATIN SMALL LETTER D ROTUNDA WITH DOT ABOVE 
  dscapdot = u'\uEBD2'	#  LATIN LETTER SMALL CAPITAL D WITH DOT ABOVE 
  Finsdot = u'\uEBD3'	#  LATIN CAPITAL LETTER INSULAR F WITH DOT ABOVE 
  finsdot = u'\uEBD4'	#  LATIN SMALL LETTER INSULAR F WITH DOT ABOVE 
  finssemiclosedot = u'\uEBD5'	#  LATIN SMALL LETTER SEMI-CLOSED INSULAR F WITH DOT ABOVE 
  finsclosedot = u'\uEBD6'	#  LATIN SMALL LETTER CLOSED INSULAR F WITH DOT ABOVE 
  fscapdot = u'\uEBD7'	#  LATIN LETTER SMALL CAPITAL F WITH DOT ABOVE 
  gscapdot = u'\uEF20'	#  LATIN LETTER SMALL CAPITAL G WITH DOT ABOVE 
  hscapdot = u'\uEBDA'	#  LATIN LETTER SMALL CAPITAL H WITH DOT ABOVE 
  Jdot = u'\uE15C'	#  LATIN CAPITAL LETTER J WITH DOT ABOVE 
  Kdot = u'\uE168'	#  LATIN CAPITAL LETTER K WITH DOT ABOVE 
  kdot = u'\uE568'	#  LATIN SMALL LETTER K WITH DOT ABOVE 
  kscapdot = u'\uEBDB'	#  LATIN LETTER SMALL CAPITAL K WITH DOT ABOVE 
  Ldot = u'\uE19E'	#  LATIN CAPITAL LETTER L WITH DOT ABOVE 
  ldot = u'\uE59E'	#  LATIN SMALL LETTER L WITH DOT ABOVE 
  lscapdot = u'\uEBDC'	#  LATIN LETTER SMALL CAPITAL L WITH DOT ABOVE 
  mscapdot = u'\uEBDD'	#  LATIN LETTER SMALL CAPITAL M WITH DOT ABOVE 
  nscapdot = u'\uEF21'	#  LATIN LETTER SMALL CAPITAL N WITH DOT ABOVE 
  Oslashdot = u'\uEBCD'	#  LATIN CAPITAL LETTER O WITH STROKE AND DOT ABOVE 
  oslashdot = u'\uEBCE'	#  LATIN SMALL LETTER O WITH STROKE AND DOT ABOVE 
  pscapdot = u'\uEBCF'	#  LATIN LETTER SMALL CAPITAL P WITH DOT ABOVE 
  Qdot = u'\uE282'	#  LATIN CAPITAL LETTER Q WITH DOT ABOVE 
  qdot = u'\uE682'	#  LATIN SMALL LETTER Q WITH DOT ABOVE 
  rscapdot = u'\uEF22'	#  LATIN LETTER SMALL CAPITAL R WITH DOT ABOVE 
  sscapdot = u'\uEF23'	#  LATIN LETTER SMALL CAPITAL S WITH DOT ABOVE 
  tscapdot = u'\uEF24'	#  LATIN LETTER SMALL CAPITAL T WITH DOT ABOVE 
  Udot = u'\uE315'	#  LATIN CAPITAL LETTER U WITH DOT ABOVE 
  udot = u'\uE715'	#  LATIN SMALL LETTER U WITH DOT ABOVE 
  Vdot = u'\uE34C'	#  LATIN CAPITAL LETTER V WITH DOT ABOVE 
  vdot = u'\uE74C'	#  LATIN SMALL LETTER V WITH DOT ABOVE 
  Vinsdot = u'\uE3E7'	#  LATIN CAPITAL LETTER INSULAR V 
  vinsdot = u'\uE7E7'	#  LATIN SMALL LETTER INSULAR V 
  
  Characters_with_dot_above = u''
  Characters_with_dot_above += AAligdot + aaligdot + AEligdot + aeligdot + AYligdot
  Characters_with_dot_above += ayligdot + bscapdot + drotdot + dscapdot + Finsdot
  Characters_with_dot_above += finsdot + finssemiclosedot + finsclosedot + fscapdot + gscapdot
  Characters_with_dot_above += hscapdot + Jdot + Kdot + kdot + kscapdot
  Characters_with_dot_above += Ldot + ldot + lscapdot + mscapdot + nscapdot
  Characters_with_dot_above += Oslashdot + oslashdot + pscapdot + Qdot + qdot
  Characters_with_dot_above += rscapdot + sscapdot + tscapdot + Udot + udot
  Characters_with_dot_above += Vdot + vdot + Vinsdot + vinsdot
  
  
  #  (19): Characters with dot below 
  
  AAligdotbl = u'\uEFF2'	#  LATIN CAPITAL LIGATURE AA WITH DOT BELOW 
  aaligdotbl = u'\uEFF3'	#  LATIN SMALL LIGATURE AA WITH DOT BELOW 
  AEligdotbl = u'\uE036'	#  LATIN CAPITAL LETTER AE WITH DOT BELOW 
  aeligdotbl = u'\uE436'	#  LATIN SMALL LETTER AE WITH DOT BELOW 
  AOligdotbl = u'\uEFF4'	#  LATIN CAPITAL LIGATURE AO WITH DOT BELOW 
  aoligdotbl = u'\uEFF5'	#  LATIN SMALL LIGATURE AO WITH DOT BELOW
  AUligdotbl = u'\uEFF6'	#  LATIN CAPITAL LIGATURE AU WITH DOT BELOW 
  auligdotbl = u'\uEFF7'	#  LATIN SMALL LIGATURE AU WITH DOT BELOW 
  AVligdotbl = u'\uEFF8'	#  LATIN CAPITAL LIGATURE AV WITH DOT BELOW 
  avligdotbl = u'\uEFF9'	#  LATIN SMALL LIGATURE AV WITH DOT BELOW 
  AYligdotbl = u'\uEFFA'	#  LATIN CAPITAL LIGATURE AY WITH DOT BELOW 
  ayligdotbl = u'\uEFFB'	#  LATIN SMALL LIGATURE AY WITH DOT BELOW 
  bscapdotbl = u'\uEF25'	#  LATIN LETTER SMALL CAPITAL B WITH DOT BELOW 
  Cdotbl = u'\uE066'	#  LATIN CAPITAL LETTER C WITH DOT BELOW 
  cdotbl = u'\uE466'	#  LATIN SMALL LETTER C WITH DOT BELOW 
  dscapdotbl = u'\uEF26'	#  LATIN LETTER SMALL CAPITAL D WITH DOT BELOW 
  ETHdotbl = u'\uE08F'	#  LATIN CAPITAL LETTER ETH WITH DOT BELOW 
  ethdotbl = u'\uE48F'	#  LATIN SMALL LETTER ETH WITH DOT BELOW 
  Fdotbl = u'\uE0EE'	#  LATIN CAPITAL LETTER F WITH DOT BELOW 
  fdotbl = u'\uE4EE'	#  LATIN SMALL LETTER F WITH DOT BELOW 
  Finsdotbl = u'\uE3E5'	#  LATIN CAPITAL LETTER INSULAR F WITH DOT BELOW 
  finsdotbl = u'\uE7E5'	#  LATIN SMALL LETTER INSULAR F WITH DOT BELOW 
  Gdotbl = u'\uE101'	#  LATIN CAPITAL LETTER G WITH DOT BELOW 
  gdotbl = u'\uE501'	#  LATIN SMALL LETTER G WITH DOT BELOW 
  gscapdotbl = u'\uEF27'	#  LATIN LETTER SMALL CAPITAL G WITH DOT BELOW 
  Jdotbl = u'\uE151'	#  LATIN CAPITAL LETTER J WITH DOT BELOW 
  jdotbl = u'\uE551'	#  LATIN SMALL LETTER J WITH DOT BELOW 
  lscapdotbl = u'\uEF28'	#  LATIN LETTER SMALL CAPITAL L WITH DOT BELOW 
  mscapdotbl = u'\uEF29'	#  LATIN LETTER SMALL CAPITAL M WITH DOT BELOW 
  nscapdotbl = u'\uEF2A'	#  LATIN LETTER SMALL CAPITAL N WITH DOT BELOW 
  Oslashdotbl = u'\uEBE0'	#  LATIN CAPITAL LETTER O WITH STROKE AND DOT BELOW 
  oslashdotbl = u'\uEBE1'	#  LATIN SMALL LETTER O WITH STROKE AND DOT BELOW 
  OOligdotbl = u'\uEFFC'	#  LATIN CAPITAL LIGATURE OO WITH DOT BELOW 
  ooligdotbl = u'\uEFFD'	#  LATIN SMALL LIGATURE OO WITH DOT BELOW 
  Pdotbl = u'\uE26D'	#  LATIN CAPITAL LETTER P WITH DOT BELOW 
  pdotbl = u'\uE66D'	#  LATIN SMALL LETTER P WITH DOT BELOW 
  Qdotbl = u'\uE288'	#  LATIN CAPITAL LETTER Q WITH DOT BELOW 
  qdotbl = u'\uE688'	#  LATIN SMALL LETTER Q WITH DOT BELOW 
  rscapdotbl = u'\uEF2B'	#  LATIN LETTER SMALL CAPITAL R WITH DOT BELOW 
  rrotdotbl = u'\uE7C1'	#  LATIN SMALL LETTER R ROTUNDA WITH DOT BELOW 
  sscapdotbl = u'\uEF2C'	#  LATIN LETTER SMALL CAPITAL S WITH DOT BELOW 
  slongdotbl = u'\uE7C2'	#  LATIN SMALL LETTER LONG S WITH DOT BELOW 
  tscapdotbl = u'\uEF2D'	#  LATIN LETTER SMALL CAPITAL T WITH DOT BELOW 
  Vinsdotbl = u'\uE3E6'	#  LATIN CAPITAL LETTER INSULAR V (VEND) WITH DOT BELOW 
  vinsdotbl = u'\uE7E6'	#  LATIN SMALL LETTER INSULAR V (VEND) WITH DOT BELOW 
  THORNdotbl = u'\uE39F'	#  LATIN CAPITAL LETTER THORN WITH DOT BELOW 
  thorndotbl = u'\uE79F'	#  LATIN SMALL LETTER THORN WITH DOT BELOW 
  
  Characters_with_dot_below = u''
  Characters_with_dot_below += AAligdotbl + aaligdotbl + AEligdotbl + aeligdotbl + AOligdotbl
  Characters_with_dot_below += aoligdotbl + AUligdotbl + auligdotbl + AVligdotbl + avligdotbl
  Characters_with_dot_below += AYligdotbl + ayligdotbl + bscapdotbl + Cdotbl + cdotbl
  Characters_with_dot_below += dscapdotbl + ETHdotbl + ethdotbl + Fdotbl + fdotbl
  Characters_with_dot_below += Finsdotbl + finsdotbl + Gdotbl + gdotbl + gscapdotbl
  Characters_with_dot_below += Jdotbl + jdotbl + lscapdotbl + mscapdotbl + nscapdotbl
  Characters_with_dot_below += Oslashdotbl + oslashdotbl + OOligdotbl + ooligdotbl + Pdotbl
  Characters_with_dot_below += pdotbl + Qdotbl + qdotbl + rscapdotbl + rrotdotbl
  Characters_with_dot_below += sscapdotbl + slongdotbl + tscapdotbl + Vinsdotbl + vinsdotbl
  Characters_with_dot_below += THORNdotbl + thorndotbl
  
  
  #  (20): Characters with diaeresis 
  
  AAliguml = u'\uEFFE'	#  LATIN CAPITAL LIGATURE AA WITH DIAERESIS 
  aaliguml = u'\uEFFF'	#  LATIN SMALL LIGATURE AA WITH DIAERESIS 
  AEliguml = u'\uE042'	#  LATIN CAPITAL LETTER AE WITH DIAERESIS 
  aeliguml = u'\uE442'	#  LATIN SMALL LETTER AE WITH DIAERESIS 
  Juml = u'\uEBE2'	#  LATIN CAPITAL LETTER J WITH DIAERESIS 
  juml = u'\uEBE3'	#  LATIN SMALL LETTER J WITH DIAERESIS 
  OOliguml = u'\uEBE4'	#  LATIN CAPITAL LIGATURE OO WITH DIAERESIS 
  ooliguml = u'\uEBE5'	#  LATIN SMALL LIGATURE OO WITH DIAERESIS 
  PPliguml = u'\uEBE6'	#  LATIN CAPITAL LIGATURE PP WITH DIAERESIS 
  ppliguml = u'\uEBE7'	#  LATIN SMALL LIGATURE PP WITH DIAERESIS 
  Vuml = u'\uE342'	#  LATIN CAPITAL LETTER V WITH DIAERESIS 
  vuml = u'\uE742'	#  LATIN SMALL LETTER V WITH DIAERESIS 
  YYliguml = u'\uEBE8'	#  LATIN CAPITAL LIGATURE YY WITH DIAERESIS 
  yyliguml = u'\uEBE9'	#  LATIN SMALL LIGATURE YY WITH DIAERESIS 
  adiaguml = u'\uE8D5'	#  LATIN SMALL LETTER A WITH DIAGONAL DIAERESIS 
  odiaguml = u'\uE8D7'	#  LATIN SMALL LETTER O WITH DIAGONAL DIAERESIS 
  
  Characters_with_diaeresis = u''
  Characters_with_diaeresis += AAliguml + aaliguml + AEliguml + aeliguml + Juml
  Characters_with_diaeresis += juml + OOliguml + ooliguml + PPliguml + ppliguml
  Characters_with_diaeresis += Vuml + vuml + YYliguml + yyliguml + adiaguml
  Characters_with_diaeresis += odiaguml
  
  
  #  (21): Characters with curl above (reversed ogonek) 
  
  Acurl = u'\uE033'	#  LATIN CAPITAL LETTER A WITH CURL 
  acurl = u'\uE433'	#  LATIN SMALL LETTER A WITH CURL 
  AEligcurl = u'\uEBEA'	#  LATIN CAPITAL LETTER AE WITH CURL 
  aeligcurl = u'\uEBEB'	#  LATIN SMALL LETTER AE WITH CURL 
  Ecurl = u'\uE0E9'	#  LATIN CAPITAL LETTER E WITH CURL 
  ecurl = u'\uE4E9'	#  LATIN SMALL LETTER E WITH CURL 
  Icurl = u'\uE12A'	#  LATIN CAPITAL LETTER I WITH CURL 
  icurl = u'\uE52A'	#  LATIN SMALL LETTER I WITH CURL 
  Jcurl = u'\uE163'	#  LATIN CAPITAL LETTER J WITH CURL 
  jcurl = u'\uE563'	#  LATIN SMALL LETTER J WITH CURL 
  Ocurl = u'\uE3D3'	#  LATIN CAPITAL LETTER O WITH CURL 
  ocurl = u'\uE7D3'	#  LATIN SMALL LETTER O WITH CURL 
  Oslashcurl = u'\uE3D4'	#  LATIN CAPITAL LETTER O WITH STROKE AND CURL 
  oslashcurl = u'\uE7D4'	#  LATIN SMALL LETTER O WITH STROKE AND CURL 
  Ucurl = u'\uE331'	#  LATIN CAPITAL LETTER U WITH CURL 
  ucurl = u'\uE731'	#  LATIN SMALL LETTER U WITH CURL 
  Ycurl = u'\uE385'	#  LATIN CAPITAL LETTER Y WITH CURL 
  ycurl = u'\uE785'	#  LATIN SMALL LETTER Y WITH CURL 
  
  Characters_with_curl_above_reversed_ogonek = u''
  Characters_with_curl_above_reversed_ogonek += Acurl + acurl + AEligcurl + aeligcurl + Ecurl
  Characters_with_curl_above_reversed_ogonek += ecurl + Icurl + icurl + Jcurl + jcurl
  Characters_with_curl_above_reversed_ogonek += Ocurl + ocurl + Oslashcurl + oslashcurl + Ucurl
  Characters_with_curl_above_reversed_ogonek += ucurl + Ycurl + ycurl
  
  
  #  (22): Characters with ogonek 
  
  AEligogon = u'\uE040'	#  LATIN CAPITAL LETTER AE WITH OGONEK 
  aeligogon = u'\uE440'	#  LATIN SMALL LETTER AE WITH OGONEK 
  AVligogon = u'\uEBF0'	#  LATIN CAPITAL LETTER AV WITH OGONEK 
  avligogon = u'\uEBF1'	#  LATIN SMALL LETTER AV WITH OGONEK 
  Cogon = u'\uE076'	#  LATIN SMALL LETTER C WITH OGONEK 
  cogon = u'\uE476'	#  LATIN SMALL LETTER C WITH OGONEK 
  Oslashogon = u'\uE255'	#  LATIN CAPITAL LETTER O WITH STROKE AND OGONEK 
  oslashogon = u'\uE655'	#  LATIN SMALL LETTER O WITH STROKE AND OGONEK 
  Togon = u'\uE2EE'	#  LATIN CAPITAL LETTER T WITH OGONEK 
  togon = u'\uE6EE'	#  LATIN SMALL LETTER T WITH OGONEK 
  
  Characters_with_ogonek = u''
  Characters_with_ogonek += AEligogon + aeligogon + AVligogon + avligogon + Cogon
  Characters_with_ogonek += cogon + Oslashogon + oslashogon + Togon + togon
  
  
  #  (23): Characters with breve 
  
  AEligbreve = u'\uE03F'	#  LATIN CAPITAL LETTER AE WITH BREVE 
  aeligbreve = u'\uE43F'	#  LATIN SMALL LETTER AE WITH BREVE 
  Oslashbreve = u'\uEBEE'	#  LATIN CAPITAL LETTER O WITH STROKE AND BREVE 
  oslashbreve = u'\uEBEF'	#  LATIN SMALL LETTER O WITH STROKE AND BREVE 
  Ybreve = u'\uE376'	#  LATIN CAPITAL LETTER Y WITH BREVE 
  ybreve = u'\uE776'	#  LATIN SMALL LETTER Y WITH BREVE 
  
  Characters_with_breve = u''
  Characters_with_breve += AEligbreve + aeligbreve + Oslashbreve + oslashbreve + Ybreve
  Characters_with_breve += ybreve
  
  
  #  (24): Characters with breve below 
  
  ibrevinvbl = u'\uE548'	#  LATIN SMALL LETTER I WITH INVERTED BREVE BELOW 
  ubrevinvbl = u'\uE727'	#  LATIN SMALL LETTER U WITH INVERTED BREVE BELOW 
  
  Characters_with_breve_below = u''
  Characters_with_breve_below += ibrevinvbl + ubrevinvbl
  
  
  #  (25): Characters with circumflex 
  
  ncirc = u'\uE5D7'	#  LATIN SMALL LETTER N WITH CIRCUMFLEX 
  Vcirc = u'\uE33B'	#  LATIN CAPITAL LETTER V WITH CIRCUMFLEX 
  vcirc = u'\uE73B'	#  LATIN SMALL LETTER V WITH CIRCUMFLEX 
  eacombcirc = u'\uEBBD'	#  LATIN SMALL LETTER EA WITH CIRCUMFLEX 
  eucombcirc = u'\uEBBE'	#  LATIN SMALL LETTER EU WITH CIRCUMFLEX 
  
  Characters_with_circumflex = u''
  Characters_with_circumflex += ncirc + Vcirc + vcirc + eacombcirc + eucombcirc
  
  
  #  (26): Characters with ring above 
  
  aeligring = u'\uE8D1'	#  LATIN SMALL LETTER AE WITH RING ABOVE 
  ering = u'\uE4CF'	#  LATIN SMALL LETTER E WITH RING ABOVE 
  oring = u'\uE637'	#  LATIN SMALL LETTER O WITH RING ABOVE 
  vring = u'\uE743'	#  LATIN SMALL LETTER V WITH RING ABOVE 
  
  Characters_with_ring_above = u''
  Characters_with_ring_above += aeligring + ering + oring + vring
  
  
  #  (27): Characters with ring below 
  
  lringbl = u'\uE5A4'	#  LATIN SMALL LETTER L WITH RING BELOW 
  mringbl = u'\uE5C5'	#  LATIN SMALL LETTER M WITH RING BELOW 
  nringbl = u'\uE5EE'	#  LATIN SMALL LETTER N WITH RING BELOW 
  rringbl = u'\uE6A3'	#  LATIN SMALL LETTER R WITH RING BELOW 
  
  Characters_with_ring_below = u''
  Characters_with_ring_below += lringbl + mringbl + nringbl + rringbl
  
  
  #  (28): Characters with tilde 
  
  qbardestilde = u'\uE68B'	#  LATIN SMALL LETTER Q WITH STROKE THROUGH DESCENDER AND TILDE 
  
  Characters_with_tilde = u''
  Characters_with_tilde += qbardestilde
  
  
  #  (29): Characters with curly bar above 
  
  ucurlbar = u'\uEBBF'	#  LATIN SMALL LETTER U WITH CURLY BAR ABOVE 
  
  Characters_with_curly_bar_above = u''
  Characters_with_curly_bar_above += ucurlbar
  
  
  #  (30): Characters with vertical bar above 
  
  Uvertline = u'\uE324'	#  LATIN CAPITAL LETTER U WITH VERTICAL LINE ABOVE 
  uvertline = u'\uE724'	#  LATIN SMALL LETTER U WITH VERTICAL LINE ABOVE 
  
  Characters_with_vertical_bar_above = u''
  Characters_with_vertical_bar_above += Uvertline + uvertline
  
  
  #  (31): Characters with superscript letters 
  
  Aesup = u'\uE02C'	#  LATIN CAPITAL LETTER A WITH LATIN SMALL LETTER E ABOVE 
  aesup = u'\uE42C'	#  LATIN SMALL LETTER A WITH LATIN SMALL LETTER E ABOVE 
  aisup = u'\uE8E0'	#  LATIN SMALL LETTER A WITH LATIN SMALL LETTER I ABOVE 
  aosup = u'\uE42D'	#  LATIN SMALL LETTER A WITH LATIN SMALL LETTER O ABOVE 
  ausup = u'\uE8E1'	#  LATIN SMALL LETTER A WITH LATIN SMALL LETTER U ABOVE 
  avsup = u'\uE42E'	#  LATIN SMALL LETTER A WITH LATIN SMALL LETTER V ABOVE 
  Easup = u'\uE0E1'	#  LATIN CAPITAL LETTER E WITH LATIN SMALL LETTER A ABOVE 
  easup = u'\uE4E1'	#  LATIN SMALL LETTER E WITH LATIN SMALL LETTER A ABOVE 
  eesup = u'\uE8E2'	#  LATIN SMALL LETTER E WITH LATIN SMALL LETTER E ABOVE 
  eisup = u'\uE4E2'	#  LATIN SMALL LETTER E WITH LATIN SMALL LETTER I ABOVE 
  eosup = u'\uE8E3'	#  LATIN SMALL LETTER E WITH LATIN SMALL LETTER O ABOVE 
  evsup = u'\uE8E2'	#  LATIN SMALL LETTER E WITH LATIN SMALL LETTER V ABOVE 
  iasup = u'\uE8E4'	#  LATIN SMALL LETTER I WITH LATIN SMALL LETTER A ABOVE 
  iesup = u'\uE54A'	#  LATIN SMALL LETTER I WITH LATIN SMALL LETTER E ABOVE 
  iosup = u'\uE8E5'	#  LATIN SMALL LETTER I WITH LATIN SMALL LETTER O ABOVE 
  iusup = u'\uE8E6'	#  LATIN SMALL LETTER I WITH LATIN SMALL LETTER U ABOVE 
  ivsup = u'\uE54B'	#  LATIN SMALL LETTER I WITH LATIN SMALL LETTER V ABOVE 
  jesup = u'\uE8E7'	#  LATIN SMALL LETTER J WITH LATIN SMALL LETTER E ABOVE 
  mesup = u'\uE8E8'	#  LATIN SMALL LETTER M WITH LATIN SMALL LETTER A ABOVE 
  oasup = u'\uE643'	#  LATIN SMALL LETTER O WITH LATIN SMALL LETTER A ABOVE 
  Oesup = u'\uE244'	#  LATIN CAPITAL LETTER O WITH LATIN SMALL LETTER E ABOVE 
  oesup = u'\uE644'	#  LATIN SMALL LETTER O WITH LATIN SMALL LETTER E ABOVE 
  oisup = u'\uE645'	#  LATIN SMALL LETTER O WITH LATIN SMALL LETTER I ABOVE 
  oosup = u'\uE8E9'	#  LATIN SMALL LETTER O WITH LATIN SMALL LETTER O ABOVE 
  Ousup = u'\uE246'	#  LATIN CAPITAL LETTER O WITH LATIN SMALL LETTER U ABOVE 
  ousup = u'\uE646'	#  LATIN SMALL LETTER O WITH LATIN SMALL LETTER U ABOVE 
  ovsup = u'\uE647'	#  LATIN SMALL LETTER O WITH LATIN SMALL LETTER U ABOVE 
  resup = u'\uE8EA'	#  LATIN SMALL LETTER R WITH LATIN SMALL LETTER E ABOVE 
  uasup = u'\uE8EB'	#  LATIN SMALL LETTER U WITH LATIN SMALL LETTER A ABOVE 
  Uesup = u'\uE32B'	#  LATIN CAPITAL LETTER U WITH LATIN SMALL LETTER E ABOVE 
  uesup = u'\uE72B'	#  LATIN SMALL LETTER U WITH LATIN SMALL LETTER E ABOVE 
  uisup = u'\uE72C'	#  LATIN SMALL LETTER U WITH LATIN SMALL LETTER I ABOVE 
  Uosup = u'\uE32D'	#  LATIN CAPITAL LETTER U WITH LATIN SMALL LETTER O ABOVE 
  uosup = u'\uE72D'	#  LATIN SMALL LETTER U WITH LATIN SMALL LETTER o ABOVE 
  uvsup = u'\uE8EC'	#  LATIN SMALL LETTER U WITH LATIN SMALL LETTER V ABOVE 
  uwsup = u'\uE8ED'	#  LATIN SMALL LETTER U WITH LATIN SMALL LETTER W ABOVE 
  yesup = u'\uE781'	#  LATIN SMALL LETTER Y WITH LATIN SMALL LETTER E ABOVE 
  wasup = u'\uE8F0'	#  LATIN SMALL LETTER W WITH LATIN SMALL LETTER A ABOVE 
  Wesup = u'\uE353'	#  LATIN CAPITAL LETTER W WITH LATIN SMALL LETTER E ABOVE 
  wesup = u'\uE753'	#  LATIN SMALL LETTER W WITH LATIN SMALL LETTER E ABOVE 
  wisup = u'\uE8F1'	#  LATIN SMALL LETTER W WITH LATIN SMALL LETTER I ABOVE 
  wosup = u'\uE754'	#  LATIN SMALL LETTER W WITH LATIN SMALL LETTER O ABOVE 
  wusup = u'\uE8F2'	#  LATIN SMALL LETTER W WITH LATIN SMALL LETTER U ABOVE 
  wvsup = u'\uE8F3'	#  LATIN SMALL LETTER W WITH LATIN SMALL LETTER V ABOVE 
  
  Characters_with_superscript_letters = u''
  Characters_with_superscript_letters += Aesup + aesup + aisup + aosup + ausup
  Characters_with_superscript_letters += avsup + Easup + easup + eesup + eisup
  Characters_with_superscript_letters += eosup + evsup + iasup + iesup + iosup
  Characters_with_superscript_letters += iusup + ivsup + jesup + mesup + oasup
  Characters_with_superscript_letters += Oesup + oesup + oisup + oosup + Ousup
  Characters_with_superscript_letters += ousup + ovsup + resup + uasup + Uesup
  Characters_with_superscript_letters += uesup + uisup + Uosup + uosup + uvsup
  Characters_with_superscript_letters += uwsup + yesup + wasup + Wesup + wesup
  Characters_with_superscript_letters += wisup + wosup + wusup + wvsup
  
  
  #  (32): Characters with acute accent and dot above 
  
  Adotacute = u'\uEBF4'	#  LATIN CAPITAL LETTER A WITH DOT ABOVE AND ACUTE 
  adotacute = u'\uEBF5'	#  LATIN SMALL LETTER A WITH DOT ABOVE AND ACUTE 
  Edotacute = u'\uE0C8'	#  LATIN CAPITAL LETTER E WITH DOT ABOVE AND ACUTE 
  edotacute = u'\uE4C8'	#  LATIN SMALL LETTER E WITH DOT ABOVE AND ACUTE 
  Idotacute = u'\uEBF6'	#  LATIN CAPITAL LETTER I WITH DOT ABOVE AND ACUTE 
  idotacute = u'\uEBF7'	#  LATIN SMALL LETTER I WITH DOT ABOVE AND ACUTE 
  Odotacute = u'\uEBF8'	#  LATIN CAPITAL LETTER O WITH DOT ABOVE AND ACUTE 
  odotacute = u'\uEBF9'	#  LATIN SMALL LETTER O WITH DOT ABOVE AND ACUTE 
  Oslashdotacute = u'\uEBFC'	#  LATIN CAPITAL LETTER O WITH STROKE AND DOT ABOVE AND ACUTE 
  oslashdotacute = u'\uEBFD'	#  LATIN SMALL LETTER O WITH STROKE AND DOT ABOVE AND ACUTE 
  Udotacute = u'\uEBFE'	#  LATIN CAPITAL LETTER U WITH DOT ABOVE AND ACUTE 
  udotacute = u'\uEBFF'	#  LATIN SMALL LETTER U WITH DOT ABOVE AND ACUTE 
  Ydotacute = u'\uE384'	#  LATIN CAPITAL LETTER Y WITH DOT ABOVE AND ACUTE 
  ydotacute = u'\uE784'	#  LATIN SMALL LETTER Y WITH DOT ABOVE AND ACUTE 
  
  Characters_with_acute_accent_and_dot_above = u''
  Characters_with_acute_accent_and_dot_above += Adotacute + adotacute + Edotacute + edotacute + Idotacute
  Characters_with_acute_accent_and_dot_above += idotacute + Odotacute + odotacute + Oslashdotacute + oslashdotacute
  Characters_with_acute_accent_and_dot_above += Udotacute + udotacute + Ydotacute + ydotacute
  
  
  #  (33): Characters with acute accent and dot below 
  
  edotblacute = u'\uE498'	#  LATIN SMALL LETTER E WITH DOT BELOW AND ACUTE 
  
  Characters_with_acute_accent_and_dot_below = u''
  Characters_with_acute_accent_and_dot_below += edotblacute
  
  
  #  (34): Characters with acute accent and diaeresis 
  
  oumlacute = u'\uE62C'	#  LATIN SMALL LETTER O WITH DIAERESIS AND ACUTE 
  
  Characters_with_acute_accent_and_diaeresis = u''
  Characters_with_acute_accent_and_diaeresis += oumlacute
  
  
  #  (35): Characters with acute accent and curl above (reversed ogonek) 
  
  Ocurlacute = u'\uEBB7'	#  LATIN CAPITAL LETTER O WITH CURL AND ACUTE 
  ocurlacute = u'\uEBB8'	#  LATIN SMALL LETTER O WITH CURL AND ACUTE 
  
  Characters_with_acute_accent_and_curl_above_reversed_ogonek = u''
  Characters_with_acute_accent_and_curl_above_reversed_ogonek += Ocurlacute + ocurlacute
  
  
  #  (36): Characters with acute accent and ogonek 
  
  Aogonacute = u'\uE004'	#  LATIN CAPITAL LETTER A WITH OGONEK AND ACUTE 
  aogonacute = u'\uE404'	#  LATIN SMALL LETTER A WITH OGONEK AND ACUTE 
  aeligogonacute = u'\uE8D3'	#  LATIN SMALL LETTER AE WITH OGONEK AND ACUTE 
  Eogonacute = u'\uE099'	#  LATIN CAPITAL LETTER E WITH OGONEK AND ACUTE 
  eogonacute = u'\uE499'	#  LATIN SMALL LETTER E WITH OGONEK AND ACUTE 
  Oogonacute = u'\uE20C'	#  LATIN CAPITAL LETTER O WITH OGONEK AND ACUTE 
  oogonacute = u'\uE60C'	#  LATIN SMALL LETTER O WITH OGONEK AND ACUTE 
  Oslashogonacute = u'\uE257'	#  LATIN CAPITAL LETTER O WITH STROKE AND OGONEK AND ACUTE 
  oslashogonacute = u'\uE657'	#  LATIN SMALL LETTER O WITH STROKE AND OGONEK AND ACUTE 
  
  Characters_with_acute_accent_and_ogonek = u''
  Characters_with_acute_accent_and_ogonek += Aogonacute + aogonacute + aeligogonacute + Eogonacute + eogonacute
  Characters_with_acute_accent_and_ogonek += Oogonacute + oogonacute + Oslashogonacute + oslashogonacute
  
  
  #  (37): Characters with double acute accent and ogonek 
  
  Eogondblac = u'\uE0EA'	#  LATIN CAPITAL LETTER E WITH OGONEK AND DOUBLE ACUTE 
  eogondblac = u'\uE4EA'	#  LATIN SMALL LETTER E WITH OGONEK AND DOUBLE ACUTE 
  Oogondblac = u'\uEBC4'	#  LATIN CAPITAL LETTER O WITH OGONEK AND DOUBLE ACUTE 
  oogondblac = u'\uEBC5'	#  LATIN SMALL LETTER O WITH OGONEK AND DOUBLE ACUTE 
  
  Characters_with_double_acute_accent_and_ogonek = u''
  Characters_with_double_acute_accent_and_ogonek += Eogondblac + eogondblac + Oogondblac + oogondblac
  
  
  #  (38): Characters with dot above and ogonek 
  
  Eogondot = u'\uE0EB'	#  LATIN CAPITAL LETTER E WITH OGONEK AND DOT ABOVE 
  eogondot = u'\uE4EB'	#  LATIN SMALL LETTER E WITH OGONEK AND DOT ABOVE 
  Oogondot = u'\uEBDE'	#  LATIN CAPITAL LETTER O WITH OGONEK AND DOT ABOVE 
  oogondot = u'\uEBDF'	#  LATIN SMALL LETTER O WITH OGONEK AND DOT ABOVE 
  
  Characters_with_dot_above_and_ogonek = u''
  Characters_with_dot_above_and_ogonek += Eogondot + eogondot + Oogondot + oogondot
  
  
  #  (39): Characters with dot below and ogonek 
  
  Eogondotbl = u'\uE0E8'	#  LATIN CAPITAL LETTER E WITH OGONEK AND DOT BELOW 
  eogondotbl = u'\uE4E8'	#  LATIN SMALL LETTER E WITH OGONEK AND DOT BELOW 
  Oogondotbl = u'\uE208'	#  LATIN CAPITAL LETTER O WITH OGONEK AND DOT BELOW 
  oogondotbl = u'\uE608'	#  LATIN SMALL LETTER O WITH OGONEK AND DOT BELOW 
  
  Characters_with_dot_below_and_ogonek = u''
  Characters_with_dot_below_and_ogonek += Eogondotbl + eogondotbl + Oogondotbl + oogondotbl
  
  
  #  (40): Characters with diaeresis and macron 
  
  eumlmacr = u'\uE4CD'	#  LATIN SMALL LETTER E WITH DIAERESIS AND MACRON 
  
  Characters_with_diaeresis_and_macron = u''
  Characters_with_diaeresis_and_macron += eumlmacr
  
  
  #  (41): Characters with diaeresis and circumflex 
  
  aumlcirc = u'\uE41A'	#  LATIN SMALL LETTER E WITH DIAERESIS AND CIRCUMFLEX 
  Oumlcirc = u'\uE22D'	#  LATIN CAPITAL LETTER O WITH DIAERESIS AND CIRCUMFLEX 
  oumlcirc = u'\uE62D'	#  LATIN SMALL LETTER O WITH DIAERESIS AND CIRCUMFLEX 
  Uumlcirc = u'\uE317'	#  LATIN CAPITAL LETTER U WITH DIAERESIS AND CIRCUMFLEX 
  uumlcirc = u'\uE717'	#  LATIN SMALL LETTER U WITH DIAERESIS AND CIRCUMFLEX 
  
  Characters_with_diaeresis_and_circumflex = u''
  Characters_with_diaeresis_and_circumflex += aumlcirc + Oumlcirc + oumlcirc + Uumlcirc + uumlcirc
  
  
  #  (42): Characters with diaeresis and dot below 
  
  adotbluml = u'\uE41D'	#  LATIN SMALL LETTER A WITH DIAERESIS AND DOT BELOW 
  
  Characters_with_diaeresis_and_dot_below = u''
  Characters_with_diaeresis_and_dot_below += adotbluml
  
  
  #  (43): Characters with ogonek and curl above (reversed ogonek) 
  
  Eogoncurl = u'\uEBF2'	#  LATIN CAPITAL LETTER E WITH OGONEK AND CURL 
  eogoncurl = u'\uEBF3'	#  LATIN SMALL LETTER E WITH OGONEK AND CURL 
  Oogoncurl = u'\uE24F'	#  LATIN CAPITAL LETTER O WITH OGONEK AND CURL 
  oogoncurl = u'\uE64F'	#  LATIN SMALL LETTER O WITH OGONEK AND CURL 
  
  Characters_with_ogonek_and_curl_above_reversed_ogonek = u''
  Characters_with_ogonek_and_curl_above_reversed_ogonek += Eogoncurl + eogoncurl + Oogoncurl + oogoncurl
  
  
  #  (44): Characters with ogonek and circumflex 
  
  eogoncirc = u'\uE49F'	#  LATIN SMALL LETTER E WITH OGONEK AND CIRCUMFLEX 
  oogoncirc = u'\uE60E'	#  LATIN SMALL LETTER O WITH OGONEK AND CIRCUMFLEX 
  
  Characters_with_ogonek_and_circumflex = u''
  Characters_with_ogonek_and_circumflex += eogoncirc + oogoncirc
  
  
  #  (45): Characters with ring above and circumflex 
  
  aringcirc = u'\uE41F'	#  LATIN SMALL LETTER A WITH RING AND CIRCUMFLEX 
  
  Characters_with_ring_above_and_circumflex = u''
  Characters_with_ring_above_and_circumflex += aringcirc
  
  
  #  (46): Characters with macron and breve 
  
  Amacrbreve = u'\uE010'	#  LATIN CAPITAL LETTER A WITH MACRON AND BREVE 
  amacrbreve = u'\uE410'	#  LATIN SMALL LETTER A WITH MACRON AND BREVE 
  AEligmacrbreve = u'\uE03D'	#  LATIN CAPITAL LETTER AE WITH MACRON AND BREVE 
  aeligmacrbreve = u'\uE43D'	#  LATIN SMALL LETTER AE WITH MACRON AND BREVE 
  Emacrbreve = u'\uE0B7'	#  LATIN CAPITAL LETTER E WITH MACRON AND BREVE 
  emacrbreve = u'\uE4B7'	#  LATIN SMALL LETTER E WITH MACRON AND BREVE 
  Imacrbreve = u'\uE137'	#  LATIN CAPITAL LETTER I WITH MACRON AND BREVE 
  imacrbreve = u'\uE537'	#  LATIN SMALL LETTER I WITH MACRON AND BREVE 
  Omacrbreve = u'\uE21B'	#  LATIN CAPITAL LETTER O WITH MACRON AND BREVE 
  omacrbreve = u'\uE61B'	#  LATIN SMALL LETTER O WITH MACRON AND BREVE 
  OEligmacrbreve = u'\uE260'	#  LATIN CAPITAL LETTER OE WITH MACRON AND BREVE 
  oeligmacrbreve = u'\uE660'	#  LATIN SMALL LETTER OE WITH MACRON AND BREVE 
  Oslashmacrbreve = u'\uE253'	#  LATIN CAPITAL LETTER O WITH STROKE AND MACRON AND BREVE 
  oslashmacrbreve = u'\uE653'	#  LATIN SMALL LETTER O WITH STROKE AND MACRON AND BREVE 
  Umacrbreve = u'\uE30B'	#  LATIN CAPITAL LETTER U WITH MACRON AND BREVE 
  umacrbreve = u'\uE70B'	#  LATIN SMALL LETTER U WITH MACRON AND BREVE 
  Ymacrbreve = u'\uE375'	#  LATIN CAPITAL LETTER Y WITH MACRON AND BREVE 
  ymacrbreve = u'\uE775'	#  LATIN SMALL LETTER Y WITH MACRON AND BREVE 
  
  Characters_with_macron_and_breve = u''
  Characters_with_macron_and_breve += Amacrbreve + amacrbreve + AEligmacrbreve + aeligmacrbreve + Emacrbreve
  Characters_with_macron_and_breve += emacrbreve + Imacrbreve + imacrbreve + Omacrbreve + omacrbreve
  Characters_with_macron_and_breve += OEligmacrbreve + oeligmacrbreve + Oslashmacrbreve + oslashmacrbreve + Umacrbreve
  Characters_with_macron_and_breve += umacrbreve + Ymacrbreve + ymacrbreve
  
  
  #  (47): Characters with macron and acute accent 
  
  Amacracute = u'\uE00A'	#  LATIN CAPITAL LETTER A WITH MACRON AND ACUTE 
  amacracute = u'\uE40A'	#  LATIN SMALL LETTER A WITH MACRON AND ACUTE 
  AEligmacracute = u'\uE03A'	#  LATIN CAPITAL LETTER AE WITH MACRON AND ACUTE 
  aeligmacracute = u'\uE43A'	#  LATIN SMALL LETTER AE WITH MACRON AND ACUTE 
  Imacracute = u'\uE135'	#  LATIN CAPITAL LETTER I WITH MACRON AND ACUTE 
  imacracute = u'\uE535'	#  LATIN SMALL LETTER I WITH MACRON AND ACUTE 
  Oslashmacracute = u'\uEBEC'	#  LATIN CAPITAL LETTER O WITH STROKE AND MACRON AND ACUTE 
  oslashmacracute = u'\uEBED'	#  LATIN SMALL LETTER O WITH STROKE AND MACRON AND ACUTE 
  Umacracute = u'\uE309'	#  LATIN CAPITAL LETTER U WITH MACRON AND ACUTE 
  umacracute = u'\uE709'	#  LATIN SMALL LETTER U WITH MACRON AND ACUTE 
  Ymacracute = u'\uE373'	#  LATIN CAPITAL LETTER Y WITH MACRON AND ACUTE 
  ymacracute = u'\uE773'	#  LATIN SMALL LETTER Y WITH MACRON AND ACUTE 
  
  Characters_with_macron_and_acute_accent = u''
  Characters_with_macron_and_acute_accent += Amacracute + amacracute + AEligmacracute + aeligmacracute + Imacracute
  Characters_with_macron_and_acute_accent += imacracute + Oslashmacracute + oslashmacracute + Umacracute + umacracute
  Characters_with_macron_and_acute_accent += Ymacracute + ymacracute
  
  
  #  (48): Characters with ogonek, dot above and acute accent 
  
  Eogondotacute = u'\uE0EC'	#  LATIN CAPITAL LETTER E WITH OGONEK AND DOT ABOVE AND ACUTE 
  eogondotacute = u'\uE4EC'	#  LATIN SMALL LETTER E WITH OGONEK AND DOT ABOVE AND ACUTE 
  Oogondotacute = u'\uEBFA'	#  LATIN CAPITAL LETTER O WITH OGONEK AND DOT ABOVE AND ACUTE 
  oogondotacute = u'\uEBFB'	#  LATIN SMALL LETTER O WITH OGONEK AND DOT ABOVE AND ACUTE 
  
  #  (49)-(50): For future additions 
  
  #  (C) Variant letter forms 
  
  Asqu = u'\uF13A'	#  LATIN CAPITAL LETTER A SQUARE FORM 
  aunc = u'\uF214'	#  LATIN SMALL LETTER A UNCIAL FORM 
  Ains = u'\uF201'	#  LATIN CAPITAL LETTER A INSULAR FORM
  ains = u'\uF200'	#  LATIN SMALL LETTER A INSULAR FORM
  aopen = u'\uF202'	#  LATIN SMALL LETTER A OPEN FORM 
  aneckless = u'\uF215'	#  LATIN SMALL LETTER NECKLESS A 
  aclose = u'\uF203'	#  LATIN SMALL LETTER CLOSED A GOTHIC FORM 
  Csqu = u'\uF106'	#  LATIN CAPITAL LETTER C SQUARE FORM 
  ccurl = u'\uF198'	#  LATIN SMALL LETTER C WITH CURL 
  dcurl = u'\uF193'	#  LATIN SMALL LETTER D WITH CURL 
  Eunc = u'\uF10A'	#  LATIN CAPITAL LETTER E UNCIAL FORM 
  Euncclose = u'\uF217'	#  LATIN CAPITAL LETTER CLOSED E UNCIAL FORM 
  eunc = u'\uF218'	#  LATIN SMALL LETTER E UNCIAL FORM 
  eext = u'\uF219'	#  LATIN SMALL LETTER E EXTENDED BAR FORM 
  etall = u'\uF21A'	#  LATIN SMALL LETTER E TALL FORM 
  finssemiclose = u'\uF21B'	#  LATIN SMALL LETTER SEMI-CLOSED INSULAR F 
  finsdothook = u'\uF21C'	#  LATIN SMALL LETTER INSULAR F WITH DOTTED HOOKS 
  finsclose = u'\uF207'	#  LATIN SMALL LETTER CLOSED INSULAR F 
  fcurl = u'\uF194'	#  LATIN SMALL LETTER F WITH CURL 
  Gsqu = u'\uF10E'	#  LATIN CAPITAL LETTER G SQUARE FORM 
  gcurl = u'\uF196'	#  LATIN SMALL LETTER G WITH CURL 
  gdivloop = u'\uF21D'	#  LATIN SMALL LETTER G WITH SEPARATE LOOPS 
  glglowloop = u'\uF21E'	#  LATIN SMALL LETTER CLOSED G WITH LARGE LOWER LOOP 
  gsmlowloop = u'\uF21F'	#  LATIN SMALL LETTER CLOSED G WITH SMALL LOWER LOOP 
  Hunc = u'\uF110'	#  LATIN CAPITAL LETTER H UNCIAL FORM 
  hrdes = u'\uF23A'	#  LATIN SMALL LETTER H WITH RIGHT DESCENDER 
  ilong = u'\uF220'	#  LATIN SMALL LETTER LONG I 
  kunc = u'\uF208'	#  LATIN SMALL LETTER K UNCIAL FORM 
  ksemiclose = u'\uF221'	#  LATIN SMALL LETTER K SEMI-CLOSED FORM 
  kclose = u'\uF209'	#  LATIN SMALL LETTER K CLOSED FORM 
  kcurl = u'\uF195'	#  LATIN SMALL LETTER K WITH CURL 
  ldes = u'\uF222'	#  LATIN SMALL LETTER L DESCENDING 
  Munc = u'\uF11A'	#  LATIN CAPITAL LETTER M UNCIAL FORM 
  Muncdes = u'\uF224'	#  LATIN CAPITAL LETTER M UNCIAL FORM WITH RIGHT DESCENDER 
  munc = u'\uF225'	#  LATIN SMALL LETTER M UNCIAL FORM 
  muncdes = u'\uF226'	#  LATIN SMALL LETTER M UNCIAL FORM WITH RIGHT DESCENDER 
  mrdes = u'\uF223'	#  LATIN SMALL LETTER M WITH RIGHT DESCENDER 
  Nrdes = u'\uF229'	#  LATIN CAPITAL LETTER N WITH RIGHT DESCENDER 
  nrdes = u'\uF228'	#  LATIN SMALL LETTER N WITH RIGHT DESCENDER 
  nscaprdes = u'\uF22A'	#  LATIN LETTER SMALL CAPITAL N WITH RIGHT DESCENDER 
  nscapldes = u'\uF22B'	#  LATIN LETTER SMALL CAPITAL N WITH LEFT DESCENDER 
  nflour = u'\uF19A'	#  LATIN SMALL LETTER N WITH FLOURISH 
  Qstem = u'\uF22C'	#  LATIN CAPITAL LETTER Q WITH STEM 
  rflour = u'\uF19B'	#  LATIN SMALL LETTER R WITH FLOURISH 
  Sclose = u'\uF126'	#  LATIN CAPITAL LETTER S CLOSED FORM 
  sclose = u'\uF128'	#  LATIN SMALL LETTER S CLOSED FORM 
  slongdes = u'\uF127'	#  LATIN SMALL LETTER LONG S DESCENDING 
  tcurl = u'\uF199'	#  LATIN SMALL LETTER T WITH CURL 
  xldes = u'\uF232'	#  LATIN SMALL LETTER X WITH LEFT DESCENDER 
  yrgmainstrok = u'\uF233'	#  LATIN SMALL LETTER Y WITH RIGHT MAIN STROKE 
  Characters_with_ogonek_dot_above_and_acute_accent = u''
  Characters_with_ogonek_dot_above_and_acute_accent += Eogondotacute + eogondotacute + Oogondotacute + oogondotacute + Asqu
  Characters_with_ogonek_dot_above_and_acute_accent += aunc + Ains + ains + aopen + aneckless
  Characters_with_ogonek_dot_above_and_acute_accent += aclose + Csqu + ccurl + dcurl + Eunc
  Characters_with_ogonek_dot_above_and_acute_accent += Euncclose + eunc + eext + etall + finssemiclose
  Characters_with_ogonek_dot_above_and_acute_accent += finsdothook + finsclose + fcurl + Gsqu + gcurl
  Characters_with_ogonek_dot_above_and_acute_accent += gdivloop + glglowloop + gsmlowloop + Hunc + hrdes
  Characters_with_ogonek_dot_above_and_acute_accent += ilong + kunc + ksemiclose + kclose + kcurl
  Characters_with_ogonek_dot_above_and_acute_accent += ldes + Munc + Muncdes + munc + muncdes
  Characters_with_ogonek_dot_above_and_acute_accent += mrdes + Nrdes + nrdes + nscaprdes + nscapldes
  Characters_with_ogonek_dot_above_and_acute_accent += nflour + Qstem + rflour + Sclose + sclose
  Characters_with_ogonek_dot_above_and_acute_accent += slongdes + tcurl + xldes + yrgmainstrok
  
  
  
  
  
  entity = {
         'edotbl': u'\u1EB9',
         'hidot': u'\uF1F8',
         'slonguumllig': u'\uEBAB',
         'Idotacute': u'\uEBF6',
         'combmacr': u'\u0304',
         'wdblac': u'\uE750',
         'Vmod': u'\uF1BE',
         'bdotbl': u'\u1E05',
         'thorndotbl': u'\uE79F',
         'mdot': u'\u1E41',
         'luhsqbNT': u'\u2E00',
         'Kdot': u'\uE168',
         'tridotupw': u'\u2234',
         'oogonacute': u'\uE60C',
         'kbar': u'\uA741',
         'ccedil': u'\u00E7',
         'ascap': u'\u1D00',
         'ouncescript': u'\uF2FD',
         'Nrdes': u'\uF229',
         'Ycurl': u'\uE385',
         'Xovlhigh': u'\uF7B3',
         'slonghlig': u'\uEBA1',
         'Qbardes': u'\uA756',
         'rrotdotbl': u'\uE7C1',
         'jacute': u'\uE553',
         'finsdotbl': u'\uE7E5',
         'Cacute': u'\u0106',
         'ethdotbl': u'\uE48F',
         'rscap': u'\u0280',
         'THORN': u'\u00DE',
         'equals': u'\u003D',
         'AUligdotbl': u'\uEFF6',
         'permil': u'\u2030',
         'Agrave': u'\u00C0',
         'Edotacute': u'\uE0C8',
         'oumlacute': u'\uE62C',
         'divide': u'\u00F7',
         'ausup': u'\uE8E1',
         'pscap': u'\u1D18',
         'udotacute': u'\uEBFF',
         'Oslashdotacute': u'\uEBFC',
         'fsup': u'\uF017',
         'oopen': u'\u0254',
         'slongplig': u'\uEBA5',
         'oscap': u'\u1D0F',
         'Facute': u'\uE0F0',
         'punctintertilde': u'\uF1E8',
         'excl': u'\u0021',
         'ttlig': u'\uEED9',
         'Pdotbl': u'\uE26D',
         'Eunc': u'\uF10A',
         'Oesup': u'\uE244',
         'kslongligbar': u'\uE7C8',
         'rumsup': u'\uF040',
         'mu': u'\u03BC',
         'squareblsm': u'\u25AA',
         'Udotbl': u'\u1EE4',
         'punctvers': u'\uF1EA',
         'ubar': u'\u0289',
         'omacrbreve': u'\uE61B',
         'fuumllig': u'\uEECC',
         'dsup': u'\u0369',
         'rpar': u'\u0029',
         'AVlig': u'\uA738',
         'aeligogonacute': u'\uE8D3',
         'Uhook': u'\u1EE6',
         'gscap': u'\u0262',
         'eng': u'\u014B',
         'jscap': u'\u1D0A',
         'nrdes': u'\uF228',
         'Eogondotacute': u'\uE0EC',
         'rscaptailstrok': u'\uA776',
         'tridotscomposit': u'\uF1E6',
         'ocurlacute': u'\uEBB8',
         'inodotsup': u'\uF02F',
         'sextans': u'\uF2FB',
         'aenlacute': u'\uEAF0',
         'pdot': u'\u1E57',
         'urlemn': u'\uF1C2',
         'oclig': u'\uEFAD',
         'fscap': u'\uA730',
         'metrlongshort': u'\u23D3',
         'AEligacute': u'\u01FC',
         'Ndot': u'\u1E44',
         'oolig': u'\uA74F',
         'ograve': u'\u00F2',
         'nscapdotbl': u'\uEF2A',
         'eogonacute': u'\uE499',
         'OEligdblac': u'\uEBC8',
         'rlhsqb': u'\u2E25',
         'jnodotsup': u'\uF031',
         'gscapdotbl': u'\uEF27',
         'bidotscomposit': u'\uF1F2',
         'icurl': u'\uE52A',
         'auligacute': u'\uEFE5',
         'otilde': u'\u00F5',
         'slongslstrok': u'\uE8B8',
         'Odblac': u'\u0150',
         'jnodot': u'\u0237',
         'lsup': u'\u1DDD',
         'rsaquo': u'\u203A',
         'AAligdot': u'\uEFEE',
         'tridotright': u'\u10FB',
         'mtailstrok': u'\uA773',
         'wenl': u'\uEEF9',
         'AAligacute': u'\uEFE0',
         'metrancgrave': u'\uF70C',
         'resup': u'\uE8EA',
         'tribull': u'\u2023',
         'jdotbl': u'\uE551',
         'edotacute': u'\uE4C8',
         'lcub': u'\u007B',
         'libraflem': u'\uF2ED',
         'ra': u'\u1DD3',
         'Gins': u'\uA77D',
         'ijlig': u'\u0133',
         'romnumCDlig': u'\u2180',
         'ydblac': u'\uE77C',
         'is_': u'\uA76D',
         'tscapsup': u'\uF02A',
         'infin': u'\u221E',
         'Wmacr': u'\uE357',
         'aogon': u'\u0105',
         'kdotbl': u'\u1E33',
         'schillgerm': u'\uF2F7',
         'wacute': u'\u1E83',
         'Vins': u'\uA768',
         'kscapdot': u'\uEBDB',
         'Odot': u'\u022E',
         'aaligdotbl': u'\uEFF3',
         'hellip': u'\u2026',
         'Oogonmacr': u'\u01EC',
         'Qdotbl': u'\uE288',
         'aflig': u'\uEFA3',
         'avligacute': u'\uEFE7',
         'emacrsup': u'\uF136',
         'aacloselig': u'\uEFA0',
         'hscapdot': u'\uEBDA',
         'oslashmacr': u'\uE652',
         'Pdot': u'\u1E56',
         'ksup': u'\u1DDC',
         'Nmacrhigh': u'\uE1DC',
         'circledot': u'\u25CC',
         'Ilong': u'\uA7FE',
         'fscapdot': u'\uEBD7',
         'anlig': u'\uEFA7',
         'aeligred': u'\uF204',
         'minus': u'\u2212',
         'WYNN': u'\u01F7',
         'cscap': u'\u1D04',
         'eunc': u'\uF218',
         'omacracute': u'\u1E53',
         'combogon': u'\u0328',
         'edotblacute': u'\uE498',
         'ldblpar': u'\u2E28',
         'ivsup': u'\uE54B',
         'combgrave': u'\u0300',
         'nflour': u'\uF19A',
         'aeligcurl': u'\uEBEB',
         'AOligdotbl': u'\uEFF4',
         'Uacute': u'\u00DA',
         'lsaquo': u'\u2039',
         'Uesup': u'\uE32B',
         'combtripbrevebl': u'\uF1FC',
         'enqd': u'\u2000',
         'erang': u'\uF1C7',
         'avligslash': u'\uA73B',
         'grave': u'\u0060',
         'AOligred': u'\uF205',
         'aunc': u'\uF214',
         'lozengedot': u'\u2058',
         'rabar': u'\uF1C1',
         'wmacr': u'\uE757',
         'arscapligsup': u'\uF130',
         'aglig': u'\uEFA5',
         'sscapdotbl': u'\uEF2C',
         'dot': u'\u02D9',
         'ltailstrok': u'\uA772',
         'slongllig': u'\uEBA3',
         'Atilde': u'\u00C3',
         'slongvinslig': u'\uEBAC',
         'YYligdblac': u'\uEBCA',
         'Sclose': u'\uF126',
         'hrarmlig': u'\uE8C3',
         'Imod': u'\u1D35',
         'oopenmacr': u'\uE7CC',
         'menl': u'\uEEEE',
         'Eacute': u'\u00C9',
         'gscapdot': u'\uEF20',
         'Frev': u'\uA7FB',
         'cacute': u'\u0107',
         'aeligacute': u'\u01FD',
         'yenl': u'\uEEFB',
         'bar': u'\u0305',
         'CONdes': u'\uA76E',
         'iasup': u'\uE8E4',
         'Obreve': u'\u014E',
         'libraital': u'\uF2EC',
         'oesup': u'\uE644',
         'Dstrok': u'\u0110',
         'vbar': u'\uE74E',
         'rdotbl': u'\u1E5B',
         'metrmacrbreve': u'\uF702',
         'iexcl': u'\u00A1',
         'ycurl': u'\uE785',
         'atilde': u'\u00E3',
         'slongbar': u'\u1E9D',
         'metrmacrgrave': u'\uF705',
         'Kbar': u'\uA740',
         'Gsqu': u'\uF10E',
         'oslashacute': u'\u01FF',
         'uumlcirc': u'\uE717',
         'Omacracute': u'\u1E52',
         'space': u'\u0020',
         'ymacrbreve': u'\uE775',
         'Egrave': u'\u00C8',
         'igrave': u'\u00EC',
         'eogondot': u'\uE4EB',
         'mdotbl': u'\u1E43',
         'punctelevhiback': u'\uF1FA',
         'Jcurl': u'\uE163',
         'Fdotbl': u'\uE0EE',
         'Oogondot': u'\uEBDE',
         'lpar': u'\u0028',
         'vcirc': u'\uE73B',
         'Qstem': u'\uF22C',
         'ubrevinvbl': u'\uE727',
         'adblac': u'\uE425',
         'puncsp': u'\u2008',
         'eogonsup': u'\uF135',
         'avligdotbl': u'\uEFF9',
         'trileftwh': u'\u25C3',
         'ilong': u'\uF220',
         'Edblac': u'\uE0D1',
         'hovlmed': u'\uE517',
         'udblac': u'\u0171',
         'Imacrbreve': u'\uE137',
         'AAligdblac': u'\uEFEA',
         'tscapdot': u'\uEF24',
         'sdotbl': u'\u1E63',
         'nlfhook': u'\u0272',
         'uhook': u'\u1EE7',
         'Vdotbl': u'\u1E7E',
         'vinsacute': u'\uEBBB',
         'msignflour': u'\uF2F3',
         'dtailstrok': u'\uA771',
         'combring': u'\u030A',
         'udot': u'\uE715',
         'jdblac': u'\uE562',
         'wscap': u'\u1D21',
         'ldquo': u'\u201C',
         'Hacute': u'\uE116',
         'Uvertline': u'\uE324',
         'rsqb': u'\u005D',
         'Minv': u'\uA7FD',
         'mscapdotbl': u'\uEF29',
         'yen': u'\u00A5',
         'ucar': u'\u01D4',
         'khook': u'\u0199',
         'CONbase': u'\u2183',
         'Yloop': u'\u1EFE',
         'tilde': u'\u02DC',
         'imacr': u'\u012B',
         'OOligdblac': u'\uEFEC',
         'Vinsdot': u'\uE3E7',
         'oslashdblac': u'\uEBC7',
         'macrhigh': u'\uF00A',
         'slongslongklig': u'\uF4FE',
         'bdot': u'\u1E03',
         'aaligenl': u'\uEFDF',
         'jcurl': u'\uE563',
         'esup': u'\u0364',
         'ftlig': u'\uEECB',
         'scruple': u'\u2108',
         'laquo': u'\u00AB',
         'oenl': u'\uEEF0',
         'bacute': u'\uE444',
         'lsquo': u'\u2018',
         'pacute': u'\u1E55',
         'Fins': u'\uA77B',
         'jmacrmed': u'\uE554',
         'afinslig': u'\uEFA4',
         'Amacr': u'\u0100',
         'ffylig': u'\uEECF',
         'Ldotbl': u'\u1E36',
         'yesup': u'\uE781',
         'dblldr': u'\u2025',
         'circ': u'\u005E',
         'uml': u'\u00A8',
         'qscap': u'\uEF0C',
         'aeligdblac': u'\uE441',
         'Wesup': u'\uE353',
         'aopen': u'\uF202',
         'evsup': u'\uE8E2',
         'M5leg': u'\uA7FF',
         'lscapdotbl': u'\uEF28',
         'lscap': u'\u029F',
         'plus': u'\u002B',
         'combdot': u'\u0307',
         'Emacrbreve': u'\uE0B7',
         'mrdes': u'\uF223',
         'q2app': u'\uE8B3',
         'sol': u'\u002F',
         'jenl': u'\uEEEB',
         'Ydotbl': u'\u1EF4',
         'OOligacute': u'\uEFE8',
         'sup8': u'\u2078',
         'sup9': u'\u2079',
         'sup0': u'\u2070',
         'sup1': u'\u00B9',
         'sup2': u'\u00B2',
         'sup3': u'\u00B3',
         'sup4': u'\u2074',
         'metrdblbrevemacr': u'\uF72E',
         'sup6': u'\u2076',
         'sup7': u'\u2077',
         'wvsup': u'\uE8F3',
         'yloop': u'\u1EFF',
         'uscap': u'\u1D1C',
         'oslashmacracute': u'\uEBED',
         'sub6': u'\u2086',
         'sub7': u'\u2087',
         'sub4': u'\u2084',
         'sub5': u'\u2085',
         'sub2': u'\u2082',
         'romdimsext': u'\uF2DC',
         'sub0': u'\u2080',
         'egrave': u'\u00E8',
         'epsilon': u'\u03B5',
         'Udot': u'\uE315',
         'qsup': u'\uF033',
         'sub8': u'\u2088',
         'sub9': u'\u2089',
         'aaliguml': u'\uEFFF',
         'AVligslashacute': u'\uEBB0',
         'AAlig': u'\uA732',
         'qvinslig': u'\uEAD1',
         'revpara': u'\u204B',
         'nacute': u'\u0144',
         'zenl': u'\uEEFC',
         'AAliguml': u'\uEFFE',
         'ddot': u'\u1E0B',
         'gsmlowloop': u'\uF21F',
         'Euml': u'\u00CB',
         'Iacute': u'\u00CD',
         'ercurl': u'\uF1C8',
         'Bdot': u'\u1E02',
         'nu': u'\u03BD',
         'Adot': u'\u0226',
         'denl': u'\uEEE3',
         'ousup': u'\uE646',
         'edot': u'\u0117',
         'omega': u'\u03C9',
         'wgrave': u'\u1E81',
         'aeligdotbl': u'\uE436',
         'Cdotbl': u'\uE066',
         'metrshort': u'\u23D1',
         'Kstrleg': u'\uA742',
         'fylig': u'\uEECD',
         'Yacute': u'\u00DD',
         'YYlig': u'\uA760',
         'aeligenl': u'\uEAF1',
         'dscap': u'\u1D05',
         'punctelevdiag': u'\uF1F0',
         'OEligmacrbreve': u'\uE260',
         'Icirc': u'\u00CE',
         'Gdotbl': u'\uE101',
         'ogon': u'\u02DB',
         'AElig': u'\u00C6',
         'Edotbl': u'\u1EB8',
         'ooligdotbl': u'\uEFFD',
         'kscapsup': u'\uF01C',
         'romunc': u'\uF2D9',
         'combcomma': u'\u0315',
         'iosup': u'\uE8E5',
         'arscaplig': u'\uEFAB',
         'ordm': u'\u00BA',
         'ordf': u'\u00AA',
         'grosch': u'\uF2E9',
         'nsup': u'\u1DE0',
         'ntilde': u'\u00F1',
         'orrotsup': u'\uF03E',
         'wdotbl': u'\u1E89',
         'eext': u'\uF219',
         'numdash': u'\u2012',
         'PPlig': u'\uEEDD',
         'romsemunc': u'\uF2DA',
         'odot': u'\u022F',
         'langb': u'\u27E8',
         'umacracute': u'\uE709',
         'Zdotbl': u'\u1E92',
         'oslashdotbl': u'\uEBE1',
         'odiaguml': u'\uE8D7',
         'slongdestlig': u'\uEADA',
         'horbar': u'\u2015',
         'aoligenl': u'\uEFDE',
         'Vwelsh': u'\u1EFC',
         'percnt': u'\u0025',
         'deg': u'\u00B0',
         'Ldot': u'\uE19E',
         'Abreve': u'\u0102',
         'gsup': u'\u1DDA',
         'fllig': u'\uFB02',
         'hrdes': u'\uF23A',
         'pscapdot': u'\uEBCF',
         'Emacr': u'\u0112',
         'Lacute': u'\u0139',
         'gdot': u'\u0121',
         'Lovlhigh': u'\uF7B4',
         'combsgvertl': u'\u030D',
         'tridotsupw': u'\u2E2B',
         'luhsqb': u'\u2E22',
         'finsenl': u'\uEEFF',
         'ohook': u'\u1ECF',
         'anscapligsup': u'\uF03A',
         'vsup': u'\u036E',
         'jbar': u'\u0249',
         'Aogonacute': u'\uE004',
         'vdotbl': u'\u1E7F',
         'vinsdotbl': u'\uE7E6',
         'Eogonacute': u'\uE099',
         'zdotbl': u'\u1E93',
         'rdquorev': u'\u201F',
         'ecurl': u'\uE4E9',
         'Eogon': u'\u0118',
         'fcurl': u'\uF194',
         'slongklig': u'\uF4FC',
         'kstrascleg': u'\uA745',
         'ndotbl': u'\u1E47',
         'combdothigh': u'\uF1CA',
         'ygrave': u'\u1EF3',
         'etslash': u'\uF158',
         'AUlig': u'\uA736',
         'metrmacrdblac': u'\uF715',
         'Acurl': u'\uE033',
         'metrbreve': u'\uF701',
         'Oslashcurl': u'\uE3D4',
         'uenl': u'\uEEF7',
         'quot': u'\u0022',
         'trirightwh': u'\u25B9',
         'Ddot': u'\u1E0A',
         'uwsup': u'\uE8ED',
         'nbsp': u'\u00A0',
         'AVligacute': u'\uEFE6',
         'OEligmacr': u'\uE25D',
         'auml': u'\u00E4',
         'Gacute': u'\u01F4',
         'finsclose': u'\uF207',
         'qdot': u'\uE682',
         'combhook': u'\u0309',
         'ndash': u'\u2013',
         'Zdot': u'\u017B',
         'Uumlcirc': u'\uE317',
         'romXbar': u'\uF2E1',
         'gscapsup': u'\u1DDB',
         'oogondotacute': u'\uEBFB',
         'kacute': u'\u1E31',
         'logand': u'\u2227',
         'amacrbreve': u'\uE410',
         'YOGH': u'\u021C',
         'drot': u'\uA77A',
         'aeligogon': u'\uE440',
         'nbar': u'\uE7B2',
         'Finsacute': u'\uEBB3',
         'oslashogonacute': u'\uE657',
         'et': u'\u204A',
         'Finsdot': u'\uEBD3',
         'er': u'\u035B',
         'cklig': u'\uEEC4',
         'Adblac': u'\uE025',
         'kenl': u'\uEEEC',
         'qcentrslstrok': u'\uE8B4',
         'bscapdotbl': u'\uEF25',
         'ayligdotbl': u'\uEFFB',
         'nlrleg': u'\u019E',
         'ENG': u'\u014A',
         'Jbar': u'\u0248',
         'mringbl': u'\uE5C5',
         'conbase': u'\u2184',
         'lbrk': u'\uA747',
         'wesup': u'\uE753',
         'oogoncirc': u'\uE60E',
         'metrbreveacute': u'\uF706',
         'xi': u'\u03BE',
         'apomod': u'\u02BC',
         'sgldr': u'\u2024',
         'rins': u'\uA783',
         'sup5': u'\u2075',
         'hwair': u'\u0195',
         'ring': u'\u02DA',
         'Ubreve': u'\u016C',
         'iovlmed': u'\uE550',
         'eesup': u'\uE8E2',
         'drotenl': u'\uEEE4',
         'nnbsp': u'\u202F',
         'verbarup': u'\u02C8',
         'Uosup': u'\uE32D',
         'brvbar': u'\u00A6',
         'iacute': u'\u00ED',
         'AOligdblac': u'\uEBC0',
         'virgmin': u'\uF1F7',
         'Ndotbl': u'\u1E46',
         'ethscap': u'\u1D06',
         'lsqb': u'\u005B',
         'copy': u'\u00A9',
         'Fturn': u'\u2132',
         'aoligdotbl': u'\uEFF5',
         'OEligacute': u'\uE259',
         'uisup': u'\uE72C',
         'Ucar': u'\u01D3',
         'Macute': u'\u1E3E',
         'slongovlmed': u'\uE79E',
         'posit': u'\uF1E2',
         'dacute': u'\uE477',
         'not_': u'\u00AC',
         'renl': u'\uEEF3',
         'Qdot': u'\uE282',
         'dash': u'\u2010',
         'dscapdot': u'\uEBD2',
         'thornbardes': u'\uA767',
         'abreve': u'\u0103',
         'aoligred': u'\uF206',
         'Bdotbl': u'\u1E04',
         'thornslonglig': u'\uE734',
         'mesup': u'\uE8E8',
         'aolig': u'\uA735',
         'metrbrevedblac': u'\uF717',
         'Idblac': u'\uE143',
         'Pflour': u'\uA752',
         'Oslashacute': u'\u01FE',
         'Ohook': u'\u1ECE',
         'Drot': u'\uA779',
         'beta': u'\u03B2',
         'pmacr': u'\uE665',
         'Lhighstrok': u'\uA748',
         'punctelev': u'\uF161',
         'middot': u'\u00B7',
         'ering': u'\uE4CF',
         'lringbl': u'\uE5A4',
         'rsquo': u'\u2019',
         'raquo': u'\u00BB',
         'triast': u'\u2042',
         'Verbar': u'\u2016',
         'uasup': u'\uE8EB',
         'sigma': u'\u03C3',
         'xmod': u'\u02E3',
         'wynn': u'\u01BF',
         'Jacute': u'\uE153',
         'AEligdblac': u'\uE041',
         'psquirrel': u'\uA755',
         'Sacute': u'\u015A',
         'aenl': u'\uEEE0',
         'trot': u'\uA787',
         'ains': u'\uF200',
         'fenl': u'\uEEE7',
         'tscapdotbl': u'\uEF2D',
         'OOliguml': u'\uEBE4',
         'Ymacr': u'\u0232',
         'glglowloop': u'\uF21E',
         'arrsgldw': u'\u2193',
         'ruhsqb': u'\u2E23',
         'metrbrevemacr': u'\uF703',
         'munc': u'\uF225',
         'sub3': u'\u2083',
         'stlig': u'\uFB06',
         'hairsp': u'\u200A',
         'dovlmed': u'\uE491',
         'Wuml': u'\u1E84',
         'prime': u'\u2032',
         'sacute': u'\u015B',
         'tdot': u'\u1E6B',
         'nscapdot': u'\uEF21',
         'ramus': u'\uF1DB',
         'sdot': u'\u1E61',
         'athornlig': u'\uEFAC',
         'Mdotbl': u'\u1E42',
         'numsp': u'\u2007',
         'OOlig': u'\uA74E',
         'combtilde': u'\u0303',
         'thornsup': u'\uF03D',
         'metrmacracute': u'\uF704',
         'Ousup': u'\uE246',
         'romscapxbar': u'\uF2E2',
         'Oogonacute': u'\uE20C',
         'vdot': u'\uE74C',
         'uumlmacr': u'\u01D6',
         'yuml': u'\u00FF',
         'tsup': u'\u036D',
         'eogonmacr': u'\uE4BC',
         'slongtilig': u'\uEBA9',
         'ydotacute': u'\uE784',
         'markflour': u'\uF2F1',
         'aeligmacr': u'\u01E3',
         'iscap': u'\u026A',
         'xslashula': u'\uE8BD',
         'Ihook': u'\u1EC8',
         'AVligogon': u'\uEBF0',
         'period': u'\u002E',
         'edblac': u'\uE4D1',
         'colon': u'\u003A',
         'metranc': u'\uF70A',
         'notequals': u'\u2260',
         'LLwelsh': u'\u1EFA',
         'ihook': u'\u1EC9',
         'macrmed': u'\uF00B',
         'AEligdotbl': u'\uE036',
         'ocar': u'\u01D2',
         'ldes': u'\uF222',
         'Oogondotacute': u'\uEBFA',
         'wusup': u'\uE8F2',
         'YYliguml': u'\uEBE8',
         'vinsdot': u'\uE7E7',
         'AYlig': u'\uA73C',
         'avlig': u'\uA739',
         'breve': u'\u02D8',
         'rflour': u'\uF19B',
         'kappa': u'\u03BA',
         'RUM': u'\uA75C',
         'metrdblshortlong': u'\u23D4',
         'AVligdblac': u'\uEBC2',
         'henl': u'\uEEE9',
         'Tdotbl': u'\u1E6C',
         'vwelsh': u'\u1EFD',
         'ugrave': u'\u00F9',
         'hacute': u'\uE516',
         'Prev': u'\uA7FC',
         'xslashlra': u'\uE8BE',
         'Munc': u'\uF11A',
         'thornacute': u'\uE737',
         'Vdiagstrok': u'\uA75E',
         'Tdot': u'\u1E6A',
         'slongdotbl': u'\uE7C2',
         'IS': u'\uA76C',
         'ssup': u'\u1DE4',
         'Oogoncurl': u'\uE24F',
         'Sdot': u'\u1E60',
         'rsup': u'\u036C',
         'Omacrbreve': u'\uE21B',
         'lstrok': u'\u0142',
         'gdotbl': u'\uE501',
         'rdes': u'\u027C',
         'penningar': u'\uF2F5',
         'romscapdslash': u'\uF2E4',
         'urrot': u'\uF153',
         'Ydblac': u'\uE37C',
         'parag': u'\uF1E1',
         'verbarql': u'\u2E20',
         'Yuml': u'\u0178',
         'yyliguml': u'\uEBE9',
         'xenl': u'\uEEFA',
         'combcurl': u'\u1DCE',
         'verbarqr': u'\u2E21',
         'psi': u'\u03C8',
         'eisup': u'\uE4E2',
         'Wacute': u'\u1E82',
         'amp': u'\u0026',
         'Vovlhigh': u'\uF7B2',
         'inodotenl': u'\uEEFD',
         'oslash': u'\u00F8',
         'acute': u'\u00B4',
         'frlig': u'\uEECA',
         'cdot': u'\u010B',
         'Igrave': u'\u00CC',
         'Oslashogonacute': u'\uE257',
         'AVligslash': u'\uA73A',
         'smallzero': u'\uF1BD',
         'AnecklessElig': u'\uEFAE',
         'Trot': u'\uA786',
         'combisbelow': u'\u1DD0',
         'oslashcurl': u'\uE7D4',
         'xscap': u'\uEF11',
         'thornslongligbar': u'\uE735',
         'ezh': u'\u0292',
         'Ocar': u'\u01D1',
         'verbar': u'\u007C',
         'llwelsh': u'\u1EFB',
         'Ycirc': u'\u0176',
         'combcurlhigh': u'\uF1C5',
         'ooligdblac': u'\uEFED',
         'oslashdotacute': u'\uEBFD',
         'ydot': u'\u1E8F',
         'eogoncurl': u'\uEBF3',
         'ethsup': u'\u1DD9',
         'qmacr': u'\uE681',
         'kscap': u'\u1D0B',
         'combcurlbar': u'\uF1CC',
         'yylig': u'\uA761',
         'msup': u'\u036B',
         'drotacute': u'\uEBB2',
         'Uring': u'\u016E',
         'uvsup': u'\uE8EC',
         'yring': u'\u1E99',
         'dagger': u'\u2020',
         'Pbardes': u'\uA750',
         'aogonacute': u'\uE404',
         'eacute': u'\u00E9',
         'kstrleg': u'\uA743',
         'Euncclose': u'\uF217',
         'Hunc': u'\uF110',
         'fMedrun': u'\u16A0',
         'avligdblac': u'\uEBC3',
         'emacr': u'\u0113',
         'tzlig': u'\uEEDC',
         'oeligscap': u'\u0276',
         'ayligdot': u'\uEFF1',
         'bscapdot': u'\uEBD0',
         'AEligmacr': u'\u01E2',
         'vring': u'\uE743',
         'Lbrk': u'\uA746',
         'Ymacrbreve': u'\uE375',
         'vdiagstrok': u'\uA75F',
         'lllig': u'\uF4F9',
         'uosup': u'\uE72D',
         'Ocurl': u'\uE3D3',
         'vvisigot': u'\uA763',
         'oslashbreve': u'\uEBEF',
         'kovlmed': u'\uE7C3',
         'fturn': u'\u214E',
         'ocurl': u'\uE7D3',
         'Idot': u'\u0130',
         'euml': u'\u00EB',
         'metrdblbrevemacracute': u'\uF71B',
         'Eogoncurl': u'\uEBF2',
         'cdotbl': u'\uE466',
         'ooligacute': u'\uEFE9',
         'gethlig': u'\uEED4',
         'eogon': u'\u0119',
         'finsacute': u'\uEBB4',
         'yogh': u'\u021D',
         'amacr': u'\u0101',
         'Oslashdotbl': u'\uEBE0',
         'Ecirc': u'\u00CA',
         'yrgmainstrok': u'\uF233',
         'aosup': u'\uE42D',
         'thorn': u'\u00FE',
         'hslongligbar': u'\uE7C7',
         'tacute': u'\uE6E2',
         'scudi': u'\uF2F9',
         'aclose': u'\uF203',
         'escap': u'\u1D07',
         'wuml': u'\u1E85',
         'aring': u'\u00E5',
         'bull': u'\u2022',
         'dollar': u'\u0024',
         'Ydot': u'\u1E8E',
         'vacute': u'\uE73A',
         'Ygrave': u'\u1EF2',
         'eacombcirc': u'\uEBBD',
         'dipledot': u'\u22D7',
         'rrot': u'\uA75B',
         'libradut': u'\uF2EA',
         'cedil': u'\u00B8',
         'oogondotbl': u'\uE608',
         'slong': u'\u017F',
         'ymacr': u'\u0233',
         'dblovl': u'\u033F',
         'Rins': u'\uA782',
         'drotdrotlig': u'\uEEC6',
         'thornbarslash': u'\uF149',
         'Jdot': u'\uE15C',
         'eylig': u'\uEEC7',
         'Otilde': u'\u00D5',
         'lbbar': u'\u2114',
         'Iovlhigh': u'\uE150',
         'ycirc': u'\u0177',
         'ctlig': u'\uEEC5',
         'nringbl': u'\uE5EE',
         'Oogondotbl': u'\uE208',
         'slongtrlig': u'\uEBAA',
         'ooliguml': u'\uEBE5',
         'arlig': u'\uEFAA',
         'nenl': u'\uEEEF',
         'Ybreve': u'\uE376',
         'vuml': u'\uE742',
         'ffilig': u'\uFB03',
         'omacrsup': u'\uF13F',
         'Udblac': u'\u0170',
         'Aring': u'\u00C5',
         'Vslstrok': u'\u2123',
         'Rdotbl': u'\u1E5A',
         'iuml': u'\u00EF',
         'emsp13': u'\u2004',
         'emsp16': u'\u2006',
         'emsp14': u'\u2005',
         'hsup': u'\u036A',
         'Jmacrhigh': u'\uE154',
         'nscapslonglig': u'\uEED5',
         'Umacracute': u'\uE309',
         'anscaplig': u'\uEFA8',
         'gopen': u'\u0261',
         'ucirc': u'\u00FB',
         'hhook': u'\u0266',
         'bblig': u'\uEEC2',
         'nscaprdes': u'\uF22A',
         'emacracute': u'\u1E17',
         'combtildevert': u'\u033E',
         'romaslibr': u'\uF2E0',
         'obol': u'\uF2F4',
         'icirc': u'\u00EE',
         'upsilon': u'\u03C5',
         'liranuov': u'\uF2EE',
         'ensp': u'\u2002',
         'lambda_': u'\u03BB',
         'rlslst': u'\u2E1C',
         'rsquorev': u'\u201B',
         'rdblpar': u'\u2E29',
         'lscapsup': u'\u1DDE',
         'slongjlig': u'\uF4FB',
         'llhsqb': u'\u2E24',
         'lhighstrok': u'\uA749',
         'bsup': u'\uF012',
         'lacute': u'\u013A',
         'fftlig': u'\uEECE',
         'hscap': u'\u029C',
         'ysup': u'\uF02B',
         'avsup': u'\uE42E',
         'Ccedil': u'\u00C7',
         'Vinsdotbl': u'\uE3E6',
         'aeligbreve': u'\uE43F',
         'Vvisigot': u'\uA762',
         'Oslashmacrbreve': u'\uE253',
         'barbl': u'\u0332',
         'PPliguml': u'\uEBE6',
         'AYligdotbl': u'\uEFFA',
         'ienl': u'\uEEEA',
         'sect': u'\u00A7',
         'sclose': u'\uF128',
         'oacute': u'\u00F3',
         'uring': u'\u016F',
         'Rslstrok': u'\u211F',
         'bscapsup': u'\uF013',
         'oelig': u'\u0153',
         'aisup': u'\uE8E0',
         'combbreve': u'\u0306',
         'aenlosmalllig': u'\uEAF2',
         'filig': u'\uFB01',
         'penl': u'\uEEF1',
         'ovlhigh': u'\uF00C',
         'mmacrmed': u'\uE5B8',
         'rrotsup': u'\u1DE3',
         'micro': u'\u00B5',
         'imacracute': u'\uE535',
         'szlig': u'\u00DF',
         'qbardestilde': u'\uE68B',
         'ppflourlig': u'\uEED7',
         'auligdotbl': u'\uEFF7',
         'finsdot': u'\uEBD4',
         'condes': u'\uA76F',
         'squarewhsm': u'\u25AB',
         'lovlhigh': u'\uE58C',
         'ast': u'\u002A',
         'slongdes': u'\uF127',
         'AVligdotbl': u'\uEFF8',
         'metrbrevedblgrave': u'\uF718',
         'ET': u'\uF142',
         'Mmacrhigh': u'\uE1B8',
         'metrshortlong': u'\u23D2',
         'acurl': u'\uE433',
         'kunc': u'\uF208',
         'lsqbqu': u'\u2045',
         'oslashdot': u'\uEBCE',
         'etfin': u'\uA76B',
         'uvertline': u'\uE724',
         'ntailstrok': u'\uA774',
         'aoligacute': u'\uEFE3',
         'lmacrhigh': u'\uE596',
         'Wgrave': u'\u1E80',
         'arbar': u'\uF1C0',
         'thornrarmlig': u'\uE8C1',
         'para': u'\u00B6',
         'muncacute': u'\uEBB6',
         'xldes': u'\uF232',
         'Ucirc': u'\u00DB',
         'jsup': u'\uF030',
         'romsiliq': u'\uF2DD',
         'Csqu': u'\uF106',
         'punctelevhack': u'\uF1FB',
         'racute': u'\u0155',
         'yacute': u'\u00FD',
         'aeligmacrbreve': u'\uE43D',
         'romnumCrevovl': u'\uF23F',
         'macr': u'\u00AF',
         'slongchlig': u'\uF4FA',
         'istrok': u'\u0268',
         'gplig': u'\uEAD2',
         'arrsglupw': u'\u2191',
         'lUbrack': u'\u2E26',
         'esse': u'\u2248',
         'rho': u'\u03C1',
         'tcurl': u'\uF199',
         'rscapdotbl': u'\uEF2B',
         'alpha': u'\u03B1',
         'Oslashdot': u'\uEBCD',
         'orumsup': u'\uF03F',
         'oeligacute': u'\uE659',
         'adiaguml': u'\uE8D5',
         'ccurl': u'\uF198',
         'Ddotbl': u'\u1E0C',
         'asup': u'\u0363',
         'commat': u'\u0040',
         'Sdotbl': u'\u1E62',
         'fins': u'\uA77C',
         'slongenl': u'\uEEDF',
         'Ntilde': u'\u00D1',
         'Adotacute': u'\uEBF4',
         'Wdotbl': u'\u1E88',
         'usmod': u'\uA770',
         'condot': u'\uA73F',
         'Prime': u'\u2033',
         'kcurl': u'\uF195',
         'Rtailstrok': u'\u211E',
         'sigmaf': u'\u03C2',
         'Jdblac': u'\uE162',
         'Oslashbreve': u'\uEBEE',
         'yscap': u'\u028F',
         'AOligacute': u'\uEFE2',
         'tscap': u'\u1D1B',
         'Auml': u'\u00C4',
         'Oslashdblac': u'\uEBC6',
         'idblac': u'\uE543',
         'Rrot': u'\uA75A',
         'vins': u'\uA769',
         'allig': u'\uEFA6',
         'tridotdw': u'\u2235',
         'colmidcomposit': u'\uF1E5',
         'emacrbreve': u'\uE4B7',
         'gdivloop': u'\uF21D',
         'hedera': u'\u2766',
         'qdotbl': u'\uE688',
         'Oslash': u'\u00D8',
         'omicron': u'\u03BF',
         'avligogon': u'\uEBF1',
         'Ydotacute': u'\uE384',
         'Pacute': u'\u1E54',
         'aringcirc': u'\uE41F',
         'finssemiclosedot': u'\uEBD5',
         'ffllig': u'\uFB04',
         'metrancacute': u'\uF70B',
         'ttailstrok': u'\uA777',
         'romas': u'\uF2D8',
         'aumlcirc': u'\uE41A',
         'Ugrave': u'\u00D9',
         'Psquirrel': u'\uA754',
         'IJlig': u'\u0132',
         'AEligdot': u'\uE043',
         'pdotbl': u'\uE66D',
         'aaligdot': u'\uEFEF',
         'lbar': u'\u019A',
         'udotbl': u'\u1EE5',
         'odblac': u'\u0151',
         'zeta': u'\u03B6',
         'imacrbreve': u'\uE537',
         'pbardes': u'\uA751',
         'q3app': u'\uE8BF',
         'Easup': u'\uE0E1',
         'Iogon': u'\u012E',
         'Edot': u'\u0116',
         'pi': u'\u03C0',
         'odotbl': u'\u1ECD',
         'Asqu': u'\uF13A',
         'oloop': u'\uA74D',
         'Movlhigh': u'\uE1D2',
         'metrbrevegrave': u'\uF707',
         'romnumDDdbllig': u'\u2182',
         'nmacrmed': u'\uE5DC',
         'AUligacute': u'\uEFE4',
         'ftylig': u'\uEED0',
         'slongoumllig': u'\uEBA4',
         'slongslongilig': u'\uEBA7',
         'pound': u'\u00A3',
         'Hrarmlig': u'\uE8C2',
         'oogondblac': u'\uEBC5',
         'medcom': u'\uF1E0',
         'ldot': u'\uE59E',
         'wdot': u'\u1E87',
         'markold': u'\uF2F0',
         'vscap': u'\u1D20',
         'Amacrbreve': u'\uE010',
         'nbhy': u'\u2011',
         'slongslongllig': u'\uEBA8',
         'slongaumllig': u'\uEBA0',
         'iogon': u'\u012F',
         'Udotacute': u'\uEBFE',
         'facute': u'\uE4F0',
         'HWAIR': u'\u01F6',
         'lovlmed': u'\uE5B1',
         'combdblbrevebl': u'\u035C',
         'dblac': u'\u02DD',
         'anecklesselig': u'\uEFA1',
         'hhalf': u'\u2C76',
         'Oogondblac': u'\uEBC4',
         'nscapsup': u'\u1DE1',
         'rdesstrok': u'\uE7E4',
         'ldotbl': u'\u1E37',
         'Cogon': u'\uE076',
         'gcurl': u'\uF196',
         'baracr': u'\u0336',
         'rwhsqb': u'\u27E7',
         'gdlig': u'\uEED2',
         'msign': u'\uF2F2',
         'eogonenl': u'\uEAF3',
         'aaligdblac': u'\uEFEB',
         'Tacute': u'\uE2E2',
         'quest': u'\u003F',
         'aneckless': u'\uF215',
         'semi': u'\u003B',
         'rdquo': u'\u201D',
         'ppliguml': u'\uEBE7',
         'foumllig': u'\uF1BC',
         'Gdot': u'\u0120',
         'Ains': u'\uF201',
         'schillgermscript': u'\uF2F8',
         'eumlmacr': u'\uE4CD',
         'romquin': u'\uF2DE',
         'oasup': u'\uE643',
         'frac14': u'\u00BC',
         'metrmacrbrevegrave': u'\uF709',
         'frac12': u'\u00BD',
         'gacute': u'\u01F5',
         'punctinterlemn': u'\uF1F1',
         'Omacr': u'\u014C',
         'combced': u'\u0327',
         'combacute': u'\u0301',
         'rum': u'\uA75D',
         'Eogonmacr': u'\uE0BC',
         'ebreve': u'\u0115',
         'qenl': u'\uEEF2',
         'romdupond': u'\uF2DF',
         'nscap': u'\u0274',
         'oogondot': u'\uEBDF',
         'combdotbl': u'\u0323',
         'eogondotacute': u'\uE4EC',
         'ounce': u'\u2125',
         'rangb': u'\u27E9',
         'gamma': u'\u03B3',
         'Kstrascleg': u'\uA744',
         'Gstrok': u'\u01E4',
         'AEligbreve': u'\uE03F',
         'oisup': u'\uE645',
         'punctexclam': u'\uF1E7',
         'Wdot': u'\u1E86',
         'slongslonglig': u'\uEBA6',
         'THORNbar': u'\uA764',
         'colelevposit': u'\uF1E4',
         'Vdot': u'\uE34C',
         'fflig': u'\uFB00',
         'Muncdes': u'\uF224',
         'Ebreve': u'\u0114',
         'drotsup': u'\u1DD8',
         'oogonmacr': u'\u01ED',
         'aaligacute': u'\uEFE1',
         'iusup': u'\uE8E6',
         'rscapdot': u'\uEF22',
         'delta': u'\u03B4',
         'pflour': u'\uA753',
         'metrancdblgrave': u'\uF71A',
         'Xmod': u'\uF1BF',
         'Ograve': u'\u00D2',
         'us': u'\u1DD2',
         'ur': u'\u1DD1',
         'Eogondblac': u'\uE0EA',
         'rsqbqu': u'\u2046',
         'curren': u'\u00A4',
         'aulig': u'\uA737',
         'Acirc': u'\u00C2',
         'oeligmacr': u'\uE65D',
         'Wdblac': u'\uE350',
         'zerosp': u'\u200B',
         'Qslstrok': u'\uA758',
         'sscapdot': u'\uEF23',
         'punctflex': u'\uF1F5',
         'aeligdot': u'\uE443',
         'ldquolow': u'\u201E',
         'aacute': u'\u00E1',
         'lenl': u'\uEEED',
         'dram': u'\uF2E6',
         'slongtlig': u'\uFB05',
         'AEligcurl': u'\uEBEA',
         'mMedrun': u'\u16D8',
         'AOlig': u'\uA734',
         'EZH': u'\u01B7',
         'Oumlmacr': u'\u022A',
         'jnodotenl': u'\uEEFE',
         'Jdotbl': u'\uE151',
         'gt': u'\u003E',
         'kslonglig': u'\uEBAE',
         'aoligdblac': u'\uEBC1',
         'Adotbl': u'\u1EA0',
         'aylig': u'\uA73D',
         'aoligsup': u'\u1DD5',
         'zstrok': u'\u01B6',
         'obreve': u'\u014F',
         'oslashsup': u'\uF032',
         'hyphpoint': u'\u2027',
         'senl': u'\uEEF4',
         'lscapdot': u'\uEBDC',
         'oosup': u'\uE8E9',
         'ersub': u'\u1DCF',
         'gdrotlig': u'\uEED3',
         'combdbvertl': u'\u030E',
         'Ocurlacute': u'\uEBB7',
         'fracsol': u'\u2044',
         'aesup': u'\uE42C',
         'gins': u'\u1D79',
         'ethenl': u'\uEEE5',
         'aeligmacracute': u'\uE43A',
         'bscap': u'\u0299',
         'aeligring': u'\uE8D1',
         'reichtalold': u'\uF2F6',
         'Amacracute': u'\uE00A',
         'slongslongtlig': u'\uF4FF',
         'juml': u'\uEBE3',
         'ucurlbar': u'\uEBBF',
         'Aesup': u'\uE02C',
         'Theta': u'\u0398',
         'bstrok': u'\u0180',
         'ccedilsup': u'\u1DD7',
         'renvoi': u'\uF1EC',
         'Covlhigh': u'\uF7B5',
         'bovlmed': u'\uE44D',
         'Kacute': u'\u1E30',
         'sins': u'\uA785',
         'llslst': u'\u2E1D',
         'thinsp': u'\u2009',
         'oeligdblac': u'\uEBC9',
         'ecirc': u'\u00EA',
         'Ymacracute': u'\uE373',
         'refmark': u'\u203B',
         'slongflour': u'\uE8B7',
         'genl': u'\uEEE8',
         'faumllig': u'\uEEC8',
         'finsclosedot': u'\uEBD6',
         'dotcross': u'\u205C',
         'eogondblac': u'\uE4EA',
         'aeligsup': u'\u1DD4',
         'combuml': u'\u0308',
         'oogoncurl': u'\uE64F',
         'chi': u'\u03C7',
         'dstrok': u'\u0111',
         'trlig': u'\uEED8',
         'emqd': u'\u2001',
         'rUbrack': u'\u2E27',
         'THORNbarslash': u'\uE337',
         'uuml': u'\u00FC',
         'Odotacute': u'\uEBF8',
         'oeligenl': u'\uEFDD',
         'lirasterl': u'\uF2EF',
         'thornenl': u'\uEEF6',
         'trottrotlig': u'\uEEDA',
         'metrdblbrevemacrdblac': u'\uF71C',
         'Vacute': u'\uE33A',
         'oeligmacrbreve': u'\uE660',
         'tau': u'\u03C4',
         'aeliguml': u'\uE442',
         'csup': u'\u0368',
         'inodot': u'\u0131',
         'osup': u'\u0366',
         'YR': u'\u01A6',
         'benl': u'\uEEE1',
         'iesup': u'\uE54A',
         'usup': u'\u0367',
         'THORNdotbl': u'\uE39F',
         'Aogon': u'\u0104',
         'ucurl': u'\uE731',
         'Ahook': u'\u1EA2',
         'Mdot': u'\u1E40',
         'de': u'\uF159',
         'dblsol': u'\u2AFD',
         'fdotbl': u'\uE4EE',
         'metrancdblac': u'\uF719',
         'ahook': u'\u1EA3',
         'grlig': u'\uEAD0',
         'rtailstrok': u'\uA775',
         'fjlig': u'\uEEC9',
         'Finsdotbl': u'\uE3E5',
         'arrsglrw': u'\u2192',
         'Oogon': u'\u01EA',
         'comma': u'\u002C',
         'metrmacr': u'\uF700',
         'oslashogon': u'\uE655',
         'mscap': u'\u1D0D',
         'wisup': u'\uE8F1',
         'Sins': u'\uA784',
         'lt': u'\u003C',
         'Oloop': u'\uA74C',
         'librafren': u'\uF2EB',
         'avligsup': u'\u1DD6',
         'thornscap': u'\uEF15',
         'uacute': u'\u00FA',
         'Oumlcirc': u'\uE22D',
         'florloop': u'\uF2E8',
         'ETHdotbl': u'\uE08F',
         'mscapdot': u'\uEBDD',
         'arrsgllw': u'\u2190',
         'psup': u'\uF025',
         'wosup': u'\uE754',
         'eogondotbl': u'\uE4E8',
         'USbase': u'\uF1A5',
         'AYligdot': u'\uEFF0',
         'Imacracute': u'\uE135',
         'kclose': u'\uF209',
         'rrotacute': u'\uEBB9',
         'agrave': u'\u00E0',
         'Ocirc': u'\u00D4',
         'Emacracute': u'\u1E16',
         'gstrok': u'\u01E5',
         'ouml': u'\u00F6',
         'Uuml': u'\u00DC',
         'AEligogon': u'\uE040',
         'reg': u'\u00AE',
         'thornbar': u'\uA765',
         'AEliguml': u'\uE042',
         'amacracute': u'\uE40A',
         'CONdot': u'\uA73E',
         'Bacute': u'\uE044',
         'ecu': u'\uF2E7',
         'omacr': u'\u014D',
         'eogoncirc': u'\uE49F',
         'dtail': u'\u0256',
         'dscapdotbl': u'\uEF26',
         'finssemiclose': u'\uF21B',
         'combdblac': u'\u030B',
         'Juml': u'\uEBE2',
         'times': u'\u00D7',
         'oslashmacrbreve': u'\uE653',
         'usbase': u'\uF1A6',
         'AEligmacrbreve': u'\uE03D',
         'dcurl': u'\uF193',
         'jesup': u'\uE8E7',
         'nscapldes': u'\uF22B',
         'Yhook': u'\u1EF6',
         'plusmn': u'\u00B1',
         'Uumlmacr': u'\u01D5',
         'frac34': u'\u00BE',
         'Muncacute': u'\uEBB5',
         'Vuml': u'\uE342',
         'Togon': u'\uE2EE',
         'Dacute': u'\uE077',
         'iota': u'\u03B9',
         'Umacr': u'\u016A',
         'Idotbl': u'\u1ECA',
         'luslst': u'\u2E0C',
         'Hdotbl': u'\u1E24',
         'lowbar': u'\u005F',
         'rdot': u'\u1E59',
         'Lturn': u'\uA780',
         'Iuml': u'\u00CF',
         'Dagger': u'\u2021',
         'wasup': u'\uE8F0',
         'metrpause': u'\uF714',
         'ybar': u'\uE77B',
         'lwhsqb': u'\u27E6',
         'rcub': u'\u007D',
         'ddotbl': u'\u1E0D',
         'sem': u'\uF1AC',
         'Odotbl': u'\u1ECC',
         'AEligmacracute': u'\uE03A',
         'Dovlhigh': u'\uF7B6',
         'adotbl': u'\u1EA1',
         'iquest': u'\u00BF',
         'eosup': u'\uE8E3',
         'ydotbl': u'\u1EF5',
         'romnumDDlig': u'\u2181',
         'Vmacr': u'\uE34D',
         'thornovlmed': u'\uE7A2',
         'Cdot': u'\u010A',
         'ginsturn': u'\uA77F',
         'Uogon': u'\u0172',
         'vmacr': u'\uE74D',
         'idotbl': u'\u1ECB',
         'finsdothook': u'\uF21C',
         'movlmed': u'\uE5D2',
         'slongilig': u'\uEBA2',
         'kdot': u'\uE568',
         'fdot': u'\u1E1F',
         'Jovlhigh': u'\uE152',
         'Icurl': u'\uE12A',
         'Wcirc': u'\u0174',
         'slongslig': u'\uF4FD',
         'jovlmed': u'\uE552',
         'aelig': u'\u00E6',
         'Umacrbreve': u'\uE30B',
         'slongacute': u'\uEBAF',
         'Kdotbl': u'\u1E32',
         'hstrok': u'\u0127',
         'umacrbreve': u'\uE70B',
         'ksemiclose': u'\uF221',
         'idotacute': u'\uEBF7',
         'combcirc': u'\u0302',
         'AAligdotbl': u'\uEFF2',
         'oogonsup': u'\uF13E',
         'ETfin': u'\uA76A',
         'sscap': u'\uA731',
         'xsup': u'\u036F',
         'wring': u'\u1E98',
         'arligsup': u'\uF038',
         'umacr': u'\u016B',
         'ybreve': u'\uE776',
         'shy': u'\u00AD',
         'sub1': u'\u2081',
         'drotdot': u'\uEBD1',
         'lturn': u'\uA781',
         'schwa': u'\u0259',
         'OElig': u'\u0152',
         'OBIIT': u'\uA74A',
         'tenl': u'\uEEF5',
         'apos': u'\u0027',
         'Abreveacute': u'\u1EAE',
         'theta': u'\u03B8',
         'Rdot': u'\u1E58',
         'lsquolow': u'\u201A',
         'rringbl': u'\uE6A3',
         'eta': u'\u03B7',
         'uogon': u'\u0173',
         'tld': u'\u007E',
         'pennygerm': u'\u20B0',
         'quaddots': u'\u2E2C',
         'eth': u'\u00F0',
         'obiit': u'\uA74B',
         'Racute': u'\u0154',
         'aeligscap': u'\u1D01',
         'Imacr': u'\u012A',
         'hyphen': u'\u002D',
         'SZlig': u'\u1E9E',
         'venl': u'\uEEF8',
         'Aacute': u'\u00C1',
         'cent': u'\u00A2',
         'wcirc': u'\u0175',
         'cenl': u'\uEEE2',
         'Ginsturn': u'\uA77E',
         'hdot': u'\u1E23',
         'ovlmed': u'\uF00D',
         'jnodotstrok': u'\u025F',
         'Lstrok': u'\u0141',
         'muncdes': u'\uF226',
         'trotsup': u'\uF03B',
         'wsup': u'\uF03C',
         'Fdot': u'\u1E1E',
         'anecklessvlig': u'\uEFA2',
         'fivedots': u'\u2E2D',
         'Vdblac': u'\uE34B',
         'adot': u'\u0227',
         'Oacute': u'\u00D3',
         'punctinter': u'\uF160',
         'virgsusp': u'\uF1F4',
         'odotacute': u'\uEBF9',
         'emsp': u'\u2003',
         'golig': u'\uEEDE',
         'ibrevinvbl': u'\uE548',
         'Oslashmacr': u'\uE252',
         'cross': u'\u271D',
         'acirc': u'\u00E2',
         'phi': u'\u03C6',
         'qbardes': u'\uA757',
         'Ecurl': u'\uE0E9',
         'bglig': u'\uEEC3',
         'abreveacute': u'\u1EAF',
         'Eogondotbl': u'\uE0E8',
         'vdblac': u'\uE74B',
         'Hhalf': u'\u2C75',
         'dblbarbl': u'\u0333',
         'slongsup': u'\u1DE5',
         'ETslash': u'\uF1A7',
         'Oslashmacracute': u'\uEBEC',
         'combastbl': u'\u0359',
         'aalig': u'\uA733',
         'mscapsup': u'\u1DDF',
         'oogon': u'\u01EB',
         'adotacute': u'\uEBF5',
         'pplig': u'\uEED6',
         'qslstrok': u'\uA759',
         'easup': u'\uE4E1',
         'quaddot': u'\u2237',
         'ubreve': u'\u016D',
         'anligsup': u'\uF036',
         'isup': u'\u0365',
         'Eogondot': u'\uE0EB',
         'Zstrok': u'\u01B5',
         'adotbluml': u'\uE41D',
         'zscap': u'\u1D22',
         'ductsimpl': u'\uF1E3',
         'oumlmacr': u'\u022B',
         'Ucurl': u'\uE331',
         'metrmacrdblgrave': u'\uF716',
         'ETH': u'\u00D0',
         'Hdot': u'\u1E22',
         'est': u'\u223B',
         'ocirc': u'\u00F4',
         'Oslashogon': u'\uE255',
         'ruslst': u'\u2E0D',
         'zsup': u'\u1DE6',
         'uesup': u'\uE72B',
         'yyligdblac': u'\uEBCB',
         'mdash': u'\u2014',
         'oumlcirc': u'\uE62D',
         'metrmacrbreveacute': u'\uF708',
         'punctpercont': u'\u2E2E',
         'Ouml': u'\u00D6',
         'macute': u'\u1E3F',
         'Ibreve': u'\u012C',
         'bsol': u'\u005C',
         'avligslashacute': u'\uEBB1',
         'wavylin': u'\uF1F9',
         'Vcirc': u'\uE33B',
         'slongbarslash': u'\u1E9C',
         'tdotbl': u'\u1E6D',
         'eucombcirc': u'\uEBBE',
         'dscapsup': u'\uF016',
         'eenl': u'\uEEE6',
         'yhook': u'\u1EF7',
         'cogon': u'\uE476',
         'tylig': u'\uEEDB',
         'togon': u'\uE6EE',
         'etall': u'\uF21A',
         'tridotsdownw': u'\u2E2A',
         'vslash': u'\uE8BA',
         'ncirc': u'\uE5D7',
         'romscapybar': u'\uF2E3',
         'THORNbardes': u'\uA766',
         'combcircdbl': u'\u1DCD',
         'midring': u'\uF1DA',
         'ymacracute': u'\uE773',
         'hdotbl': u'\u1E25',
         'sestert': u'\uF2FA',
         'hslonglig': u'\uEBAD',
         'gglig': u'\uEED1',
         'ndot': u'\u1E45',
         'aplig': u'\uEFA9',
         'ovsup': u'\uE647',
         'ibreve': u'\u012D',
         'Vinsacute': u'\uEBBA',
         'romsext': u'\uF2DB',
         'oring': u'\uE637',
         'dscript': u'\u1E9F',
         'krarmlig': u'\uE8C5',
         'zdot': u'\u017C',
         'Nacute': u'\u0143',
         'dbloblhyph': u'\u2E17',
         'OOligdotbl': u'\uEFFC',
         'sstrok': u'\uA778',
         'hederarot': u'\u2767',
         }
  
  
  
  codeof = {
         'edotbl': '1EB9',
         'hidot': 'F1F8',
         'slonguumllig': 'EBAB',
         'Idotacute': 'EBF6',
         'combmacr': '0304',
         'wdblac': 'E750',
         'Vmod': 'F1BE',
         'bdotbl': '1E05',
         'thorndotbl': 'E79F',
         'mdot': '1E41',
         'luhsqbNT': '2E00',
         'Kdot': 'E168',
         'tridotupw': '2234',
         'oogonacute': 'E60C',
         'kbar': 'A741',
         'ccedil': '00E7',
         'ascap': '1D00',
         'ouncescript': 'F2FD',
         'Nrdes': 'F229',
         'Ycurl': 'E385',
         'Xovlhigh': 'F7B3',
         'slonghlig': 'EBA1',
         'Qbardes': 'A756',
         'rrotdotbl': 'E7C1',
         'jacute': 'E553',
         'finsdotbl': 'E7E5',
         'Cacute': '0106',
         'ethdotbl': 'E48F',
         'rscap': '0280',
         'THORN': '00DE',
         'equals': '003D',
         'AUligdotbl': 'EFF6',
         'permil': '2030',
         'Agrave': '00C0',
         'Edotacute': 'E0C8',
         'oumlacute': 'E62C',
         'divide': '00F7',
         'ausup': 'E8E1',
         'pscap': '1D18',
         'udotacute': 'EBFF',
         'Oslashdotacute': 'EBFC',
         'fsup': 'F017',
         'oopen': '0254',
         'slongplig': 'EBA5',
         'oscap': '1D0F',
         'Facute': 'E0F0',
         'punctintertilde': 'F1E8',
         'excl': '0021',
         'ttlig': 'EED9',
         'Pdotbl': 'E26D',
         'Eunc': 'F10A',
         'Oesup': 'E244',
         'kslongligbar': 'E7C8',
         'rumsup': 'F040',
         'mu': '03BC',
         'squareblsm': '25AA',
         'Udotbl': '1EE4',
         'punctvers': 'F1EA',
         'ubar': '0289',
         'omacrbreve': 'E61B',
         'fuumllig': 'EECC',
         'dsup': '0369',
         'rpar': '0029',
         'AVlig': 'A738',
         'aeligogonacute': 'E8D3',
         'Uhook': '1EE6',
         'gscap': '0262',
         'eng': '014B',
         'jscap': '1D0A',
         'nrdes': 'F228',
         'Eogondotacute': 'E0EC',
         'rscaptailstrok': 'A776',
         'tridotscomposit': 'F1E6',
         'ocurlacute': 'EBB8',
         'inodotsup': 'F02F',
         'sextans': 'F2FB',
         'aenlacute': 'EAF0',
         'pdot': '1E57',
         'urlemn': 'F1C2',
         'oclig': 'EFAD',
         'fscap': 'A730',
         'metrlongshort': '23D3',
         'AEligacute': '01FC',
         'Ndot': '1E44',
         'oolig': 'A74F',
         'ograve': '00F2',
         'nscapdotbl': 'EF2A',
         'eogonacute': 'E499',
         'OEligdblac': 'EBC8',
         'rlhsqb': '2E25',
         'jnodotsup': 'F031',
         'gscapdotbl': 'EF27',
         'bidotscomposit': 'F1F2',
         'icurl': 'E52A',
         'auligacute': 'EFE5',
         'otilde': '00F5',
         'slongslstrok': 'E8B8',
         'Odblac': '0150',
         'jnodot': '0237',
         'lsup': '1DDD',
         'rsaquo': '203A',
         'AAligdot': 'EFEE',
         'tridotright': '10FB',
         'mtailstrok': 'A773',
         'wenl': 'EEF9',
         'AAligacute': 'EFE0',
         'metrancgrave': 'F70C',
         'resup': 'E8EA',
         'tribull': '2023',
         'jdotbl': 'E551',
         'edotacute': 'E4C8',
         'lcub': '007B',
         'libraflem': 'F2ED',
         'ra': '1DD3',
         'Gins': 'A77D',
         'ijlig': '0133',
         'romnumCDlig': '2180',
         'ydblac': 'E77C',
         'is_': 'A76D',
         'tscapsup': 'F02A',
         'infin': '221E',
         'Wmacr': 'E357',
         'aogon': '0105',
         'kdotbl': '1E33',
         'schillgerm': 'F2F7',
         'wacute': '1E83',
         'Vins': 'A768',
         'kscapdot': 'EBDB',
         'Odot': '022E',
         'aaligdotbl': 'EFF3',
         'hellip': '2026',
         'Oogonmacr': '01EC',
         'Qdotbl': 'E288',
         'aflig': 'EFA3',
         'avligacute': 'EFE7',
         'emacrsup': 'F136',
         'aacloselig': 'EFA0',
         'hscapdot': 'EBDA',
         'oslashmacr': 'E652',
         'Pdot': '1E56',
         'ksup': '1DDC',
         'Nmacrhigh': 'E1DC',
         'circledot': '25CC',
         'Ilong': 'A7FE',
         'fscapdot': 'EBD7',
         'anlig': 'EFA7',
         'aeligred': 'F204',
         'minus': '2212',
         'WYNN': '01F7',
         'cscap': '1D04',
         'eunc': 'F218',
         'omacracute': '1E53',
         'combogon': '0328',
         'edotblacute': 'E498',
         'ldblpar': '2E28',
         'ivsup': 'E54B',
         'combgrave': '0300',
         'nflour': 'F19A',
         'aeligcurl': 'EBEB',
         'AOligdotbl': 'EFF4',
         'Uacute': '00DA',
         'lsaquo': '2039',
         'Uesup': 'E32B',
         'combtripbrevebl': 'F1FC',
         'enqd': '2000',
         'erang': 'F1C7',
         'avligslash': 'A73B',
         'grave': '0060',
         'AOligred': 'F205',
         'aunc': 'F214',
         'lozengedot': '2058',
         'rabar': 'F1C1',
         'wmacr': 'E757',
         'arscapligsup': 'F130',
         'aglig': 'EFA5',
         'sscapdotbl': 'EF2C',
         'dot': '02D9',
         'ltailstrok': 'A772',
         'slongllig': 'EBA3',
         'Atilde': '00C3',
         'slongvinslig': 'EBAC',
         'YYligdblac': 'EBCA',
         'Sclose': 'F126',
         'hrarmlig': 'E8C3',
         'Imod': '1D35',
         'oopenmacr': 'E7CC',
         'menl': 'EEEE',
         'Eacute': '00C9',
         'gscapdot': 'EF20',
         'Frev': 'A7FB',
         'cacute': '0107',
         'aeligacute': '01FD',
         'yenl': 'EEFB',
         'bar': '0305',
         'CONdes': 'A76E',
         'iasup': 'E8E4',
         'Obreve': '014E',
         'libraital': 'F2EC',
         'oesup': 'E644',
         'Dstrok': '0110',
         'vbar': 'E74E',
         'rdotbl': '1E5B',
         'metrmacrbreve': 'F702',
         'iexcl': '00A1',
         'ycurl': 'E785',
         'atilde': '00E3',
         'slongbar': '1E9D',
         'metrmacrgrave': 'F705',
         'Kbar': 'A740',
         'Gsqu': 'F10E',
         'oslashacute': '01FF',
         'uumlcirc': 'E717',
         'Omacracute': '1E52',
         'space': '0020',
         'ymacrbreve': 'E775',
         'Egrave': '00C8',
         'igrave': '00EC',
         'eogondot': 'E4EB',
         'mdotbl': '1E43',
         'punctelevhiback': 'F1FA',
         'Jcurl': 'E163',
         'Fdotbl': 'E0EE',
         'Oogondot': 'EBDE',
         'lpar': '0028',
         'vcirc': 'E73B',
         'Qstem': 'F22C',
         'ubrevinvbl': 'E727',
         'adblac': 'E425',
         'puncsp': '2008',
         'eogonsup': 'F135',
         'avligdotbl': 'EFF9',
         'trileftwh': '25C3',
         'ilong': 'F220',
         'Edblac': 'E0D1',
         'hovlmed': 'E517',
         'udblac': '0171',
         'Imacrbreve': 'E137',
         'AAligdblac': 'EFEA',
         'tscapdot': 'EF24',
         'sdotbl': '1E63',
         'nlfhook': '0272',
         'uhook': '1EE7',
         'Vdotbl': '1E7E',
         'vinsacute': 'EBBB',
         'msignflour': 'F2F3',
         'dtailstrok': 'A771',
         'combring': '030A',
         'udot': 'E715',
         'jdblac': 'E562',
         'wscap': '1D21',
         'ldquo': '201C',
         'Hacute': 'E116',
         'Uvertline': 'E324',
         'rsqb': '005D',
         'Minv': 'A7FD',
         'mscapdotbl': 'EF29',
         'yen': '00A5',
         'ucar': '01D4',
         'khook': '0199',
         'CONbase': '2183',
         'Yloop': '1EFE',
         'tilde': '02DC',
         'imacr': '012B',
         'OOligdblac': 'EFEC',
         'Vinsdot': 'E3E7',
         'oslashdblac': 'EBC7',
         'macrhigh': 'F00A',
         'slongslongklig': 'F4FE',
         'bdot': '1E03',
         'aaligenl': 'EFDF',
         'jcurl': 'E563',
         'esup': '0364',
         'ftlig': 'EECB',
         'scruple': '2108',
         'laquo': '00AB',
         'oenl': 'EEF0',
         'bacute': 'E444',
         'lsquo': '2018',
         'pacute': '1E55',
         'Fins': 'A77B',
         'jmacrmed': 'E554',
         'afinslig': 'EFA4',
         'Amacr': '0100',
         'ffylig': 'EECF',
         'Ldotbl': '1E36',
         'yesup': 'E781',
         'dblldr': '2025',
         'circ': '005E',
         'uml': '00A8',
         'qscap': 'EF0C',
         'aeligdblac': 'E441',
         'Wesup': 'E353',
         'aopen': 'F202',
         'evsup': 'E8E2',
         'M5leg': 'A7FF',
         'lscapdotbl': 'EF28',
         'lscap': '029F',
         'plus': '002B',
         'combdot': '0307',
         'Emacrbreve': 'E0B7',
         'mrdes': 'F223',
         'q2app': 'E8B3',
         'sol': '002F',
         'jenl': 'EEEB',
         'Ydotbl': '1EF4',
         'OOligacute': 'EFE8',
         'sup8': '2078',
         'sup9': '2079',
         'sup0': '2070',
         'sup1': '00B9',
         'sup2': '00B2',
         'sup3': '00B3',
         'sup4': '2074',
         'metrdblbrevemacr': 'F72E',
         'sup6': '2076',
         'sup7': '2077',
         'wvsup': 'E8F3',
         'yloop': '1EFF',
         'uscap': '1D1C',
         'oslashmacracute': 'EBED',
         'sub6': '2086',
         'sub7': '2087',
         'sub4': '2084',
         'sub5': '2085',
         'sub2': '2082',
         'romdimsext': 'F2DC',
         'sub0': '2080',
         'egrave': '00E8',
         'epsilon': '03B5',
         'Udot': 'E315',
         'qsup': 'F033',
         'sub8': '2088',
         'sub9': '2089',
         'aaliguml': 'EFFF',
         'AVligslashacute': 'EBB0',
         'AAlig': 'A732',
         'qvinslig': 'EAD1',
         'revpara': '204B',
         'nacute': '0144',
         'zenl': 'EEFC',
         'AAliguml': 'EFFE',
         'ddot': '1E0B',
         'gsmlowloop': 'F21F',
         'Euml': '00CB',
         'Iacute': '00CD',
         'ercurl': 'F1C8',
         'Bdot': '1E02',
         'nu': '03BD',
         'Adot': '0226',
         'denl': 'EEE3',
         'ousup': 'E646',
         'edot': '0117',
         'omega': '03C9',
         'wgrave': '1E81',
         'aeligdotbl': 'E436',
         'Cdotbl': 'E066',
         'metrshort': '23D1',
         'Kstrleg': 'A742',
         'fylig': 'EECD',
         'Yacute': '00DD',
         'YYlig': 'A760',
         'aeligenl': 'EAF1',
         'dscap': '1D05',
         'punctelevdiag': 'F1F0',
         'OEligmacrbreve': 'E260',
         'Icirc': '00CE',
         'Gdotbl': 'E101',
         'ogon': '02DB',
         'AElig': '00C6',
         'Edotbl': '1EB8',
         'ooligdotbl': 'EFFD',
         'kscapsup': 'F01C',
         'romunc': 'F2D9',
         'combcomma': '0315',
         'iosup': 'E8E5',
         'arscaplig': 'EFAB',
         'ordm': '00BA',
         'ordf': '00AA',
         'grosch': 'F2E9',
         'nsup': '1DE0',
         'ntilde': '00F1',
         'orrotsup': 'F03E',
         'wdotbl': '1E89',
         'eext': 'F219',
         'numdash': '2012',
         'PPlig': 'EEDD',
         'romsemunc': 'F2DA',
         'odot': '022F',
         'langb': '27E8',
         'umacracute': 'E709',
         'Zdotbl': '1E92',
         'oslashdotbl': 'EBE1',
         'odiaguml': 'E8D7',
         'slongdestlig': 'EADA',
         'horbar': '2015',
         'aoligenl': 'EFDE',
         'Vwelsh': '1EFC',
         'percnt': '0025',
         'deg': '00B0',
         'Ldot': 'E19E',
         'Abreve': '0102',
         'gsup': '1DDA',
         'fllig': 'FB02',
         'hrdes': 'F23A',
         'pscapdot': 'EBCF',
         'Emacr': '0112',
         'Lacute': '0139',
         'gdot': '0121',
         'Lovlhigh': 'F7B4',
         'combsgvertl': '030D',
         'tridotsupw': '2E2B',
         'luhsqb': '2E22',
         'finsenl': 'EEFF',
         'ohook': '1ECF',
         'anscapligsup': 'F03A',
         'vsup': '036E',
         'jbar': '0249',
         'Aogonacute': 'E004',
         'vdotbl': '1E7F',
         'vinsdotbl': 'E7E6',
         'Eogonacute': 'E099',
         'zdotbl': '1E93',
         'rdquorev': '201F',
         'ecurl': 'E4E9',
         'Eogon': '0118',
         'fcurl': 'F194',
         'slongklig': 'F4FC',
         'kstrascleg': 'A745',
         'ndotbl': '1E47',
         'combdothigh': 'F1CA',
         'ygrave': '1EF3',
         'etslash': 'F158',
         'AUlig': 'A736',
         'metrmacrdblac': 'F715',
         'Acurl': 'E033',
         'metrbreve': 'F701',
         'Oslashcurl': 'E3D4',
         'uenl': 'EEF7',
         'quot': '0022',
         'trirightwh': '25B9',
         'Ddot': '1E0A',
         'uwsup': 'E8ED',
         'nbsp': '00A0',
         'AVligacute': 'EFE6',
         'OEligmacr': 'E25D',
         'auml': '00E4',
         'Gacute': '01F4',
         'finsclose': 'F207',
         'qdot': 'E682',
         'combhook': '0309',
         'ndash': '2013',
         'Zdot': '017B',
         'Uumlcirc': 'E317',
         'romXbar': 'F2E1',
         'gscapsup': '1DDB',
         'oogondotacute': 'EBFB',
         'kacute': '1E31',
         'logand': '2227',
         'amacrbreve': 'E410',
         'YOGH': '021C',
         'drot': 'A77A',
         'aeligogon': 'E440',
         'nbar': 'E7B2',
         'Finsacute': 'EBB3',
         'oslashogonacute': 'E657',
         'et': '204A',
         'Finsdot': 'EBD3',
         'er': '035B',
         'cklig': 'EEC4',
         'Adblac': 'E025',
         'kenl': 'EEEC',
         'qcentrslstrok': 'E8B4',
         'bscapdotbl': 'EF25',
         'ayligdotbl': 'EFFB',
         'nlrleg': '019E',
         'ENG': '014A',
         'Jbar': '0248',
         'mringbl': 'E5C5',
         'conbase': '2184',
         'lbrk': 'A747',
         'wesup': 'E753',
         'oogoncirc': 'E60E',
         'metrbreveacute': 'F706',
         'xi': '03BE',
         'apomod': '02BC',
         'sgldr': '2024',
         'rins': 'A783',
         'sup5': '2075',
         'hwair': '0195',
         'ring': '02DA',
         'Ubreve': '016C',
         'iovlmed': 'E550',
         'eesup': 'E8E2',
         'drotenl': 'EEE4',
         'nnbsp': '202F',
         'verbarup': '02C8',
         'Uosup': 'E32D',
         'brvbar': '00A6',
         'iacute': '00ED',
         'AOligdblac': 'EBC0',
         'virgmin': 'F1F7',
         'Ndotbl': '1E46',
         'ethscap': '1D06',
         'lsqb': '005B',
         'copy': '00A9',
         'Fturn': '2132',
         'aoligdotbl': 'EFF5',
         'OEligacute': 'E259',
         'uisup': 'E72C',
         'Ucar': '01D3',
         'Macute': '1E3E',
         'slongovlmed': 'E79E',
         'posit': 'F1E2',
         'dacute': 'E477',
         'not_': '00AC',
         'renl': 'EEF3',
         'Qdot': 'E282',
         'dash': '2010',
         'dscapdot': 'EBD2',
         'thornbardes': 'A767',
         'abreve': '0103',
         'aoligred': 'F206',
         'Bdotbl': '1E04',
         'thornslonglig': 'E734',
         'mesup': 'E8E8',
         'aolig': 'A735',
         'metrbrevedblac': 'F717',
         'Idblac': 'E143',
         'Pflour': 'A752',
         'Oslashacute': '01FE',
         'Ohook': '1ECE',
         'Drot': 'A779',
         'beta': '03B2',
         'pmacr': 'E665',
         'Lhighstrok': 'A748',
         'punctelev': 'F161',
         'middot': '00B7',
         'ering': 'E4CF',
         'lringbl': 'E5A4',
         'rsquo': '2019',
         'raquo': '00BB',
         'triast': '2042',
         'Verbar': '2016',
         'uasup': 'E8EB',
         'sigma': '03C3',
         'xmod': '02E3',
         'wynn': '01BF',
         'Jacute': 'E153',
         'AEligdblac': 'E041',
         'psquirrel': 'A755',
         'Sacute': '015A',
         'aenl': 'EEE0',
         'trot': 'A787',
         'ains': 'F200',
         'fenl': 'EEE7',
         'tscapdotbl': 'EF2D',
         'OOliguml': 'EBE4',
         'Ymacr': '0232',
         'glglowloop': 'F21E',
         'arrsgldw': '2193',
         'ruhsqb': '2E23',
         'metrbrevemacr': 'F703',
         'munc': 'F225',
         'sub3': '2083',
         'stlig': 'FB06',
         'hairsp': '200A',
         'dovlmed': 'E491',
         'Wuml': '1E84',
         'prime': '2032',
         'sacute': '015B',
         'tdot': '1E6B',
         'nscapdot': 'EF21',
         'ramus': 'F1DB',
         'sdot': '1E61',
         'athornlig': 'EFAC',
         'Mdotbl': '1E42',
         'numsp': '2007',
         'OOlig': 'A74E',
         'combtilde': '0303',
         'thornsup': 'F03D',
         'metrmacracute': 'F704',
         'Ousup': 'E246',
         'romscapxbar': 'F2E2',
         'Oogonacute': 'E20C',
         'vdot': 'E74C',
         'uumlmacr': '01D6',
         'yuml': '00FF',
         'tsup': '036D',
         'eogonmacr': 'E4BC',
         'slongtilig': 'EBA9',
         'ydotacute': 'E784',
         'markflour': 'F2F1',
         'aeligmacr': '01E3',
         'iscap': '026A',
         'xslashula': 'E8BD',
         'Ihook': '1EC8',
         'AVligogon': 'EBF0',
         'period': '002E',
         'edblac': 'E4D1',
         'colon': '003A',
         'metranc': 'F70A',
         'notequals': '2260',
         'LLwelsh': '1EFA',
         'ihook': '1EC9',
         'macrmed': 'F00B',
         'AEligdotbl': 'E036',
         'ocar': '01D2',
         'ldes': 'F222',
         'Oogondotacute': 'EBFA',
         'wusup': 'E8F2',
         'YYliguml': 'EBE8',
         'vinsdot': 'E7E7',
         'AYlig': 'A73C',
         'avlig': 'A739',
         'breve': '02D8',
         'rflour': 'F19B',
         'kappa': '03BA',
         'RUM': 'A75C',
         'metrdblshortlong': '23D4',
         'AVligdblac': 'EBC2',
         'henl': 'EEE9',
         'Tdotbl': '1E6C',
         'vwelsh': '1EFD',
         'ugrave': '00F9',
         'hacute': 'E516',
         'Prev': 'A7FC',
         'xslashlra': 'E8BE',
         'Munc': 'F11A',
         'thornacute': 'E737',
         'Vdiagstrok': 'A75E',
         'Tdot': '1E6A',
         'slongdotbl': 'E7C2',
         'IS': 'A76C',
         'ssup': '1DE4',
         'Oogoncurl': 'E24F',
         'Sdot': '1E60',
         'rsup': '036C',
         'Omacrbreve': 'E21B',
         'lstrok': '0142',
         'gdotbl': 'E501',
         'rdes': '027C',
         'penningar': 'F2F5',
         'romscapdslash': 'F2E4',
         'urrot': 'F153',
         'Ydblac': 'E37C',
         'parag': 'F1E1',
         'verbarql': '2E20',
         'Yuml': '0178',
         'yyliguml': 'EBE9',
         'xenl': 'EEFA',
         'combcurl': '1DCE',
         'verbarqr': '2E21',
         'psi': '03C8',
         'eisup': 'E4E2',
         'Wacute': '1E82',
         'amp': '0026',
         'Vovlhigh': 'F7B2',
         'inodotenl': 'EEFD',
         'oslash': '00F8',
         'acute': '00B4',
         'frlig': 'EECA',
         'cdot': '010B',
         'Igrave': '00CC',
         'Oslashogonacute': 'E257',
         'AVligslash': 'A73A',
         'smallzero': 'F1BD',
         'AnecklessElig': 'EFAE',
         'Trot': 'A786',
         'combisbelow': '1DD0',
         'oslashcurl': 'E7D4',
         'xscap': 'EF11',
         'thornslongligbar': 'E735',
         'ezh': '0292',
         'Ocar': '01D1',
         'verbar': '007C',
         'llwelsh': '1EFB',
         'Ycirc': '0176',
         'combcurlhigh': 'F1C5',
         'ooligdblac': 'EFED',
         'oslashdotacute': 'EBFD',
         'ydot': '1E8F',
         'eogoncurl': 'EBF3',
         'ethsup': '1DD9',
         'qmacr': 'E681',
         'kscap': '1D0B',
         'combcurlbar': 'F1CC',
         'yylig': 'A761',
         'msup': '036B',
         'drotacute': 'EBB2',
         'Uring': '016E',
         'uvsup': 'E8EC',
         'yring': '1E99',
         'dagger': '2020',
         'Pbardes': 'A750',
         'aogonacute': 'E404',
         'eacute': '00E9',
         'kstrleg': 'A743',
         'Euncclose': 'F217',
         'Hunc': 'F110',
         'fMedrun': '16A0',
         'avligdblac': 'EBC3',
         'emacr': '0113',
         'tzlig': 'EEDC',
         'oeligscap': '0276',
         'ayligdot': 'EFF1',
         'bscapdot': 'EBD0',
         'AEligmacr': '01E2',
         'vring': 'E743',
         'Lbrk': 'A746',
         'Ymacrbreve': 'E375',
         'vdiagstrok': 'A75F',
         'lllig': 'F4F9',
         'uosup': 'E72D',
         'Ocurl': 'E3D3',
         'vvisigot': 'A763',
         'oslashbreve': 'EBEF',
         'kovlmed': 'E7C3',
         'fturn': '214E',
         'ocurl': 'E7D3',
         'Idot': '0130',
         'euml': '00EB',
         'metrdblbrevemacracute': 'F71B',
         'Eogoncurl': 'EBF2',
         'cdotbl': 'E466',
         'ooligacute': 'EFE9',
         'gethlig': 'EED4',
         'eogon': '0119',
         'finsacute': 'EBB4',
         'yogh': '021D',
         'amacr': '0101',
         'Oslashdotbl': 'EBE0',
         'Ecirc': '00CA',
         'yrgmainstrok': 'F233',
         'aosup': 'E42D',
         'thorn': '00FE',
         'hslongligbar': 'E7C7',
         'tacute': 'E6E2',
         'scudi': 'F2F9',
         'aclose': 'F203',
         'escap': '1D07',
         'wuml': '1E85',
         'aring': '00E5',
         'bull': '2022',
         'dollar': '0024',
         'Ydot': '1E8E',
         'vacute': 'E73A',
         'Ygrave': '1EF2',
         'eacombcirc': 'EBBD',
         'dipledot': '22D7',
         'rrot': 'A75B',
         'libradut': 'F2EA',
         'cedil': '00B8',
         'oogondotbl': 'E608',
         'slong': '017F',
         'ymacr': '0233',
         'dblovl': '033F',
         'Rins': 'A782',
         'drotdrotlig': 'EEC6',
         'thornbarslash': 'F149',
         'Jdot': 'E15C',
         'eylig': 'EEC7',
         'Otilde': '00D5',
         'lbbar': '2114',
         'Iovlhigh': 'E150',
         'ycirc': '0177',
         'ctlig': 'EEC5',
         'nringbl': 'E5EE',
         'Oogondotbl': 'E208',
         'slongtrlig': 'EBAA',
         'ooliguml': 'EBE5',
         'arlig': 'EFAA',
         'nenl': 'EEEF',
         'Ybreve': 'E376',
         'vuml': 'E742',
         'ffilig': 'FB03',
         'omacrsup': 'F13F',
         'Udblac': '0170',
         'Aring': '00C5',
         'Vslstrok': '2123',
         'Rdotbl': '1E5A',
         'iuml': '00EF',
         'emsp13': '2004',
         'emsp16': '2006',
         'emsp14': '2005',
         'hsup': '036A',
         'Jmacrhigh': 'E154',
         'nscapslonglig': 'EED5',
         'Umacracute': 'E309',
         'anscaplig': 'EFA8',
         'gopen': '0261',
         'ucirc': '00FB',
         'hhook': '0266',
         'bblig': 'EEC2',
         'nscaprdes': 'F22A',
         'emacracute': '1E17',
         'combtildevert': '033E',
         'romaslibr': 'F2E0',
         'obol': 'F2F4',
         'icirc': '00EE',
         'upsilon': '03C5',
         'liranuov': 'F2EE',
         'ensp': '2002',
         'lambda_': '03BB',
         'rlslst': '2E1C',
         'rsquorev': '201B',
         'rdblpar': '2E29',
         'lscapsup': '1DDE',
         'slongjlig': 'F4FB',
         'llhsqb': '2E24',
         'lhighstrok': 'A749',
         'bsup': 'F012',
         'lacute': '013A',
         'fftlig': 'EECE',
         'hscap': '029C',
         'ysup': 'F02B',
         'avsup': 'E42E',
         'Ccedil': '00C7',
         'Vinsdotbl': 'E3E6',
         'aeligbreve': 'E43F',
         'Vvisigot': 'A762',
         'Oslashmacrbreve': 'E253',
         'barbl': '0332',
         'PPliguml': 'EBE6',
         'AYligdotbl': 'EFFA',
         'ienl': 'EEEA',
         'sect': '00A7',
         'sclose': 'F128',
         'oacute': '00F3',
         'uring': '016F',
         'Rslstrok': '211F',
         'bscapsup': 'F013',
         'oelig': '0153',
         'aisup': 'E8E0',
         'combbreve': '0306',
         'aenlosmalllig': 'EAF2',
         'filig': 'FB01',
         'penl': 'EEF1',
         'ovlhigh': 'F00C',
         'mmacrmed': 'E5B8',
         'rrotsup': '1DE3',
         'micro': '00B5',
         'imacracute': 'E535',
         'szlig': '00DF',
         'qbardestilde': 'E68B',
         'ppflourlig': 'EED7',
         'auligdotbl': 'EFF7',
         'finsdot': 'EBD4',
         'condes': 'A76F',
         'squarewhsm': '25AB',
         'lovlhigh': 'E58C',
         'ast': '002A',
         'slongdes': 'F127',
         'AVligdotbl': 'EFF8',
         'metrbrevedblgrave': 'F718',
         'ET': 'F142',
         'Mmacrhigh': 'E1B8',
         'metrshortlong': '23D2',
         'acurl': 'E433',
         'kunc': 'F208',
         'lsqbqu': '2045',
         'oslashdot': 'EBCE',
         'etfin': 'A76B',
         'uvertline': 'E724',
         'ntailstrok': 'A774',
         'aoligacute': 'EFE3',
         'lmacrhigh': 'E596',
         'Wgrave': '1E80',
         'arbar': 'F1C0',
         'thornrarmlig': 'E8C1',
         'para': '00B6',
         'muncacute': 'EBB6',
         'xldes': 'F232',
         'Ucirc': '00DB',
         'jsup': 'F030',
         'romsiliq': 'F2DD',
         'Csqu': 'F106',
         'punctelevhack': 'F1FB',
         'racute': '0155',
         'yacute': '00FD',
         'aeligmacrbreve': 'E43D',
         'romnumCrevovl': 'F23F',
         'macr': '00AF',
         'slongchlig': 'F4FA',
         'istrok': '0268',
         'gplig': 'EAD2',
         'arrsglupw': '2191',
         'lUbrack': '2E26',
         'esse': '2248',
         'rho': '03C1',
         'tcurl': 'F199',
         'rscapdotbl': 'EF2B',
         'alpha': '03B1',
         'Oslashdot': 'EBCD',
         'orumsup': 'F03F',
         'oeligacute': 'E659',
         'adiaguml': 'E8D5',
         'ccurl': 'F198',
         'Ddotbl': '1E0C',
         'asup': '0363',
         'commat': '0040',
         'Sdotbl': '1E62',
         'fins': 'A77C',
         'slongenl': 'EEDF',
         'Ntilde': '00D1',
         'Adotacute': 'EBF4',
         'Wdotbl': '1E88',
         'usmod': 'A770',
         'condot': 'A73F',
         'Prime': '2033',
         'kcurl': 'F195',
         'Rtailstrok': '211E',
         'sigmaf': '03C2',
         'Jdblac': 'E162',
         'Oslashbreve': 'EBEE',
         'yscap': '028F',
         'AOligacute': 'EFE2',
         'tscap': '1D1B',
         'Auml': '00C4',
         'Oslashdblac': 'EBC6',
         'idblac': 'E543',
         'Rrot': 'A75A',
         'vins': 'A769',
         'allig': 'EFA6',
         'tridotdw': '2235',
         'colmidcomposit': 'F1E5',
         'emacrbreve': 'E4B7',
         'gdivloop': 'F21D',
         'hedera': '2766',
         'qdotbl': 'E688',
         'Oslash': '00D8',
         'omicron': '03BF',
         'avligogon': 'EBF1',
         'Ydotacute': 'E384',
         'Pacute': '1E54',
         'aringcirc': 'E41F',
         'finssemiclosedot': 'EBD5',
         'ffllig': 'FB04',
         'metrancacute': 'F70B',
         'ttailstrok': 'A777',
         'romas': 'F2D8',
         'aumlcirc': 'E41A',
         'Ugrave': '00D9',
         'Psquirrel': 'A754',
         'IJlig': '0132',
         'AEligdot': 'E043',
         'pdotbl': 'E66D',
         'aaligdot': 'EFEF',
         'lbar': '019A',
         'udotbl': '1EE5',
         'odblac': '0151',
         'zeta': '03B6',
         'imacrbreve': 'E537',
         'pbardes': 'A751',
         'q3app': 'E8BF',
         'Easup': 'E0E1',
         'Iogon': '012E',
         'Edot': '0116',
         'pi': '03C0',
         'odotbl': '1ECD',
         'Asqu': 'F13A',
         'oloop': 'A74D',
         'Movlhigh': 'E1D2',
         'metrbrevegrave': 'F707',
         'romnumDDdbllig': '2182',
         'nmacrmed': 'E5DC',
         'AUligacute': 'EFE4',
         'ftylig': 'EED0',
         'slongoumllig': 'EBA4',
         'slongslongilig': 'EBA7',
         'pound': '00A3',
         'Hrarmlig': 'E8C2',
         'oogondblac': 'EBC5',
         'medcom': 'F1E0',
         'ldot': 'E59E',
         'wdot': '1E87',
         'markold': 'F2F0',
         'vscap': '1D20',
         'Amacrbreve': 'E010',
         'nbhy': '2011',
         'slongslongllig': 'EBA8',
         'slongaumllig': 'EBA0',
         'iogon': '012F',
         'Udotacute': 'EBFE',
         'facute': 'E4F0',
         'HWAIR': '01F6',
         'lovlmed': 'E5B1',
         'combdblbrevebl': '035C',
         'dblac': '02DD',
         'anecklesselig': 'EFA1',
         'hhalf': '2C76',
         'Oogondblac': 'EBC4',
         'nscapsup': '1DE1',
         'rdesstrok': 'E7E4',
         'ldotbl': '1E37',
         'Cogon': 'E076',
         'gcurl': 'F196',
         'baracr': '0336',
         'rwhsqb': '27E7',
         'gdlig': 'EED2',
         'msign': 'F2F2',
         'eogonenl': 'EAF3',
         'aaligdblac': 'EFEB',
         'Tacute': 'E2E2',
         'quest': '003F',
         'aneckless': 'F215',
         'semi': '003B',
         'rdquo': '201D',
         'ppliguml': 'EBE7',
         'foumllig': 'F1BC',
         'Gdot': '0120',
         'Ains': 'F201',
         'schillgermscript': 'F2F8',
         'eumlmacr': 'E4CD',
         'romquin': 'F2DE',
         'oasup': 'E643',
         'frac14': '00BC',
         'metrmacrbrevegrave': 'F709',
         'frac12': '00BD',
         'gacute': '01F5',
         'punctinterlemn': 'F1F1',
         'Omacr': '014C',
         'combced': '0327',
         'combacute': '0301',
         'rum': 'A75D',
         'Eogonmacr': 'E0BC',
         'ebreve': '0115',
         'qenl': 'EEF2',
         'romdupond': 'F2DF',
         'nscap': '0274',
         'oogondot': 'EBDF',
         'combdotbl': '0323',
         'eogondotacute': 'E4EC',
         'ounce': '2125',
         'rangb': '27E9',
         'gamma': '03B3',
         'Kstrascleg': 'A744',
         'Gstrok': '01E4',
         'AEligbreve': 'E03F',
         'oisup': 'E645',
         'punctexclam': 'F1E7',
         'Wdot': '1E86',
         'slongslonglig': 'EBA6',
         'THORNbar': 'A764',
         'colelevposit': 'F1E4',
         'Vdot': 'E34C',
         'fflig': 'FB00',
         'Muncdes': 'F224',
         'Ebreve': '0114',
         'drotsup': '1DD8',
         'oogonmacr': '01ED',
         'aaligacute': 'EFE1',
         'iusup': 'E8E6',
         'rscapdot': 'EF22',
         'delta': '03B4',
         'pflour': 'A753',
         'metrancdblgrave': 'F71A',
         'Xmod': 'F1BF',
         'Ograve': '00D2',
         'us': '1DD2',
         'ur': '1DD1',
         'Eogondblac': 'E0EA',
         'rsqbqu': '2046',
         'curren': '00A4',
         'aulig': 'A737',
         'Acirc': '00C2',
         'oeligmacr': 'E65D',
         'Wdblac': 'E350',
         'zerosp': '200B',
         'Qslstrok': 'A758',
         'sscapdot': 'EF23',
         'punctflex': 'F1F5',
         'aeligdot': 'E443',
         'ldquolow': '201E',
         'aacute': '00E1',
         'lenl': 'EEED',
         'dram': 'F2E6',
         'slongtlig': 'FB05',
         'AEligcurl': 'EBEA',
         'mMedrun': '16D8',
         'AOlig': 'A734',
         'EZH': '01B7',
         'Oumlmacr': '022A',
         'jnodotenl': 'EEFE',
         'Jdotbl': 'E151',
         'gt': '003E',
         'kslonglig': 'EBAE',
         'aoligdblac': 'EBC1',
         'Adotbl': '1EA0',
         'aylig': 'A73D',
         'aoligsup': '1DD5',
         'zstrok': '01B6',
         'obreve': '014F',
         'oslashsup': 'F032',
         'hyphpoint': '2027',
         'senl': 'EEF4',
         'lscapdot': 'EBDC',
         'oosup': 'E8E9',
         'ersub': '1DCF',
         'gdrotlig': 'EED3',
         'combdbvertl': '030E',
         'Ocurlacute': 'EBB7',
         'fracsol': '2044',
         'aesup': 'E42C',
         'gins': '1D79',
         'ethenl': 'EEE5',
         'aeligmacracute': 'E43A',
         'bscap': '0299',
         'aeligring': 'E8D1',
         'reichtalold': 'F2F6',
         'Amacracute': 'E00A',
         'slongslongtlig': 'F4FF',
         'juml': 'EBE3',
         'ucurlbar': 'EBBF',
         'Aesup': 'E02C',
         'Theta': '0398',
         'bstrok': '0180',
         'ccedilsup': '1DD7',
         'renvoi': 'F1EC',
         'Covlhigh': 'F7B5',
         'bovlmed': 'E44D',
         'Kacute': '1E30',
         'sins': 'A785',
         'llslst': '2E1D',
         'thinsp': '2009',
         'oeligdblac': 'EBC9',
         'ecirc': '00EA',
         'Ymacracute': 'E373',
         'refmark': '203B',
         'slongflour': 'E8B7',
         'genl': 'EEE8',
         'faumllig': 'EEC8',
         'finsclosedot': 'EBD6',
         'dotcross': '205C',
         'eogondblac': 'E4EA',
         'aeligsup': '1DD4',
         'combuml': '0308',
         'oogoncurl': 'E64F',
         'chi': '03C7',
         'dstrok': '0111',
         'trlig': 'EED8',
         'emqd': '2001',
         'rUbrack': '2E27',
         'THORNbarslash': 'E337',
         'uuml': '00FC',
         'Odotacute': 'EBF8',
         'oeligenl': 'EFDD',
         'lirasterl': 'F2EF',
         'thornenl': 'EEF6',
         'trottrotlig': 'EEDA',
         'metrdblbrevemacrdblac': 'F71C',
         'Vacute': 'E33A',
         'oeligmacrbreve': 'E660',
         'tau': '03C4',
         'aeliguml': 'E442',
         'csup': '0368',
         'inodot': '0131',
         'osup': '0366',
         'YR': '01A6',
         'benl': 'EEE1',
         'iesup': 'E54A',
         'usup': '0367',
         'THORNdotbl': 'E39F',
         'Aogon': '0104',
         'ucurl': 'E731',
         'Ahook': '1EA2',
         'Mdot': '1E40',
         'de': 'F159',
         'dblsol': '2AFD',
         'fdotbl': 'E4EE',
         'metrancdblac': 'F719',
         'ahook': '1EA3',
         'grlig': 'EAD0',
         'rtailstrok': 'A775',
         'fjlig': 'EEC9',
         'Finsdotbl': 'E3E5',
         'arrsglrw': '2192',
         'Oogon': '01EA',
         'comma': '002C',
         'metrmacr': 'F700',
         'oslashogon': 'E655',
         'mscap': '1D0D',
         'wisup': 'E8F1',
         'Sins': 'A784',
         'lt': '003C',
         'Oloop': 'A74C',
         'librafren': 'F2EB',
         'avligsup': '1DD6',
         'thornscap': 'EF15',
         'uacute': '00FA',
         'Oumlcirc': 'E22D',
         'florloop': 'F2E8',
         'ETHdotbl': 'E08F',
         'mscapdot': 'EBDD',
         'arrsgllw': '2190',
         'psup': 'F025',
         'wosup': 'E754',
         'eogondotbl': 'E4E8',
         'USbase': 'F1A5',
         'AYligdot': 'EFF0',
         'Imacracute': 'E135',
         'kclose': 'F209',
         'rrotacute': 'EBB9',
         'agrave': '00E0',
         'Ocirc': '00D4',
         'Emacracute': '1E16',
         'gstrok': '01E5',
         'ouml': '00F6',
         'Uuml': '00DC',
         'AEligogon': 'E040',
         'reg': '00AE',
         'thornbar': 'A765',
         'AEliguml': 'E042',
         'amacracute': 'E40A',
         'CONdot': 'A73E',
         'Bacute': 'E044',
         'ecu': 'F2E7',
         'omacr': '014D',
         'eogoncirc': 'E49F',
         'dtail': '0256',
         'dscapdotbl': 'EF26',
         'finssemiclose': 'F21B',
         'combdblac': '030B',
         'Juml': 'EBE2',
         'times': '00D7',
         'oslashmacrbreve': 'E653',
         'usbase': 'F1A6',
         'AEligmacrbreve': 'E03D',
         'dcurl': 'F193',
         'jesup': 'E8E7',
         'nscapldes': 'F22B',
         'Yhook': '1EF6',
         'plusmn': '00B1',
         'Uumlmacr': '01D5',
         'frac34': '00BE',
         'Muncacute': 'EBB5',
         'Vuml': 'E342',
         'Togon': 'E2EE',
         'Dacute': 'E077',
         'iota': '03B9',
         'Umacr': '016A',
         'Idotbl': '1ECA',
         'luslst': '2E0C',
         'Hdotbl': '1E24',
         'lowbar': '005F',
         'rdot': '1E59',
         'Lturn': 'A780',
         'Iuml': '00CF',
         'Dagger': '2021',
         'wasup': 'E8F0',
         'metrpause': 'F714',
         'ybar': 'E77B',
         'lwhsqb': '27E6',
         'rcub': '007D',
         'ddotbl': '1E0D',
         'sem': 'F1AC',
         'Odotbl': '1ECC',
         'AEligmacracute': 'E03A',
         'Dovlhigh': 'F7B6',
         'adotbl': '1EA1',
         'iquest': '00BF',
         'eosup': 'E8E3',
         'ydotbl': '1EF5',
         'romnumDDlig': '2181',
         'Vmacr': 'E34D',
         'thornovlmed': 'E7A2',
         'Cdot': '010A',
         'ginsturn': 'A77F',
         'Uogon': '0172',
         'vmacr': 'E74D',
         'idotbl': '1ECB',
         'finsdothook': 'F21C',
         'movlmed': 'E5D2',
         'slongilig': 'EBA2',
         'kdot': 'E568',
         'fdot': '1E1F',
         'Jovlhigh': 'E152',
         'Icurl': 'E12A',
         'Wcirc': '0174',
         'slongslig': 'F4FD',
         'jovlmed': 'E552',
         'aelig': '00E6',
         'Umacrbreve': 'E30B',
         'slongacute': 'EBAF',
         'Kdotbl': '1E32',
         'hstrok': '0127',
         'umacrbreve': 'E70B',
         'ksemiclose': 'F221',
         'idotacute': 'EBF7',
         'combcirc': '0302',
         'AAligdotbl': 'EFF2',
         'oogonsup': 'F13E',
         'ETfin': 'A76A',
         'sscap': 'A731',
         'xsup': '036F',
         'wring': '1E98',
         'arligsup': 'F038',
         'umacr': '016B',
         'ybreve': 'E776',
         'shy': '00AD',
         'sub1': '2081',
         'drotdot': 'EBD1',
         'lturn': 'A781',
         'schwa': '0259',
         'OElig': '0152',
         'OBIIT': 'A74A',
         'tenl': 'EEF5',
         'apos': '0027',
         'Abreveacute': '1EAE',
         'theta': '03B8',
         'Rdot': '1E58',
         'lsquolow': '201A',
         'rringbl': 'E6A3',
         'eta': '03B7',
         'uogon': '0173',
         'tld': '007E',
         'pennygerm': '20B0',
         'quaddots': '2E2C',
         'eth': '00F0',
         'obiit': 'A74B',
         'Racute': '0154',
         'aeligscap': '1D01',
         'Imacr': '012A',
         'hyphen': '002D',
         'SZlig': '1E9E',
         'venl': 'EEF8',
         'Aacute': '00C1',
         'cent': '00A2',
         'wcirc': '0175',
         'cenl': 'EEE2',
         'Ginsturn': 'A77E',
         'hdot': '1E23',
         'ovlmed': 'F00D',
         'jnodotstrok': '025F',
         'Lstrok': '0141',
         'muncdes': 'F226',
         'trotsup': 'F03B',
         'wsup': 'F03C',
         'Fdot': '1E1E',
         'anecklessvlig': 'EFA2',
         'fivedots': '2E2D',
         'Vdblac': 'E34B',
         'adot': '0227',
         'Oacute': '00D3',
         'punctinter': 'F160',
         'virgsusp': 'F1F4',
         'odotacute': 'EBF9',
         'emsp': '2003',
         'golig': 'EEDE',
         'ibrevinvbl': 'E548',
         'Oslashmacr': 'E252',
         'cross': '271D',
         'acirc': '00E2',
         'phi': '03C6',
         'qbardes': 'A757',
         'Ecurl': 'E0E9',
         'bglig': 'EEC3',
         'abreveacute': '1EAF',
         'Eogondotbl': 'E0E8',
         'vdblac': 'E74B',
         'Hhalf': '2C75',
         'dblbarbl': '0333',
         'slongsup': '1DE5',
         'ETslash': 'F1A7',
         'Oslashmacracute': 'EBEC',
         'combastbl': '0359',
         'aalig': 'A733',
         'mscapsup': '1DDF',
         'oogon': '01EB',
         'adotacute': 'EBF5',
         'pplig': 'EED6',
         'qslstrok': 'A759',
         'easup': 'E4E1',
         'quaddot': '2237',
         'ubreve': '016D',
         'anligsup': 'F036',
         'isup': '0365',
         'Eogondot': 'E0EB',
         'Zstrok': '01B5',
         'adotbluml': 'E41D',
         'zscap': '1D22',
         'ductsimpl': 'F1E3',
         'oumlmacr': '022B',
         'Ucurl': 'E331',
         'metrmacrdblgrave': 'F716',
         'ETH': '00D0',
         'Hdot': '1E22',
         'est': '223B',
         'ocirc': '00F4',
         'Oslashogon': 'E255',
         'ruslst': '2E0D',
         'zsup': '1DE6',
         'uesup': 'E72B',
         'yyligdblac': 'EBCB',
         'mdash': '2014',
         'oumlcirc': 'E62D',
         'metrmacrbreveacute': 'F708',
         'punctpercont': '2E2E',
         'Ouml': '00D6',
         'macute': '1E3F',
         'Ibreve': '012C',
         'bsol': '005C',
         'avligslashacute': 'EBB1',
         'wavylin': 'F1F9',
         'Vcirc': 'E33B',
         'slongbarslash': '1E9C',
         'tdotbl': '1E6D',
         'eucombcirc': 'EBBE',
         'dscapsup': 'F016',
         'eenl': 'EEE6',
         'yhook': '1EF7',
         'cogon': 'E476',
         'tylig': 'EEDB',
         'togon': 'E6EE',
         'etall': 'F21A',
         'tridotsdownw': '2E2A',
         'vslash': 'E8BA',
         'ncirc': 'E5D7',
         'romscapybar': 'F2E3',
         'THORNbardes': 'A766',
         'combcircdbl': '1DCD',
         'midring': 'F1DA',
         'ymacracute': 'E773',
         'hdotbl': '1E25',
         'sestert': 'F2FA',
         'hslonglig': 'EBAD',
         'gglig': 'EED1',
         'ndot': '1E45',
         'aplig': 'EFA9',
         'ovsup': 'E647',
         'ibreve': '012D',
         'Vinsacute': 'EBBA',
         'romsext': 'F2DB',
         'oring': 'E637',
         'dscript': '1E9F',
         'krarmlig': 'E8C5',
         'zdot': '017C',
         'Nacute': '0143',
         'dbloblhyph': '2E17',
         'OOligdotbl': 'EFFC',
         'sstrok': 'A778',
         'hederarot': '2767',
         }
  
  
  
  nameof = {
         u'\u1EB9': 'edotbl',
         u'\uF1F8': 'hidot',
         u'\uEBAB': 'slonguumllig',
         u'\uEBF6': 'Idotacute',
         u'\u0304': 'combmacr',
         u'\uE750': 'wdblac',
         u'\uF1BE': 'Vmod',
         u'\u1E05': 'bdotbl',
         u'\uE79F': 'thorndotbl',
         u'\u1E41': 'mdot',
         u'\u2E00': 'luhsqbNT',
         u'\uE168': 'Kdot',
         u'\u2234': 'tridotupw',
         u'\uE60C': 'oogonacute',
         u'\uA741': 'kbar',
         u'\u00E7': 'ccedil',
         u'\u1D00': 'ascap',
         u'\uF2FD': 'ouncescript',
         u'\uF229': 'Nrdes',
         u'\uE385': 'Ycurl',
         u'\uF7B3': 'Xovlhigh',
         u'\uEBA1': 'slonghlig',
         u'\uA756': 'Qbardes',
         u'\uE7C1': 'rrotdotbl',
         u'\uE553': 'jacute',
         u'\uE7E5': 'finsdotbl',
         u'\u0106': 'Cacute',
         u'\uE48F': 'ethdotbl',
         u'\u0280': 'rscap',
         u'\u00DE': 'THORN',
         u'\u003D': 'equals',
         u'\uEFF6': 'AUligdotbl',
         u'\u2030': 'permil',
         u'\u00C0': 'Agrave',
         u'\uE0C8': 'Edotacute',
         u'\uE62C': 'oumlacute',
         u'\u00F7': 'divide',
         u'\uE8E1': 'ausup',
         u'\u1D18': 'pscap',
         u'\uEBFF': 'udotacute',
         u'\uEBFC': 'Oslashdotacute',
         u'\uF017': 'fsup',
         u'\u0254': 'oopen',
         u'\uEBA5': 'slongplig',
         u'\u1D0F': 'oscap',
         u'\uE0F0': 'Facute',
         u'\uF1E8': 'punctintertilde',
         u'\u0021': 'excl',
         u'\uEED9': 'ttlig',
         u'\uE26D': 'Pdotbl',
         u'\uF10A': 'Eunc',
         u'\uE244': 'Oesup',
         u'\uE7C8': 'kslongligbar',
         u'\uF040': 'rumsup',
         u'\u03BC': 'mu',
         u'\u25AA': 'squareblsm',
         u'\u1EE4': 'Udotbl',
         u'\uF1EA': 'punctvers',
         u'\u0289': 'ubar',
         u'\uE61B': 'omacrbreve',
         u'\uEECC': 'fuumllig',
         u'\u0369': 'dsup',
         u'\u0029': 'rpar',
         u'\uA738': 'AVlig',
         u'\uE8D3': 'aeligogonacute',
         u'\u1EE6': 'Uhook',
         u'\u0262': 'gscap',
         u'\u014B': 'eng',
         u'\u1D0A': 'jscap',
         u'\uF228': 'nrdes',
         u'\uE0EC': 'Eogondotacute',
         u'\uA776': 'rscaptailstrok',
         u'\uF1E6': 'tridotscomposit',
         u'\uEBB8': 'ocurlacute',
         u'\uF02F': 'inodotsup',
         u'\uF2FB': 'sextans',
         u'\uEAF0': 'aenlacute',
         u'\u1E57': 'pdot',
         u'\uF1C2': 'urlemn',
         u'\uEFAD': 'oclig',
         u'\uA730': 'fscap',
         u'\u23D3': 'metrlongshort',
         u'\u01FC': 'AEligacute',
         u'\u1E44': 'Ndot',
         u'\uA74F': 'oolig',
         u'\u00F2': 'ograve',
         u'\uEF2A': 'nscapdotbl',
         u'\uE499': 'eogonacute',
         u'\uEBC8': 'OEligdblac',
         u'\u2E25': 'rlhsqb',
         u'\uF031': 'jnodotsup',
         u'\uEF27': 'gscapdotbl',
         u'\uF1F2': 'bidotscomposit',
         u'\uE52A': 'icurl',
         u'\uEFE5': 'auligacute',
         u'\u00F5': 'otilde',
         u'\uE8B8': 'slongslstrok',
         u'\u0150': 'Odblac',
         u'\u0237': 'jnodot',
         u'\u1DDD': 'lsup',
         u'\u203A': 'rsaquo',
         u'\uEFEE': 'AAligdot',
         u'\u10FB': 'tridotright',
         u'\uA773': 'mtailstrok',
         u'\uEEF9': 'wenl',
         u'\uEFE0': 'AAligacute',
         u'\uF70C': 'metrancgrave',
         u'\uE8EA': 'resup',
         u'\u2023': 'tribull',
         u'\uE551': 'jdotbl',
         u'\uE4C8': 'edotacute',
         u'\u007B': 'lcub',
         u'\uF2ED': 'libraflem',
         u'\u1DD3': 'ra',
         u'\uA77D': 'Gins',
         u'\u0133': 'ijlig',
         u'\u2180': 'romnumCDlig',
         u'\uE77C': 'ydblac',
         u'\uA76D': 'is_',
         u'\uF02A': 'tscapsup',
         u'\u221E': 'infin',
         u'\uE357': 'Wmacr',
         u'\u0105': 'aogon',
         u'\u1E33': 'kdotbl',
         u'\uF2F7': 'schillgerm',
         u'\u1E83': 'wacute',
         u'\uA768': 'Vins',
         u'\uEBDB': 'kscapdot',
         u'\u022E': 'Odot',
         u'\uEFF3': 'aaligdotbl',
         u'\u2026': 'hellip',
         u'\u01EC': 'Oogonmacr',
         u'\uE288': 'Qdotbl',
         u'\uEFA3': 'aflig',
         u'\uEFE7': 'avligacute',
         u'\uF136': 'emacrsup',
         u'\uEFA0': 'aacloselig',
         u'\uEBDA': 'hscapdot',
         u'\uE652': 'oslashmacr',
         u'\u1E56': 'Pdot',
         u'\u1DDC': 'ksup',
         u'\uE1DC': 'Nmacrhigh',
         u'\u25CC': 'circledot',
         u'\uA7FE': 'Ilong',
         u'\uEBD7': 'fscapdot',
         u'\uEFA7': 'anlig',
         u'\uF204': 'aeligred',
         u'\u2212': 'minus',
         u'\u01F7': 'WYNN',
         u'\u1D04': 'cscap',
         u'\uF218': 'eunc',
         u'\u1E53': 'omacracute',
         u'\u0328': 'combogon',
         u'\uE498': 'edotblacute',
         u'\u2E28': 'ldblpar',
         u'\uE54B': 'ivsup',
         u'\u0300': 'combgrave',
         u'\uF19A': 'nflour',
         u'\uEBEB': 'aeligcurl',
         u'\uEFF4': 'AOligdotbl',
         u'\u00DA': 'Uacute',
         u'\u2039': 'lsaquo',
         u'\uE32B': 'Uesup',
         u'\uF1FC': 'combtripbrevebl',
         u'\u2000': 'enqd',
         u'\uF1C7': 'erang',
         u'\uA73B': 'avligslash',
         u'\u0060': 'grave',
         u'\uF205': 'AOligred',
         u'\uF214': 'aunc',
         u'\u2058': 'lozengedot',
         u'\uF1C1': 'rabar',
         u'\uE757': 'wmacr',
         u'\uF130': 'arscapligsup',
         u'\uEFA5': 'aglig',
         u'\uEF2C': 'sscapdotbl',
         u'\u02D9': 'dot',
         u'\uA772': 'ltailstrok',
         u'\uEBA3': 'slongllig',
         u'\u00C3': 'Atilde',
         u'\uEBAC': 'slongvinslig',
         u'\uEBCA': 'YYligdblac',
         u'\uF126': 'Sclose',
         u'\uE8C3': 'hrarmlig',
         u'\u1D35': 'Imod',
         u'\uE7CC': 'oopenmacr',
         u'\uEEEE': 'menl',
         u'\u00C9': 'Eacute',
         u'\uEF20': 'gscapdot',
         u'\uA7FB': 'Frev',
         u'\u0107': 'cacute',
         u'\u01FD': 'aeligacute',
         u'\uEEFB': 'yenl',
         u'\u0305': 'bar',
         u'\uA76E': 'CONdes',
         u'\uE8E4': 'iasup',
         u'\u014E': 'Obreve',
         u'\uF2EC': 'libraital',
         u'\uE644': 'oesup',
         u'\u0110': 'Dstrok',
         u'\uE74E': 'vbar',
         u'\u1E5B': 'rdotbl',
         u'\uF702': 'metrmacrbreve',
         u'\u00A1': 'iexcl',
         u'\uE785': 'ycurl',
         u'\u00E3': 'atilde',
         u'\u1E9D': 'slongbar',
         u'\uF705': 'metrmacrgrave',
         u'\uA740': 'Kbar',
         u'\uF10E': 'Gsqu',
         u'\u01FF': 'oslashacute',
         u'\uE717': 'uumlcirc',
         u'\u1E52': 'Omacracute',
         u'\u0020': 'space',
         u'\uE775': 'ymacrbreve',
         u'\u00C8': 'Egrave',
         u'\u00EC': 'igrave',
         u'\uE4EB': 'eogondot',
         u'\u1E43': 'mdotbl',
         u'\uF1FA': 'punctelevhiback',
         u'\uE163': 'Jcurl',
         u'\uE0EE': 'Fdotbl',
         u'\uEBDE': 'Oogondot',
         u'\u0028': 'lpar',
         u'\uE73B': 'vcirc',
         u'\uF22C': 'Qstem',
         u'\uE727': 'ubrevinvbl',
         u'\uE425': 'adblac',
         u'\u2008': 'puncsp',
         u'\uF135': 'eogonsup',
         u'\uEFF9': 'avligdotbl',
         u'\u25C3': 'trileftwh',
         u'\uF220': 'ilong',
         u'\uE0D1': 'Edblac',
         u'\uE517': 'hovlmed',
         u'\u0171': 'udblac',
         u'\uE137': 'Imacrbreve',
         u'\uEFEA': 'AAligdblac',
         u'\uEF24': 'tscapdot',
         u'\u1E63': 'sdotbl',
         u'\u0272': 'nlfhook',
         u'\u1EE7': 'uhook',
         u'\u1E7E': 'Vdotbl',
         u'\uEBBB': 'vinsacute',
         u'\uF2F3': 'msignflour',
         u'\uA771': 'dtailstrok',
         u'\u030A': 'combring',
         u'\uE715': 'udot',
         u'\uE562': 'jdblac',
         u'\u1D21': 'wscap',
         u'\u201C': 'ldquo',
         u'\uE116': 'Hacute',
         u'\uE324': 'Uvertline',
         u'\u005D': 'rsqb',
         u'\uA7FD': 'Minv',
         u'\uEF29': 'mscapdotbl',
         u'\u00A5': 'yen',
         u'\u01D4': 'ucar',
         u'\u0199': 'khook',
         u'\u2183': 'CONbase',
         u'\u1EFE': 'Yloop',
         u'\u02DC': 'tilde',
         u'\u012B': 'imacr',
         u'\uEFEC': 'OOligdblac',
         u'\uE3E7': 'Vinsdot',
         u'\uEBC7': 'oslashdblac',
         u'\uF00A': 'macrhigh',
         u'\uF4FE': 'slongslongklig',
         u'\u1E03': 'bdot',
         u'\uEFDF': 'aaligenl',
         u'\uE563': 'jcurl',
         u'\u0364': 'esup',
         u'\uEECB': 'ftlig',
         u'\u2108': 'scruple',
         u'\u00AB': 'laquo',
         u'\uEEF0': 'oenl',
         u'\uE444': 'bacute',
         u'\u2018': 'lsquo',
         u'\u1E55': 'pacute',
         u'\uA77B': 'Fins',
         u'\uE554': 'jmacrmed',
         u'\uEFA4': 'afinslig',
         u'\u0100': 'Amacr',
         u'\uEECF': 'ffylig',
         u'\u1E36': 'Ldotbl',
         u'\uE781': 'yesup',
         u'\u2025': 'dblldr',
         u'\u005E': 'circ',
         u'\u00A8': 'uml',
         u'\uEF0C': 'qscap',
         u'\uE441': 'aeligdblac',
         u'\uE353': 'Wesup',
         u'\uF202': 'aopen',
         u'\uE8E2': 'evsup',
         u'\uA7FF': 'M5leg',
         u'\uEF28': 'lscapdotbl',
         u'\u029F': 'lscap',
         u'\u002B': 'plus',
         u'\u0307': 'combdot',
         u'\uE0B7': 'Emacrbreve',
         u'\uF223': 'mrdes',
         u'\uE8B3': 'q2app',
         u'\u002F': 'sol',
         u'\uEEEB': 'jenl',
         u'\u1EF4': 'Ydotbl',
         u'\uEFE8': 'OOligacute',
         u'\u2078': 'sup8',
         u'\u2079': 'sup9',
         u'\u2070': 'sup0',
         u'\u00B9': 'sup1',
         u'\u00B2': 'sup2',
         u'\u00B3': 'sup3',
         u'\u2074': 'sup4',
         u'\uF72E': 'metrdblbrevemacr',
         u'\u2076': 'sup6',
         u'\u2077': 'sup7',
         u'\uE8F3': 'wvsup',
         u'\u1EFF': 'yloop',
         u'\u1D1C': 'uscap',
         u'\uEBED': 'oslashmacracute',
         u'\u2086': 'sub6',
         u'\u2087': 'sub7',
         u'\u2084': 'sub4',
         u'\u2085': 'sub5',
         u'\u2082': 'sub2',
         u'\uF2DC': 'romdimsext',
         u'\u2080': 'sub0',
         u'\u00E8': 'egrave',
         u'\u03B5': 'epsilon',
         u'\uE315': 'Udot',
         u'\uF033': 'qsup',
         u'\u2088': 'sub8',
         u'\u2089': 'sub9',
         u'\uEFFF': 'aaliguml',
         u'\uEBB0': 'AVligslashacute',
         u'\uA732': 'AAlig',
         u'\uEAD1': 'qvinslig',
         u'\u204B': 'revpara',
         u'\u0144': 'nacute',
         u'\uEEFC': 'zenl',
         u'\uEFFE': 'AAliguml',
         u'\u1E0B': 'ddot',
         u'\uF21F': 'gsmlowloop',
         u'\u00CB': 'Euml',
         u'\u00CD': 'Iacute',
         u'\uF1C8': 'ercurl',
         u'\u1E02': 'Bdot',
         u'\u03BD': 'nu',
         u'\u0226': 'Adot',
         u'\uEEE3': 'denl',
         u'\uE646': 'ousup',
         u'\u0117': 'edot',
         u'\u03C9': 'omega',
         u'\u1E81': 'wgrave',
         u'\uE436': 'aeligdotbl',
         u'\uE066': 'Cdotbl',
         u'\u23D1': 'metrshort',
         u'\uA742': 'Kstrleg',
         u'\uEECD': 'fylig',
         u'\u00DD': 'Yacute',
         u'\uA760': 'YYlig',
         u'\uEAF1': 'aeligenl',
         u'\u1D05': 'dscap',
         u'\uF1F0': 'punctelevdiag',
         u'\uE260': 'OEligmacrbreve',
         u'\u00CE': 'Icirc',
         u'\uE101': 'Gdotbl',
         u'\u02DB': 'ogon',
         u'\u00C6': 'AElig',
         u'\u1EB8': 'Edotbl',
         u'\uEFFD': 'ooligdotbl',
         u'\uF01C': 'kscapsup',
         u'\uF2D9': 'romunc',
         u'\u0315': 'combcomma',
         u'\uE8E5': 'iosup',
         u'\uEFAB': 'arscaplig',
         u'\u00BA': 'ordm',
         u'\u00AA': 'ordf',
         u'\uF2E9': 'grosch',
         u'\u1DE0': 'nsup',
         u'\u00F1': 'ntilde',
         u'\uF03E': 'orrotsup',
         u'\u1E89': 'wdotbl',
         u'\uF219': 'eext',
         u'\u2012': 'numdash',
         u'\uEEDD': 'PPlig',
         u'\uF2DA': 'romsemunc',
         u'\u022F': 'odot',
         u'\u27E8': 'langb',
         u'\uE709': 'umacracute',
         u'\u1E92': 'Zdotbl',
         u'\uEBE1': 'oslashdotbl',
         u'\uE8D7': 'odiaguml',
         u'\uEADA': 'slongdestlig',
         u'\u2015': 'horbar',
         u'\uEFDE': 'aoligenl',
         u'\u1EFC': 'Vwelsh',
         u'\u0025': 'percnt',
         u'\u00B0': 'deg',
         u'\uE19E': 'Ldot',
         u'\u0102': 'Abreve',
         u'\u1DDA': 'gsup',
         u'\uFB02': 'fllig',
         u'\uF23A': 'hrdes',
         u'\uEBCF': 'pscapdot',
         u'\u0112': 'Emacr',
         u'\u0139': 'Lacute',
         u'\u0121': 'gdot',
         u'\uF7B4': 'Lovlhigh',
         u'\u030D': 'combsgvertl',
         u'\u2E2B': 'tridotsupw',
         u'\u2E22': 'luhsqb',
         u'\uEEFF': 'finsenl',
         u'\u1ECF': 'ohook',
         u'\uF03A': 'anscapligsup',
         u'\u036E': 'vsup',
         u'\u0249': 'jbar',
         u'\uE004': 'Aogonacute',
         u'\u1E7F': 'vdotbl',
         u'\uE7E6': 'vinsdotbl',
         u'\uE099': 'Eogonacute',
         u'\u1E93': 'zdotbl',
         u'\u201F': 'rdquorev',
         u'\uE4E9': 'ecurl',
         u'\u0118': 'Eogon',
         u'\uF194': 'fcurl',
         u'\uF4FC': 'slongklig',
         u'\uA745': 'kstrascleg',
         u'\u1E47': 'ndotbl',
         u'\uF1CA': 'combdothigh',
         u'\u1EF3': 'ygrave',
         u'\uF158': 'etslash',
         u'\uA736': 'AUlig',
         u'\uF715': 'metrmacrdblac',
         u'\uE033': 'Acurl',
         u'\uF701': 'metrbreve',
         u'\uE3D4': 'Oslashcurl',
         u'\uEEF7': 'uenl',
         u'\u0022': 'quot',
         u'\u25B9': 'trirightwh',
         u'\u1E0A': 'Ddot',
         u'\uE8ED': 'uwsup',
         u'\u00A0': 'nbsp',
         u'\uEFE6': 'AVligacute',
         u'\uE25D': 'OEligmacr',
         u'\u00E4': 'auml',
         u'\u01F4': 'Gacute',
         u'\uF207': 'finsclose',
         u'\uE682': 'qdot',
         u'\u0309': 'combhook',
         u'\u2013': 'ndash',
         u'\u017B': 'Zdot',
         u'\uE317': 'Uumlcirc',
         u'\uF2E1': 'romXbar',
         u'\u1DDB': 'gscapsup',
         u'\uEBFB': 'oogondotacute',
         u'\u1E31': 'kacute',
         u'\u2227': 'logand',
         u'\uE410': 'amacrbreve',
         u'\u021C': 'YOGH',
         u'\uA77A': 'drot',
         u'\uE440': 'aeligogon',
         u'\uE7B2': 'nbar',
         u'\uEBB3': 'Finsacute',
         u'\uE657': 'oslashogonacute',
         u'\u204A': 'et',
         u'\uEBD3': 'Finsdot',
         u'\u035B': 'er',
         u'\uEEC4': 'cklig',
         u'\uE025': 'Adblac',
         u'\uEEEC': 'kenl',
         u'\uE8B4': 'qcentrslstrok',
         u'\uEF25': 'bscapdotbl',
         u'\uEFFB': 'ayligdotbl',
         u'\u019E': 'nlrleg',
         u'\u014A': 'ENG',
         u'\u0248': 'Jbar',
         u'\uE5C5': 'mringbl',
         u'\u2184': 'conbase',
         u'\uA747': 'lbrk',
         u'\uE753': 'wesup',
         u'\uE60E': 'oogoncirc',
         u'\uF706': 'metrbreveacute',
         u'\u03BE': 'xi',
         u'\u02BC': 'apomod',
         u'\u2024': 'sgldr',
         u'\uA783': 'rins',
         u'\u2075': 'sup5',
         u'\u0195': 'hwair',
         u'\u02DA': 'ring',
         u'\u016C': 'Ubreve',
         u'\uE550': 'iovlmed',
         u'\uE8E2': 'eesup',
         u'\uEEE4': 'drotenl',
         u'\u202F': 'nnbsp',
         u'\u02C8': 'verbarup',
         u'\uE32D': 'Uosup',
         u'\u00A6': 'brvbar',
         u'\u00ED': 'iacute',
         u'\uEBC0': 'AOligdblac',
         u'\uF1F7': 'virgmin',
         u'\u1E46': 'Ndotbl',
         u'\u1D06': 'ethscap',
         u'\u005B': 'lsqb',
         u'\u00A9': 'copy',
         u'\u2132': 'Fturn',
         u'\uEFF5': 'aoligdotbl',
         u'\uE259': 'OEligacute',
         u'\uE72C': 'uisup',
         u'\u01D3': 'Ucar',
         u'\u1E3E': 'Macute',
         u'\uE79E': 'slongovlmed',
         u'\uF1E2': 'posit',
         u'\uE477': 'dacute',
         u'\u00AC': 'not_',
         u'\uEEF3': 'renl',
         u'\uE282': 'Qdot',
         u'\u2010': 'dash',
         u'\uEBD2': 'dscapdot',
         u'\uA767': 'thornbardes',
         u'\u0103': 'abreve',
         u'\uF206': 'aoligred',
         u'\u1E04': 'Bdotbl',
         u'\uE734': 'thornslonglig',
         u'\uE8E8': 'mesup',
         u'\uA735': 'aolig',
         u'\uF717': 'metrbrevedblac',
         u'\uE143': 'Idblac',
         u'\uA752': 'Pflour',
         u'\u01FE': 'Oslashacute',
         u'\u1ECE': 'Ohook',
         u'\uA779': 'Drot',
         u'\u03B2': 'beta',
         u'\uE665': 'pmacr',
         u'\uA748': 'Lhighstrok',
         u'\uF161': 'punctelev',
         u'\u00B7': 'middot',
         u'\uE4CF': 'ering',
         u'\uE5A4': 'lringbl',
         u'\u2019': 'rsquo',
         u'\u00BB': 'raquo',
         u'\u2042': 'triast',
         u'\u2016': 'Verbar',
         u'\uE8EB': 'uasup',
         u'\u03C3': 'sigma',
         u'\u02E3': 'xmod',
         u'\u01BF': 'wynn',
         u'\uE153': 'Jacute',
         u'\uE041': 'AEligdblac',
         u'\uA755': 'psquirrel',
         u'\u015A': 'Sacute',
         u'\uEEE0': 'aenl',
         u'\uA787': 'trot',
         u'\uF200': 'ains',
         u'\uEEE7': 'fenl',
         u'\uEF2D': 'tscapdotbl',
         u'\uEBE4': 'OOliguml',
         u'\u0232': 'Ymacr',
         u'\uF21E': 'glglowloop',
         u'\u2193': 'arrsgldw',
         u'\u2E23': 'ruhsqb',
         u'\uF703': 'metrbrevemacr',
         u'\uF225': 'munc',
         u'\u2083': 'sub3',
         u'\uFB06': 'stlig',
         u'\u200A': 'hairsp',
         u'\uE491': 'dovlmed',
         u'\u1E84': 'Wuml',
         u'\u2032': 'prime',
         u'\u015B': 'sacute',
         u'\u1E6B': 'tdot',
         u'\uEF21': 'nscapdot',
         u'\uF1DB': 'ramus',
         u'\u1E61': 'sdot',
         u'\uEFAC': 'athornlig',
         u'\u1E42': 'Mdotbl',
         u'\u2007': 'numsp',
         u'\uA74E': 'OOlig',
         u'\u0303': 'combtilde',
         u'\uF03D': 'thornsup',
         u'\uF704': 'metrmacracute',
         u'\uE246': 'Ousup',
         u'\uF2E2': 'romscapxbar',
         u'\uE20C': 'Oogonacute',
         u'\uE74C': 'vdot',
         u'\u01D6': 'uumlmacr',
         u'\u00FF': 'yuml',
         u'\u036D': 'tsup',
         u'\uE4BC': 'eogonmacr',
         u'\uEBA9': 'slongtilig',
         u'\uE784': 'ydotacute',
         u'\uF2F1': 'markflour',
         u'\u01E3': 'aeligmacr',
         u'\u026A': 'iscap',
         u'\uE8BD': 'xslashula',
         u'\u1EC8': 'Ihook',
         u'\uEBF0': 'AVligogon',
         u'\u002E': 'period',
         u'\uE4D1': 'edblac',
         u'\u003A': 'colon',
         u'\uF70A': 'metranc',
         u'\u2260': 'notequals',
         u'\u1EFA': 'LLwelsh',
         u'\u1EC9': 'ihook',
         u'\uF00B': 'macrmed',
         u'\uE036': 'AEligdotbl',
         u'\u01D2': 'ocar',
         u'\uF222': 'ldes',
         u'\uEBFA': 'Oogondotacute',
         u'\uE8F2': 'wusup',
         u'\uEBE8': 'YYliguml',
         u'\uE7E7': 'vinsdot',
         u'\uA73C': 'AYlig',
         u'\uA739': 'avlig',
         u'\u02D8': 'breve',
         u'\uF19B': 'rflour',
         u'\u03BA': 'kappa',
         u'\uA75C': 'RUM',
         u'\u23D4': 'metrdblshortlong',
         u'\uEBC2': 'AVligdblac',
         u'\uEEE9': 'henl',
         u'\u1E6C': 'Tdotbl',
         u'\u1EFD': 'vwelsh',
         u'\u00F9': 'ugrave',
         u'\uE516': 'hacute',
         u'\uA7FC': 'Prev',
         u'\uE8BE': 'xslashlra',
         u'\uF11A': 'Munc',
         u'\uE737': 'thornacute',
         u'\uA75E': 'Vdiagstrok',
         u'\u1E6A': 'Tdot',
         u'\uE7C2': 'slongdotbl',
         u'\uA76C': 'IS',
         u'\u1DE4': 'ssup',
         u'\uE24F': 'Oogoncurl',
         u'\u1E60': 'Sdot',
         u'\u036C': 'rsup',
         u'\uE21B': 'Omacrbreve',
         u'\u0142': 'lstrok',
         u'\uE501': 'gdotbl',
         u'\u027C': 'rdes',
         u'\uF2F5': 'penningar',
         u'\uF2E4': 'romscapdslash',
         u'\uF153': 'urrot',
         u'\uE37C': 'Ydblac',
         u'\uF1E1': 'parag',
         u'\u2E20': 'verbarql',
         u'\u0178': 'Yuml',
         u'\uEBE9': 'yyliguml',
         u'\uEEFA': 'xenl',
         u'\u1DCE': 'combcurl',
         u'\u2E21': 'verbarqr',
         u'\u03C8': 'psi',
         u'\uE4E2': 'eisup',
         u'\u1E82': 'Wacute',
         u'\u0026': 'amp',
         u'\uF7B2': 'Vovlhigh',
         u'\uEEFD': 'inodotenl',
         u'\u00F8': 'oslash',
         u'\u00B4': 'acute',
         u'\uEECA': 'frlig',
         u'\u010B': 'cdot',
         u'\u00CC': 'Igrave',
         u'\uE257': 'Oslashogonacute',
         u'\uA73A': 'AVligslash',
         u'\uF1BD': 'smallzero',
         u'\uEFAE': 'AnecklessElig',
         u'\uA786': 'Trot',
         u'\u1DD0': 'combisbelow',
         u'\uE7D4': 'oslashcurl',
         u'\uEF11': 'xscap',
         u'\uE735': 'thornslongligbar',
         u'\u0292': 'ezh',
         u'\u01D1': 'Ocar',
         u'\u007C': 'verbar',
         u'\u1EFB': 'llwelsh',
         u'\u0176': 'Ycirc',
         u'\uF1C5': 'combcurlhigh',
         u'\uEFED': 'ooligdblac',
         u'\uEBFD': 'oslashdotacute',
         u'\u1E8F': 'ydot',
         u'\uEBF3': 'eogoncurl',
         u'\u1DD9': 'ethsup',
         u'\uE681': 'qmacr',
         u'\u1D0B': 'kscap',
         u'\uF1CC': 'combcurlbar',
         u'\uA761': 'yylig',
         u'\u036B': 'msup',
         u'\uEBB2': 'drotacute',
         u'\u016E': 'Uring',
         u'\uE8EC': 'uvsup',
         u'\u1E99': 'yring',
         u'\u2020': 'dagger',
         u'\uA750': 'Pbardes',
         u'\uE404': 'aogonacute',
         u'\u00E9': 'eacute',
         u'\uA743': 'kstrleg',
         u'\uF217': 'Euncclose',
         u'\uF110': 'Hunc',
         u'\u16A0': 'fMedrun',
         u'\uEBC3': 'avligdblac',
         u'\u0113': 'emacr',
         u'\uEEDC': 'tzlig',
         u'\u0276': 'oeligscap',
         u'\uEFF1': 'ayligdot',
         u'\uEBD0': 'bscapdot',
         u'\u01E2': 'AEligmacr',
         u'\uE743': 'vring',
         u'\uA746': 'Lbrk',
         u'\uE375': 'Ymacrbreve',
         u'\uA75F': 'vdiagstrok',
         u'\uF4F9': 'lllig',
         u'\uE72D': 'uosup',
         u'\uE3D3': 'Ocurl',
         u'\uA763': 'vvisigot',
         u'\uEBEF': 'oslashbreve',
         u'\uE7C3': 'kovlmed',
         u'\u214E': 'fturn',
         u'\uE7D3': 'ocurl',
         u'\u0130': 'Idot',
         u'\u00EB': 'euml',
         u'\uF71B': 'metrdblbrevemacracute',
         u'\uEBF2': 'Eogoncurl',
         u'\uE466': 'cdotbl',
         u'\uEFE9': 'ooligacute',
         u'\uEED4': 'gethlig',
         u'\u0119': 'eogon',
         u'\uEBB4': 'finsacute',
         u'\u021D': 'yogh',
         u'\u0101': 'amacr',
         u'\uEBE0': 'Oslashdotbl',
         u'\u00CA': 'Ecirc',
         u'\uF233': 'yrgmainstrok',
         u'\uE42D': 'aosup',
         u'\u00FE': 'thorn',
         u'\uE7C7': 'hslongligbar',
         u'\uE6E2': 'tacute',
         u'\uF2F9': 'scudi',
         u'\uF203': 'aclose',
         u'\u1D07': 'escap',
         u'\u1E85': 'wuml',
         u'\u00E5': 'aring',
         u'\u2022': 'bull',
         u'\u0024': 'dollar',
         u'\u1E8E': 'Ydot',
         u'\uE73A': 'vacute',
         u'\u1EF2': 'Ygrave',
         u'\uEBBD': 'eacombcirc',
         u'\u22D7': 'dipledot',
         u'\uA75B': 'rrot',
         u'\uF2EA': 'libradut',
         u'\u00B8': 'cedil',
         u'\uE608': 'oogondotbl',
         u'\u017F': 'slong',
         u'\u0233': 'ymacr',
         u'\u033F': 'dblovl',
         u'\uA782': 'Rins',
         u'\uEEC6': 'drotdrotlig',
         u'\uF149': 'thornbarslash',
         u'\uE15C': 'Jdot',
         u'\uEEC7': 'eylig',
         u'\u00D5': 'Otilde',
         u'\u2114': 'lbbar',
         u'\uE150': 'Iovlhigh',
         u'\u0177': 'ycirc',
         u'\uEEC5': 'ctlig',
         u'\uE5EE': 'nringbl',
         u'\uE208': 'Oogondotbl',
         u'\uEBAA': 'slongtrlig',
         u'\uEBE5': 'ooliguml',
         u'\uEFAA': 'arlig',
         u'\uEEEF': 'nenl',
         u'\uE376': 'Ybreve',
         u'\uE742': 'vuml',
         u'\uFB03': 'ffilig',
         u'\uF13F': 'omacrsup',
         u'\u0170': 'Udblac',
         u'\u00C5': 'Aring',
         u'\u2123': 'Vslstrok',
         u'\u1E5A': 'Rdotbl',
         u'\u00EF': 'iuml',
         u'\u2004': 'emsp13',
         u'\u2006': 'emsp16',
         u'\u2005': 'emsp14',
         u'\u036A': 'hsup',
         u'\uE154': 'Jmacrhigh',
         u'\uEED5': 'nscapslonglig',
         u'\uE309': 'Umacracute',
         u'\uEFA8': 'anscaplig',
         u'\u0261': 'gopen',
         u'\u00FB': 'ucirc',
         u'\u0266': 'hhook',
         u'\uEEC2': 'bblig',
         u'\uF22A': 'nscaprdes',
         u'\u1E17': 'emacracute',
         u'\u033E': 'combtildevert',
         u'\uF2E0': 'romaslibr',
         u'\uF2F4': 'obol',
         u'\u00EE': 'icirc',
         u'\u03C5': 'upsilon',
         u'\uF2EE': 'liranuov',
         u'\u2002': 'ensp',
         u'\u03BB': 'lambda_',
         u'\u2E1C': 'rlslst',
         u'\u201B': 'rsquorev',
         u'\u2E29': 'rdblpar',
         u'\u1DDE': 'lscapsup',
         u'\uF4FB': 'slongjlig',
         u'\u2E24': 'llhsqb',
         u'\uA749': 'lhighstrok',
         u'\uF012': 'bsup',
         u'\u013A': 'lacute',
         u'\uEECE': 'fftlig',
         u'\u029C': 'hscap',
         u'\uF02B': 'ysup',
         u'\uE42E': 'avsup',
         u'\u00C7': 'Ccedil',
         u'\uE3E6': 'Vinsdotbl',
         u'\uE43F': 'aeligbreve',
         u'\uA762': 'Vvisigot',
         u'\uE253': 'Oslashmacrbreve',
         u'\u0332': 'barbl',
         u'\uEBE6': 'PPliguml',
         u'\uEFFA': 'AYligdotbl',
         u'\uEEEA': 'ienl',
         u'\u00A7': 'sect',
         u'\uF128': 'sclose',
         u'\u00F3': 'oacute',
         u'\u016F': 'uring',
         u'\u211F': 'Rslstrok',
         u'\uF013': 'bscapsup',
         u'\u0153': 'oelig',
         u'\uE8E0': 'aisup',
         u'\u0306': 'combbreve',
         u'\uEAF2': 'aenlosmalllig',
         u'\uFB01': 'filig',
         u'\uEEF1': 'penl',
         u'\uF00C': 'ovlhigh',
         u'\uE5B8': 'mmacrmed',
         u'\u1DE3': 'rrotsup',
         u'\u00B5': 'micro',
         u'\uE535': 'imacracute',
         u'\u00DF': 'szlig',
         u'\uE68B': 'qbardestilde',
         u'\uEED7': 'ppflourlig',
         u'\uEFF7': 'auligdotbl',
         u'\uEBD4': 'finsdot',
         u'\uA76F': 'condes',
         u'\u25AB': 'squarewhsm',
         u'\uE58C': 'lovlhigh',
         u'\u002A': 'ast',
         u'\uF127': 'slongdes',
         u'\uEFF8': 'AVligdotbl',
         u'\uF718': 'metrbrevedblgrave',
         u'\uF142': 'ET',
         u'\uE1B8': 'Mmacrhigh',
         u'\u23D2': 'metrshortlong',
         u'\uE433': 'acurl',
         u'\uF208': 'kunc',
         u'\u2045': 'lsqbqu',
         u'\uEBCE': 'oslashdot',
         u'\uA76B': 'etfin',
         u'\uE724': 'uvertline',
         u'\uA774': 'ntailstrok',
         u'\uEFE3': 'aoligacute',
         u'\uE596': 'lmacrhigh',
         u'\u1E80': 'Wgrave',
         u'\uF1C0': 'arbar',
         u'\uE8C1': 'thornrarmlig',
         u'\u00B6': 'para',
         u'\uEBB6': 'muncacute',
         u'\uF232': 'xldes',
         u'\u00DB': 'Ucirc',
         u'\uF030': 'jsup',
         u'\uF2DD': 'romsiliq',
         u'\uF106': 'Csqu',
         u'\uF1FB': 'punctelevhack',
         u'\u0155': 'racute',
         u'\u00FD': 'yacute',
         u'\uE43D': 'aeligmacrbreve',
         u'\uF23F': 'romnumCrevovl',
         u'\u00AF': 'macr',
         u'\uF4FA': 'slongchlig',
         u'\u0268': 'istrok',
         u'\uEAD2': 'gplig',
         u'\u2191': 'arrsglupw',
         u'\u2E26': 'lUbrack',
         u'\u2248': 'esse',
         u'\u03C1': 'rho',
         u'\uF199': 'tcurl',
         u'\uEF2B': 'rscapdotbl',
         u'\u03B1': 'alpha',
         u'\uEBCD': 'Oslashdot',
         u'\uF03F': 'orumsup',
         u'\uE659': 'oeligacute',
         u'\uE8D5': 'adiaguml',
         u'\uF198': 'ccurl',
         u'\u1E0C': 'Ddotbl',
         u'\u0363': 'asup',
         u'\u0040': 'commat',
         u'\u1E62': 'Sdotbl',
         u'\uA77C': 'fins',
         u'\uEEDF': 'slongenl',
         u'\u00D1': 'Ntilde',
         u'\uEBF4': 'Adotacute',
         u'\u1E88': 'Wdotbl',
         u'\uA770': 'usmod',
         u'\uA73F': 'condot',
         u'\u2033': 'Prime',
         u'\uF195': 'kcurl',
         u'\u211E': 'Rtailstrok',
         u'\u03C2': 'sigmaf',
         u'\uE162': 'Jdblac',
         u'\uEBEE': 'Oslashbreve',
         u'\u028F': 'yscap',
         u'\uEFE2': 'AOligacute',
         u'\u1D1B': 'tscap',
         u'\u00C4': 'Auml',
         u'\uEBC6': 'Oslashdblac',
         u'\uE543': 'idblac',
         u'\uA75A': 'Rrot',
         u'\uA769': 'vins',
         u'\uEFA6': 'allig',
         u'\u2235': 'tridotdw',
         u'\uF1E5': 'colmidcomposit',
         u'\uE4B7': 'emacrbreve',
         u'\uF21D': 'gdivloop',
         u'\u2766': 'hedera',
         u'\uE688': 'qdotbl',
         u'\u00D8': 'Oslash',
         u'\u03BF': 'omicron',
         u'\uEBF1': 'avligogon',
         u'\uE384': 'Ydotacute',
         u'\u1E54': 'Pacute',
         u'\uE41F': 'aringcirc',
         u'\uEBD5': 'finssemiclosedot',
         u'\uFB04': 'ffllig',
         u'\uF70B': 'metrancacute',
         u'\uA777': 'ttailstrok',
         u'\uF2D8': 'romas',
         u'\uE41A': 'aumlcirc',
         u'\u00D9': 'Ugrave',
         u'\uA754': 'Psquirrel',
         u'\u0132': 'IJlig',
         u'\uE043': 'AEligdot',
         u'\uE66D': 'pdotbl',
         u'\uEFEF': 'aaligdot',
         u'\u019A': 'lbar',
         u'\u1EE5': 'udotbl',
         u'\u0151': 'odblac',
         u'\u03B6': 'zeta',
         u'\uE537': 'imacrbreve',
         u'\uA751': 'pbardes',
         u'\uE8BF': 'q3app',
         u'\uE0E1': 'Easup',
         u'\u012E': 'Iogon',
         u'\u0116': 'Edot',
         u'\u03C0': 'pi',
         u'\u1ECD': 'odotbl',
         u'\uF13A': 'Asqu',
         u'\uA74D': 'oloop',
         u'\uE1D2': 'Movlhigh',
         u'\uF707': 'metrbrevegrave',
         u'\u2182': 'romnumDDdbllig',
         u'\uE5DC': 'nmacrmed',
         u'\uEFE4': 'AUligacute',
         u'\uEED0': 'ftylig',
         u'\uEBA4': 'slongoumllig',
         u'\uEBA7': 'slongslongilig',
         u'\u00A3': 'pound',
         u'\uE8C2': 'Hrarmlig',
         u'\uEBC5': 'oogondblac',
         u'\uF1E0': 'medcom',
         u'\uE59E': 'ldot',
         u'\u1E87': 'wdot',
         u'\uF2F0': 'markold',
         u'\u1D20': 'vscap',
         u'\uE010': 'Amacrbreve',
         u'\u2011': 'nbhy',
         u'\uEBA8': 'slongslongllig',
         u'\uEBA0': 'slongaumllig',
         u'\u012F': 'iogon',
         u'\uEBFE': 'Udotacute',
         u'\uE4F0': 'facute',
         u'\u01F6': 'HWAIR',
         u'\uE5B1': 'lovlmed',
         u'\u035C': 'combdblbrevebl',
         u'\u02DD': 'dblac',
         u'\uEFA1': 'anecklesselig',
         u'\u2C76': 'hhalf',
         u'\uEBC4': 'Oogondblac',
         u'\u1DE1': 'nscapsup',
         u'\uE7E4': 'rdesstrok',
         u'\u1E37': 'ldotbl',
         u'\uE076': 'Cogon',
         u'\uF196': 'gcurl',
         u'\u0336': 'baracr',
         u'\u27E7': 'rwhsqb',
         u'\uEED2': 'gdlig',
         u'\uF2F2': 'msign',
         u'\uEAF3': 'eogonenl',
         u'\uEFEB': 'aaligdblac',
         u'\uE2E2': 'Tacute',
         u'\u003F': 'quest',
         u'\uF215': 'aneckless',
         u'\u003B': 'semi',
         u'\u201D': 'rdquo',
         u'\uEBE7': 'ppliguml',
         u'\uF1BC': 'foumllig',
         u'\u0120': 'Gdot',
         u'\uF201': 'Ains',
         u'\uF2F8': 'schillgermscript',
         u'\uE4CD': 'eumlmacr',
         u'\uF2DE': 'romquin',
         u'\uE643': 'oasup',
         u'\u00BC': 'frac14',
         u'\uF709': 'metrmacrbrevegrave',
         u'\u00BD': 'frac12',
         u'\u01F5': 'gacute',
         u'\uF1F1': 'punctinterlemn',
         u'\u014C': 'Omacr',
         u'\u0327': 'combced',
         u'\u0301': 'combacute',
         u'\uA75D': 'rum',
         u'\uE0BC': 'Eogonmacr',
         u'\u0115': 'ebreve',
         u'\uEEF2': 'qenl',
         u'\uF2DF': 'romdupond',
         u'\u0274': 'nscap',
         u'\uEBDF': 'oogondot',
         u'\u0323': 'combdotbl',
         u'\uE4EC': 'eogondotacute',
         u'\u2125': 'ounce',
         u'\u27E9': 'rangb',
         u'\u03B3': 'gamma',
         u'\uA744': 'Kstrascleg',
         u'\u01E4': 'Gstrok',
         u'\uE03F': 'AEligbreve',
         u'\uE645': 'oisup',
         u'\uF1E7': 'punctexclam',
         u'\u1E86': 'Wdot',
         u'\uEBA6': 'slongslonglig',
         u'\uA764': 'THORNbar',
         u'\uF1E4': 'colelevposit',
         u'\uE34C': 'Vdot',
         u'\uFB00': 'fflig',
         u'\uF224': 'Muncdes',
         u'\u0114': 'Ebreve',
         u'\u1DD8': 'drotsup',
         u'\u01ED': 'oogonmacr',
         u'\uEFE1': 'aaligacute',
         u'\uE8E6': 'iusup',
         u'\uEF22': 'rscapdot',
         u'\u03B4': 'delta',
         u'\uA753': 'pflour',
         u'\uF71A': 'metrancdblgrave',
         u'\uF1BF': 'Xmod',
         u'\u00D2': 'Ograve',
         u'\u1DD2': 'us',
         u'\u1DD1': 'ur',
         u'\uE0EA': 'Eogondblac',
         u'\u2046': 'rsqbqu',
         u'\u00A4': 'curren',
         u'\uA737': 'aulig',
         u'\u00C2': 'Acirc',
         u'\uE65D': 'oeligmacr',
         u'\uE350': 'Wdblac',
         u'\u200B': 'zerosp',
         u'\uA758': 'Qslstrok',
         u'\uEF23': 'sscapdot',
         u'\uF1F5': 'punctflex',
         u'\uE443': 'aeligdot',
         u'\u201E': 'ldquolow',
         u'\u00E1': 'aacute',
         u'\uEEED': 'lenl',
         u'\uF2E6': 'dram',
         u'\uFB05': 'slongtlig',
         u'\uEBEA': 'AEligcurl',
         u'\u16D8': 'mMedrun',
         u'\uA734': 'AOlig',
         u'\u01B7': 'EZH',
         u'\u022A': 'Oumlmacr',
         u'\uEEFE': 'jnodotenl',
         u'\uE151': 'Jdotbl',
         u'\u003E': 'gt',
         u'\uEBAE': 'kslonglig',
         u'\uEBC1': 'aoligdblac',
         u'\u1EA0': 'Adotbl',
         u'\uA73D': 'aylig',
         u'\u1DD5': 'aoligsup',
         u'\u01B6': 'zstrok',
         u'\u014F': 'obreve',
         u'\uF032': 'oslashsup',
         u'\u2027': 'hyphpoint',
         u'\uEEF4': 'senl',
         u'\uEBDC': 'lscapdot',
         u'\uE8E9': 'oosup',
         u'\u1DCF': 'ersub',
         u'\uEED3': 'gdrotlig',
         u'\u030E': 'combdbvertl',
         u'\uEBB7': 'Ocurlacute',
         u'\u2044': 'fracsol',
         u'\uE42C': 'aesup',
         u'\u1D79': 'gins',
         u'\uEEE5': 'ethenl',
         u'\uE43A': 'aeligmacracute',
         u'\u0299': 'bscap',
         u'\uE8D1': 'aeligring',
         u'\uF2F6': 'reichtalold',
         u'\uE00A': 'Amacracute',
         u'\uF4FF': 'slongslongtlig',
         u'\uEBE3': 'juml',
         u'\uEBBF': 'ucurlbar',
         u'\uE02C': 'Aesup',
         u'\u0398': 'Theta',
         u'\u0180': 'bstrok',
         u'\u1DD7': 'ccedilsup',
         u'\uF1EC': 'renvoi',
         u'\uF7B5': 'Covlhigh',
         u'\uE44D': 'bovlmed',
         u'\u1E30': 'Kacute',
         u'\uA785': 'sins',
         u'\u2E1D': 'llslst',
         u'\u2009': 'thinsp',
         u'\uEBC9': 'oeligdblac',
         u'\u00EA': 'ecirc',
         u'\uE373': 'Ymacracute',
         u'\u203B': 'refmark',
         u'\uE8B7': 'slongflour',
         u'\uEEE8': 'genl',
         u'\uEEC8': 'faumllig',
         u'\uEBD6': 'finsclosedot',
         u'\u205C': 'dotcross',
         u'\uE4EA': 'eogondblac',
         u'\u1DD4': 'aeligsup',
         u'\u0308': 'combuml',
         u'\uE64F': 'oogoncurl',
         u'\u03C7': 'chi',
         u'\u0111': 'dstrok',
         u'\uEED8': 'trlig',
         u'\u2001': 'emqd',
         u'\u2E27': 'rUbrack',
         u'\uE337': 'THORNbarslash',
         u'\u00FC': 'uuml',
         u'\uEBF8': 'Odotacute',
         u'\uEFDD': 'oeligenl',
         u'\uF2EF': 'lirasterl',
         u'\uEEF6': 'thornenl',
         u'\uEEDA': 'trottrotlig',
         u'\uF71C': 'metrdblbrevemacrdblac',
         u'\uE33A': 'Vacute',
         u'\uE660': 'oeligmacrbreve',
         u'\u03C4': 'tau',
         u'\uE442': 'aeliguml',
         u'\u0368': 'csup',
         u'\u0131': 'inodot',
         u'\u0366': 'osup',
         u'\u01A6': 'YR',
         u'\uEEE1': 'benl',
         u'\uE54A': 'iesup',
         u'\u0367': 'usup',
         u'\uE39F': 'THORNdotbl',
         u'\u0104': 'Aogon',
         u'\uE731': 'ucurl',
         u'\u1EA2': 'Ahook',
         u'\u1E40': 'Mdot',
         u'\uF159': 'de',
         u'\u2AFD': 'dblsol',
         u'\uE4EE': 'fdotbl',
         u'\uF719': 'metrancdblac',
         u'\u1EA3': 'ahook',
         u'\uEAD0': 'grlig',
         u'\uA775': 'rtailstrok',
         u'\uEEC9': 'fjlig',
         u'\uE3E5': 'Finsdotbl',
         u'\u2192': 'arrsglrw',
         u'\u01EA': 'Oogon',
         u'\u002C': 'comma',
         u'\uF700': 'metrmacr',
         u'\uE655': 'oslashogon',
         u'\u1D0D': 'mscap',
         u'\uE8F1': 'wisup',
         u'\uA784': 'Sins',
         u'\u003C': 'lt',
         u'\uA74C': 'Oloop',
         u'\uF2EB': 'librafren',
         u'\u1DD6': 'avligsup',
         u'\uEF15': 'thornscap',
         u'\u00FA': 'uacute',
         u'\uE22D': 'Oumlcirc',
         u'\uF2E8': 'florloop',
         u'\uE08F': 'ETHdotbl',
         u'\uEBDD': 'mscapdot',
         u'\u2190': 'arrsgllw',
         u'\uF025': 'psup',
         u'\uE754': 'wosup',
         u'\uE4E8': 'eogondotbl',
         u'\uF1A5': 'USbase',
         u'\uEFF0': 'AYligdot',
         u'\uE135': 'Imacracute',
         u'\uF209': 'kclose',
         u'\uEBB9': 'rrotacute',
         u'\u00E0': 'agrave',
         u'\u00D4': 'Ocirc',
         u'\u1E16': 'Emacracute',
         u'\u01E5': 'gstrok',
         u'\u00F6': 'ouml',
         u'\u00DC': 'Uuml',
         u'\uE040': 'AEligogon',
         u'\u00AE': 'reg',
         u'\uA765': 'thornbar',
         u'\uE042': 'AEliguml',
         u'\uE40A': 'amacracute',
         u'\uA73E': 'CONdot',
         u'\uE044': 'Bacute',
         u'\uF2E7': 'ecu',
         u'\u014D': 'omacr',
         u'\uE49F': 'eogoncirc',
         u'\u0256': 'dtail',
         u'\uEF26': 'dscapdotbl',
         u'\uF21B': 'finssemiclose',
         u'\u030B': 'combdblac',
         u'\uEBE2': 'Juml',
         u'\u00D7': 'times',
         u'\uE653': 'oslashmacrbreve',
         u'\uF1A6': 'usbase',
         u'\uE03D': 'AEligmacrbreve',
         u'\uF193': 'dcurl',
         u'\uE8E7': 'jesup',
         u'\uF22B': 'nscapldes',
         u'\u1EF6': 'Yhook',
         u'\u00B1': 'plusmn',
         u'\u01D5': 'Uumlmacr',
         u'\u00BE': 'frac34',
         u'\uEBB5': 'Muncacute',
         u'\uE342': 'Vuml',
         u'\uE2EE': 'Togon',
         u'\uE077': 'Dacute',
         u'\u03B9': 'iota',
         u'\u016A': 'Umacr',
         u'\u1ECA': 'Idotbl',
         u'\u2E0C': 'luslst',
         u'\u1E24': 'Hdotbl',
         u'\u005F': 'lowbar',
         u'\u1E59': 'rdot',
         u'\uA780': 'Lturn',
         u'\u00CF': 'Iuml',
         u'\u2021': 'Dagger',
         u'\uE8F0': 'wasup',
         u'\uF714': 'metrpause',
         u'\uE77B': 'ybar',
         u'\u27E6': 'lwhsqb',
         u'\u007D': 'rcub',
         u'\u1E0D': 'ddotbl',
         u'\uF1AC': 'sem',
         u'\u1ECC': 'Odotbl',
         u'\uE03A': 'AEligmacracute',
         u'\uF7B6': 'Dovlhigh',
         u'\u1EA1': 'adotbl',
         u'\u00BF': 'iquest',
         u'\uE8E3': 'eosup',
         u'\u1EF5': 'ydotbl',
         u'\u2181': 'romnumDDlig',
         u'\uE34D': 'Vmacr',
         u'\uE7A2': 'thornovlmed',
         u'\u010A': 'Cdot',
         u'\uA77F': 'ginsturn',
         u'\u0172': 'Uogon',
         u'\uE74D': 'vmacr',
         u'\u1ECB': 'idotbl',
         u'\uF21C': 'finsdothook',
         u'\uE5D2': 'movlmed',
         u'\uEBA2': 'slongilig',
         u'\uE568': 'kdot',
         u'\u1E1F': 'fdot',
         u'\uE152': 'Jovlhigh',
         u'\uE12A': 'Icurl',
         u'\u0174': 'Wcirc',
         u'\uF4FD': 'slongslig',
         u'\uE552': 'jovlmed',
         u'\u00E6': 'aelig',
         u'\uE30B': 'Umacrbreve',
         u'\uEBAF': 'slongacute',
         u'\u1E32': 'Kdotbl',
         u'\u0127': 'hstrok',
         u'\uE70B': 'umacrbreve',
         u'\uF221': 'ksemiclose',
         u'\uEBF7': 'idotacute',
         u'\u0302': 'combcirc',
         u'\uEFF2': 'AAligdotbl',
         u'\uF13E': 'oogonsup',
         u'\uA76A': 'ETfin',
         u'\uA731': 'sscap',
         u'\u036F': 'xsup',
         u'\u1E98': 'wring',
         u'\uF038': 'arligsup',
         u'\u016B': 'umacr',
         u'\uE776': 'ybreve',
         u'\u00AD': 'shy',
         u'\u2081': 'sub1',
         u'\uEBD1': 'drotdot',
         u'\uA781': 'lturn',
         u'\u0259': 'schwa',
         u'\u0152': 'OElig',
         u'\uA74A': 'OBIIT',
         u'\uEEF5': 'tenl',
         u'\u0027': 'apos',
         u'\u1EAE': 'Abreveacute',
         u'\u03B8': 'theta',
         u'\u1E58': 'Rdot',
         u'\u201A': 'lsquolow',
         u'\uE6A3': 'rringbl',
         u'\u03B7': 'eta',
         u'\u0173': 'uogon',
         u'\u007E': 'tld',
         u'\u20B0': 'pennygerm',
         u'\u2E2C': 'quaddots',
         u'\u00F0': 'eth',
         u'\uA74B': 'obiit',
         u'\u0154': 'Racute',
         u'\u1D01': 'aeligscap',
         u'\u012A': 'Imacr',
         u'\u002D': 'hyphen',
         u'\u1E9E': 'SZlig',
         u'\uEEF8': 'venl',
         u'\u00C1': 'Aacute',
         u'\u00A2': 'cent',
         u'\u0175': 'wcirc',
         u'\uEEE2': 'cenl',
         u'\uA77E': 'Ginsturn',
         u'\u1E23': 'hdot',
         u'\uF00D': 'ovlmed',
         u'\u025F': 'jnodotstrok',
         u'\u0141': 'Lstrok',
         u'\uF226': 'muncdes',
         u'\uF03B': 'trotsup',
         u'\uF03C': 'wsup',
         u'\u1E1E': 'Fdot',
         u'\uEFA2': 'anecklessvlig',
         u'\u2E2D': 'fivedots',
         u'\uE34B': 'Vdblac',
         u'\u0227': 'adot',
         u'\u00D3': 'Oacute',
         u'\uF160': 'punctinter',
         u'\uF1F4': 'virgsusp',
         u'\uEBF9': 'odotacute',
         u'\u2003': 'emsp',
         u'\uEEDE': 'golig',
         u'\uE548': 'ibrevinvbl',
         u'\uE252': 'Oslashmacr',
         u'\u271D': 'cross',
         u'\u00E2': 'acirc',
         u'\u03C6': 'phi',
         u'\uA757': 'qbardes',
         u'\uE0E9': 'Ecurl',
         u'\uEEC3': 'bglig',
         u'\u1EAF': 'abreveacute',
         u'\uE0E8': 'Eogondotbl',
         u'\uE74B': 'vdblac',
         u'\u2C75': 'Hhalf',
         u'\u0333': 'dblbarbl',
         u'\u1DE5': 'slongsup',
         u'\uF1A7': 'ETslash',
         u'\uEBEC': 'Oslashmacracute',
         u'\u0359': 'combastbl',
         u'\uA733': 'aalig',
         u'\u1DDF': 'mscapsup',
         u'\u01EB': 'oogon',
         u'\uEBF5': 'adotacute',
         u'\uEED6': 'pplig',
         u'\uA759': 'qslstrok',
         u'\uE4E1': 'easup',
         u'\u2237': 'quaddot',
         u'\u016D': 'ubreve',
         u'\uF036': 'anligsup',
         u'\u0365': 'isup',
         u'\uE0EB': 'Eogondot',
         u'\u01B5': 'Zstrok',
         u'\uE41D': 'adotbluml',
         u'\u1D22': 'zscap',
         u'\uF1E3': 'ductsimpl',
         u'\u022B': 'oumlmacr',
         u'\uE331': 'Ucurl',
         u'\uF716': 'metrmacrdblgrave',
         u'\u00D0': 'ETH',
         u'\u1E22': 'Hdot',
         u'\u223B': 'est',
         u'\u00F4': 'ocirc',
         u'\uE255': 'Oslashogon',
         u'\u2E0D': 'ruslst',
         u'\u1DE6': 'zsup',
         u'\uE72B': 'uesup',
         u'\uEBCB': 'yyligdblac',
         u'\u2014': 'mdash',
         u'\uE62D': 'oumlcirc',
         u'\uF708': 'metrmacrbreveacute',
         u'\u2E2E': 'punctpercont',
         u'\u00D6': 'Ouml',
         u'\u1E3F': 'macute',
         u'\u012C': 'Ibreve',
         u'\u005C': 'bsol',
         u'\uEBB1': 'avligslashacute',
         u'\uF1F9': 'wavylin',
         u'\uE33B': 'Vcirc',
         u'\u1E9C': 'slongbarslash',
         u'\u1E6D': 'tdotbl',
         u'\uEBBE': 'eucombcirc',
         u'\uF016': 'dscapsup',
         u'\uEEE6': 'eenl',
         u'\u1EF7': 'yhook',
         u'\uE476': 'cogon',
         u'\uEEDB': 'tylig',
         u'\uE6EE': 'togon',
         u'\uF21A': 'etall',
         u'\u2E2A': 'tridotsdownw',
         u'\uE8BA': 'vslash',
         u'\uE5D7': 'ncirc',
         u'\uF2E3': 'romscapybar',
         u'\uA766': 'THORNbardes',
         u'\u1DCD': 'combcircdbl',
         u'\uF1DA': 'midring',
         u'\uE773': 'ymacracute',
         u'\u1E25': 'hdotbl',
         u'\uF2FA': 'sestert',
         u'\uEBAD': 'hslonglig',
         u'\uEED1': 'gglig',
         u'\u1E45': 'ndot',
         u'\uEFA9': 'aplig',
         u'\uE647': 'ovsup',
         u'\u012D': 'ibreve',
         u'\uEBBA': 'Vinsacute',
         u'\uF2DB': 'romsext',
         u'\uE637': 'oring',
         u'\u1E9F': 'dscript',
         u'\uE8C5': 'krarmlig',
         u'\u017C': 'zdot',
         u'\u0143': 'Nacute',
         u'\u2E17': 'dbloblhyph',
         u'\uEFFC': 'OOligdotbl',
         u'\uA778': 'sstrok',
         u'\u2767': 'hederarot',
         }
  
  
  
  categoryof = {
         'sup0': 'Superscripts_and_subscripts',
         'sup4': 'Superscripts_and_subscripts',
         'sup5': 'Superscripts_and_subscripts',
         'sup6': 'Superscripts_and_subscripts',
         'sup7': 'Superscripts_and_subscripts',
         'sup8': 'Superscripts_and_subscripts',
         'sup9': 'Superscripts_and_subscripts',
         'sub0': 'Superscripts_and_subscripts',
         'sub1': 'Superscripts_and_subscripts',
         'sub2': 'Superscripts_and_subscripts',
         'sub3': 'Superscripts_and_subscripts',
         'sub4': 'Superscripts_and_subscripts',
         'sub5': 'Superscripts_and_subscripts',
         'sub6': 'Superscripts_and_subscripts',
         'sub7': 'Superscripts_and_subscripts',
         'sub8': 'Superscripts_and_subscripts',
         'sub9': 'Superscripts_and_subscripts',
         'cross': 'Dingbats',
         'hedera': 'Dingbats',
         'hederarot': 'Dingbats',
         'nbsp': 'Latin1_Supplement',
         'iexcl': 'Latin1_Supplement',
         'cent': 'Latin1_Supplement',
         'pound': 'Latin1_Supplement',
         'curren': 'Latin1_Supplement',
         'yen': 'Latin1_Supplement',
         'brvbar': 'Latin1_Supplement',
         'sect': 'Latin1_Supplement',
         'uml': 'Latin1_Supplement',
         'copy': 'Latin1_Supplement',
         'ordf': 'Latin1_Supplement',
         'laquo': 'Latin1_Supplement',
         'not_': 'Latin1_Supplement',
         'shy': 'Latin1_Supplement',
         'reg': 'Latin1_Supplement',
         'macr': 'Latin1_Supplement',
         'deg': 'Latin1_Supplement',
         'plusmn': 'Latin1_Supplement',
         'sup2': 'Latin1_Supplement',
         'sup3': 'Latin1_Supplement',
         'acute': 'Latin1_Supplement',
         'micro': 'Latin1_Supplement',
         'para': 'Latin1_Supplement',
         'middot': 'Latin1_Supplement',
         'cedil': 'Latin1_Supplement',
         'sup1': 'Latin1_Supplement',
         'ordm': 'Latin1_Supplement',
         'raquo': 'Latin1_Supplement',
         'frac14': 'Latin1_Supplement',
         'frac12': 'Latin1_Supplement',
         'frac34': 'Latin1_Supplement',
         'iquest': 'Latin1_Supplement',
         'Agrave': 'Latin1_Supplement',
         'Aacute': 'Latin1_Supplement',
         'Acirc': 'Latin1_Supplement',
         'Atilde': 'Latin1_Supplement',
         'Auml': 'Latin1_Supplement',
         'Aring': 'Latin1_Supplement',
         'AElig': 'Latin1_Supplement',
         'Ccedil': 'Latin1_Supplement',
         'Egrave': 'Latin1_Supplement',
         'Eacute': 'Latin1_Supplement',
         'Ecirc': 'Latin1_Supplement',
         'Euml': 'Latin1_Supplement',
         'Igrave': 'Latin1_Supplement',
         'Iacute': 'Latin1_Supplement',
         'Icirc': 'Latin1_Supplement',
         'Iuml': 'Latin1_Supplement',
         'ETH': 'Latin1_Supplement',
         'Ntilde': 'Latin1_Supplement',
         'Ograve': 'Latin1_Supplement',
         'Oacute': 'Latin1_Supplement',
         'Ocirc': 'Latin1_Supplement',
         'Otilde': 'Latin1_Supplement',
         'Ouml': 'Latin1_Supplement',
         'times': 'Latin1_Supplement',
         'Oslash': 'Latin1_Supplement',
         'Ugrave': 'Latin1_Supplement',
         'Uacute': 'Latin1_Supplement',
         'Ucirc': 'Latin1_Supplement',
         'Uuml': 'Latin1_Supplement',
         'Yacute': 'Latin1_Supplement',
         'THORN': 'Latin1_Supplement',
         'szlig': 'Latin1_Supplement',
         'agrave': 'Latin1_Supplement',
         'aacute': 'Latin1_Supplement',
         'acirc': 'Latin1_Supplement',
         'atilde': 'Latin1_Supplement',
         'auml': 'Latin1_Supplement',
         'aring': 'Latin1_Supplement',
         'aelig': 'Latin1_Supplement',
         'ccedil': 'Latin1_Supplement',
         'egrave': 'Latin1_Supplement',
         'eacute': 'Latin1_Supplement',
         'ecirc': 'Latin1_Supplement',
         'euml': 'Latin1_Supplement',
         'igrave': 'Latin1_Supplement',
         'iacute': 'Latin1_Supplement',
         'icirc': 'Latin1_Supplement',
         'iuml': 'Latin1_Supplement',
         'eth': 'Latin1_Supplement',
         'ntilde': 'Latin1_Supplement',
         'ograve': 'Latin1_Supplement',
         'oacute': 'Latin1_Supplement',
         'ocirc': 'Latin1_Supplement',
         'otilde': 'Latin1_Supplement',
         'ouml': 'Latin1_Supplement',
         'divide': 'Latin1_Supplement',
         'oslash': 'Latin1_Supplement',
         'ugrave': 'Latin1_Supplement',
         'uacute': 'Latin1_Supplement',
         'ucirc': 'Latin1_Supplement',
         'uuml': 'Latin1_Supplement',
         'yacute': 'Latin1_Supplement',
         'thorn': 'Latin1_Supplement',
         'yuml': 'Latin1_Supplement',
         'oumlacute': 'Characters_with_acute_accent_and_diaeresis',
         'Amacr': 'Latin_ExtendedA',
         'amacr': 'Latin_ExtendedA',
         'Abreve': 'Latin_ExtendedA',
         'abreve': 'Latin_ExtendedA',
         'Aogon': 'Latin_ExtendedA',
         'aogon': 'Latin_ExtendedA',
         'Cacute': 'Latin_ExtendedA',
         'cacute': 'Latin_ExtendedA',
         'Cdot': 'Latin_ExtendedA',
         'cdot': 'Latin_ExtendedA',
         'Dstrok': 'Latin_ExtendedA',
         'dstrok': 'Latin_ExtendedA',
         'Emacr': 'Latin_ExtendedA',
         'emacr': 'Latin_ExtendedA',
         'Ebreve': 'Latin_ExtendedA',
         'ebreve': 'Latin_ExtendedA',
         'Edot': 'Latin_ExtendedA',
         'edot': 'Latin_ExtendedA',
         'Eogon': 'Latin_ExtendedA',
         'eogon': 'Latin_ExtendedA',
         'Gdot': 'Latin_ExtendedA',
         'gdot': 'Latin_ExtendedA',
         'hstrok': 'Latin_ExtendedA',
         'Imacr': 'Latin_ExtendedA',
         'imacr': 'Latin_ExtendedA',
         'Ibreve': 'Latin_ExtendedA',
         'ibreve': 'Latin_ExtendedA',
         'Iogon': 'Latin_ExtendedA',
         'iogon': 'Latin_ExtendedA',
         'Idot': 'Latin_ExtendedA',
         'inodot': 'Latin_ExtendedA',
         'IJlig': 'Latin_ExtendedA',
         'ijlig': 'Latin_ExtendedA',
         'Lacute': 'Latin_ExtendedA',
         'lacute': 'Latin_ExtendedA',
         'Lstrok': 'Latin_ExtendedA',
         'lstrok': 'Latin_ExtendedA',
         'Nacute': 'Latin_ExtendedA',
         'nacute': 'Latin_ExtendedA',
         'ENG': 'Latin_ExtendedA',
         'eng': 'Latin_ExtendedA',
         'Omacr': 'Latin_ExtendedA',
         'omacr': 'Latin_ExtendedA',
         'Obreve': 'Latin_ExtendedA',
         'obreve': 'Latin_ExtendedA',
         'Odblac': 'Latin_ExtendedA',
         'odblac': 'Latin_ExtendedA',
         'OElig': 'Latin_ExtendedA',
         'oelig': 'Latin_ExtendedA',
         'Racute': 'Latin_ExtendedA',
         'racute': 'Latin_ExtendedA',
         'Sacute': 'Latin_ExtendedA',
         'sacute': 'Latin_ExtendedA',
         'Umacr': 'Latin_ExtendedA',
         'umacr': 'Latin_ExtendedA',
         'Ubreve': 'Latin_ExtendedA',
         'ubreve': 'Latin_ExtendedA',
         'Uring': 'Latin_ExtendedA',
         'uring': 'Latin_ExtendedA',
         'Udblac': 'Latin_ExtendedA',
         'udblac': 'Latin_ExtendedA',
         'Uogon': 'Latin_ExtendedA',
         'uogon': 'Latin_ExtendedA',
         'Wcirc': 'Latin_ExtendedA',
         'wcirc': 'Latin_ExtendedA',
         'Ycirc': 'Latin_ExtendedA',
         'ycirc': 'Latin_ExtendedA',
         'Yuml': 'Latin_ExtendedA',
         'Zdot': 'Latin_ExtendedA',
         'zdot': 'Latin_ExtendedA',
         'slong': 'Latin_ExtendedA',
         'Hhalf': 'Latin_ExtendedC',
         'hhalf': 'Latin_ExtendedC',
         'pennygerm': 'Currency_Symbols',
         'fscap': 'Latin_ExtendedD',
         'sscap': 'Latin_ExtendedD',
         'AAlig': 'Latin_ExtendedD',
         'aalig': 'Latin_ExtendedD',
         'AOlig': 'Latin_ExtendedD',
         'aolig': 'Latin_ExtendedD',
         'AUlig': 'Latin_ExtendedD',
         'aulig': 'Latin_ExtendedD',
         'AVlig': 'Latin_ExtendedD',
         'avlig': 'Latin_ExtendedD',
         'AVligslash': 'Latin_ExtendedD',
         'avligslash': 'Latin_ExtendedD',
         'AYlig': 'Latin_ExtendedD',
         'aylig': 'Latin_ExtendedD',
         'CONdot': 'Latin_ExtendedD',
         'condot': 'Latin_ExtendedD',
         'Kbar': 'Latin_ExtendedD',
         'kbar': 'Latin_ExtendedD',
         'Kstrleg': 'Latin_ExtendedD',
         'kstrleg': 'Latin_ExtendedD',
         'Kstrascleg': 'Latin_ExtendedD',
         'kstrascleg': 'Latin_ExtendedD',
         'Lbrk': 'Latin_ExtendedD',
         'lbrk': 'Latin_ExtendedD',
         'Lhighstrok': 'Latin_ExtendedD',
         'lhighstrok': 'Latin_ExtendedD',
         'OBIIT': 'Latin_ExtendedD',
         'obiit': 'Latin_ExtendedD',
         'Oloop': 'Latin_ExtendedD',
         'oloop': 'Latin_ExtendedD',
         'OOlig': 'Latin_ExtendedD',
         'oolig': 'Latin_ExtendedD',
         'Pbardes': 'Latin_ExtendedD',
         'pbardes': 'Latin_ExtendedD',
         'Pflour': 'Latin_ExtendedD',
         'pflour': 'Latin_ExtendedD',
         'Psquirrel': 'Latin_ExtendedD',
         'psquirrel': 'Latin_ExtendedD',
         'Qbardes': 'Latin_ExtendedD',
         'qbardes': 'Latin_ExtendedD',
         'Qslstrok': 'Latin_ExtendedD',
         'qslstrok': 'Latin_ExtendedD',
         'Rrot': 'Latin_ExtendedD',
         'rrot': 'Latin_ExtendedD',
         'RUM': 'Latin_ExtendedD',
         'rum': 'Latin_ExtendedD',
         'Vdiagstrok': 'Latin_ExtendedD',
         'vdiagstrok': 'Latin_ExtendedD',
         'YYlig': 'Latin_ExtendedD',
         'yylig': 'Latin_ExtendedD',
         'Vvisigot': 'Latin_ExtendedD',
         'vvisigot': 'Latin_ExtendedD',
         'THORNbar': 'Latin_ExtendedD',
         'thornbar': 'Latin_ExtendedD',
         'THORNbardes': 'Latin_ExtendedD',
         'thornbardes': 'Latin_ExtendedD',
         'Vins': 'Latin_ExtendedD',
         'vins': 'Latin_ExtendedD',
         'ETfin': 'Latin_ExtendedD',
         'etfin': 'Latin_ExtendedD',
         'IS': 'Latin_ExtendedD',
         'is_': 'Latin_ExtendedD',
         'CONdes': 'Latin_ExtendedD',
         'condes': 'Latin_ExtendedD',
         'usmod': 'Latin_ExtendedD',
         'dtailstrok': 'Latin_ExtendedD',
         'ltailstrok': 'Latin_ExtendedD',
         'mtailstrok': 'Latin_ExtendedD',
         'ntailstrok': 'Latin_ExtendedD',
         'rtailstrok': 'Latin_ExtendedD',
         'rscaptailstrok': 'Latin_ExtendedD',
         'ttailstrok': 'Latin_ExtendedD',
         'sstrok': 'Latin_ExtendedD',
         'Drot': 'Latin_ExtendedD',
         'drot': 'Latin_ExtendedD',
         'Fins': 'Latin_ExtendedD',
         'fins': 'Latin_ExtendedD',
         'Gins': 'Latin_ExtendedD',
         'Ginsturn': 'Latin_ExtendedD',
         'ginsturn': 'Latin_ExtendedD',
         'Lturn': 'Latin_ExtendedD',
         'lturn': 'Latin_ExtendedD',
         'Rins': 'Latin_ExtendedD',
         'rins': 'Latin_ExtendedD',
         'Sins': 'Latin_ExtendedD',
         'sins': 'Latin_ExtendedD',
         'Trot': 'Latin_ExtendedD',
         'trot': 'Latin_ExtendedD',
         'Frev': 'Latin_ExtendedD',
         'Prev': 'Latin_ExtendedD',
         'Minv': 'Latin_ExtendedD',
         'Ilong': 'Latin_ExtendedD',
         'M5leg': 'Latin_ExtendedD',
         'ucurlbar': 'Characters_with_curly_bar_above',
         'AEligogon': 'Characters_with_ogonek',
         'aeligogon': 'Characters_with_ogonek',
         'AVligogon': 'Characters_with_ogonek',
         'avligogon': 'Characters_with_ogonek',
         'Cogon': 'Characters_with_ogonek',
         'cogon': 'Characters_with_ogonek',
         'Oslashogon': 'Characters_with_ogonek',
         'oslashogon': 'Characters_with_ogonek',
         'Togon': 'Characters_with_ogonek',
         'togon': 'Characters_with_ogonek',
         'aumlcirc': 'Characters_with_diaeresis_and_circumflex',
         'Oumlcirc': 'Characters_with_diaeresis_and_circumflex',
         'oumlcirc': 'Characters_with_diaeresis_and_circumflex',
         'Uumlcirc': 'Characters_with_diaeresis_and_circumflex',
         'uumlcirc': 'Characters_with_diaeresis_and_circumflex',
         'ascap': 'Phonetic_Extensions',
         'aeligscap': 'Phonetic_Extensions',
         'cscap': 'Phonetic_Extensions',
         'dscap': 'Phonetic_Extensions',
         'ethscap': 'Phonetic_Extensions',
         'escap': 'Phonetic_Extensions',
         'jscap': 'Phonetic_Extensions',
         'kscap': 'Phonetic_Extensions',
         'mscap': 'Phonetic_Extensions',
         'oscap': 'Phonetic_Extensions',
         'pscap': 'Phonetic_Extensions',
         'tscap': 'Phonetic_Extensions',
         'uscap': 'Phonetic_Extensions',
         'vscap': 'Phonetic_Extensions',
         'wscap': 'Phonetic_Extensions',
         'zscap': 'Phonetic_Extensions',
         'Imod': 'Phonetic_Extensions',
         'gins': 'Phonetic_Extensions',
         'combcircdbl': 'Combining_Diacritical_Marks_Supplement',
         'combcurl': 'Combining_Diacritical_Marks_Supplement',
         'ersub': 'Combining_Diacritical_Marks_Supplement',
         'combisbelow': 'Combining_Diacritical_Marks_Supplement',
         'ur': 'Combining_Diacritical_Marks_Supplement',
         'us': 'Combining_Diacritical_Marks_Supplement',
         'ra': 'Combining_Diacritical_Marks_Supplement',
         'aeligsup': 'Combining_Diacritical_Marks_Supplement',
         'aoligsup': 'Combining_Diacritical_Marks_Supplement',
         'avligsup': 'Combining_Diacritical_Marks_Supplement',
         'ccedilsup': 'Combining_Diacritical_Marks_Supplement',
         'drotsup': 'Combining_Diacritical_Marks_Supplement',
         'ethsup': 'Combining_Diacritical_Marks_Supplement',
         'gsup': 'Combining_Diacritical_Marks_Supplement',
         'gscapsup': 'Combining_Diacritical_Marks_Supplement',
         'ksup': 'Combining_Diacritical_Marks_Supplement',
         'lsup': 'Combining_Diacritical_Marks_Supplement',
         'lscapsup': 'Combining_Diacritical_Marks_Supplement',
         'mscapsup': 'Combining_Diacritical_Marks_Supplement',
         'nsup': 'Combining_Diacritical_Marks_Supplement',
         'nscapsup': 'Combining_Diacritical_Marks_Supplement',
         'rrotsup': 'Combining_Diacritical_Marks_Supplement',
         'ssup': 'Combining_Diacritical_Marks_Supplement',
         'slongsup': 'Combining_Diacritical_Marks_Supplement',
         'zsup': 'Combining_Diacritical_Marks_Supplement',
         'Eogondotacute': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'eogondotacute': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Oogondotacute': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'oogondotacute': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Asqu': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'aunc': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Ains': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'ains': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'aopen': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'aneckless': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'aclose': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Csqu': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'ccurl': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'dcurl': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Eunc': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Euncclose': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'eunc': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'eext': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'etall': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'finssemiclose': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'finsdothook': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'finsclose': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'fcurl': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Gsqu': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'gcurl': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'gdivloop': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'glglowloop': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'gsmlowloop': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Hunc': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'hrdes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'ilong': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'kunc': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'ksemiclose': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'kclose': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'kcurl': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'ldes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Munc': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Muncdes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'munc': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'muncdes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'mrdes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Nrdes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'nrdes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'nscaprdes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'nscapldes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'nflour': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Qstem': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'rflour': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'Sclose': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'sclose': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'slongdes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'tcurl': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'xldes': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'yrgmainstrok': 'Characters_with_ogonek_dot_above_and_acute_accent',
         'edotblacute': 'Characters_with_acute_accent_and_dot_below',
         'hslonglig': 'Modified_baseline_abbreviation_characters',
         'hslongligbar': 'Modified_baseline_abbreviation_characters',
         'kslonglig': 'Modified_baseline_abbreviation_characters',
         'kslongligbar': 'Modified_baseline_abbreviation_characters',
         'q2app': 'Modified_baseline_abbreviation_characters',
         'q3app': 'Modified_baseline_abbreviation_characters',
         'qcentrslstrok': 'Modified_baseline_abbreviation_characters',
         'rdesstrok': 'Modified_baseline_abbreviation_characters',
         'slongflour': 'Modified_baseline_abbreviation_characters',
         'slongslstrok': 'Modified_baseline_abbreviation_characters',
         'vslash': 'Modified_baseline_abbreviation_characters',
         'xslashula': 'Modified_baseline_abbreviation_characters',
         'xslashlra': 'Modified_baseline_abbreviation_characters',
         'THORNbarslash': 'Modified_baseline_abbreviation_characters',
         'thornbarslash': 'Modified_baseline_abbreviation_characters',
         'thornslonglig': 'Modified_baseline_abbreviation_characters',
         'thornslongligbar': 'Modified_baseline_abbreviation_characters',
         'Eogondotbl': 'Characters_with_dot_below_and_ogonek',
         'eogondotbl': 'Characters_with_dot_below_and_ogonek',
         'Oogondotbl': 'Characters_with_dot_below_and_ogonek',
         'oogondotbl': 'Characters_with_dot_below_and_ogonek',
         'AEligbreve': 'Characters_with_breve',
         'aeligbreve': 'Characters_with_breve',
         'Oslashbreve': 'Characters_with_breve',
         'oslashbreve': 'Characters_with_breve',
         'Ybreve': 'Characters_with_breve',
         'ybreve': 'Characters_with_breve',
         'Uvertline': 'Characters_with_vertical_bar_above',
         'uvertline': 'Characters_with_vertical_bar_above',
         'AAligdot': 'Characters_with_dot_above',
         'aaligdot': 'Characters_with_dot_above',
         'AEligdot': 'Characters_with_dot_above',
         'aeligdot': 'Characters_with_dot_above',
         'AYligdot': 'Characters_with_dot_above',
         'ayligdot': 'Characters_with_dot_above',
         'bscapdot': 'Characters_with_dot_above',
         'drotdot': 'Characters_with_dot_above',
         'dscapdot': 'Characters_with_dot_above',
         'Finsdot': 'Characters_with_dot_above',
         'finsdot': 'Characters_with_dot_above',
         'finssemiclosedot': 'Characters_with_dot_above',
         'finsclosedot': 'Characters_with_dot_above',
         'fscapdot': 'Characters_with_dot_above',
         'gscapdot': 'Characters_with_dot_above',
         'hscapdot': 'Characters_with_dot_above',
         'Jdot': 'Characters_with_dot_above',
         'Kdot': 'Characters_with_dot_above',
         'kdot': 'Characters_with_dot_above',
         'kscapdot': 'Characters_with_dot_above',
         'Ldot': 'Characters_with_dot_above',
         'ldot': 'Characters_with_dot_above',
         'lscapdot': 'Characters_with_dot_above',
         'mscapdot': 'Characters_with_dot_above',
         'nscapdot': 'Characters_with_dot_above',
         'Oslashdot': 'Characters_with_dot_above',
         'oslashdot': 'Characters_with_dot_above',
         'pscapdot': 'Characters_with_dot_above',
         'Qdot': 'Characters_with_dot_above',
         'qdot': 'Characters_with_dot_above',
         'rscapdot': 'Characters_with_dot_above',
         'sscapdot': 'Characters_with_dot_above',
         'tscapdot': 'Characters_with_dot_above',
         'Udot': 'Characters_with_dot_above',
         'udot': 'Characters_with_dot_above',
         'Vdot': 'Characters_with_dot_above',
         'vdot': 'Characters_with_dot_above',
         'Vinsdot': 'Characters_with_dot_above',
         'vinsdot': 'Characters_with_dot_above',
         'qbardestilde': 'Characters_with_tilde',
         'oopen': 'IPA_Extensions',
         'dtail': 'IPA_Extensions',
         'schwa': 'IPA_Extensions',
         'jnodotstrok': 'IPA_Extensions',
         'gopen': 'IPA_Extensions',
         'gscap': 'IPA_Extensions',
         'hhook': 'IPA_Extensions',
         'istrok': 'IPA_Extensions',
         'iscap': 'IPA_Extensions',
         'nlfhook': 'IPA_Extensions',
         'nscap': 'IPA_Extensions',
         'oeligscap': 'IPA_Extensions',
         'rdes': 'IPA_Extensions',
         'rscap': 'IPA_Extensions',
         'ubar': 'IPA_Extensions',
         'yscap': 'IPA_Extensions',
         'ezh': 'IPA_Extensions',
         'bscap': 'IPA_Extensions',
         'hscap': 'IPA_Extensions',
         'lscap': 'IPA_Extensions',
         'lringbl': 'Characters_with_ring_below',
         'mringbl': 'Characters_with_ring_below',
         'nringbl': 'Characters_with_ring_below',
         'rringbl': 'Characters_with_ring_below',
         'qscap': 'Small_capitals',
         'xscap': 'Small_capitals',
         'thornscap': 'Small_capitals',
         'quot': 'Basic_Latin',
         'amp': 'Basic_Latin',
         'apos': 'Basic_Latin',
         'lt': 'Basic_Latin',
         'gt': 'Basic_Latin',
         'excl': 'Basic_Latin',
         'dollar': 'Basic_Latin',
         'percnt': 'Basic_Latin',
         'lpar': 'Basic_Latin',
         'rpar': 'Basic_Latin',
         'ast': 'Basic_Latin',
         'plus': 'Basic_Latin',
         'comma': 'Basic_Latin',
         'hyphen': 'Basic_Latin',
         'period': 'Basic_Latin',
         'sol': 'Basic_Latin',
         'colon': 'Basic_Latin',
         'semi': 'Basic_Latin',
         'equals': 'Basic_Latin',
         'quest': 'Basic_Latin',
         'commat': 'Basic_Latin',
         'lsqb': 'Basic_Latin',
         'bsol': 'Basic_Latin',
         'rsqb': 'Basic_Latin',
         'circ': 'Basic_Latin',
         'lowbar': 'Basic_Latin',
         'grave': 'Basic_Latin',
         'lcub': 'Basic_Latin',
         'verbar': 'Basic_Latin',
         'rcub': 'Basic_Latin',
         'tld': 'Basic_Latin',
         'Bdot': 'Latin_Extended_Additional',
         'bdot': 'Latin_Extended_Additional',
         'Bdotbl': 'Latin_Extended_Additional',
         'bdotbl': 'Latin_Extended_Additional',
         'Ddot': 'Latin_Extended_Additional',
         'ddot': 'Latin_Extended_Additional',
         'Ddotbl': 'Latin_Extended_Additional',
         'ddotbl': 'Latin_Extended_Additional',
         'Emacracute': 'Latin_Extended_Additional',
         'emacracute': 'Latin_Extended_Additional',
         'Fdot': 'Latin_Extended_Additional',
         'fdot': 'Latin_Extended_Additional',
         'Hdot': 'Latin_Extended_Additional',
         'hdot': 'Latin_Extended_Additional',
         'Hdotbl': 'Latin_Extended_Additional',
         'hdotbl': 'Latin_Extended_Additional',
         'Kacute': 'Latin_Extended_Additional',
         'kacute': 'Latin_Extended_Additional',
         'Kdotbl': 'Latin_Extended_Additional',
         'kdotbl': 'Latin_Extended_Additional',
         'Ldotbl': 'Latin_Extended_Additional',
         'ldotbl': 'Latin_Extended_Additional',
         'Macute': 'Latin_Extended_Additional',
         'macute': 'Latin_Extended_Additional',
         'Mdot': 'Latin_Extended_Additional',
         'mdot': 'Latin_Extended_Additional',
         'Mdotbl': 'Latin_Extended_Additional',
         'mdotbl': 'Latin_Extended_Additional',
         'Ndot': 'Latin_Extended_Additional',
         'ndot': 'Latin_Extended_Additional',
         'Ndotbl': 'Latin_Extended_Additional',
         'ndotbl': 'Latin_Extended_Additional',
         'Omacracute': 'Latin_Extended_Additional',
         'omacracute': 'Latin_Extended_Additional',
         'Pacute': 'Latin_Extended_Additional',
         'pacute': 'Latin_Extended_Additional',
         'Pdot': 'Latin_Extended_Additional',
         'pdot': 'Latin_Extended_Additional',
         'Rdot': 'Latin_Extended_Additional',
         'rdot': 'Latin_Extended_Additional',
         'Rdotbl': 'Latin_Extended_Additional',
         'rdotbl': 'Latin_Extended_Additional',
         'Sdot': 'Latin_Extended_Additional',
         'sdot': 'Latin_Extended_Additional',
         'Sdotbl': 'Latin_Extended_Additional',
         'sdotbl': 'Latin_Extended_Additional',
         'Tdot': 'Latin_Extended_Additional',
         'tdot': 'Latin_Extended_Additional',
         'Tdotbl': 'Latin_Extended_Additional',
         'tdotbl': 'Latin_Extended_Additional',
         'Vdotbl': 'Latin_Extended_Additional',
         'vdotbl': 'Latin_Extended_Additional',
         'Wgrave': 'Latin_Extended_Additional',
         'wgrave': 'Latin_Extended_Additional',
         'Wacute': 'Latin_Extended_Additional',
         'wacute': 'Latin_Extended_Additional',
         'Wuml': 'Latin_Extended_Additional',
         'wuml': 'Latin_Extended_Additional',
         'Wdot': 'Latin_Extended_Additional',
         'wdot': 'Latin_Extended_Additional',
         'Wdotbl': 'Latin_Extended_Additional',
         'wdotbl': 'Latin_Extended_Additional',
         'Ydot': 'Latin_Extended_Additional',
         'ydot': 'Latin_Extended_Additional',
         'Zdotbl': 'Latin_Extended_Additional',
         'zdotbl': 'Latin_Extended_Additional',
         'wring': 'Latin_Extended_Additional',
         'yring': 'Latin_Extended_Additional',
         'slongbarslash': 'Latin_Extended_Additional',
         'slongbar': 'Latin_Extended_Additional',
         'SZlig': 'Latin_Extended_Additional',
         'dscript': 'Latin_Extended_Additional',
         'Adotbl': 'Latin_Extended_Additional',
         'adotbl': 'Latin_Extended_Additional',
         'Ahook': 'Latin_Extended_Additional',
         'ahook': 'Latin_Extended_Additional',
         'Abreveacute': 'Latin_Extended_Additional',
         'abreveacute': 'Latin_Extended_Additional',
         'Edotbl': 'Latin_Extended_Additional',
         'edotbl': 'Latin_Extended_Additional',
         'Ihook': 'Latin_Extended_Additional',
         'ihook': 'Latin_Extended_Additional',
         'Idotbl': 'Latin_Extended_Additional',
         'idotbl': 'Latin_Extended_Additional',
         'Odotbl': 'Latin_Extended_Additional',
         'odotbl': 'Latin_Extended_Additional',
         'Ohook': 'Latin_Extended_Additional',
         'ohook': 'Latin_Extended_Additional',
         'Udotbl': 'Latin_Extended_Additional',
         'udotbl': 'Latin_Extended_Additional',
         'Uhook': 'Latin_Extended_Additional',
         'uhook': 'Latin_Extended_Additional',
         'Ygrave': 'Latin_Extended_Additional',
         'ygrave': 'Latin_Extended_Additional',
         'Ydotbl': 'Latin_Extended_Additional',
         'ydotbl': 'Latin_Extended_Additional',
         'Yhook': 'Latin_Extended_Additional',
         'yhook': 'Latin_Extended_Additional',
         'LLwelsh': 'Latin_Extended_Additional',
         'llwelsh': 'Latin_Extended_Additional',
         'Vwelsh': 'Latin_Extended_Additional',
         'vwelsh': 'Latin_Extended_Additional',
         'Yloop': 'Latin_Extended_Additional',
         'yloop': 'Latin_Extended_Additional',
         'nbar': 'Modified_baseline_characters',
         'vbar': 'Modified_baseline_characters',
         'ybar': 'Modified_baseline_characters',
         'tridotright': 'Georgian',
         'alpha': 'Greek_and_Coptic',
         'beta': 'Greek_and_Coptic',
         'gamma': 'Greek_and_Coptic',
         'delta': 'Greek_and_Coptic',
         'epsilon': 'Greek_and_Coptic',
         'zeta': 'Greek_and_Coptic',
         'eta': 'Greek_and_Coptic',
         'Theta': 'Greek_and_Coptic',
         'theta': 'Greek_and_Coptic',
         'iota': 'Greek_and_Coptic',
         'kappa': 'Greek_and_Coptic',
         'lambda_': 'Greek_and_Coptic',
         'mu': 'Greek_and_Coptic',
         'nu': 'Greek_and_Coptic',
         'xi': 'Greek_and_Coptic',
         'omicron': 'Greek_and_Coptic',
         'pi': 'Greek_and_Coptic',
         'rho': 'Greek_and_Coptic',
         'sigmaf': 'Greek_and_Coptic',
         'sigma': 'Greek_and_Coptic',
         'tau': 'Greek_and_Coptic',
         'upsilon': 'Greek_and_Coptic',
         'phi': 'Greek_and_Coptic',
         'chi': 'Greek_and_Coptic',
         'psi': 'Greek_and_Coptic',
         'omega': 'Greek_and_Coptic',
         'lwhsqb': 'Miscellaneous_Mathematical_SymbolsA',
         'rwhsqb': 'Miscellaneous_Mathematical_SymbolsA',
         'langb': 'Miscellaneous_Mathematical_SymbolsA',
         'rangb': 'Miscellaneous_Mathematical_SymbolsA',
         'aacloselig': 'Ligatures',
         'aeligred': 'Ligatures',
         'AnecklessElig': 'Ligatures',
         'anecklesselig': 'Ligatures',
         'anecklessvlig': 'Ligatures',
         'AOligred': 'Ligatures',
         'aoligred': 'Ligatures',
         'aflig': 'Ligatures',
         'afinslig': 'Ligatures',
         'aglig': 'Ligatures',
         'allig': 'Ligatures',
         'anlig': 'Ligatures',
         'anscaplig': 'Ligatures',
         'aplig': 'Ligatures',
         'arlig': 'Ligatures',
         'arscaplig': 'Ligatures',
         'athornlig': 'Ligatures',
         'bblig': 'Ligatures',
         'bglig': 'Ligatures',
         'cklig': 'Ligatures',
         'ctlig': 'Ligatures',
         'drotdrotlig': 'Ligatures',
         'eylig': 'Ligatures',
         'faumllig': 'Ligatures',
         'fjlig': 'Ligatures',
         'foumllig': 'Ligatures',
         'frlig': 'Ligatures',
         'ftlig': 'Ligatures',
         'fuumllig': 'Ligatures',
         'fylig': 'Ligatures',
         'fftlig': 'Ligatures',
         'ffylig': 'Ligatures',
         'ftylig': 'Ligatures',
         'gglig': 'Ligatures',
         'gdlig': 'Ligatures',
         'gdrotlig': 'Ligatures',
         'gethlig': 'Ligatures',
         'golig': 'Ligatures',
         'gplig': 'Ligatures',
         'grlig': 'Ligatures',
         'qvinslig': 'Ligatures',
         'hrarmlig': 'Ligatures',
         'Hrarmlig': 'Ligatures',
         'krarmlig': 'Ligatures',
         'lllig': 'Ligatures',
         'nscapslonglig': 'Ligatures',
         'oclig': 'Ligatures',
         'PPlig': 'Ligatures',
         'pplig': 'Ligatures',
         'ppflourlig': 'Ligatures',
         'slongaumllig': 'Ligatures',
         'slongchlig': 'Ligatures',
         'slonghlig': 'Ligatures',
         'slongilig': 'Ligatures',
         'slongjlig': 'Ligatures',
         'slongklig': 'Ligatures',
         'slongllig': 'Ligatures',
         'slongoumllig': 'Ligatures',
         'slongplig': 'Ligatures',
         'slongslig': 'Ligatures',
         'slongslonglig': 'Ligatures',
         'slongslongilig': 'Ligatures',
         'slongslongklig': 'Ligatures',
         'slongslongllig': 'Ligatures',
         'slongslongtlig': 'Ligatures',
         'slongtilig': 'Ligatures',
         'slongtrlig': 'Ligatures',
         'slonguumllig': 'Ligatures',
         'slongvinslig': 'Ligatures',
         'slongdestlig': 'Ligatures',
         'trlig': 'Ligatures',
         'ttlig': 'Ligatures',
         'trottrotlig': 'Ligatures',
         'tylig': 'Ligatures',
         'tzlig': 'Ligatures',
         'thornrarmlig': 'Ligatures',
         'ncirc': 'Characters_with_circumflex',
         'Vcirc': 'Characters_with_circumflex',
         'vcirc': 'Characters_with_circumflex',
         'eacombcirc': 'Characters_with_circumflex',
         'eucombcirc': 'Characters_with_circumflex',
         'ibrevinvbl': 'Characters_with_breve_below',
         'ubrevinvbl': 'Characters_with_breve_below',
         'Eogondot': 'Characters_with_dot_above_and_ogonek',
         'eogondot': 'Characters_with_dot_above_and_ogonek',
         'Oogondot': 'Characters_with_dot_above_and_ogonek',
         'oogondot': 'Characters_with_dot_above_and_ogonek',
         'Eogondblac': 'Characters_with_double_acute_accent_and_ogonek',
         'eogondblac': 'Characters_with_double_acute_accent_and_ogonek',
         'Oogondblac': 'Characters_with_double_acute_accent_and_ogonek',
         'oogondblac': 'Characters_with_double_acute_accent_and_ogonek',
         'Aogonacute': 'Characters_with_acute_accent_and_ogonek',
         'aogonacute': 'Characters_with_acute_accent_and_ogonek',
         'aeligogonacute': 'Characters_with_acute_accent_and_ogonek',
         'Eogonacute': 'Characters_with_acute_accent_and_ogonek',
         'eogonacute': 'Characters_with_acute_accent_and_ogonek',
         'Oogonacute': 'Characters_with_acute_accent_and_ogonek',
         'oogonacute': 'Characters_with_acute_accent_and_ogonek',
         'Oslashogonacute': 'Characters_with_acute_accent_and_ogonek',
         'oslashogonacute': 'Characters_with_acute_accent_and_ogonek',
         'Adotacute': 'Characters_with_acute_accent_and_dot_above',
         'adotacute': 'Characters_with_acute_accent_and_dot_above',
         'Edotacute': 'Characters_with_acute_accent_and_dot_above',
         'edotacute': 'Characters_with_acute_accent_and_dot_above',
         'Idotacute': 'Characters_with_acute_accent_and_dot_above',
         'idotacute': 'Characters_with_acute_accent_and_dot_above',
         'Odotacute': 'Characters_with_acute_accent_and_dot_above',
         'odotacute': 'Characters_with_acute_accent_and_dot_above',
         'Oslashdotacute': 'Characters_with_acute_accent_and_dot_above',
         'oslashdotacute': 'Characters_with_acute_accent_and_dot_above',
         'Udotacute': 'Characters_with_acute_accent_and_dot_above',
         'udotacute': 'Characters_with_acute_accent_and_dot_above',
         'Ydotacute': 'Characters_with_acute_accent_and_dot_above',
         'ydotacute': 'Characters_with_acute_accent_and_dot_above',
         'bstrok': 'Latin_ExtendedB',
         'hwair': 'Latin_ExtendedB',
         'khook': 'Latin_ExtendedB',
         'lbar': 'Latin_ExtendedB',
         'nlrleg': 'Latin_ExtendedB',
         'YR': 'Latin_ExtendedB',
         'Zstrok': 'Latin_ExtendedB',
         'zstrok': 'Latin_ExtendedB',
         'EZH': 'Latin_ExtendedB',
         'wynn': 'Latin_ExtendedB',
         'Ocar': 'Latin_ExtendedB',
         'ocar': 'Latin_ExtendedB',
         'Ucar': 'Latin_ExtendedB',
         'ucar': 'Latin_ExtendedB',
         'Uumlmacr': 'Latin_ExtendedB',
         'uumlmacr': 'Latin_ExtendedB',
         'AEligmacr': 'Latin_ExtendedB',
         'aeligmacr': 'Latin_ExtendedB',
         'Gstrok': 'Latin_ExtendedB',
         'gstrok': 'Latin_ExtendedB',
         'Oogon': 'Latin_ExtendedB',
         'oogon': 'Latin_ExtendedB',
         'Oogonmacr': 'Latin_ExtendedB',
         'oogonmacr': 'Latin_ExtendedB',
         'Gacute': 'Latin_ExtendedB',
         'gacute': 'Latin_ExtendedB',
         'HWAIR': 'Latin_ExtendedB',
         'WYNN': 'Latin_ExtendedB',
         'AEligacute': 'Latin_ExtendedB',
         'aeligacute': 'Latin_ExtendedB',
         'Oslashacute': 'Latin_ExtendedB',
         'oslashacute': 'Latin_ExtendedB',
         'YOGH': 'Latin_ExtendedB',
         'yogh': 'Latin_ExtendedB',
         'Adot': 'Latin_ExtendedB',
         'adot': 'Latin_ExtendedB',
         'Oumlmacr': 'Latin_ExtendedB',
         'oumlmacr': 'Latin_ExtendedB',
         'Odot': 'Latin_ExtendedB',
         'odot': 'Latin_ExtendedB',
         'Ymacr': 'Latin_ExtendedB',
         'ymacr': 'Latin_ExtendedB',
         'jnodot': 'Latin_ExtendedB',
         'Jbar': 'Latin_ExtendedB',
         'jbar': 'Latin_ExtendedB',
         'Amacracute': 'Characters_with_macron_and_acute_accent',
         'amacracute': 'Characters_with_macron_and_acute_accent',
         'AEligmacracute': 'Characters_with_macron_and_acute_accent',
         'aeligmacracute': 'Characters_with_macron_and_acute_accent',
         'Imacracute': 'Characters_with_macron_and_acute_accent',
         'imacracute': 'Characters_with_macron_and_acute_accent',
         'Oslashmacracute': 'Characters_with_macron_and_acute_accent',
         'oslashmacracute': 'Characters_with_macron_and_acute_accent',
         'Umacracute': 'Characters_with_macron_and_acute_accent',
         'umacracute': 'Characters_with_macron_and_acute_accent',
         'Ymacracute': 'Characters_with_macron_and_acute_accent',
         'ymacracute': 'Characters_with_macron_and_acute_accent',
         'aenl': 'Enlarged_minuscules',
         'aenlacute': 'Enlarged_minuscules',
         'aaligenl': 'Enlarged_minuscules',
         'aeligenl': 'Enlarged_minuscules',
         'aoligenl': 'Enlarged_minuscules',
         'aenlosmalllig': 'Enlarged_minuscules',
         'benl': 'Enlarged_minuscules',
         'cenl': 'Enlarged_minuscules',
         'denl': 'Enlarged_minuscules',
         'drotenl': 'Enlarged_minuscules',
         'ethenl': 'Enlarged_minuscules',
         'eenl': 'Enlarged_minuscules',
         'eogonenl': 'Enlarged_minuscules',
         'fenl': 'Enlarged_minuscules',
         'finsenl': 'Enlarged_minuscules',
         'genl': 'Enlarged_minuscules',
         'henl': 'Enlarged_minuscules',
         'ienl': 'Enlarged_minuscules',
         'inodotenl': 'Enlarged_minuscules',
         'jenl': 'Enlarged_minuscules',
         'jnodotenl': 'Enlarged_minuscules',
         'kenl': 'Enlarged_minuscules',
         'lenl': 'Enlarged_minuscules',
         'menl': 'Enlarged_minuscules',
         'nenl': 'Enlarged_minuscules',
         'oenl': 'Enlarged_minuscules',
         'oeligenl': 'Enlarged_minuscules',
         'penl': 'Enlarged_minuscules',
         'qenl': 'Enlarged_minuscules',
         'renl': 'Enlarged_minuscules',
         'senl': 'Enlarged_minuscules',
         'slongenl': 'Enlarged_minuscules',
         'tenl': 'Enlarged_minuscules',
         'uenl': 'Enlarged_minuscules',
         'venl': 'Enlarged_minuscules',
         'wenl': 'Enlarged_minuscules',
         'xenl': 'Enlarged_minuscules',
         'yenl': 'Enlarged_minuscules',
         'zenl': 'Enlarged_minuscules',
         'thornenl': 'Enlarged_minuscules',
         'apomod': 'Spacing_Modifying_Letters',
         'verbarup': 'Spacing_Modifying_Letters',
         'breve': 'Spacing_Modifying_Letters',
         'dot': 'Spacing_Modifying_Letters',
         'ring': 'Spacing_Modifying_Letters',
         'ogon': 'Spacing_Modifying_Letters',
         'tilde': 'Spacing_Modifying_Letters',
         'dblac': 'Spacing_Modifying_Letters',
         'xmod': 'Spacing_Modifying_Letters',
         'aeligring': 'Characters_with_ring_above',
         'ering': 'Characters_with_ring_above',
         'oring': 'Characters_with_ring_above',
         'vring': 'Characters_with_ring_above',
         'fflig': 'Alphabetic_Presentation_Forms',
         'filig': 'Alphabetic_Presentation_Forms',
         'fllig': 'Alphabetic_Presentation_Forms',
         'ffilig': 'Alphabetic_Presentation_Forms',
         'ffllig': 'Alphabetic_Presentation_Forms',
         'slongtlig': 'Alphabetic_Presentation_Forms',
         'stlig': 'Alphabetic_Presentation_Forms',
         'adotbluml': 'Characters_with_diaeresis_and_dot_below',
         'aringcirc': 'Characters_with_ring_above_and_circumflex',
         'arrsgllw': 'Arrows',
         'arrsglupw': 'Arrows',
         'arrsglrw': 'Arrows',
         'arrsgldw': 'Arrows',
         'macrhigh': 'Characters_with_macron_or_overline',
         'macrmed': 'Characters_with_macron_or_overline',
         'ovlhigh': 'Characters_with_macron_or_overline',
         'ovlmed': 'Characters_with_macron_or_overline',
         'bovlmed': 'Characters_with_macron_or_overline',
         'Covlhigh': 'Characters_with_macron_or_overline',
         'romnumCrevovl': 'Characters_with_macron_or_overline',
         'Dovlhigh': 'Characters_with_macron_or_overline',
         'dovlmed': 'Characters_with_macron_or_overline',
         'Eogonmacr': 'Characters_with_macron_or_overline',
         'eogonmacr': 'Characters_with_macron_or_overline',
         'hovlmed': 'Characters_with_macron_or_overline',
         'Iovlhigh': 'Characters_with_macron_or_overline',
         'iovlmed': 'Characters_with_macron_or_overline',
         'Jmacrhigh': 'Characters_with_macron_or_overline',
         'Jovlhigh': 'Characters_with_macron_or_overline',
         'jmacrmed': 'Characters_with_macron_or_overline',
         'jovlmed': 'Characters_with_macron_or_overline',
         'kovlmed': 'Characters_with_macron_or_overline',
         'lovlmed': 'Characters_with_macron_or_overline',
         'Lovlhigh': 'Characters_with_macron_or_overline',
         'lmacrhigh': 'Characters_with_macron_or_overline',
         'lovlhigh': 'Characters_with_macron_or_overline',
         'Mmacrhigh': 'Characters_with_macron_or_overline',
         'Movlhigh': 'Characters_with_macron_or_overline',
         'mmacrmed': 'Characters_with_macron_or_overline',
         'movlmed': 'Characters_with_macron_or_overline',
         'Nmacrhigh': 'Characters_with_macron_or_overline',
         'nmacrmed': 'Characters_with_macron_or_overline',
         'Oslashmacr': 'Characters_with_macron_or_overline',
         'oslashmacr': 'Characters_with_macron_or_overline',
         'OEligmacr': 'Characters_with_macron_or_overline',
         'oeligmacr': 'Characters_with_macron_or_overline',
         'oopenmacr': 'Characters_with_macron_or_overline',
         'pmacr': 'Characters_with_macron_or_overline',
         'qmacr': 'Characters_with_macron_or_overline',
         'slongovlmed': 'Characters_with_macron_or_overline',
         'Vmacr': 'Characters_with_macron_or_overline',
         'Vovlhigh': 'Characters_with_macron_or_overline',
         'vmacr': 'Characters_with_macron_or_overline',
         'Wmacr': 'Characters_with_macron_or_overline',
         'wmacr': 'Characters_with_macron_or_overline',
         'Xovlhigh': 'Characters_with_macron_or_overline',
         'thornovlmed': 'Characters_with_macron_or_overline',
         'enqd': 'General_Punctuation',
         'emqd': 'General_Punctuation',
         'ensp': 'General_Punctuation',
         'emsp': 'General_Punctuation',
         'emsp13': 'General_Punctuation',
         'emsp14': 'General_Punctuation',
         'emsp16': 'General_Punctuation',
         'numsp': 'General_Punctuation',
         'puncsp': 'General_Punctuation',
         'thinsp': 'General_Punctuation',
         'hairsp': 'General_Punctuation',
         'zerosp': 'General_Punctuation',
         'dash': 'General_Punctuation',
         'nbhy': 'General_Punctuation',
         'numdash': 'General_Punctuation',
         'ndash': 'General_Punctuation',
         'mdash': 'General_Punctuation',
         'horbar': 'General_Punctuation',
         'Verbar': 'General_Punctuation',
         'lsquo': 'General_Punctuation',
         'rsquo': 'General_Punctuation',
         'lsquolow': 'General_Punctuation',
         'rsquorev': 'General_Punctuation',
         'ldquo': 'General_Punctuation',
         'rdquo': 'General_Punctuation',
         'ldquolow': 'General_Punctuation',
         'rdquorev': 'General_Punctuation',
         'dagger': 'General_Punctuation',
         'Dagger': 'General_Punctuation',
         'bull': 'General_Punctuation',
         'tribull': 'General_Punctuation',
         'sgldr': 'General_Punctuation',
         'dblldr': 'General_Punctuation',
         'hellip': 'General_Punctuation',
         'hyphpoint': 'General_Punctuation',
         'nnbsp': 'General_Punctuation',
         'permil': 'General_Punctuation',
         'prime': 'General_Punctuation',
         'Prime': 'General_Punctuation',
         'lsaquo': 'General_Punctuation',
         'rsaquo': 'General_Punctuation',
         'refmark': 'General_Punctuation',
         'triast': 'General_Punctuation',
         'fracsol': 'General_Punctuation',
         'lsqbqu': 'General_Punctuation',
         'rsqbqu': 'General_Punctuation',
         'et': 'General_Punctuation',
         'revpara': 'General_Punctuation',
         'lozengedot': 'General_Punctuation',
         'dotcross': 'General_Punctuation',
         'Adblac': 'Characters_with_double_acute_accent',
         'adblac': 'Characters_with_double_acute_accent',
         'AAligdblac': 'Characters_with_double_acute_accent',
         'aaligdblac': 'Characters_with_double_acute_accent',
         'AEligdblac': 'Characters_with_double_acute_accent',
         'aeligdblac': 'Characters_with_double_acute_accent',
         'AOligdblac': 'Characters_with_double_acute_accent',
         'aoligdblac': 'Characters_with_double_acute_accent',
         'AVligdblac': 'Characters_with_double_acute_accent',
         'avligdblac': 'Characters_with_double_acute_accent',
         'Edblac': 'Characters_with_double_acute_accent',
         'edblac': 'Characters_with_double_acute_accent',
         'Idblac': 'Characters_with_double_acute_accent',
         'idblac': 'Characters_with_double_acute_accent',
         'Jdblac': 'Characters_with_double_acute_accent',
         'jdblac': 'Characters_with_double_acute_accent',
         'Oslashdblac': 'Characters_with_double_acute_accent',
         'oslashdblac': 'Characters_with_double_acute_accent',
         'OEligdblac': 'Characters_with_double_acute_accent',
         'oeligdblac': 'Characters_with_double_acute_accent',
         'OOligdblac': 'Characters_with_double_acute_accent',
         'ooligdblac': 'Characters_with_double_acute_accent',
         'Vdblac': 'Characters_with_double_acute_accent',
         'vdblac': 'Characters_with_double_acute_accent',
         'Wdblac': 'Characters_with_double_acute_accent',
         'wdblac': 'Characters_with_double_acute_accent',
         'Ydblac': 'Characters_with_double_acute_accent',
         'ydblac': 'Characters_with_double_acute_accent',
         'YYligdblac': 'Characters_with_double_acute_accent',
         'yyligdblac': 'Characters_with_double_acute_accent',
         'scruple': 'Letterlike_Symbols',
         'lbbar': 'Letterlike_Symbols',
         'Rtailstrok': 'Letterlike_Symbols',
         'Rslstrok': 'Letterlike_Symbols',
         'Vslstrok': 'Letterlike_Symbols',
         'ounce': 'Letterlike_Symbols',
         'Fturn': 'Letterlike_Symbols',
         'fturn': 'Letterlike_Symbols',
         'eogoncirc': 'Characters_with_ogonek_and_circumflex',
         'oogoncirc': 'Characters_with_ogonek_and_circumflex',
         'dblsol': 'Supplemental_Mathematical_Operators',
         'arbar': 'Combining_marks',
         'erang': 'Combining_marks',
         'ercurl': 'Combining_marks',
         'rabar': 'Combining_marks',
         'urrot': 'Combining_marks',
         'urlemn': 'Combining_marks',
         'combcurlhigh': 'Combining_marks',
         'combdothigh': 'Combining_marks',
         'combcurlbar': 'Combining_marks',
         'combtripbrevebl': 'Combining_marks',
         'romaslibr': 'Weight_currency_and_measurement',
         'romXbar': 'Weight_currency_and_measurement',
         'romscapxbar': 'Weight_currency_and_measurement',
         'romscapybar': 'Weight_currency_and_measurement',
         'romscapdslash': 'Weight_currency_and_measurement',
         'dram': 'Weight_currency_and_measurement',
         'ecu': 'Weight_currency_and_measurement',
         'florloop': 'Weight_currency_and_measurement',
         'grosch': 'Weight_currency_and_measurement',
         'libradut': 'Weight_currency_and_measurement',
         'librafren': 'Weight_currency_and_measurement',
         'libraital': 'Weight_currency_and_measurement',
         'libraflem': 'Weight_currency_and_measurement',
         'liranuov': 'Weight_currency_and_measurement',
         'lirasterl': 'Weight_currency_and_measurement',
         'markold': 'Weight_currency_and_measurement',
         'markflour': 'Weight_currency_and_measurement',
         'msign': 'Weight_currency_and_measurement',
         'msignflour': 'Weight_currency_and_measurement',
         'obol': 'Weight_currency_and_measurement',
         'penningar': 'Weight_currency_and_measurement',
         'reichtalold': 'Weight_currency_and_measurement',
         'schillgerm': 'Weight_currency_and_measurement',
         'schillgermscript': 'Weight_currency_and_measurement',
         'scudi': 'Weight_currency_and_measurement',
         'sestert': 'Weight_currency_and_measurement',
         'sextans': 'Weight_currency_and_measurement',
         'ouncescript': 'Weight_currency_and_measurement',
         'romas': 'Weight_currency_and_measurement',
         'romunc': 'Weight_currency_and_measurement',
         'romsemunc': 'Weight_currency_and_measurement',
         'romsext': 'Weight_currency_and_measurement',
         'romdimsext': 'Weight_currency_and_measurement',
         'romsiliq': 'Weight_currency_and_measurement',
         'romquin': 'Weight_currency_and_measurement',
         'romdupond': 'Weight_currency_and_measurement',
         'AAliguml': 'Characters_with_diaeresis',
         'aaliguml': 'Characters_with_diaeresis',
         'AEliguml': 'Characters_with_diaeresis',
         'aeliguml': 'Characters_with_diaeresis',
         'Juml': 'Characters_with_diaeresis',
         'juml': 'Characters_with_diaeresis',
         'OOliguml': 'Characters_with_diaeresis',
         'ooliguml': 'Characters_with_diaeresis',
         'PPliguml': 'Characters_with_diaeresis',
         'ppliguml': 'Characters_with_diaeresis',
         'Vuml': 'Characters_with_diaeresis',
         'vuml': 'Characters_with_diaeresis',
         'YYliguml': 'Characters_with_diaeresis',
         'yyliguml': 'Characters_with_diaeresis',
         'adiaguml': 'Characters_with_diaeresis',
         'odiaguml': 'Characters_with_diaeresis',
         'Ocurlacute': 'Characters_with_acute_accent_and_curl_above_reversed_ogonek',
         'ocurlacute': 'Characters_with_acute_accent_and_curl_above_reversed_ogonek',
         'metranc': 'Metrical_symbols',
         'metrancacute': 'Metrical_symbols',
         'metrancdblac': 'Metrical_symbols',
         'metrancgrave': 'Metrical_symbols',
         'metrancdblgrave': 'Metrical_symbols',
         'metrbreve': 'Metrical_symbols',
         'metrbreveacute': 'Metrical_symbols',
         'metrbrevedblac': 'Metrical_symbols',
         'metrbrevegrave': 'Metrical_symbols',
         'metrbrevedblgrave': 'Metrical_symbols',
         'metrmacr': 'Metrical_symbols',
         'metrmacracute': 'Metrical_symbols',
         'metrmacrdblac': 'Metrical_symbols',
         'metrmacrgrave': 'Metrical_symbols',
         'metrmacrdblgrave': 'Metrical_symbols',
         'metrmacrbreve': 'Metrical_symbols',
         'metrbrevemacr': 'Metrical_symbols',
         'metrmacrbreveacute': 'Metrical_symbols',
         'metrmacrbrevegrave': 'Metrical_symbols',
         'metrdblbrevemacr': 'Metrical_symbols',
         'metrdblbrevemacracute': 'Metrical_symbols',
         'metrdblbrevemacrdblac': 'Metrical_symbols',
         'metrpause': 'Metrical_symbols',
         'midring': 'Critical_and_epigraphical_signs',
         'ramus': 'Critical_and_epigraphical_signs',
         'anligsup': 'Combining_superscript_characters',
         'anscapligsup': 'Combining_superscript_characters',
         'arligsup': 'Combining_superscript_characters',
         'arscapligsup': 'Combining_superscript_characters',
         'bsup': 'Combining_superscript_characters',
         'bscapsup': 'Combining_superscript_characters',
         'dscapsup': 'Combining_superscript_characters',
         'eogonsup': 'Combining_superscript_characters',
         'emacrsup': 'Combining_superscript_characters',
         'fsup': 'Combining_superscript_characters',
         'inodotsup': 'Combining_superscript_characters',
         'jsup': 'Combining_superscript_characters',
         'jnodotsup': 'Combining_superscript_characters',
         'kscapsup': 'Combining_superscript_characters',
         'omacrsup': 'Combining_superscript_characters',
         'oogonsup': 'Combining_superscript_characters',
         'oslashsup': 'Combining_superscript_characters',
         'orrotsup': 'Combining_superscript_characters',
         'orumsup': 'Combining_superscript_characters',
         'psup': 'Combining_superscript_characters',
         'qsup': 'Combining_superscript_characters',
         'rumsup': 'Combining_superscript_characters',
         'tscapsup': 'Combining_superscript_characters',
         'trotsup': 'Combining_superscript_characters',
         'wsup': 'Combining_superscript_characters',
         'ysup': 'Combining_superscript_characters',
         'thornsup': 'Combining_superscript_characters',
         'Acurl': 'Characters_with_curl_above_reversed_ogonek',
         'acurl': 'Characters_with_curl_above_reversed_ogonek',
         'AEligcurl': 'Characters_with_curl_above_reversed_ogonek',
         'aeligcurl': 'Characters_with_curl_above_reversed_ogonek',
         'Ecurl': 'Characters_with_curl_above_reversed_ogonek',
         'ecurl': 'Characters_with_curl_above_reversed_ogonek',
         'Icurl': 'Characters_with_curl_above_reversed_ogonek',
         'icurl': 'Characters_with_curl_above_reversed_ogonek',
         'Jcurl': 'Characters_with_curl_above_reversed_ogonek',
         'jcurl': 'Characters_with_curl_above_reversed_ogonek',
         'Ocurl': 'Characters_with_curl_above_reversed_ogonek',
         'ocurl': 'Characters_with_curl_above_reversed_ogonek',
         'Oslashcurl': 'Characters_with_curl_above_reversed_ogonek',
         'oslashcurl': 'Characters_with_curl_above_reversed_ogonek',
         'Ucurl': 'Characters_with_curl_above_reversed_ogonek',
         'ucurl': 'Characters_with_curl_above_reversed_ogonek',
         'Ycurl': 'Characters_with_curl_above_reversed_ogonek',
         'ycurl': 'Characters_with_curl_above_reversed_ogonek',
         'squareblsm': 'Geometric_Shapes',
         'squarewhsm': 'Geometric_Shapes',
         'trirightwh': 'Geometric_Shapes',
         'trileftwh': 'Geometric_Shapes',
         'circledot': 'Geometric_Shapes',
         'Aesup': 'Characters_with_superscript_letters',
         'aesup': 'Characters_with_superscript_letters',
         'aisup': 'Characters_with_superscript_letters',
         'aosup': 'Characters_with_superscript_letters',
         'ausup': 'Characters_with_superscript_letters',
         'avsup': 'Characters_with_superscript_letters',
         'Easup': 'Characters_with_superscript_letters',
         'easup': 'Characters_with_superscript_letters',
         'eesup': 'Characters_with_superscript_letters',
         'eisup': 'Characters_with_superscript_letters',
         'eosup': 'Characters_with_superscript_letters',
         'evsup': 'Characters_with_superscript_letters',
         'iasup': 'Characters_with_superscript_letters',
         'iesup': 'Characters_with_superscript_letters',
         'iosup': 'Characters_with_superscript_letters',
         'iusup': 'Characters_with_superscript_letters',
         'ivsup': 'Characters_with_superscript_letters',
         'jesup': 'Characters_with_superscript_letters',
         'mesup': 'Characters_with_superscript_letters',
         'oasup': 'Characters_with_superscript_letters',
         'Oesup': 'Characters_with_superscript_letters',
         'oesup': 'Characters_with_superscript_letters',
         'oisup': 'Characters_with_superscript_letters',
         'oosup': 'Characters_with_superscript_letters',
         'Ousup': 'Characters_with_superscript_letters',
         'ousup': 'Characters_with_superscript_letters',
         'ovsup': 'Characters_with_superscript_letters',
         'resup': 'Characters_with_superscript_letters',
         'uasup': 'Characters_with_superscript_letters',
         'Uesup': 'Characters_with_superscript_letters',
         'uesup': 'Characters_with_superscript_letters',
         'uisup': 'Characters_with_superscript_letters',
         'Uosup': 'Characters_with_superscript_letters',
         'uosup': 'Characters_with_superscript_letters',
         'uvsup': 'Characters_with_superscript_letters',
         'uwsup': 'Characters_with_superscript_letters',
         'yesup': 'Characters_with_superscript_letters',
         'wasup': 'Characters_with_superscript_letters',
         'Wesup': 'Characters_with_superscript_letters',
         'wesup': 'Characters_with_superscript_letters',
         'wisup': 'Characters_with_superscript_letters',
         'wosup': 'Characters_with_superscript_letters',
         'wusup': 'Characters_with_superscript_letters',
         'wvsup': 'Characters_with_superscript_letters',
         'combgrave': 'Combining_Diacritical_Marks',
         'combacute': 'Combining_Diacritical_Marks',
         'combcirc': 'Combining_Diacritical_Marks',
         'combtilde': 'Combining_Diacritical_Marks',
         'combmacr': 'Combining_Diacritical_Marks',
         'bar': 'Combining_Diacritical_Marks',
         'combbreve': 'Combining_Diacritical_Marks',
         'combdot': 'Combining_Diacritical_Marks',
         'combuml': 'Combining_Diacritical_Marks',
         'combhook': 'Combining_Diacritical_Marks',
         'combring': 'Combining_Diacritical_Marks',
         'combdblac': 'Combining_Diacritical_Marks',
         'combsgvertl': 'Combining_Diacritical_Marks',
         'combdbvertl': 'Combining_Diacritical_Marks',
         'combcomma': 'Combining_Diacritical_Marks',
         'combdotbl': 'Combining_Diacritical_Marks',
         'combced': 'Combining_Diacritical_Marks',
         'combogon': 'Combining_Diacritical_Marks',
         'barbl': 'Combining_Diacritical_Marks',
         'dblbarbl': 'Combining_Diacritical_Marks',
         'baracr': 'Combining_Diacritical_Marks',
         'combtildevert': 'Combining_Diacritical_Marks',
         'dblovl': 'Combining_Diacritical_Marks',
         'combastbl': 'Combining_Diacritical_Marks',
         'er': 'Combining_Diacritical_Marks',
         'combdblbrevebl': 'Combining_Diacritical_Marks',
         'asup': 'Combining_Diacritical_Marks',
         'esup': 'Combining_Diacritical_Marks',
         'isup': 'Combining_Diacritical_Marks',
         'osup': 'Combining_Diacritical_Marks',
         'usup': 'Combining_Diacritical_Marks',
         'csup': 'Combining_Diacritical_Marks',
         'dsup': 'Combining_Diacritical_Marks',
         'hsup': 'Combining_Diacritical_Marks',
         'msup': 'Combining_Diacritical_Marks',
         'rsup': 'Combining_Diacritical_Marks',
         'tsup': 'Combining_Diacritical_Marks',
         'vsup': 'Combining_Diacritical_Marks',
         'xsup': 'Combining_Diacritical_Marks',
         'USbase': 'Baseline_abbreviation_characters',
         'usbase': 'Baseline_abbreviation_characters',
         'ET': 'Baseline_abbreviation_characters',
         'ETslash': 'Baseline_abbreviation_characters',
         'etslash': 'Baseline_abbreviation_characters',
         'de': 'Baseline_abbreviation_characters',
         'sem': 'Baseline_abbreviation_characters',
         'smallzero': 'Additional_number_forms',
         'Vmod': 'Additional_number_forms',
         'Xmod': 'Additional_number_forms',
         'AAligacute': 'Characters_with_acute_accent',
         'aaligacute': 'Characters_with_acute_accent',
         'AOligacute': 'Characters_with_acute_accent',
         'aoligacute': 'Characters_with_acute_accent',
         'AUligacute': 'Characters_with_acute_accent',
         'auligacute': 'Characters_with_acute_accent',
         'AVligacute': 'Characters_with_acute_accent',
         'avligacute': 'Characters_with_acute_accent',
         'AVligslashacute': 'Characters_with_acute_accent',
         'avligslashacute': 'Characters_with_acute_accent',
         'Bacute': 'Characters_with_acute_accent',
         'bacute': 'Characters_with_acute_accent',
         'Dacute': 'Characters_with_acute_accent',
         'dacute': 'Characters_with_acute_accent',
         'drotacute': 'Characters_with_acute_accent',
         'Facute': 'Characters_with_acute_accent',
         'facute': 'Characters_with_acute_accent',
         'Finsacute': 'Characters_with_acute_accent',
         'finsacute': 'Characters_with_acute_accent',
         'Hacute': 'Characters_with_acute_accent',
         'hacute': 'Characters_with_acute_accent',
         'Jacute': 'Characters_with_acute_accent',
         'jacute': 'Characters_with_acute_accent',
         'Muncacute': 'Characters_with_acute_accent',
         'muncacute': 'Characters_with_acute_accent',
         'OEligacute': 'Characters_with_acute_accent',
         'oeligacute': 'Characters_with_acute_accent',
         'OOligacute': 'Characters_with_acute_accent',
         'ooligacute': 'Characters_with_acute_accent',
         'rrotacute': 'Characters_with_acute_accent',
         'slongacute': 'Characters_with_acute_accent',
         'Tacute': 'Characters_with_acute_accent',
         'tacute': 'Characters_with_acute_accent',
         'Vacute': 'Characters_with_acute_accent',
         'vacute': 'Characters_with_acute_accent',
         'Vinsacute': 'Characters_with_acute_accent',
         'vinsacute': 'Characters_with_acute_accent',
         'thornacute': 'Characters_with_acute_accent',
         'fMedrun': 'Runic',
         'mMedrun': 'Runic',
         'romnumCDlig': 'Number_forms',
         'romnumDDlig': 'Number_forms',
         'romnumDDdbllig': 'Number_forms',
         'CONbase': 'Number_forms',
         'conbase': 'Number_forms',
         'eumlmacr': 'Characters_with_diaeresis_and_macron',
         'metrshort': 'Miscellaneous_Technical',
         'metrshortlong': 'Miscellaneous_Technical',
         'metrlongshort': 'Miscellaneous_Technical',
         'metrdblshortlong': 'Miscellaneous_Technical',
         'AAligdotbl': 'Characters_with_dot_below',
         'aaligdotbl': 'Characters_with_dot_below',
         'AEligdotbl': 'Characters_with_dot_below',
         'aeligdotbl': 'Characters_with_dot_below',
         'AOligdotbl': 'Characters_with_dot_below',
         'aoligdotbl': 'Characters_with_dot_below',
         'AUligdotbl': 'Characters_with_dot_below',
         'auligdotbl': 'Characters_with_dot_below',
         'AVligdotbl': 'Characters_with_dot_below',
         'avligdotbl': 'Characters_with_dot_below',
         'AYligdotbl': 'Characters_with_dot_below',
         'ayligdotbl': 'Characters_with_dot_below',
         'bscapdotbl': 'Characters_with_dot_below',
         'Cdotbl': 'Characters_with_dot_below',
         'cdotbl': 'Characters_with_dot_below',
         'dscapdotbl': 'Characters_with_dot_below',
         'ETHdotbl': 'Characters_with_dot_below',
         'ethdotbl': 'Characters_with_dot_below',
         'Fdotbl': 'Characters_with_dot_below',
         'fdotbl': 'Characters_with_dot_below',
         'Finsdotbl': 'Characters_with_dot_below',
         'finsdotbl': 'Characters_with_dot_below',
         'Gdotbl': 'Characters_with_dot_below',
         'gdotbl': 'Characters_with_dot_below',
         'gscapdotbl': 'Characters_with_dot_below',
         'Jdotbl': 'Characters_with_dot_below',
         'jdotbl': 'Characters_with_dot_below',
         'lscapdotbl': 'Characters_with_dot_below',
         'mscapdotbl': 'Characters_with_dot_below',
         'nscapdotbl': 'Characters_with_dot_below',
         'Oslashdotbl': 'Characters_with_dot_below',
         'oslashdotbl': 'Characters_with_dot_below',
         'OOligdotbl': 'Characters_with_dot_below',
         'ooligdotbl': 'Characters_with_dot_below',
         'Pdotbl': 'Characters_with_dot_below',
         'pdotbl': 'Characters_with_dot_below',
         'Qdotbl': 'Characters_with_dot_below',
         'qdotbl': 'Characters_with_dot_below',
         'rscapdotbl': 'Characters_with_dot_below',
         'rrotdotbl': 'Characters_with_dot_below',
         'sscapdotbl': 'Characters_with_dot_below',
         'slongdotbl': 'Characters_with_dot_below',
         'tscapdotbl': 'Characters_with_dot_below',
         'Vinsdotbl': 'Characters_with_dot_below',
         'vinsdotbl': 'Characters_with_dot_below',
         'THORNdotbl': 'Characters_with_dot_below',
         'thorndotbl': 'Characters_with_dot_below',
         'Eogoncurl': 'Characters_with_ogonek_and_curl_above_reversed_ogonek',
         'eogoncurl': 'Characters_with_ogonek_and_curl_above_reversed_ogonek',
         'Oogoncurl': 'Characters_with_ogonek_and_curl_above_reversed_ogonek',
         'oogoncurl': 'Characters_with_ogonek_and_curl_above_reversed_ogonek',
         'hidot': 'Punctuation_marks',
         'posit': 'Punctuation_marks',
         'ductsimpl': 'Punctuation_marks',
         'punctvers': 'Punctuation_marks',
         'colelevposit': 'Punctuation_marks',
         'colmidcomposit': 'Punctuation_marks',
         'bidotscomposit': 'Punctuation_marks',
         'tridotscomposit': 'Punctuation_marks',
         'punctelev': 'Punctuation_marks',
         'punctelevdiag': 'Punctuation_marks',
         'punctelevhiback': 'Punctuation_marks',
         'punctelevhack': 'Punctuation_marks',
         'punctflex': 'Punctuation_marks',
         'punctexclam': 'Punctuation_marks',
         'punctinter': 'Punctuation_marks',
         'punctintertilde': 'Punctuation_marks',
         'punctinterlemn': 'Punctuation_marks',
         'wavylin': 'Punctuation_marks',
         'medcom': 'Punctuation_marks',
         'parag': 'Punctuation_marks',
         'renvoi': 'Punctuation_marks',
         'virgsusp': 'Punctuation_marks',
         'virgmin': 'Punctuation_marks',
         'luhsqbNT': 'Supplemental_Punctuation',
         'luslst': 'Supplemental_Punctuation',
         'ruslst': 'Supplemental_Punctuation',
         'dbloblhyph': 'Supplemental_Punctuation',
         'rlslst': 'Supplemental_Punctuation',
         'llslst': 'Supplemental_Punctuation',
         'verbarql': 'Supplemental_Punctuation',
         'verbarqr': 'Supplemental_Punctuation',
         'luhsqb': 'Supplemental_Punctuation',
         'ruhsqb': 'Supplemental_Punctuation',
         'llhsqb': 'Supplemental_Punctuation',
         'rlhsqb': 'Supplemental_Punctuation',
         'lUbrack': 'Supplemental_Punctuation',
         'rUbrack': 'Supplemental_Punctuation',
         'ldblpar': 'Supplemental_Punctuation',
         'rdblpar': 'Supplemental_Punctuation',
         'tridotsdownw': 'Supplemental_Punctuation',
         'tridotsupw': 'Supplemental_Punctuation',
         'quaddots': 'Supplemental_Punctuation',
         'fivedots': 'Supplemental_Punctuation',
         'punctpercont': 'Supplemental_Punctuation',
         'minus': 'Mathematical_Operators',
         'infin': 'Mathematical_Operators',
         'logand': 'Mathematical_Operators',
         'tridotupw': 'Mathematical_Operators',
         'tridotdw': 'Mathematical_Operators',
         'quaddot': 'Mathematical_Operators',
         'est': 'Mathematical_Operators',
         'esse': 'Mathematical_Operators',
         'notequals': 'Mathematical_Operators',
         'dipledot': 'Mathematical_Operators',
         'Amacrbreve': 'Characters_with_macron_and_breve',
         'amacrbreve': 'Characters_with_macron_and_breve',
         'AEligmacrbreve': 'Characters_with_macron_and_breve',
         'aeligmacrbreve': 'Characters_with_macron_and_breve',
         'Emacrbreve': 'Characters_with_macron_and_breve',
         'emacrbreve': 'Characters_with_macron_and_breve',
         'Imacrbreve': 'Characters_with_macron_and_breve',
         'imacrbreve': 'Characters_with_macron_and_breve',
         'Omacrbreve': 'Characters_with_macron_and_breve',
         'omacrbreve': 'Characters_with_macron_and_breve',
         'OEligmacrbreve': 'Characters_with_macron_and_breve',
         'oeligmacrbreve': 'Characters_with_macron_and_breve',
         'Oslashmacrbreve': 'Characters_with_macron_and_breve',
         'oslashmacrbreve': 'Characters_with_macron_and_breve',
         'Umacrbreve': 'Characters_with_macron_and_breve',
         'umacrbreve': 'Characters_with_macron_and_breve',
         'Ymacrbreve': 'Characters_with_macron_and_breve',
         'ymacrbreve': 'Characters_with_macron_and_breve',
         }
  
  
  
  categories = {
         'Superscripts_and_subscripts': {
             'names': [ 
                     'sup0', 
                     'sup4', 
                     'sup5', 
                     'sup6', 
                     'sup7', 
                     'sup8', 
                     'sup9', 
                     'sub0', 
                     'sub1', 
                     'sub2', 
                     'sub3', 
                     'sub4', 
                     'sub5', 
                     'sub6', 
                     'sub7', 
                     'sub8', 
                     'sub9', 
                      ],
             'chars': [ 
                     u'\u2070', 
                     u'\u2074', 
                     u'\u2075', 
                     u'\u2076', 
                     u'\u2077', 
                     u'\u2078', 
                     u'\u2079', 
                     u'\u2080', 
                     u'\u2081', 
                     u'\u2082', 
                     u'\u2083', 
                     u'\u2084', 
                     u'\u2085', 
                     u'\u2086', 
                     u'\u2087', 
                     u'\u2088', 
                     u'\u2089', 
                      ],
             'codes': [ 
                     '2070', 
                     '2074', 
                     '2075', 
                     '2076', 
                     '2077', 
                     '2078', 
                     '2079', 
                     '2080', 
                     '2081', 
                     '2082', 
                     '2083', 
                     '2084', 
                     '2085', 
                     '2086', 
                     '2087', 
                     '2088', 
                     '2089', 
                      ],
             }, 
         'Dingbats': {
             'names': [ 
                     'cross', 
                     'hedera', 
                     'hederarot', 
                      ],
             'chars': [ 
                     u'\u271D', 
                     u'\u2766', 
                     u'\u2767', 
                      ],
             'codes': [ 
                     '271D', 
                     '2766', 
                     '2767', 
                      ],
             }, 
         'Latin1_Supplement': {
             'names': [ 
                     'nbsp', 
                     'iexcl', 
                     'cent', 
                     'pound', 
                     'curren', 
                     'yen', 
                     'brvbar', 
                     'sect', 
                     'uml', 
                     'copy', 
                     'ordf', 
                     'laquo', 
                     'not_', 
                     'shy', 
                     'reg', 
                     'macr', 
                     'deg', 
                     'plusmn', 
                     'sup2', 
                     'sup3', 
                     'acute', 
                     'micro', 
                     'para', 
                     'middot', 
                     'cedil', 
                     'sup1', 
                     'ordm', 
                     'raquo', 
                     'frac14', 
                     'frac12', 
                     'frac34', 
                     'iquest', 
                     'Agrave', 
                     'Aacute', 
                     'Acirc', 
                     'Atilde', 
                     'Auml', 
                     'Aring', 
                     'AElig', 
                     'Ccedil', 
                     'Egrave', 
                     'Eacute', 
                     'Ecirc', 
                     'Euml', 
                     'Igrave', 
                     'Iacute', 
                     'Icirc', 
                     'Iuml', 
                     'ETH', 
                     'Ntilde', 
                     'Ograve', 
                     'Oacute', 
                     'Ocirc', 
                     'Otilde', 
                     'Ouml', 
                     'times', 
                     'Oslash', 
                     'Ugrave', 
                     'Uacute', 
                     'Ucirc', 
                     'Uuml', 
                     'Yacute', 
                     'THORN', 
                     'szlig', 
                     'agrave', 
                     'aacute', 
                     'acirc', 
                     'atilde', 
                     'auml', 
                     'aring', 
                     'aelig', 
                     'ccedil', 
                     'egrave', 
                     'eacute', 
                     'ecirc', 
                     'euml', 
                     'igrave', 
                     'iacute', 
                     'icirc', 
                     'iuml', 
                     'eth', 
                     'ntilde', 
                     'ograve', 
                     'oacute', 
                     'ocirc', 
                     'otilde', 
                     'ouml', 
                     'divide', 
                     'oslash', 
                     'ugrave', 
                     'uacute', 
                     'ucirc', 
                     'uuml', 
                     'yacute', 
                     'thorn', 
                     'yuml', 
                      ],
             'chars': [ 
                     u'\u00A0', 
                     u'\u00A1', 
                     u'\u00A2', 
                     u'\u00A3', 
                     u'\u00A4', 
                     u'\u00A5', 
                     u'\u00A6', 
                     u'\u00A7', 
                     u'\u00A8', 
                     u'\u00A9', 
                     u'\u00AA', 
                     u'\u00AB', 
                     u'\u00AC', 
                     u'\u00AD', 
                     u'\u00AE', 
                     u'\u00AF', 
                     u'\u00B0', 
                     u'\u00B1', 
                     u'\u00B2', 
                     u'\u00B3', 
                     u'\u00B4', 
                     u'\u00B5', 
                     u'\u00B6', 
                     u'\u00B7', 
                     u'\u00B8', 
                     u'\u00B9', 
                     u'\u00BA', 
                     u'\u00BB', 
                     u'\u00BC', 
                     u'\u00BD', 
                     u'\u00BE', 
                     u'\u00BF', 
                     u'\u00C0', 
                     u'\u00C1', 
                     u'\u00C2', 
                     u'\u00C3', 
                     u'\u00C4', 
                     u'\u00C5', 
                     u'\u00C6', 
                     u'\u00C7', 
                     u'\u00C8', 
                     u'\u00C9', 
                     u'\u00CA', 
                     u'\u00CB', 
                     u'\u00CC', 
                     u'\u00CD', 
                     u'\u00CE', 
                     u'\u00CF', 
                     u'\u00D0', 
                     u'\u00D1', 
                     u'\u00D2', 
                     u'\u00D3', 
                     u'\u00D4', 
                     u'\u00D5', 
                     u'\u00D6', 
                     u'\u00D7', 
                     u'\u00D8', 
                     u'\u00D9', 
                     u'\u00DA', 
                     u'\u00DB', 
                     u'\u00DC', 
                     u'\u00DD', 
                     u'\u00DE', 
                     u'\u00DF', 
                     u'\u00E0', 
                     u'\u00E1', 
                     u'\u00E2', 
                     u'\u00E3', 
                     u'\u00E4', 
                     u'\u00E5', 
                     u'\u00E6', 
                     u'\u00E7', 
                     u'\u00E8', 
                     u'\u00E9', 
                     u'\u00EA', 
                     u'\u00EB', 
                     u'\u00EC', 
                     u'\u00ED', 
                     u'\u00EE', 
                     u'\u00EF', 
                     u'\u00F0', 
                     u'\u00F1', 
                     u'\u00F2', 
                     u'\u00F3', 
                     u'\u00F4', 
                     u'\u00F5', 
                     u'\u00F6', 
                     u'\u00F7', 
                     u'\u00F8', 
                     u'\u00F9', 
                     u'\u00FA', 
                     u'\u00FB', 
                     u'\u00FC', 
                     u'\u00FD', 
                     u'\u00FE', 
                     u'\u00FF', 
                      ],
             'codes': [ 
                     '00A0', 
                     '00A1', 
                     '00A2', 
                     '00A3', 
                     '00A4', 
                     '00A5', 
                     '00A6', 
                     '00A7', 
                     '00A8', 
                     '00A9', 
                     '00AA', 
                     '00AB', 
                     '00AC', 
                     '00AD', 
                     '00AE', 
                     '00AF', 
                     '00B0', 
                     '00B1', 
                     '00B2', 
                     '00B3', 
                     '00B4', 
                     '00B5', 
                     '00B6', 
                     '00B7', 
                     '00B8', 
                     '00B9', 
                     '00BA', 
                     '00BB', 
                     '00BC', 
                     '00BD', 
                     '00BE', 
                     '00BF', 
                     '00C0', 
                     '00C1', 
                     '00C2', 
                     '00C3', 
                     '00C4', 
                     '00C5', 
                     '00C6', 
                     '00C7', 
                     '00C8', 
                     '00C9', 
                     '00CA', 
                     '00CB', 
                     '00CC', 
                     '00CD', 
                     '00CE', 
                     '00CF', 
                     '00D0', 
                     '00D1', 
                     '00D2', 
                     '00D3', 
                     '00D4', 
                     '00D5', 
                     '00D6', 
                     '00D7', 
                     '00D8', 
                     '00D9', 
                     '00DA', 
                     '00DB', 
                     '00DC', 
                     '00DD', 
                     '00DE', 
                     '00DF', 
                     '00E0', 
                     '00E1', 
                     '00E2', 
                     '00E3', 
                     '00E4', 
                     '00E5', 
                     '00E6', 
                     '00E7', 
                     '00E8', 
                     '00E9', 
                     '00EA', 
                     '00EB', 
                     '00EC', 
                     '00ED', 
                     '00EE', 
                     '00EF', 
                     '00F0', 
                     '00F1', 
                     '00F2', 
                     '00F3', 
                     '00F4', 
                     '00F5', 
                     '00F6', 
                     '00F7', 
                     '00F8', 
                     '00F9', 
                     '00FA', 
                     '00FB', 
                     '00FC', 
                     '00FD', 
                     '00FE', 
                     '00FF', 
                      ],
             }, 
         'Characters_with_acute_accent_and_diaeresis': {
             'names': [ 
                     'oumlacute', 
                      ],
             'chars': [ 
                     u'\uE62C', 
                      ],
             'codes': [ 
                     'E62C', 
                      ],
             }, 
         'Latin_ExtendedA': {
             'names': [ 
                     'Amacr', 
                     'amacr', 
                     'Abreve', 
                     'abreve', 
                     'Aogon', 
                     'aogon', 
                     'Cacute', 
                     'cacute', 
                     'Cdot', 
                     'cdot', 
                     'Dstrok', 
                     'dstrok', 
                     'Emacr', 
                     'emacr', 
                     'Ebreve', 
                     'ebreve', 
                     'Edot', 
                     'edot', 
                     'Eogon', 
                     'eogon', 
                     'Gdot', 
                     'gdot', 
                     'hstrok', 
                     'Imacr', 
                     'imacr', 
                     'Ibreve', 
                     'ibreve', 
                     'Iogon', 
                     'iogon', 
                     'Idot', 
                     'inodot', 
                     'IJlig', 
                     'ijlig', 
                     'Lacute', 
                     'lacute', 
                     'Lstrok', 
                     'lstrok', 
                     'Nacute', 
                     'nacute', 
                     'ENG', 
                     'eng', 
                     'Omacr', 
                     'omacr', 
                     'Obreve', 
                     'obreve', 
                     'Odblac', 
                     'odblac', 
                     'OElig', 
                     'oelig', 
                     'Racute', 
                     'racute', 
                     'Sacute', 
                     'sacute', 
                     'Umacr', 
                     'umacr', 
                     'Ubreve', 
                     'ubreve', 
                     'Uring', 
                     'uring', 
                     'Udblac', 
                     'udblac', 
                     'Uogon', 
                     'uogon', 
                     'Wcirc', 
                     'wcirc', 
                     'Ycirc', 
                     'ycirc', 
                     'Yuml', 
                     'Zdot', 
                     'zdot', 
                     'slong', 
                      ],
             'chars': [ 
                     u'\u0100', 
                     u'\u0101', 
                     u'\u0102', 
                     u'\u0103', 
                     u'\u0104', 
                     u'\u0105', 
                     u'\u0106', 
                     u'\u0107', 
                     u'\u010A', 
                     u'\u010B', 
                     u'\u0110', 
                     u'\u0111', 
                     u'\u0112', 
                     u'\u0113', 
                     u'\u0114', 
                     u'\u0115', 
                     u'\u0116', 
                     u'\u0117', 
                     u'\u0118', 
                     u'\u0119', 
                     u'\u0120', 
                     u'\u0121', 
                     u'\u0127', 
                     u'\u012A', 
                     u'\u012B', 
                     u'\u012C', 
                     u'\u012D', 
                     u'\u012E', 
                     u'\u012F', 
                     u'\u0130', 
                     u'\u0131', 
                     u'\u0132', 
                     u'\u0133', 
                     u'\u0139', 
                     u'\u013A', 
                     u'\u0141', 
                     u'\u0142', 
                     u'\u0143', 
                     u'\u0144', 
                     u'\u014A', 
                     u'\u014B', 
                     u'\u014C', 
                     u'\u014D', 
                     u'\u014E', 
                     u'\u014F', 
                     u'\u0150', 
                     u'\u0151', 
                     u'\u0152', 
                     u'\u0153', 
                     u'\u0154', 
                     u'\u0155', 
                     u'\u015A', 
                     u'\u015B', 
                     u'\u016A', 
                     u'\u016B', 
                     u'\u016C', 
                     u'\u016D', 
                     u'\u016E', 
                     u'\u016F', 
                     u'\u0170', 
                     u'\u0171', 
                     u'\u0172', 
                     u'\u0173', 
                     u'\u0174', 
                     u'\u0175', 
                     u'\u0176', 
                     u'\u0177', 
                     u'\u0178', 
                     u'\u017B', 
                     u'\u017C', 
                     u'\u017F', 
                      ],
             'codes': [ 
                     '0100', 
                     '0101', 
                     '0102', 
                     '0103', 
                     '0104', 
                     '0105', 
                     '0106', 
                     '0107', 
                     '010A', 
                     '010B', 
                     '0110', 
                     '0111', 
                     '0112', 
                     '0113', 
                     '0114', 
                     '0115', 
                     '0116', 
                     '0117', 
                     '0118', 
                     '0119', 
                     '0120', 
                     '0121', 
                     '0127', 
                     '012A', 
                     '012B', 
                     '012C', 
                     '012D', 
                     '012E', 
                     '012F', 
                     '0130', 
                     '0131', 
                     '0132', 
                     '0133', 
                     '0139', 
                     '013A', 
                     '0141', 
                     '0142', 
                     '0143', 
                     '0144', 
                     '014A', 
                     '014B', 
                     '014C', 
                     '014D', 
                     '014E', 
                     '014F', 
                     '0150', 
                     '0151', 
                     '0152', 
                     '0153', 
                     '0154', 
                     '0155', 
                     '015A', 
                     '015B', 
                     '016A', 
                     '016B', 
                     '016C', 
                     '016D', 
                     '016E', 
                     '016F', 
                     '0170', 
                     '0171', 
                     '0172', 
                     '0173', 
                     '0174', 
                     '0175', 
                     '0176', 
                     '0177', 
                     '0178', 
                     '017B', 
                     '017C', 
                     '017F', 
                      ],
             }, 
         'Latin_ExtendedC': {
             'names': [ 
                     'Hhalf', 
                     'hhalf', 
                      ],
             'chars': [ 
                     u'\u2C75', 
                     u'\u2C76', 
                      ],
             'codes': [ 
                     '2C75', 
                     '2C76', 
                      ],
             }, 
         'Currency_Symbols': {
             'names': [ 
                     'pennygerm', 
                      ],
             'chars': [ 
                     u'\u20B0', 
                      ],
             'codes': [ 
                     '20B0', 
                      ],
             }, 
         'Latin_ExtendedD': {
             'names': [ 
                     'fscap', 
                     'sscap', 
                     'AAlig', 
                     'aalig', 
                     'AOlig', 
                     'aolig', 
                     'AUlig', 
                     'aulig', 
                     'AVlig', 
                     'avlig', 
                     'AVligslash', 
                     'avligslash', 
                     'AYlig', 
                     'aylig', 
                     'CONdot', 
                     'condot', 
                     'Kbar', 
                     'kbar', 
                     'Kstrleg', 
                     'kstrleg', 
                     'Kstrascleg', 
                     'kstrascleg', 
                     'Lbrk', 
                     'lbrk', 
                     'Lhighstrok', 
                     'lhighstrok', 
                     'OBIIT', 
                     'obiit', 
                     'Oloop', 
                     'oloop', 
                     'OOlig', 
                     'oolig', 
                     'Pbardes', 
                     'pbardes', 
                     'Pflour', 
                     'pflour', 
                     'Psquirrel', 
                     'psquirrel', 
                     'Qbardes', 
                     'qbardes', 
                     'Qslstrok', 
                     'qslstrok', 
                     'Rrot', 
                     'rrot', 
                     'RUM', 
                     'rum', 
                     'Vdiagstrok', 
                     'vdiagstrok', 
                     'YYlig', 
                     'yylig', 
                     'Vvisigot', 
                     'vvisigot', 
                     'THORNbar', 
                     'thornbar', 
                     'THORNbardes', 
                     'thornbardes', 
                     'Vins', 
                     'vins', 
                     'ETfin', 
                     'etfin', 
                     'IS', 
                     'is_', 
                     'CONdes', 
                     'condes', 
                     'usmod', 
                     'dtailstrok', 
                     'ltailstrok', 
                     'mtailstrok', 
                     'ntailstrok', 
                     'rtailstrok', 
                     'rscaptailstrok', 
                     'ttailstrok', 
                     'sstrok', 
                     'Drot', 
                     'drot', 
                     'Fins', 
                     'fins', 
                     'Gins', 
                     'Ginsturn', 
                     'ginsturn', 
                     'Lturn', 
                     'lturn', 
                     'Rins', 
                     'rins', 
                     'Sins', 
                     'sins', 
                     'Trot', 
                     'trot', 
                     'Frev', 
                     'Prev', 
                     'Minv', 
                     'Ilong', 
                     'M5leg', 
                      ],
             'chars': [ 
                     u'\uA730', 
                     u'\uA731', 
                     u'\uA732', 
                     u'\uA733', 
                     u'\uA734', 
                     u'\uA735', 
                     u'\uA736', 
                     u'\uA737', 
                     u'\uA738', 
                     u'\uA739', 
                     u'\uA73A', 
                     u'\uA73B', 
                     u'\uA73C', 
                     u'\uA73D', 
                     u'\uA73E', 
                     u'\uA73F', 
                     u'\uA740', 
                     u'\uA741', 
                     u'\uA742', 
                     u'\uA743', 
                     u'\uA744', 
                     u'\uA745', 
                     u'\uA746', 
                     u'\uA747', 
                     u'\uA748', 
                     u'\uA749', 
                     u'\uA74A', 
                     u'\uA74B', 
                     u'\uA74C', 
                     u'\uA74D', 
                     u'\uA74E', 
                     u'\uA74F', 
                     u'\uA750', 
                     u'\uA751', 
                     u'\uA752', 
                     u'\uA753', 
                     u'\uA754', 
                     u'\uA755', 
                     u'\uA756', 
                     u'\uA757', 
                     u'\uA758', 
                     u'\uA759', 
                     u'\uA75A', 
                     u'\uA75B', 
                     u'\uA75C', 
                     u'\uA75D', 
                     u'\uA75E', 
                     u'\uA75F', 
                     u'\uA760', 
                     u'\uA761', 
                     u'\uA762', 
                     u'\uA763', 
                     u'\uA764', 
                     u'\uA765', 
                     u'\uA766', 
                     u'\uA767', 
                     u'\uA768', 
                     u'\uA769', 
                     u'\uA76A', 
                     u'\uA76B', 
                     u'\uA76C', 
                     u'\uA76D', 
                     u'\uA76E', 
                     u'\uA76F', 
                     u'\uA770', 
                     u'\uA771', 
                     u'\uA772', 
                     u'\uA773', 
                     u'\uA774', 
                     u'\uA775', 
                     u'\uA776', 
                     u'\uA777', 
                     u'\uA778', 
                     u'\uA779', 
                     u'\uA77A', 
                     u'\uA77B', 
                     u'\uA77C', 
                     u'\uA77D', 
                     u'\uA77E', 
                     u'\uA77F', 
                     u'\uA780', 
                     u'\uA781', 
                     u'\uA782', 
                     u'\uA783', 
                     u'\uA784', 
                     u'\uA785', 
                     u'\uA786', 
                     u'\uA787', 
                     u'\uA7FB', 
                     u'\uA7FC', 
                     u'\uA7FD', 
                     u'\uA7FE', 
                     u'\uA7FF', 
                      ],
             'codes': [ 
                     'A730', 
                     'A731', 
                     'A732', 
                     'A733', 
                     'A734', 
                     'A735', 
                     'A736', 
                     'A737', 
                     'A738', 
                     'A739', 
                     'A73A', 
                     'A73B', 
                     'A73C', 
                     'A73D', 
                     'A73E', 
                     'A73F', 
                     'A740', 
                     'A741', 
                     'A742', 
                     'A743', 
                     'A744', 
                     'A745', 
                     'A746', 
                     'A747', 
                     'A748', 
                     'A749', 
                     'A74A', 
                     'A74B', 
                     'A74C', 
                     'A74D', 
                     'A74E', 
                     'A74F', 
                     'A750', 
                     'A751', 
                     'A752', 
                     'A753', 
                     'A754', 
                     'A755', 
                     'A756', 
                     'A757', 
                     'A758', 
                     'A759', 
                     'A75A', 
                     'A75B', 
                     'A75C', 
                     'A75D', 
                     'A75E', 
                     'A75F', 
                     'A760', 
                     'A761', 
                     'A762', 
                     'A763', 
                     'A764', 
                     'A765', 
                     'A766', 
                     'A767', 
                     'A768', 
                     'A769', 
                     'A76A', 
                     'A76B', 
                     'A76C', 
                     'A76D', 
                     'A76E', 
                     'A76F', 
                     'A770', 
                     'A771', 
                     'A772', 
                     'A773', 
                     'A774', 
                     'A775', 
                     'A776', 
                     'A777', 
                     'A778', 
                     'A779', 
                     'A77A', 
                     'A77B', 
                     'A77C', 
                     'A77D', 
                     'A77E', 
                     'A77F', 
                     'A780', 
                     'A781', 
                     'A782', 
                     'A783', 
                     'A784', 
                     'A785', 
                     'A786', 
                     'A787', 
                     'A7FB', 
                     'A7FC', 
                     'A7FD', 
                     'A7FE', 
                     'A7FF', 
                      ],
             }, 
         'Characters_with_curly_bar_above': {
             'names': [ 
                     'ucurlbar', 
                      ],
             'chars': [ 
                     u'\uEBBF', 
                      ],
             'codes': [ 
                     'EBBF', 
                      ],
             }, 
         'Characters_with_ogonek': {
             'names': [ 
                     'AEligogon', 
                     'aeligogon', 
                     'AVligogon', 
                     'avligogon', 
                     'Cogon', 
                     'cogon', 
                     'Oslashogon', 
                     'oslashogon', 
                     'Togon', 
                     'togon', 
                      ],
             'chars': [ 
                     u'\uE040', 
                     u'\uE440', 
                     u'\uEBF0', 
                     u'\uEBF1', 
                     u'\uE076', 
                     u'\uE476', 
                     u'\uE255', 
                     u'\uE655', 
                     u'\uE2EE', 
                     u'\uE6EE', 
                      ],
             'codes': [ 
                     'E040', 
                     'E440', 
                     'EBF0', 
                     'EBF1', 
                     'E076', 
                     'E476', 
                     'E255', 
                     'E655', 
                     'E2EE', 
                     'E6EE', 
                      ],
             }, 
         'Characters_with_diaeresis_and_circumflex': {
             'names': [ 
                     'aumlcirc', 
                     'Oumlcirc', 
                     'oumlcirc', 
                     'Uumlcirc', 
                     'uumlcirc', 
                      ],
             'chars': [ 
                     u'\uE41A', 
                     u'\uE22D', 
                     u'\uE62D', 
                     u'\uE317', 
                     u'\uE717', 
                      ],
             'codes': [ 
                     'E41A', 
                     'E22D', 
                     'E62D', 
                     'E317', 
                     'E717', 
                      ],
             }, 
         'Phonetic_Extensions': {
             'names': [ 
                     'ascap', 
                     'aeligscap', 
                     'cscap', 
                     'dscap', 
                     'ethscap', 
                     'escap', 
                     'jscap', 
                     'kscap', 
                     'mscap', 
                     'oscap', 
                     'pscap', 
                     'tscap', 
                     'uscap', 
                     'vscap', 
                     'wscap', 
                     'zscap', 
                     'Imod', 
                     'gins', 
                      ],
             'chars': [ 
                     u'\u1D00', 
                     u'\u1D01', 
                     u'\u1D04', 
                     u'\u1D05', 
                     u'\u1D06', 
                     u'\u1D07', 
                     u'\u1D0A', 
                     u'\u1D0B', 
                     u'\u1D0D', 
                     u'\u1D0F', 
                     u'\u1D18', 
                     u'\u1D1B', 
                     u'\u1D1C', 
                     u'\u1D20', 
                     u'\u1D21', 
                     u'\u1D22', 
                     u'\u1D35', 
                     u'\u1D79', 
                      ],
             'codes': [ 
                     '1D00', 
                     '1D01', 
                     '1D04', 
                     '1D05', 
                     '1D06', 
                     '1D07', 
                     '1D0A', 
                     '1D0B', 
                     '1D0D', 
                     '1D0F', 
                     '1D18', 
                     '1D1B', 
                     '1D1C', 
                     '1D20', 
                     '1D21', 
                     '1D22', 
                     '1D35', 
                     '1D79', 
                      ],
             }, 
         'Combining_Diacritical_Marks_Supplement': {
             'names': [ 
                     'combcircdbl', 
                     'combcurl', 
                     'ersub', 
                     'combisbelow', 
                     'ur', 
                     'us', 
                     'ra', 
                     'aeligsup', 
                     'aoligsup', 
                     'avligsup', 
                     'ccedilsup', 
                     'drotsup', 
                     'ethsup', 
                     'gsup', 
                     'gscapsup', 
                     'ksup', 
                     'lsup', 
                     'lscapsup', 
                     'mscapsup', 
                     'nsup', 
                     'nscapsup', 
                     'rrotsup', 
                     'ssup', 
                     'slongsup', 
                     'zsup', 
                      ],
             'chars': [ 
                     u'\u1DCD', 
                     u'\u1DCE', 
                     u'\u1DCF', 
                     u'\u1DD0', 
                     u'\u1DD1', 
                     u'\u1DD2', 
                     u'\u1DD3', 
                     u'\u1DD4', 
                     u'\u1DD5', 
                     u'\u1DD6', 
                     u'\u1DD7', 
                     u'\u1DD8', 
                     u'\u1DD9', 
                     u'\u1DDA', 
                     u'\u1DDB', 
                     u'\u1DDC', 
                     u'\u1DDD', 
                     u'\u1DDE', 
                     u'\u1DDF', 
                     u'\u1DE0', 
                     u'\u1DE1', 
                     u'\u1DE3', 
                     u'\u1DE4', 
                     u'\u1DE5', 
                     u'\u1DE6', 
                      ],
             'codes': [ 
                     '1DCD', 
                     '1DCE', 
                     '1DCF', 
                     '1DD0', 
                     '1DD1', 
                     '1DD2', 
                     '1DD3', 
                     '1DD4', 
                     '1DD5', 
                     '1DD6', 
                     '1DD7', 
                     '1DD8', 
                     '1DD9', 
                     '1DDA', 
                     '1DDB', 
                     '1DDC', 
                     '1DDD', 
                     '1DDE', 
                     '1DDF', 
                     '1DE0', 
                     '1DE1', 
                     '1DE3', 
                     '1DE4', 
                     '1DE5', 
                     '1DE6', 
                      ],
             }, 
         'Characters_with_ogonek_dot_above_and_acute_accent': {
             'names': [ 
                     'Eogondotacute', 
                     'eogondotacute', 
                     'Oogondotacute', 
                     'oogondotacute', 
                     'Asqu', 
                     'aunc', 
                     'Ains', 
                     'ains', 
                     'aopen', 
                     'aneckless', 
                     'aclose', 
                     'Csqu', 
                     'ccurl', 
                     'dcurl', 
                     'Eunc', 
                     'Euncclose', 
                     'eunc', 
                     'eext', 
                     'etall', 
                     'finssemiclose', 
                     'finsdothook', 
                     'finsclose', 
                     'fcurl', 
                     'Gsqu', 
                     'gcurl', 
                     'gdivloop', 
                     'glglowloop', 
                     'gsmlowloop', 
                     'Hunc', 
                     'hrdes', 
                     'ilong', 
                     'kunc', 
                     'ksemiclose', 
                     'kclose', 
                     'kcurl', 
                     'ldes', 
                     'Munc', 
                     'Muncdes', 
                     'munc', 
                     'muncdes', 
                     'mrdes', 
                     'Nrdes', 
                     'nrdes', 
                     'nscaprdes', 
                     'nscapldes', 
                     'nflour', 
                     'Qstem', 
                     'rflour', 
                     'Sclose', 
                     'sclose', 
                     'slongdes', 
                     'tcurl', 
                     'xldes', 
                     'yrgmainstrok', 
                      ],
             'chars': [ 
                     u'\uE0EC', 
                     u'\uE4EC', 
                     u'\uEBFA', 
                     u'\uEBFB', 
                     u'\uF13A', 
                     u'\uF214', 
                     u'\uF201', 
                     u'\uF200', 
                     u'\uF202', 
                     u'\uF215', 
                     u'\uF203', 
                     u'\uF106', 
                     u'\uF198', 
                     u'\uF193', 
                     u'\uF10A', 
                     u'\uF217', 
                     u'\uF218', 
                     u'\uF219', 
                     u'\uF21A', 
                     u'\uF21B', 
                     u'\uF21C', 
                     u'\uF207', 
                     u'\uF194', 
                     u'\uF10E', 
                     u'\uF196', 
                     u'\uF21D', 
                     u'\uF21E', 
                     u'\uF21F', 
                     u'\uF110', 
                     u'\uF23A', 
                     u'\uF220', 
                     u'\uF208', 
                     u'\uF221', 
                     u'\uF209', 
                     u'\uF195', 
                     u'\uF222', 
                     u'\uF11A', 
                     u'\uF224', 
                     u'\uF225', 
                     u'\uF226', 
                     u'\uF223', 
                     u'\uF229', 
                     u'\uF228', 
                     u'\uF22A', 
                     u'\uF22B', 
                     u'\uF19A', 
                     u'\uF22C', 
                     u'\uF19B', 
                     u'\uF126', 
                     u'\uF128', 
                     u'\uF127', 
                     u'\uF199', 
                     u'\uF232', 
                     u'\uF233', 
                      ],
             'codes': [ 
                     'E0EC', 
                     'E4EC', 
                     'EBFA', 
                     'EBFB', 
                     'F13A', 
                     'F214', 
                     'F201', 
                     'F200', 
                     'F202', 
                     'F215', 
                     'F203', 
                     'F106', 
                     'F198', 
                     'F193', 
                     'F10A', 
                     'F217', 
                     'F218', 
                     'F219', 
                     'F21A', 
                     'F21B', 
                     'F21C', 
                     'F207', 
                     'F194', 
                     'F10E', 
                     'F196', 
                     'F21D', 
                     'F21E', 
                     'F21F', 
                     'F110', 
                     'F23A', 
                     'F220', 
                     'F208', 
                     'F221', 
                     'F209', 
                     'F195', 
                     'F222', 
                     'F11A', 
                     'F224', 
                     'F225', 
                     'F226', 
                     'F223', 
                     'F229', 
                     'F228', 
                     'F22A', 
                     'F22B', 
                     'F19A', 
                     'F22C', 
                     'F19B', 
                     'F126', 
                     'F128', 
                     'F127', 
                     'F199', 
                     'F232', 
                     'F233', 
                      ],
             }, 
         'Characters_with_acute_accent_and_dot_below': {
             'names': [ 
                     'edotblacute', 
                      ],
             'chars': [ 
                     u'\uE498', 
                      ],
             'codes': [ 
                     'E498', 
                      ],
             }, 
         'Modified_baseline_abbreviation_characters': {
             'names': [ 
                     'hslonglig', 
                     'hslongligbar', 
                     'kslonglig', 
                     'kslongligbar', 
                     'q2app', 
                     'q3app', 
                     'qcentrslstrok', 
                     'rdesstrok', 
                     'slongflour', 
                     'slongslstrok', 
                     'vslash', 
                     'xslashula', 
                     'xslashlra', 
                     'THORNbarslash', 
                     'thornbarslash', 
                     'thornslonglig', 
                     'thornslongligbar', 
                      ],
             'chars': [ 
                     u'\uEBAD', 
                     u'\uE7C7', 
                     u'\uEBAE', 
                     u'\uE7C8', 
                     u'\uE8B3', 
                     u'\uE8BF', 
                     u'\uE8B4', 
                     u'\uE7E4', 
                     u'\uE8B7', 
                     u'\uE8B8', 
                     u'\uE8BA', 
                     u'\uE8BD', 
                     u'\uE8BE', 
                     u'\uE337', 
                     u'\uF149', 
                     u'\uE734', 
                     u'\uE735', 
                      ],
             'codes': [ 
                     'EBAD', 
                     'E7C7', 
                     'EBAE', 
                     'E7C8', 
                     'E8B3', 
                     'E8BF', 
                     'E8B4', 
                     'E7E4', 
                     'E8B7', 
                     'E8B8', 
                     'E8BA', 
                     'E8BD', 
                     'E8BE', 
                     'E337', 
                     'F149', 
                     'E734', 
                     'E735', 
                      ],
             }, 
         'For_future_additions': {
             'names': [ 
                      ],
             'chars': [ 
                      ],
             'codes': [ 
                      ],
             }, 
         'Characters_with_dot_below_and_ogonek': {
             'names': [ 
                     'Eogondotbl', 
                     'eogondotbl', 
                     'Oogondotbl', 
                     'oogondotbl', 
                      ],
             'chars': [ 
                     u'\uE0E8', 
                     u'\uE4E8', 
                     u'\uE208', 
                     u'\uE608', 
                      ],
             'codes': [ 
                     'E0E8', 
                     'E4E8', 
                     'E208', 
                     'E608', 
                      ],
             }, 
         'Characters_with_breve': {
             'names': [ 
                     'AEligbreve', 
                     'aeligbreve', 
                     'Oslashbreve', 
                     'oslashbreve', 
                     'Ybreve', 
                     'ybreve', 
                      ],
             'chars': [ 
                     u'\uE03F', 
                     u'\uE43F', 
                     u'\uEBEE', 
                     u'\uEBEF', 
                     u'\uE376', 
                     u'\uE776', 
                      ],
             'codes': [ 
                     'E03F', 
                     'E43F', 
                     'EBEE', 
                     'EBEF', 
                     'E376', 
                     'E776', 
                      ],
             }, 
         'Characters_with_vertical_bar_above': {
             'names': [ 
                     'Uvertline', 
                     'uvertline', 
                      ],
             'chars': [ 
                     u'\uE324', 
                     u'\uE724', 
                      ],
             'codes': [ 
                     'E324', 
                     'E724', 
                      ],
             }, 
         'Characters_with_dot_above': {
             'names': [ 
                     'AAligdot', 
                     'aaligdot', 
                     'AEligdot', 
                     'aeligdot', 
                     'AYligdot', 
                     'ayligdot', 
                     'bscapdot', 
                     'drotdot', 
                     'dscapdot', 
                     'Finsdot', 
                     'finsdot', 
                     'finssemiclosedot', 
                     'finsclosedot', 
                     'fscapdot', 
                     'gscapdot', 
                     'hscapdot', 
                     'Jdot', 
                     'Kdot', 
                     'kdot', 
                     'kscapdot', 
                     'Ldot', 
                     'ldot', 
                     'lscapdot', 
                     'mscapdot', 
                     'nscapdot', 
                     'Oslashdot', 
                     'oslashdot', 
                     'pscapdot', 
                     'Qdot', 
                     'qdot', 
                     'rscapdot', 
                     'sscapdot', 
                     'tscapdot', 
                     'Udot', 
                     'udot', 
                     'Vdot', 
                     'vdot', 
                     'Vinsdot', 
                     'vinsdot', 
                      ],
             'chars': [ 
                     u'\uEFEE', 
                     u'\uEFEF', 
                     u'\uE043', 
                     u'\uE443', 
                     u'\uEFF0', 
                     u'\uEFF1', 
                     u'\uEBD0', 
                     u'\uEBD1', 
                     u'\uEBD2', 
                     u'\uEBD3', 
                     u'\uEBD4', 
                     u'\uEBD5', 
                     u'\uEBD6', 
                     u'\uEBD7', 
                     u'\uEF20', 
                     u'\uEBDA', 
                     u'\uE15C', 
                     u'\uE168', 
                     u'\uE568', 
                     u'\uEBDB', 
                     u'\uE19E', 
                     u'\uE59E', 
                     u'\uEBDC', 
                     u'\uEBDD', 
                     u'\uEF21', 
                     u'\uEBCD', 
                     u'\uEBCE', 
                     u'\uEBCF', 
                     u'\uE282', 
                     u'\uE682', 
                     u'\uEF22', 
                     u'\uEF23', 
                     u'\uEF24', 
                     u'\uE315', 
                     u'\uE715', 
                     u'\uE34C', 
                     u'\uE74C', 
                     u'\uE3E7', 
                     u'\uE7E7', 
                      ],
             'codes': [ 
                     'EFEE', 
                     'EFEF', 
                     'E043', 
                     'E443', 
                     'EFF0', 
                     'EFF1', 
                     'EBD0', 
                     'EBD1', 
                     'EBD2', 
                     'EBD3', 
                     'EBD4', 
                     'EBD5', 
                     'EBD6', 
                     'EBD7', 
                     'EF20', 
                     'EBDA', 
                     'E15C', 
                     'E168', 
                     'E568', 
                     'EBDB', 
                     'E19E', 
                     'E59E', 
                     'EBDC', 
                     'EBDD', 
                     'EF21', 
                     'EBCD', 
                     'EBCE', 
                     'EBCF', 
                     'E282', 
                     'E682', 
                     'EF22', 
                     'EF23', 
                     'EF24', 
                     'E315', 
                     'E715', 
                     'E34C', 
                     'E74C', 
                     'E3E7', 
                     'E7E7', 
                      ],
             }, 
         'Characters_with_tilde': {
             'names': [ 
                     'qbardestilde', 
                      ],
             'chars': [ 
                     u'\uE68B', 
                      ],
             'codes': [ 
                     'E68B', 
                      ],
             }, 
         'IPA_Extensions': {
             'names': [ 
                     'oopen', 
                     'dtail', 
                     'schwa', 
                     'jnodotstrok', 
                     'gopen', 
                     'gscap', 
                     'hhook', 
                     'istrok', 
                     'iscap', 
                     'nlfhook', 
                     'nscap', 
                     'oeligscap', 
                     'rdes', 
                     'rscap', 
                     'ubar', 
                     'yscap', 
                     'ezh', 
                     'bscap', 
                     'hscap', 
                     'lscap', 
                      ],
             'chars': [ 
                     u'\u0254', 
                     u'\u0256', 
                     u'\u0259', 
                     u'\u025F', 
                     u'\u0261', 
                     u'\u0262', 
                     u'\u0266', 
                     u'\u0268', 
                     u'\u026A', 
                     u'\u0272', 
                     u'\u0274', 
                     u'\u0276', 
                     u'\u027C', 
                     u'\u0280', 
                     u'\u0289', 
                     u'\u028F', 
                     u'\u0292', 
                     u'\u0299', 
                     u'\u029C', 
                     u'\u029F', 
                      ],
             'codes': [ 
                     '0254', 
                     '0256', 
                     '0259', 
                     '025F', 
                     '0261', 
                     '0262', 
                     '0266', 
                     '0268', 
                     '026A', 
                     '0272', 
                     '0274', 
                     '0276', 
                     '027C', 
                     '0280', 
                     '0289', 
                     '028F', 
                     '0292', 
                     '0299', 
                     '029C', 
                     '029F', 
                      ],
             }, 
         'Characters_with_ring_below': {
             'names': [ 
                     'lringbl', 
                     'mringbl', 
                     'nringbl', 
                     'rringbl', 
                      ],
             'chars': [ 
                     u'\uE5A4', 
                     u'\uE5C5', 
                     u'\uE5EE', 
                     u'\uE6A3', 
                      ],
             'codes': [ 
                     'E5A4', 
                     'E5C5', 
                     'E5EE', 
                     'E6A3', 
                      ],
             }, 
         'Small_capitals': {
             'names': [ 
                     'qscap', 
                     'xscap', 
                     'thornscap', 
                      ],
             'chars': [ 
                     u'\uEF0C', 
                     u'\uEF11', 
                     u'\uEF15', 
                      ],
             'codes': [ 
                     'EF0C', 
                     'EF11', 
                     'EF15', 
                      ],
             }, 
         'Basic_Latin': {
             'names': [ 
                     'quot', 
                     'amp', 
                     'apos', 
                     'lt', 
                     'gt', 
                     'excl', 
                     'dollar', 
                     'percnt', 
                     'lpar', 
                     'rpar', 
                     'ast', 
                     'plus', 
                     'comma', 
                     'hyphen', 
                     'period', 
                     'sol', 
                     'colon', 
                     'semi', 
                     'equals', 
                     'quest', 
                     'commat', 
                     'lsqb', 
                     'bsol', 
                     'rsqb', 
                     'circ', 
                     'lowbar', 
                     'grave', 
                     'lcub', 
                     'verbar', 
                     'rcub', 
                     'tld', 
                      ],
             'chars': [ 
                     u'\u0022', 
                     u'\u0026', 
                     u'\u0027', 
                     u'\u003C', 
                     u'\u003E', 
                     u'\u0021', 
                     u'\u0024', 
                     u'\u0025', 
                     u'\u0028', 
                     u'\u0029', 
                     u'\u002A', 
                     u'\u002B', 
                     u'\u002C', 
                     u'\u002D', 
                     u'\u002E', 
                     u'\u002F', 
                     u'\u003A', 
                     u'\u003B', 
                     u'\u003D', 
                     u'\u003F', 
                     u'\u0040', 
                     u'\u005B', 
                     u'\u005C', 
                     u'\u005D', 
                     u'\u005E', 
                     u'\u005F', 
                     u'\u0060', 
                     u'\u007B', 
                     u'\u007C', 
                     u'\u007D', 
                     u'\u007E', 
                      ],
             'codes': [ 
                     '0022', 
                     '0026', 
                     '0027', 
                     '003C', 
                     '003E', 
                     '0021', 
                     '0024', 
                     '0025', 
                     '0028', 
                     '0029', 
                     '002A', 
                     '002B', 
                     '002C', 
                     '002D', 
                     '002E', 
                     '002F', 
                     '003A', 
                     '003B', 
                     '003D', 
                     '003F', 
                     '0040', 
                     '005B', 
                     '005C', 
                     '005D', 
                     '005E', 
                     '005F', 
                     '0060', 
                     '007B', 
                     '007C', 
                     '007D', 
                     '007E', 
                      ],
             }, 
         'Latin_Extended_Additional': {
             'names': [ 
                     'Bdot', 
                     'bdot', 
                     'Bdotbl', 
                     'bdotbl', 
                     'Ddot', 
                     'ddot', 
                     'Ddotbl', 
                     'ddotbl', 
                     'Emacracute', 
                     'emacracute', 
                     'Fdot', 
                     'fdot', 
                     'Hdot', 
                     'hdot', 
                     'Hdotbl', 
                     'hdotbl', 
                     'Kacute', 
                     'kacute', 
                     'Kdotbl', 
                     'kdotbl', 
                     'Ldotbl', 
                     'ldotbl', 
                     'Macute', 
                     'macute', 
                     'Mdot', 
                     'mdot', 
                     'Mdotbl', 
                     'mdotbl', 
                     'Ndot', 
                     'ndot', 
                     'Ndotbl', 
                     'ndotbl', 
                     'Omacracute', 
                     'omacracute', 
                     'Pacute', 
                     'pacute', 
                     'Pdot', 
                     'pdot', 
                     'Rdot', 
                     'rdot', 
                     'Rdotbl', 
                     'rdotbl', 
                     'Sdot', 
                     'sdot', 
                     'Sdotbl', 
                     'sdotbl', 
                     'Tdot', 
                     'tdot', 
                     'Tdotbl', 
                     'tdotbl', 
                     'Vdotbl', 
                     'vdotbl', 
                     'Wgrave', 
                     'wgrave', 
                     'Wacute', 
                     'wacute', 
                     'Wuml', 
                     'wuml', 
                     'Wdot', 
                     'wdot', 
                     'Wdotbl', 
                     'wdotbl', 
                     'Ydot', 
                     'ydot', 
                     'Zdotbl', 
                     'zdotbl', 
                     'wring', 
                     'yring', 
                     'slongbarslash', 
                     'slongbar', 
                     'SZlig', 
                     'dscript', 
                     'Adotbl', 
                     'adotbl', 
                     'Ahook', 
                     'ahook', 
                     'Abreveacute', 
                     'abreveacute', 
                     'Edotbl', 
                     'edotbl', 
                     'Ihook', 
                     'ihook', 
                     'Idotbl', 
                     'idotbl', 
                     'Odotbl', 
                     'odotbl', 
                     'Ohook', 
                     'ohook', 
                     'Udotbl', 
                     'udotbl', 
                     'Uhook', 
                     'uhook', 
                     'Ygrave', 
                     'ygrave', 
                     'Ydotbl', 
                     'ydotbl', 
                     'Yhook', 
                     'yhook', 
                     'LLwelsh', 
                     'llwelsh', 
                     'Vwelsh', 
                     'vwelsh', 
                     'Yloop', 
                     'yloop', 
                      ],
             'chars': [ 
                     u'\u1E02', 
                     u'\u1E03', 
                     u'\u1E04', 
                     u'\u1E05', 
                     u'\u1E0A', 
                     u'\u1E0B', 
                     u'\u1E0C', 
                     u'\u1E0D', 
                     u'\u1E16', 
                     u'\u1E17', 
                     u'\u1E1E', 
                     u'\u1E1F', 
                     u'\u1E22', 
                     u'\u1E23', 
                     u'\u1E24', 
                     u'\u1E25', 
                     u'\u1E30', 
                     u'\u1E31', 
                     u'\u1E32', 
                     u'\u1E33', 
                     u'\u1E36', 
                     u'\u1E37', 
                     u'\u1E3E', 
                     u'\u1E3F', 
                     u'\u1E40', 
                     u'\u1E41', 
                     u'\u1E42', 
                     u'\u1E43', 
                     u'\u1E44', 
                     u'\u1E45', 
                     u'\u1E46', 
                     u'\u1E47', 
                     u'\u1E52', 
                     u'\u1E53', 
                     u'\u1E54', 
                     u'\u1E55', 
                     u'\u1E56', 
                     u'\u1E57', 
                     u'\u1E58', 
                     u'\u1E59', 
                     u'\u1E5A', 
                     u'\u1E5B', 
                     u'\u1E60', 
                     u'\u1E61', 
                     u'\u1E62', 
                     u'\u1E63', 
                     u'\u1E6A', 
                     u'\u1E6B', 
                     u'\u1E6C', 
                     u'\u1E6D', 
                     u'\u1E7E', 
                     u'\u1E7F', 
                     u'\u1E80', 
                     u'\u1E81', 
                     u'\u1E82', 
                     u'\u1E83', 
                     u'\u1E84', 
                     u'\u1E85', 
                     u'\u1E86', 
                     u'\u1E87', 
                     u'\u1E88', 
                     u'\u1E89', 
                     u'\u1E8E', 
                     u'\u1E8F', 
                     u'\u1E92', 
                     u'\u1E93', 
                     u'\u1E98', 
                     u'\u1E99', 
                     u'\u1E9C', 
                     u'\u1E9D', 
                     u'\u1E9E', 
                     u'\u1E9F', 
                     u'\u1EA0', 
                     u'\u1EA1', 
                     u'\u1EA2', 
                     u'\u1EA3', 
                     u'\u1EAE', 
                     u'\u1EAF', 
                     u'\u1EB8', 
                     u'\u1EB9', 
                     u'\u1EC8', 
                     u'\u1EC9', 
                     u'\u1ECA', 
                     u'\u1ECB', 
                     u'\u1ECC', 
                     u'\u1ECD', 
                     u'\u1ECE', 
                     u'\u1ECF', 
                     u'\u1EE4', 
                     u'\u1EE5', 
                     u'\u1EE6', 
                     u'\u1EE7', 
                     u'\u1EF2', 
                     u'\u1EF3', 
                     u'\u1EF4', 
                     u'\u1EF5', 
                     u'\u1EF6', 
                     u'\u1EF7', 
                     u'\u1EFA', 
                     u'\u1EFB', 
                     u'\u1EFC', 
                     u'\u1EFD', 
                     u'\u1EFE', 
                     u'\u1EFF', 
                      ],
             'codes': [ 
                     '1E02', 
                     '1E03', 
                     '1E04', 
                     '1E05', 
                     '1E0A', 
                     '1E0B', 
                     '1E0C', 
                     '1E0D', 
                     '1E16', 
                     '1E17', 
                     '1E1E', 
                     '1E1F', 
                     '1E22', 
                     '1E23', 
                     '1E24', 
                     '1E25', 
                     '1E30', 
                     '1E31', 
                     '1E32', 
                     '1E33', 
                     '1E36', 
                     '1E37', 
                     '1E3E', 
                     '1E3F', 
                     '1E40', 
                     '1E41', 
                     '1E42', 
                     '1E43', 
                     '1E44', 
                     '1E45', 
                     '1E46', 
                     '1E47', 
                     '1E52', 
                     '1E53', 
                     '1E54', 
                     '1E55', 
                     '1E56', 
                     '1E57', 
                     '1E58', 
                     '1E59', 
                     '1E5A', 
                     '1E5B', 
                     '1E60', 
                     '1E61', 
                     '1E62', 
                     '1E63', 
                     '1E6A', 
                     '1E6B', 
                     '1E6C', 
                     '1E6D', 
                     '1E7E', 
                     '1E7F', 
                     '1E80', 
                     '1E81', 
                     '1E82', 
                     '1E83', 
                     '1E84', 
                     '1E85', 
                     '1E86', 
                     '1E87', 
                     '1E88', 
                     '1E89', 
                     '1E8E', 
                     '1E8F', 
                     '1E92', 
                     '1E93', 
                     '1E98', 
                     '1E99', 
                     '1E9C', 
                     '1E9D', 
                     '1E9E', 
                     '1E9F', 
                     '1EA0', 
                     '1EA1', 
                     '1EA2', 
                     '1EA3', 
                     '1EAE', 
                     '1EAF', 
                     '1EB8', 
                     '1EB9', 
                     '1EC8', 
                     '1EC9', 
                     '1ECA', 
                     '1ECB', 
                     '1ECC', 
                     '1ECD', 
                     '1ECE', 
                     '1ECF', 
                     '1EE4', 
                     '1EE5', 
                     '1EE6', 
                     '1EE7', 
                     '1EF2', 
                     '1EF3', 
                     '1EF4', 
                     '1EF5', 
                     '1EF6', 
                     '1EF7', 
                     '1EFA', 
                     '1EFB', 
                     '1EFC', 
                     '1EFD', 
                     '1EFE', 
                     '1EFF', 
                      ],
             }, 
         'Modified_baseline_characters': {
             'names': [ 
                     'nbar', 
                     'vbar', 
                     'ybar', 
                      ],
             'chars': [ 
                     u'\uE7B2', 
                     u'\uE74E', 
                     u'\uE77B', 
                      ],
             'codes': [ 
                     'E7B2', 
                     'E74E', 
                     'E77B', 
                      ],
             }, 
         'Georgian': {
             'names': [ 
                     'tridotright', 
                      ],
             'chars': [ 
                     u'\u10FB', 
                      ],
             'codes': [ 
                     '10FB', 
                      ],
             }, 
         'Greek_and_Coptic': {
             'names': [ 
                     'alpha', 
                     'beta', 
                     'gamma', 
                     'delta', 
                     'epsilon', 
                     'zeta', 
                     'eta', 
                     'Theta', 
                     'theta', 
                     'iota', 
                     'kappa', 
                     'lambda_', 
                     'mu', 
                     'nu', 
                     'xi', 
                     'omicron', 
                     'pi', 
                     'rho', 
                     'sigmaf', 
                     'sigma', 
                     'tau', 
                     'upsilon', 
                     'phi', 
                     'chi', 
                     'psi', 
                     'omega', 
                      ],
             'chars': [ 
                     u'\u03B1', 
                     u'\u03B2', 
                     u'\u03B3', 
                     u'\u03B4', 
                     u'\u03B5', 
                     u'\u03B6', 
                     u'\u03B7', 
                     u'\u0398', 
                     u'\u03B8', 
                     u'\u03B9', 
                     u'\u03BA', 
                     u'\u03BB', 
                     u'\u03BC', 
                     u'\u03BD', 
                     u'\u03BE', 
                     u'\u03BF', 
                     u'\u03C0', 
                     u'\u03C1', 
                     u'\u03C2', 
                     u'\u03C3', 
                     u'\u03C4', 
                     u'\u03C5', 
                     u'\u03C6', 
                     u'\u03C7', 
                     u'\u03C8', 
                     u'\u03C9', 
                      ],
             'codes': [ 
                     '03B1', 
                     '03B2', 
                     '03B3', 
                     '03B4', 
                     '03B5', 
                     '03B6', 
                     '03B7', 
                     '0398', 
                     '03B8', 
                     '03B9', 
                     '03BA', 
                     '03BB', 
                     '03BC', 
                     '03BD', 
                     '03BE', 
                     '03BF', 
                     '03C0', 
                     '03C1', 
                     '03C2', 
                     '03C3', 
                     '03C4', 
                     '03C5', 
                     '03C6', 
                     '03C7', 
                     '03C8', 
                     '03C9', 
                      ],
             }, 
         'Miscellaneous_Mathematical_SymbolsA': {
             'names': [ 
                     'lwhsqb', 
                     'rwhsqb', 
                     'langb', 
                     'rangb', 
                      ],
             'chars': [ 
                     u'\u27E6', 
                     u'\u27E7', 
                     u'\u27E8', 
                     u'\u27E9', 
                      ],
             'codes': [ 
                     '27E6', 
                     '27E7', 
                     '27E8', 
                     '27E9', 
                      ],
             }, 
         'Ligatures': {
             'names': [ 
                     'aacloselig', 
                     'aeligred', 
                     'AnecklessElig', 
                     'anecklesselig', 
                     'anecklessvlig', 
                     'AOligred', 
                     'aoligred', 
                     'aflig', 
                     'afinslig', 
                     'aglig', 
                     'allig', 
                     'anlig', 
                     'anscaplig', 
                     'aplig', 
                     'arlig', 
                     'arscaplig', 
                     'athornlig', 
                     'bblig', 
                     'bglig', 
                     'cklig', 
                     'ctlig', 
                     'drotdrotlig', 
                     'eylig', 
                     'faumllig', 
                     'fjlig', 
                     'foumllig', 
                     'frlig', 
                     'ftlig', 
                     'fuumllig', 
                     'fylig', 
                     'fftlig', 
                     'ffylig', 
                     'ftylig', 
                     'gglig', 
                     'gdlig', 
                     'gdrotlig', 
                     'gethlig', 
                     'golig', 
                     'gplig', 
                     'grlig', 
                     'qvinslig', 
                     'hrarmlig', 
                     'Hrarmlig', 
                     'krarmlig', 
                     'lllig', 
                     'nscapslonglig', 
                     'oclig', 
                     'PPlig', 
                     'pplig', 
                     'ppflourlig', 
                     'slongaumllig', 
                     'slongchlig', 
                     'slonghlig', 
                     'slongilig', 
                     'slongjlig', 
                     'slongklig', 
                     'slongllig', 
                     'slongoumllig', 
                     'slongplig', 
                     'slongslig', 
                     'slongslonglig', 
                     'slongslongilig', 
                     'slongslongklig', 
                     'slongslongllig', 
                     'slongslongtlig', 
                     'slongtilig', 
                     'slongtrlig', 
                     'slonguumllig', 
                     'slongvinslig', 
                     'slongdestlig', 
                     'trlig', 
                     'ttlig', 
                     'trottrotlig', 
                     'tylig', 
                     'tzlig', 
                     'thornrarmlig', 
                      ],
             'chars': [ 
                     u'\uEFA0', 
                     u'\uF204', 
                     u'\uEFAE', 
                     u'\uEFA1', 
                     u'\uEFA2', 
                     u'\uF205', 
                     u'\uF206', 
                     u'\uEFA3', 
                     u'\uEFA4', 
                     u'\uEFA5', 
                     u'\uEFA6', 
                     u'\uEFA7', 
                     u'\uEFA8', 
                     u'\uEFA9', 
                     u'\uEFAA', 
                     u'\uEFAB', 
                     u'\uEFAC', 
                     u'\uEEC2', 
                     u'\uEEC3', 
                     u'\uEEC4', 
                     u'\uEEC5', 
                     u'\uEEC6', 
                     u'\uEEC7', 
                     u'\uEEC8', 
                     u'\uEEC9', 
                     u'\uF1BC', 
                     u'\uEECA', 
                     u'\uEECB', 
                     u'\uEECC', 
                     u'\uEECD', 
                     u'\uEECE', 
                     u'\uEECF', 
                     u'\uEED0', 
                     u'\uEED1', 
                     u'\uEED2', 
                     u'\uEED3', 
                     u'\uEED4', 
                     u'\uEEDE', 
                     u'\uEAD2', 
                     u'\uEAD0', 
                     u'\uEAD1', 
                     u'\uE8C3', 
                     u'\uE8C2', 
                     u'\uE8C5', 
                     u'\uF4F9', 
                     u'\uEED5', 
                     u'\uEFAD', 
                     u'\uEEDD', 
                     u'\uEED6', 
                     u'\uEED7', 
                     u'\uEBA0', 
                     u'\uF4FA', 
                     u'\uEBA1', 
                     u'\uEBA2', 
                     u'\uF4FB', 
                     u'\uF4FC', 
                     u'\uEBA3', 
                     u'\uEBA4', 
                     u'\uEBA5', 
                     u'\uF4FD', 
                     u'\uEBA6', 
                     u'\uEBA7', 
                     u'\uF4FE', 
                     u'\uEBA8', 
                     u'\uF4FF', 
                     u'\uEBA9', 
                     u'\uEBAA', 
                     u'\uEBAB', 
                     u'\uEBAC', 
                     u'\uEADA', 
                     u'\uEED8', 
                     u'\uEED9', 
                     u'\uEEDA', 
                     u'\uEEDB', 
                     u'\uEEDC', 
                     u'\uE8C1', 
                      ],
             'codes': [ 
                     'EFA0', 
                     'F204', 
                     'EFAE', 
                     'EFA1', 
                     'EFA2', 
                     'F205', 
                     'F206', 
                     'EFA3', 
                     'EFA4', 
                     'EFA5', 
                     'EFA6', 
                     'EFA7', 
                     'EFA8', 
                     'EFA9', 
                     'EFAA', 
                     'EFAB', 
                     'EFAC', 
                     'EEC2', 
                     'EEC3', 
                     'EEC4', 
                     'EEC5', 
                     'EEC6', 
                     'EEC7', 
                     'EEC8', 
                     'EEC9', 
                     'F1BC', 
                     'EECA', 
                     'EECB', 
                     'EECC', 
                     'EECD', 
                     'EECE', 
                     'EECF', 
                     'EED0', 
                     'EED1', 
                     'EED2', 
                     'EED3', 
                     'EED4', 
                     'EEDE', 
                     'EAD2', 
                     'EAD0', 
                     'EAD1', 
                     'E8C3', 
                     'E8C2', 
                     'E8C5', 
                     'F4F9', 
                     'EED5', 
                     'EFAD', 
                     'EEDD', 
                     'EED6', 
                     'EED7', 
                     'EBA0', 
                     'F4FA', 
                     'EBA1', 
                     'EBA2', 
                     'F4FB', 
                     'F4FC', 
                     'EBA3', 
                     'EBA4', 
                     'EBA5', 
                     'F4FD', 
                     'EBA6', 
                     'EBA7', 
                     'F4FE', 
                     'EBA8', 
                     'F4FF', 
                     'EBA9', 
                     'EBAA', 
                     'EBAB', 
                     'EBAC', 
                     'EADA', 
                     'EED8', 
                     'EED9', 
                     'EEDA', 
                     'EEDB', 
                     'EEDC', 
                     'E8C1', 
                      ],
             }, 
         'Characters_with_circumflex': {
             'names': [ 
                     'ncirc', 
                     'Vcirc', 
                     'vcirc', 
                     'eacombcirc', 
                     'eucombcirc', 
                      ],
             'chars': [ 
                     u'\uE5D7', 
                     u'\uE33B', 
                     u'\uE73B', 
                     u'\uEBBD', 
                     u'\uEBBE', 
                      ],
             'codes': [ 
                     'E5D7', 
                     'E33B', 
                     'E73B', 
                     'EBBD', 
                     'EBBE', 
                      ],
             }, 
         'Characters_with_breve_below': {
             'names': [ 
                     'ibrevinvbl', 
                     'ubrevinvbl', 
                      ],
             'chars': [ 
                     u'\uE548', 
                     u'\uE727', 
                      ],
             'codes': [ 
                     'E548', 
                     'E727', 
                      ],
             }, 
         'Characters_with_dot_above_and_ogonek': {
             'names': [ 
                     'Eogondot', 
                     'eogondot', 
                     'Oogondot', 
                     'oogondot', 
                      ],
             'chars': [ 
                     u'\uE0EB', 
                     u'\uE4EB', 
                     u'\uEBDE', 
                     u'\uEBDF', 
                      ],
             'codes': [ 
                     'E0EB', 
                     'E4EB', 
                     'EBDE', 
                     'EBDF', 
                      ],
             }, 
         'Characters_with_double_acute_accent_and_ogonek': {
             'names': [ 
                     'Eogondblac', 
                     'eogondblac', 
                     'Oogondblac', 
                     'oogondblac', 
                      ],
             'chars': [ 
                     u'\uE0EA', 
                     u'\uE4EA', 
                     u'\uEBC4', 
                     u'\uEBC5', 
                      ],
             'codes': [ 
                     'E0EA', 
                     'E4EA', 
                     'EBC4', 
                     'EBC5', 
                      ],
             }, 
         'Characters_with_acute_accent_and_ogonek': {
             'names': [ 
                     'Aogonacute', 
                     'aogonacute', 
                     'aeligogonacute', 
                     'Eogonacute', 
                     'eogonacute', 
                     'Oogonacute', 
                     'oogonacute', 
                     'Oslashogonacute', 
                     'oslashogonacute', 
                      ],
             'chars': [ 
                     u'\uE004', 
                     u'\uE404', 
                     u'\uE8D3', 
                     u'\uE099', 
                     u'\uE499', 
                     u'\uE20C', 
                     u'\uE60C', 
                     u'\uE257', 
                     u'\uE657', 
                      ],
             'codes': [ 
                     'E004', 
                     'E404', 
                     'E8D3', 
                     'E099', 
                     'E499', 
                     'E20C', 
                     'E60C', 
                     'E257', 
                     'E657', 
                      ],
             }, 
         'Characters_with_acute_accent_and_dot_above': {
             'names': [ 
                     'Adotacute', 
                     'adotacute', 
                     'Edotacute', 
                     'edotacute', 
                     'Idotacute', 
                     'idotacute', 
                     'Odotacute', 
                     'odotacute', 
                     'Oslashdotacute', 
                     'oslashdotacute', 
                     'Udotacute', 
                     'udotacute', 
                     'Ydotacute', 
                     'ydotacute', 
                      ],
             'chars': [ 
                     u'\uEBF4', 
                     u'\uEBF5', 
                     u'\uE0C8', 
                     u'\uE4C8', 
                     u'\uEBF6', 
                     u'\uEBF7', 
                     u'\uEBF8', 
                     u'\uEBF9', 
                     u'\uEBFC', 
                     u'\uEBFD', 
                     u'\uEBFE', 
                     u'\uEBFF', 
                     u'\uE384', 
                     u'\uE784', 
                      ],
             'codes': [ 
                     'EBF4', 
                     'EBF5', 
                     'E0C8', 
                     'E4C8', 
                     'EBF6', 
                     'EBF7', 
                     'EBF8', 
                     'EBF9', 
                     'EBFC', 
                     'EBFD', 
                     'EBFE', 
                     'EBFF', 
                     'E384', 
                     'E784', 
                      ],
             }, 
         'Latin_ExtendedB': {
             'names': [ 
                     'bstrok', 
                     'hwair', 
                     'khook', 
                     'lbar', 
                     'nlrleg', 
                     'YR', 
                     'Zstrok', 
                     'zstrok', 
                     'EZH', 
                     'wynn', 
                     'Ocar', 
                     'ocar', 
                     'Ucar', 
                     'ucar', 
                     'Uumlmacr', 
                     'uumlmacr', 
                     'AEligmacr', 
                     'aeligmacr', 
                     'Gstrok', 
                     'gstrok', 
                     'Oogon', 
                     'oogon', 
                     'Oogonmacr', 
                     'oogonmacr', 
                     'Gacute', 
                     'gacute', 
                     'HWAIR', 
                     'WYNN', 
                     'AEligacute', 
                     'aeligacute', 
                     'Oslashacute', 
                     'oslashacute', 
                     'YOGH', 
                     'yogh', 
                     'Adot', 
                     'adot', 
                     'Oumlmacr', 
                     'oumlmacr', 
                     'Odot', 
                     'odot', 
                     'Ymacr', 
                     'ymacr', 
                     'jnodot', 
                     'Jbar', 
                     'jbar', 
                      ],
             'chars': [ 
                     u'\u0180', 
                     u'\u0195', 
                     u'\u0199', 
                     u'\u019A', 
                     u'\u019E', 
                     u'\u01A6', 
                     u'\u01B5', 
                     u'\u01B6', 
                     u'\u01B7', 
                     u'\u01BF', 
                     u'\u01D1', 
                     u'\u01D2', 
                     u'\u01D3', 
                     u'\u01D4', 
                     u'\u01D5', 
                     u'\u01D6', 
                     u'\u01E2', 
                     u'\u01E3', 
                     u'\u01E4', 
                     u'\u01E5', 
                     u'\u01EA', 
                     u'\u01EB', 
                     u'\u01EC', 
                     u'\u01ED', 
                     u'\u01F4', 
                     u'\u01F5', 
                     u'\u01F6', 
                     u'\u01F7', 
                     u'\u01FC', 
                     u'\u01FD', 
                     u'\u01FE', 
                     u'\u01FF', 
                     u'\u021C', 
                     u'\u021D', 
                     u'\u0226', 
                     u'\u0227', 
                     u'\u022A', 
                     u'\u022B', 
                     u'\u022E', 
                     u'\u022F', 
                     u'\u0232', 
                     u'\u0233', 
                     u'\u0237', 
                     u'\u0248', 
                     u'\u0249', 
                      ],
             'codes': [ 
                     '0180', 
                     '0195', 
                     '0199', 
                     '019A', 
                     '019E', 
                     '01A6', 
                     '01B5', 
                     '01B6', 
                     '01B7', 
                     '01BF', 
                     '01D1', 
                     '01D2', 
                     '01D3', 
                     '01D4', 
                     '01D5', 
                     '01D6', 
                     '01E2', 
                     '01E3', 
                     '01E4', 
                     '01E5', 
                     '01EA', 
                     '01EB', 
                     '01EC', 
                     '01ED', 
                     '01F4', 
                     '01F5', 
                     '01F6', 
                     '01F7', 
                     '01FC', 
                     '01FD', 
                     '01FE', 
                     '01FF', 
                     '021C', 
                     '021D', 
                     '0226', 
                     '0227', 
                     '022A', 
                     '022B', 
                     '022E', 
                     '022F', 
                     '0232', 
                     '0233', 
                     '0237', 
                     '0248', 
                     '0249', 
                      ],
             }, 
         'Characters_with_macron_and_acute_accent': {
             'names': [ 
                     'Amacracute', 
                     'amacracute', 
                     'AEligmacracute', 
                     'aeligmacracute', 
                     'Imacracute', 
                     'imacracute', 
                     'Oslashmacracute', 
                     'oslashmacracute', 
                     'Umacracute', 
                     'umacracute', 
                     'Ymacracute', 
                     'ymacracute', 
                      ],
             'chars': [ 
                     u'\uE00A', 
                     u'\uE40A', 
                     u'\uE03A', 
                     u'\uE43A', 
                     u'\uE135', 
                     u'\uE535', 
                     u'\uEBEC', 
                     u'\uEBED', 
                     u'\uE309', 
                     u'\uE709', 
                     u'\uE373', 
                     u'\uE773', 
                      ],
             'codes': [ 
                     'E00A', 
                     'E40A', 
                     'E03A', 
                     'E43A', 
                     'E135', 
                     'E535', 
                     'EBEC', 
                     'EBED', 
                     'E309', 
                     'E709', 
                     'E373', 
                     'E773', 
                      ],
             }, 
         'Enlarged_minuscules': {
             'names': [ 
                     'aenl', 
                     'aenlacute', 
                     'aaligenl', 
                     'aeligenl', 
                     'aoligenl', 
                     'aenlosmalllig', 
                     'benl', 
                     'cenl', 
                     'denl', 
                     'drotenl', 
                     'ethenl', 
                     'eenl', 
                     'eogonenl', 
                     'fenl', 
                     'finsenl', 
                     'genl', 
                     'henl', 
                     'ienl', 
                     'inodotenl', 
                     'jenl', 
                     'jnodotenl', 
                     'kenl', 
                     'lenl', 
                     'menl', 
                     'nenl', 
                     'oenl', 
                     'oeligenl', 
                     'penl', 
                     'qenl', 
                     'renl', 
                     'senl', 
                     'slongenl', 
                     'tenl', 
                     'uenl', 
                     'venl', 
                     'wenl', 
                     'xenl', 
                     'yenl', 
                     'zenl', 
                     'thornenl', 
                      ],
             'chars': [ 
                     u'\uEEE0', 
                     u'\uEAF0', 
                     u'\uEFDF', 
                     u'\uEAF1', 
                     u'\uEFDE', 
                     u'\uEAF2', 
                     u'\uEEE1', 
                     u'\uEEE2', 
                     u'\uEEE3', 
                     u'\uEEE4', 
                     u'\uEEE5', 
                     u'\uEEE6', 
                     u'\uEAF3', 
                     u'\uEEE7', 
                     u'\uEEFF', 
                     u'\uEEE8', 
                     u'\uEEE9', 
                     u'\uEEEA', 
                     u'\uEEFD', 
                     u'\uEEEB', 
                     u'\uEEFE', 
                     u'\uEEEC', 
                     u'\uEEED', 
                     u'\uEEEE', 
                     u'\uEEEF', 
                     u'\uEEF0', 
                     u'\uEFDD', 
                     u'\uEEF1', 
                     u'\uEEF2', 
                     u'\uEEF3', 
                     u'\uEEF4', 
                     u'\uEEDF', 
                     u'\uEEF5', 
                     u'\uEEF7', 
                     u'\uEEF8', 
                     u'\uEEF9', 
                     u'\uEEFA', 
                     u'\uEEFB', 
                     u'\uEEFC', 
                     u'\uEEF6', 
                      ],
             'codes': [ 
                     'EEE0', 
                     'EAF0', 
                     'EFDF', 
                     'EAF1', 
                     'EFDE', 
                     'EAF2', 
                     'EEE1', 
                     'EEE2', 
                     'EEE3', 
                     'EEE4', 
                     'EEE5', 
                     'EEE6', 
                     'EAF3', 
                     'EEE7', 
                     'EEFF', 
                     'EEE8', 
                     'EEE9', 
                     'EEEA', 
                     'EEFD', 
                     'EEEB', 
                     'EEFE', 
                     'EEEC', 
                     'EEED', 
                     'EEEE', 
                     'EEEF', 
                     'EEF0', 
                     'EFDD', 
                     'EEF1', 
                     'EEF2', 
                     'EEF3', 
                     'EEF4', 
                     'EEDF', 
                     'EEF5', 
                     'EEF7', 
                     'EEF8', 
                     'EEF9', 
                     'EEFA', 
                     'EEFB', 
                     'EEFC', 
                     'EEF6', 
                      ],
             }, 
         'Spacing_Modifying_Letters': {
             'names': [ 
                     'apomod', 
                     'verbarup', 
                     'breve', 
                     'dot', 
                     'ring', 
                     'ogon', 
                     'tilde', 
                     'dblac', 
                     'xmod', 
                      ],
             'chars': [ 
                     u'\u02BC', 
                     u'\u02C8', 
                     u'\u02D8', 
                     u'\u02D9', 
                     u'\u02DA', 
                     u'\u02DB', 
                     u'\u02DC', 
                     u'\u02DD', 
                     u'\u02E3', 
                      ],
             'codes': [ 
                     '02BC', 
                     '02C8', 
                     '02D8', 
                     '02D9', 
                     '02DA', 
                     '02DB', 
                     '02DC', 
                     '02DD', 
                     '02E3', 
                      ],
             }, 
         'Characters_with_ring_above': {
             'names': [ 
                     'aeligring', 
                     'ering', 
                     'oring', 
                     'vring', 
                      ],
             'chars': [ 
                     u'\uE8D1', 
                     u'\uE4CF', 
                     u'\uE637', 
                     u'\uE743', 
                      ],
             'codes': [ 
                     'E8D1', 
                     'E4CF', 
                     'E637', 
                     'E743', 
                      ],
             }, 
         'Alphabetic_Presentation_Forms': {
             'names': [ 
                     'fflig', 
                     'filig', 
                     'fllig', 
                     'ffilig', 
                     'ffllig', 
                     'slongtlig', 
                     'stlig', 
                      ],
             'chars': [ 
                     u'\uFB00', 
                     u'\uFB01', 
                     u'\uFB02', 
                     u'\uFB03', 
                     u'\uFB04', 
                     u'\uFB05', 
                     u'\uFB06', 
                      ],
             'codes': [ 
                     'FB00', 
                     'FB01', 
                     'FB02', 
                     'FB03', 
                     'FB04', 
                     'FB05', 
                     'FB06', 
                      ],
             }, 
         'Characters_with_diaeresis_and_dot_below': {
             'names': [ 
                     'adotbluml', 
                      ],
             'chars': [ 
                     u'\uE41D', 
                      ],
             'codes': [ 
                     'E41D', 
                      ],
             }, 
         'Characters_with_ring_above_and_circumflex': {
             'names': [ 
                     'aringcirc', 
                      ],
             'chars': [ 
                     u'\uE41F', 
                      ],
             'codes': [ 
                     'E41F', 
                      ],
             }, 
         'Arrows': {
             'names': [ 
                     'arrsgllw', 
                     'arrsglupw', 
                     'arrsglrw', 
                     'arrsgldw', 
                      ],
             'chars': [ 
                     u'\u2190', 
                     u'\u2191', 
                     u'\u2192', 
                     u'\u2193', 
                      ],
             'codes': [ 
                     '2190', 
                     '2191', 
                     '2192', 
                     '2193', 
                      ],
             }, 
         'Characters_with_macron_or_overline': {
             'names': [ 
                     'macrhigh', 
                     'macrmed', 
                     'ovlhigh', 
                     'ovlmed', 
                     'bovlmed', 
                     'Covlhigh', 
                     'romnumCrevovl', 
                     'Dovlhigh', 
                     'dovlmed', 
                     'Eogonmacr', 
                     'eogonmacr', 
                     'hovlmed', 
                     'Iovlhigh', 
                     'iovlmed', 
                     'Jmacrhigh', 
                     'Jovlhigh', 
                     'jmacrmed', 
                     'jovlmed', 
                     'kovlmed', 
                     'lovlmed', 
                     'Lovlhigh', 
                     'lmacrhigh', 
                     'lovlhigh', 
                     'Mmacrhigh', 
                     'Movlhigh', 
                     'mmacrmed', 
                     'movlmed', 
                     'Nmacrhigh', 
                     'nmacrmed', 
                     'Oslashmacr', 
                     'oslashmacr', 
                     'OEligmacr', 
                     'oeligmacr', 
                     'oopenmacr', 
                     'pmacr', 
                     'qmacr', 
                     'slongovlmed', 
                     'Vmacr', 
                     'Vovlhigh', 
                     'vmacr', 
                     'Wmacr', 
                     'wmacr', 
                     'Xovlhigh', 
                     'thornovlmed', 
                      ],
             'chars': [ 
                     u'\uF00A', 
                     u'\uF00B', 
                     u'\uF00C', 
                     u'\uF00D', 
                     u'\uE44D', 
                     u'\uF7B5', 
                     u'\uF23F', 
                     u'\uF7B6', 
                     u'\uE491', 
                     u'\uE0BC', 
                     u'\uE4BC', 
                     u'\uE517', 
                     u'\uE150', 
                     u'\uE550', 
                     u'\uE154', 
                     u'\uE152', 
                     u'\uE554', 
                     u'\uE552', 
                     u'\uE7C3', 
                     u'\uE5B1', 
                     u'\uF7B4', 
                     u'\uE596', 
                     u'\uE58C', 
                     u'\uE1B8', 
                     u'\uE1D2', 
                     u'\uE5B8', 
                     u'\uE5D2', 
                     u'\uE1DC', 
                     u'\uE5DC', 
                     u'\uE252', 
                     u'\uE652', 
                     u'\uE25D', 
                     u'\uE65D', 
                     u'\uE7CC', 
                     u'\uE665', 
                     u'\uE681', 
                     u'\uE79E', 
                     u'\uE34D', 
                     u'\uF7B2', 
                     u'\uE74D', 
                     u'\uE357', 
                     u'\uE757', 
                     u'\uF7B3', 
                     u'\uE7A2', 
                      ],
             'codes': [ 
                     'F00A', 
                     'F00B', 
                     'F00C', 
                     'F00D', 
                     'E44D', 
                     'F7B5', 
                     'F23F', 
                     'F7B6', 
                     'E491', 
                     'E0BC', 
                     'E4BC', 
                     'E517', 
                     'E150', 
                     'E550', 
                     'E154', 
                     'E152', 
                     'E554', 
                     'E552', 
                     'E7C3', 
                     'E5B1', 
                     'F7B4', 
                     'E596', 
                     'E58C', 
                     'E1B8', 
                     'E1D2', 
                     'E5B8', 
                     'E5D2', 
                     'E1DC', 
                     'E5DC', 
                     'E252', 
                     'E652', 
                     'E25D', 
                     'E65D', 
                     'E7CC', 
                     'E665', 
                     'E681', 
                     'E79E', 
                     'E34D', 
                     'F7B2', 
                     'E74D', 
                     'E357', 
                     'E757', 
                     'F7B3', 
                     'E7A2', 
                      ],
             }, 
         'General_Punctuation': {
             'names': [ 
                     'enqd', 
                     'emqd', 
                     'ensp', 
                     'emsp', 
                     'emsp13', 
                     'emsp14', 
                     'emsp16', 
                     'numsp', 
                     'puncsp', 
                     'thinsp', 
                     'hairsp', 
                     'zerosp', 
                     'dash', 
                     'nbhy', 
                     'numdash', 
                     'ndash', 
                     'mdash', 
                     'horbar', 
                     'Verbar', 
                     'lsquo', 
                     'rsquo', 
                     'lsquolow', 
                     'rsquorev', 
                     'ldquo', 
                     'rdquo', 
                     'ldquolow', 
                     'rdquorev', 
                     'dagger', 
                     'Dagger', 
                     'bull', 
                     'tribull', 
                     'sgldr', 
                     'dblldr', 
                     'hellip', 
                     'hyphpoint', 
                     'nnbsp', 
                     'permil', 
                     'prime', 
                     'Prime', 
                     'lsaquo', 
                     'rsaquo', 
                     'refmark', 
                     'triast', 
                     'fracsol', 
                     'lsqbqu', 
                     'rsqbqu', 
                     'et', 
                     'revpara', 
                     'lozengedot', 
                     'dotcross', 
                      ],
             'chars': [ 
                     u'\u2000', 
                     u'\u2001', 
                     u'\u2002', 
                     u'\u2003', 
                     u'\u2004', 
                     u'\u2005', 
                     u'\u2006', 
                     u'\u2007', 
                     u'\u2008', 
                     u'\u2009', 
                     u'\u200A', 
                     u'\u200B', 
                     u'\u2010', 
                     u'\u2011', 
                     u'\u2012', 
                     u'\u2013', 
                     u'\u2014', 
                     u'\u2015', 
                     u'\u2016', 
                     u'\u2018', 
                     u'\u2019', 
                     u'\u201A', 
                     u'\u201B', 
                     u'\u201C', 
                     u'\u201D', 
                     u'\u201E', 
                     u'\u201F', 
                     u'\u2020', 
                     u'\u2021', 
                     u'\u2022', 
                     u'\u2023', 
                     u'\u2024', 
                     u'\u2025', 
                     u'\u2026', 
                     u'\u2027', 
                     u'\u202F', 
                     u'\u2030', 
                     u'\u2032', 
                     u'\u2033', 
                     u'\u2039', 
                     u'\u203A', 
                     u'\u203B', 
                     u'\u2042', 
                     u'\u2044', 
                     u'\u2045', 
                     u'\u2046', 
                     u'\u204A', 
                     u'\u204B', 
                     u'\u2058', 
                     u'\u205C', 
                      ],
             'codes': [ 
                     '2000', 
                     '2001', 
                     '2002', 
                     '2003', 
                     '2004', 
                     '2005', 
                     '2006', 
                     '2007', 
                     '2008', 
                     '2009', 
                     '200A', 
                     '200B', 
                     '2010', 
                     '2011', 
                     '2012', 
                     '2013', 
                     '2014', 
                     '2015', 
                     '2016', 
                     '2018', 
                     '2019', 
                     '201A', 
                     '201B', 
                     '201C', 
                     '201D', 
                     '201E', 
                     '201F', 
                     '2020', 
                     '2021', 
                     '2022', 
                     '2023', 
                     '2024', 
                     '2025', 
                     '2026', 
                     '2027', 
                     '202F', 
                     '2030', 
                     '2032', 
                     '2033', 
                     '2039', 
                     '203A', 
                     '203B', 
                     '2042', 
                     '2044', 
                     '2045', 
                     '2046', 
                     '204A', 
                     '204B', 
                     '2058', 
                     '205C', 
                      ],
             }, 
         'Characters_with_double_acute_accent': {
             'names': [ 
                     'Adblac', 
                     'adblac', 
                     'AAligdblac', 
                     'aaligdblac', 
                     'AEligdblac', 
                     'aeligdblac', 
                     'AOligdblac', 
                     'aoligdblac', 
                     'AVligdblac', 
                     'avligdblac', 
                     'Edblac', 
                     'edblac', 
                     'Idblac', 
                     'idblac', 
                     'Jdblac', 
                     'jdblac', 
                     'Oslashdblac', 
                     'oslashdblac', 
                     'OEligdblac', 
                     'oeligdblac', 
                     'OOligdblac', 
                     'ooligdblac', 
                     'Vdblac', 
                     'vdblac', 
                     'Wdblac', 
                     'wdblac', 
                     'Ydblac', 
                     'ydblac', 
                     'YYligdblac', 
                     'yyligdblac', 
                      ],
             'chars': [ 
                     u'\uE025', 
                     u'\uE425', 
                     u'\uEFEA', 
                     u'\uEFEB', 
                     u'\uE041', 
                     u'\uE441', 
                     u'\uEBC0', 
                     u'\uEBC1', 
                     u'\uEBC2', 
                     u'\uEBC3', 
                     u'\uE0D1', 
                     u'\uE4D1', 
                     u'\uE143', 
                     u'\uE543', 
                     u'\uE162', 
                     u'\uE562', 
                     u'\uEBC6', 
                     u'\uEBC7', 
                     u'\uEBC8', 
                     u'\uEBC9', 
                     u'\uEFEC', 
                     u'\uEFED', 
                     u'\uE34B', 
                     u'\uE74B', 
                     u'\uE350', 
                     u'\uE750', 
                     u'\uE37C', 
                     u'\uE77C', 
                     u'\uEBCA', 
                     u'\uEBCB', 
                      ],
             'codes': [ 
                     'E025', 
                     'E425', 
                     'EFEA', 
                     'EFEB', 
                     'E041', 
                     'E441', 
                     'EBC0', 
                     'EBC1', 
                     'EBC2', 
                     'EBC3', 
                     'E0D1', 
                     'E4D1', 
                     'E143', 
                     'E543', 
                     'E162', 
                     'E562', 
                     'EBC6', 
                     'EBC7', 
                     'EBC8', 
                     'EBC9', 
                     'EFEC', 
                     'EFED', 
                     'E34B', 
                     'E74B', 
                     'E350', 
                     'E750', 
                     'E37C', 
                     'E77C', 
                     'EBCA', 
                     'EBCB', 
                      ],
             }, 
         'Letterlike_Symbols': {
             'names': [ 
                     'scruple', 
                     'lbbar', 
                     'Rtailstrok', 
                     'Rslstrok', 
                     'Vslstrok', 
                     'ounce', 
                     'Fturn', 
                     'fturn', 
                      ],
             'chars': [ 
                     u'\u2108', 
                     u'\u2114', 
                     u'\u211E', 
                     u'\u211F', 
                     u'\u2123', 
                     u'\u2125', 
                     u'\u2132', 
                     u'\u214E', 
                      ],
             'codes': [ 
                     '2108', 
                     '2114', 
                     '211E', 
                     '211F', 
                     '2123', 
                     '2125', 
                     '2132', 
                     '214E', 
                      ],
             }, 
         'Characters_with_ogonek_and_circumflex': {
             'names': [ 
                     'eogoncirc', 
                     'oogoncirc', 
                      ],
             'chars': [ 
                     u'\uE49F', 
                     u'\uE60E', 
                      ],
             'codes': [ 
                     'E49F', 
                     'E60E', 
                      ],
             }, 
         'Supplemental_Mathematical_Operators': {
             'names': [ 
                     'dblsol', 
                      ],
             'chars': [ 
                     u'\u2AFD', 
                      ],
             'codes': [ 
                     '2AFD', 
                      ],
             }, 
         'Combining_marks': {
             'names': [ 
                     'arbar', 
                     'erang', 
                     'ercurl', 
                     'rabar', 
                     'urrot', 
                     'urlemn', 
                     'combcurlhigh', 
                     'combdothigh', 
                     'combcurlbar', 
                     'combtripbrevebl', 
                      ],
             'chars': [ 
                     u'\uF1C0', 
                     u'\uF1C7', 
                     u'\uF1C8', 
                     u'\uF1C1', 
                     u'\uF153', 
                     u'\uF1C2', 
                     u'\uF1C5', 
                     u'\uF1CA', 
                     u'\uF1CC', 
                     u'\uF1FC', 
                      ],
             'codes': [ 
                     'F1C0', 
                     'F1C7', 
                     'F1C8', 
                     'F1C1', 
                     'F153', 
                     'F1C2', 
                     'F1C5', 
                     'F1CA', 
                     'F1CC', 
                     'F1FC', 
                      ],
             }, 
         'Weight_currency_and_measurement': {
             'names': [ 
                     'romaslibr', 
                     'romXbar', 
                     'romscapxbar', 
                     'romscapybar', 
                     'romscapdslash', 
                     'dram', 
                     'ecu', 
                     'florloop', 
                     'grosch', 
                     'libradut', 
                     'librafren', 
                     'libraital', 
                     'libraflem', 
                     'liranuov', 
                     'lirasterl', 
                     'markold', 
                     'markflour', 
                     'msign', 
                     'msignflour', 
                     'obol', 
                     'penningar', 
                     'reichtalold', 
                     'schillgerm', 
                     'schillgermscript', 
                     'scudi', 
                     'sestert', 
                     'sextans', 
                     'ouncescript', 
                     'romas', 
                     'romunc', 
                     'romsemunc', 
                     'romsext', 
                     'romdimsext', 
                     'romsiliq', 
                     'romquin', 
                     'romdupond', 
                      ],
             'chars': [ 
                     u'\uF2E0', 
                     u'\uF2E1', 
                     u'\uF2E2', 
                     u'\uF2E3', 
                     u'\uF2E4', 
                     u'\uF2E6', 
                     u'\uF2E7', 
                     u'\uF2E8', 
                     u'\uF2E9', 
                     u'\uF2EA', 
                     u'\uF2EB', 
                     u'\uF2EC', 
                     u'\uF2ED', 
                     u'\uF2EE', 
                     u'\uF2EF', 
                     u'\uF2F0', 
                     u'\uF2F1', 
                     u'\uF2F2', 
                     u'\uF2F3', 
                     u'\uF2F4', 
                     u'\uF2F5', 
                     u'\uF2F6', 
                     u'\uF2F7', 
                     u'\uF2F8', 
                     u'\uF2F9', 
                     u'\uF2FA', 
                     u'\uF2FB', 
                     u'\uF2FD', 
                     u'\uF2D8', 
                     u'\uF2D9', 
                     u'\uF2DA', 
                     u'\uF2DB', 
                     u'\uF2DC', 
                     u'\uF2DD', 
                     u'\uF2DE', 
                     u'\uF2DF', 
                      ],
             'codes': [ 
                     'F2E0', 
                     'F2E1', 
                     'F2E2', 
                     'F2E3', 
                     'F2E4', 
                     'F2E6', 
                     'F2E7', 
                     'F2E8', 
                     'F2E9', 
                     'F2EA', 
                     'F2EB', 
                     'F2EC', 
                     'F2ED', 
                     'F2EE', 
                     'F2EF', 
                     'F2F0', 
                     'F2F1', 
                     'F2F2', 
                     'F2F3', 
                     'F2F4', 
                     'F2F5', 
                     'F2F6', 
                     'F2F7', 
                     'F2F8', 
                     'F2F9', 
                     'F2FA', 
                     'F2FB', 
                     'F2FD', 
                     'F2D8', 
                     'F2D9', 
                     'F2DA', 
                     'F2DB', 
                     'F2DC', 
                     'F2DD', 
                     'F2DE', 
                     'F2DF', 
                      ],
             }, 
         'Characters_with_diaeresis': {
             'names': [ 
                     'AAliguml', 
                     'aaliguml', 
                     'AEliguml', 
                     'aeliguml', 
                     'Juml', 
                     'juml', 
                     'OOliguml', 
                     'ooliguml', 
                     'PPliguml', 
                     'ppliguml', 
                     'Vuml', 
                     'vuml', 
                     'YYliguml', 
                     'yyliguml', 
                     'adiaguml', 
                     'odiaguml', 
                      ],
             'chars': [ 
                     u'\uEFFE', 
                     u'\uEFFF', 
                     u'\uE042', 
                     u'\uE442', 
                     u'\uEBE2', 
                     u'\uEBE3', 
                     u'\uEBE4', 
                     u'\uEBE5', 
                     u'\uEBE6', 
                     u'\uEBE7', 
                     u'\uE342', 
                     u'\uE742', 
                     u'\uEBE8', 
                     u'\uEBE9', 
                     u'\uE8D5', 
                     u'\uE8D7', 
                      ],
             'codes': [ 
                     'EFFE', 
                     'EFFF', 
                     'E042', 
                     'E442', 
                     'EBE2', 
                     'EBE3', 
                     'EBE4', 
                     'EBE5', 
                     'EBE6', 
                     'EBE7', 
                     'E342', 
                     'E742', 
                     'EBE8', 
                     'EBE9', 
                     'E8D5', 
                     'E8D7', 
                      ],
             }, 
         'Characters_with_acute_accent_and_curl_above_reversed_ogonek': {
             'names': [ 
                     'Ocurlacute', 
                     'ocurlacute', 
                      ],
             'chars': [ 
                     u'\uEBB7', 
                     u'\uEBB8', 
                      ],
             'codes': [ 
                     'EBB7', 
                     'EBB8', 
                      ],
             }, 
         'Metrical_symbols': {
             'names': [ 
                     'metranc', 
                     'metrancacute', 
                     'metrancdblac', 
                     'metrancgrave', 
                     'metrancdblgrave', 
                     'metrbreve', 
                     'metrbreveacute', 
                     'metrbrevedblac', 
                     'metrbrevegrave', 
                     'metrbrevedblgrave', 
                     'metrmacr', 
                     'metrmacracute', 
                     'metrmacrdblac', 
                     'metrmacrgrave', 
                     'metrmacrdblgrave', 
                     'metrmacrbreve', 
                     'metrbrevemacr', 
                     'metrmacrbreveacute', 
                     'metrmacrbrevegrave', 
                     'metrdblbrevemacr', 
                     'metrdblbrevemacracute', 
                     'metrdblbrevemacrdblac', 
                     'metrpause', 
                      ],
             'chars': [ 
                     u'\uF70A', 
                     u'\uF70B', 
                     u'\uF719', 
                     u'\uF70C', 
                     u'\uF71A', 
                     u'\uF701', 
                     u'\uF706', 
                     u'\uF717', 
                     u'\uF707', 
                     u'\uF718', 
                     u'\uF700', 
                     u'\uF704', 
                     u'\uF715', 
                     u'\uF705', 
                     u'\uF716', 
                     u'\uF702', 
                     u'\uF703', 
                     u'\uF708', 
                     u'\uF709', 
                     u'\uF72E', 
                     u'\uF71B', 
                     u'\uF71C', 
                     u'\uF714', 
                      ],
             'codes': [ 
                     'F70A', 
                     'F70B', 
                     'F719', 
                     'F70C', 
                     'F71A', 
                     'F701', 
                     'F706', 
                     'F717', 
                     'F707', 
                     'F718', 
                     'F700', 
                     'F704', 
                     'F715', 
                     'F705', 
                     'F716', 
                     'F702', 
                     'F703', 
                     'F708', 
                     'F709', 
                     'F72E', 
                     'F71B', 
                     'F71C', 
                     'F714', 
                      ],
             }, 
         'Critical_and_epigraphical_signs': {
             'names': [ 
                     'midring', 
                     'ramus', 
                      ],
             'chars': [ 
                     u'\uF1DA', 
                     u'\uF1DB', 
                      ],
             'codes': [ 
                     'F1DA', 
                     'F1DB', 
                      ],
             }, 
         'Combining_superscript_characters': {
             'names': [ 
                     'anligsup', 
                     'anscapligsup', 
                     'arligsup', 
                     'arscapligsup', 
                     'bsup', 
                     'bscapsup', 
                     'dscapsup', 
                     'eogonsup', 
                     'emacrsup', 
                     'fsup', 
                     'inodotsup', 
                     'jsup', 
                     'jnodotsup', 
                     'kscapsup', 
                     'omacrsup', 
                     'oogonsup', 
                     'oslashsup', 
                     'orrotsup', 
                     'orumsup', 
                     'psup', 
                     'qsup', 
                     'rumsup', 
                     'tscapsup', 
                     'trotsup', 
                     'wsup', 
                     'ysup', 
                     'thornsup', 
                      ],
             'chars': [ 
                     u'\uF036', 
                     u'\uF03A', 
                     u'\uF038', 
                     u'\uF130', 
                     u'\uF012', 
                     u'\uF013', 
                     u'\uF016', 
                     u'\uF135', 
                     u'\uF136', 
                     u'\uF017', 
                     u'\uF02F', 
                     u'\uF030', 
                     u'\uF031', 
                     u'\uF01C', 
                     u'\uF13F', 
                     u'\uF13E', 
                     u'\uF032', 
                     u'\uF03E', 
                     u'\uF03F', 
                     u'\uF025', 
                     u'\uF033', 
                     u'\uF040', 
                     u'\uF02A', 
                     u'\uF03B', 
                     u'\uF03C', 
                     u'\uF02B', 
                     u'\uF03D', 
                      ],
             'codes': [ 
                     'F036', 
                     'F03A', 
                     'F038', 
                     'F130', 
                     'F012', 
                     'F013', 
                     'F016', 
                     'F135', 
                     'F136', 
                     'F017', 
                     'F02F', 
                     'F030', 
                     'F031', 
                     'F01C', 
                     'F13F', 
                     'F13E', 
                     'F032', 
                     'F03E', 
                     'F03F', 
                     'F025', 
                     'F033', 
                     'F040', 
                     'F02A', 
                     'F03B', 
                     'F03C', 
                     'F02B', 
                     'F03D', 
                      ],
             }, 
         'Characters_with_curl_above_reversed_ogonek': {
             'names': [ 
                     'Acurl', 
                     'acurl', 
                     'AEligcurl', 
                     'aeligcurl', 
                     'Ecurl', 
                     'ecurl', 
                     'Icurl', 
                     'icurl', 
                     'Jcurl', 
                     'jcurl', 
                     'Ocurl', 
                     'ocurl', 
                     'Oslashcurl', 
                     'oslashcurl', 
                     'Ucurl', 
                     'ucurl', 
                     'Ycurl', 
                     'ycurl', 
                      ],
             'chars': [ 
                     u'\uE033', 
                     u'\uE433', 
                     u'\uEBEA', 
                     u'\uEBEB', 
                     u'\uE0E9', 
                     u'\uE4E9', 
                     u'\uE12A', 
                     u'\uE52A', 
                     u'\uE163', 
                     u'\uE563', 
                     u'\uE3D3', 
                     u'\uE7D3', 
                     u'\uE3D4', 
                     u'\uE7D4', 
                     u'\uE331', 
                     u'\uE731', 
                     u'\uE385', 
                     u'\uE785', 
                      ],
             'codes': [ 
                     'E033', 
                     'E433', 
                     'EBEA', 
                     'EBEB', 
                     'E0E9', 
                     'E4E9', 
                     'E12A', 
                     'E52A', 
                     'E163', 
                     'E563', 
                     'E3D3', 
                     'E7D3', 
                     'E3D4', 
                     'E7D4', 
                     'E331', 
                     'E731', 
                     'E385', 
                     'E785', 
                      ],
             }, 
         'Geometric_Shapes': {
             'names': [ 
                     'squareblsm', 
                     'squarewhsm', 
                     'trirightwh', 
                     'trileftwh', 
                     'circledot', 
                      ],
             'chars': [ 
                     u'\u25AA', 
                     u'\u25AB', 
                     u'\u25B9', 
                     u'\u25C3', 
                     u'\u25CC', 
                      ],
             'codes': [ 
                     '25AA', 
                     '25AB', 
                     '25B9', 
                     '25C3', 
                     '25CC', 
                      ],
             }, 
         'Characters_with_superscript_letters': {
             'names': [ 
                     'Aesup', 
                     'aesup', 
                     'aisup', 
                     'aosup', 
                     'ausup', 
                     'avsup', 
                     'Easup', 
                     'easup', 
                     'eesup', 
                     'eisup', 
                     'eosup', 
                     'evsup', 
                     'iasup', 
                     'iesup', 
                     'iosup', 
                     'iusup', 
                     'ivsup', 
                     'jesup', 
                     'mesup', 
                     'oasup', 
                     'Oesup', 
                     'oesup', 
                     'oisup', 
                     'oosup', 
                     'Ousup', 
                     'ousup', 
                     'ovsup', 
                     'resup', 
                     'uasup', 
                     'Uesup', 
                     'uesup', 
                     'uisup', 
                     'Uosup', 
                     'uosup', 
                     'uvsup', 
                     'uwsup', 
                     'yesup', 
                     'wasup', 
                     'Wesup', 
                     'wesup', 
                     'wisup', 
                     'wosup', 
                     'wusup', 
                     'wvsup', 
                      ],
             'chars': [ 
                     u'\uE02C', 
                     u'\uE42C', 
                     u'\uE8E0', 
                     u'\uE42D', 
                     u'\uE8E1', 
                     u'\uE42E', 
                     u'\uE0E1', 
                     u'\uE4E1', 
                     u'\uE8E2', 
                     u'\uE4E2', 
                     u'\uE8E3', 
                     u'\uE8E2', 
                     u'\uE8E4', 
                     u'\uE54A', 
                     u'\uE8E5', 
                     u'\uE8E6', 
                     u'\uE54B', 
                     u'\uE8E7', 
                     u'\uE8E8', 
                     u'\uE643', 
                     u'\uE244', 
                     u'\uE644', 
                     u'\uE645', 
                     u'\uE8E9', 
                     u'\uE246', 
                     u'\uE646', 
                     u'\uE647', 
                     u'\uE8EA', 
                     u'\uE8EB', 
                     u'\uE32B', 
                     u'\uE72B', 
                     u'\uE72C', 
                     u'\uE32D', 
                     u'\uE72D', 
                     u'\uE8EC', 
                     u'\uE8ED', 
                     u'\uE781', 
                     u'\uE8F0', 
                     u'\uE353', 
                     u'\uE753', 
                     u'\uE8F1', 
                     u'\uE754', 
                     u'\uE8F2', 
                     u'\uE8F3', 
                      ],
             'codes': [ 
                     'E02C', 
                     'E42C', 
                     'E8E0', 
                     'E42D', 
                     'E8E1', 
                     'E42E', 
                     'E0E1', 
                     'E4E1', 
                     'E8E2', 
                     'E4E2', 
                     'E8E3', 
                     'E8E2', 
                     'E8E4', 
                     'E54A', 
                     'E8E5', 
                     'E8E6', 
                     'E54B', 
                     'E8E7', 
                     'E8E8', 
                     'E643', 
                     'E244', 
                     'E644', 
                     'E645', 
                     'E8E9', 
                     'E246', 
                     'E646', 
                     'E647', 
                     'E8EA', 
                     'E8EB', 
                     'E32B', 
                     'E72B', 
                     'E72C', 
                     'E32D', 
                     'E72D', 
                     'E8EC', 
                     'E8ED', 
                     'E781', 
                     'E8F0', 
                     'E353', 
                     'E753', 
                     'E8F1', 
                     'E754', 
                     'E8F2', 
                     'E8F3', 
                      ],
             }, 
         'Combining_Diacritical_Marks': {
             'names': [ 
                     'combgrave', 
                     'combacute', 
                     'combcirc', 
                     'combtilde', 
                     'combmacr', 
                     'bar', 
                     'combbreve', 
                     'combdot', 
                     'combuml', 
                     'combhook', 
                     'combring', 
                     'combdblac', 
                     'combsgvertl', 
                     'combdbvertl', 
                     'combcomma', 
                     'combdotbl', 
                     'combced', 
                     'combogon', 
                     'barbl', 
                     'dblbarbl', 
                     'baracr', 
                     'combtildevert', 
                     'dblovl', 
                     'combastbl', 
                     'er', 
                     'combdblbrevebl', 
                     'asup', 
                     'esup', 
                     'isup', 
                     'osup', 
                     'usup', 
                     'csup', 
                     'dsup', 
                     'hsup', 
                     'msup', 
                     'rsup', 
                     'tsup', 
                     'vsup', 
                     'xsup', 
                      ],
             'chars': [ 
                     u'\u0300', 
                     u'\u0301', 
                     u'\u0302', 
                     u'\u0303', 
                     u'\u0304', 
                     u'\u0305', 
                     u'\u0306', 
                     u'\u0307', 
                     u'\u0308', 
                     u'\u0309', 
                     u'\u030A', 
                     u'\u030B', 
                     u'\u030D', 
                     u'\u030E', 
                     u'\u0315', 
                     u'\u0323', 
                     u'\u0327', 
                     u'\u0328', 
                     u'\u0332', 
                     u'\u0333', 
                     u'\u0336', 
                     u'\u033E', 
                     u'\u033F', 
                     u'\u0359', 
                     u'\u035B', 
                     u'\u035C', 
                     u'\u0363', 
                     u'\u0364', 
                     u'\u0365', 
                     u'\u0366', 
                     u'\u0367', 
                     u'\u0368', 
                     u'\u0369', 
                     u'\u036A', 
                     u'\u036B', 
                     u'\u036C', 
                     u'\u036D', 
                     u'\u036E', 
                     u'\u036F', 
                      ],
             'codes': [ 
                     '0300', 
                     '0301', 
                     '0302', 
                     '0303', 
                     '0304', 
                     '0305', 
                     '0306', 
                     '0307', 
                     '0308', 
                     '0309', 
                     '030A', 
                     '030B', 
                     '030D', 
                     '030E', 
                     '0315', 
                     '0323', 
                     '0327', 
                     '0328', 
                     '0332', 
                     '0333', 
                     '0336', 
                     '033E', 
                     '033F', 
                     '0359', 
                     '035B', 
                     '035C', 
                     '0363', 
                     '0364', 
                     '0365', 
                     '0366', 
                     '0367', 
                     '0368', 
                     '0369', 
                     '036A', 
                     '036B', 
                     '036C', 
                     '036D', 
                     '036E', 
                     '036F', 
                      ],
             }, 
         'Baseline_abbreviation_characters': {
             'names': [ 
                     'USbase', 
                     'usbase', 
                     'ET', 
                     'ETslash', 
                     'etslash', 
                     'de', 
                     'sem', 
                      ],
             'chars': [ 
                     u'\uF1A5', 
                     u'\uF1A6', 
                     u'\uF142', 
                     u'\uF1A7', 
                     u'\uF158', 
                     u'\uF159', 
                     u'\uF1AC', 
                      ],
             'codes': [ 
                     'F1A5', 
                     'F1A6', 
                     'F142', 
                     'F1A7', 
                     'F158', 
                     'F159', 
                     'F1AC', 
                      ],
             }, 
         'Additional_number_forms': {
             'names': [ 
                     'smallzero', 
                     'Vmod', 
                     'Xmod', 
                      ],
             'chars': [ 
                     u'\uF1BD', 
                     u'\uF1BE', 
                     u'\uF1BF', 
                      ],
             'codes': [ 
                     'F1BD', 
                     'F1BE', 
                     'F1BF', 
                      ],
             }, 
         'Characters_with_acute_accent': {
             'names': [ 
                     'AAligacute', 
                     'aaligacute', 
                     'AOligacute', 
                     'aoligacute', 
                     'AUligacute', 
                     'auligacute', 
                     'AVligacute', 
                     'avligacute', 
                     'AVligslashacute', 
                     'avligslashacute', 
                     'Bacute', 
                     'bacute', 
                     'Dacute', 
                     'dacute', 
                     'drotacute', 
                     'Facute', 
                     'facute', 
                     'Finsacute', 
                     'finsacute', 
                     'Hacute', 
                     'hacute', 
                     'Jacute', 
                     'jacute', 
                     'Muncacute', 
                     'muncacute', 
                     'OEligacute', 
                     'oeligacute', 
                     'OOligacute', 
                     'ooligacute', 
                     'rrotacute', 
                     'slongacute', 
                     'Tacute', 
                     'tacute', 
                     'Vacute', 
                     'vacute', 
                     'Vinsacute', 
                     'vinsacute', 
                     'thornacute', 
                      ],
             'chars': [ 
                     u'\uEFE0', 
                     u'\uEFE1', 
                     u'\uEFE2', 
                     u'\uEFE3', 
                     u'\uEFE4', 
                     u'\uEFE5', 
                     u'\uEFE6', 
                     u'\uEFE7', 
                     u'\uEBB0', 
                     u'\uEBB1', 
                     u'\uE044', 
                     u'\uE444', 
                     u'\uE077', 
                     u'\uE477', 
                     u'\uEBB2', 
                     u'\uE0F0', 
                     u'\uE4F0', 
                     u'\uEBB3', 
                     u'\uEBB4', 
                     u'\uE116', 
                     u'\uE516', 
                     u'\uE153', 
                     u'\uE553', 
                     u'\uEBB5', 
                     u'\uEBB6', 
                     u'\uE259', 
                     u'\uE659', 
                     u'\uEFE8', 
                     u'\uEFE9', 
                     u'\uEBB9', 
                     u'\uEBAF', 
                     u'\uE2E2', 
                     u'\uE6E2', 
                     u'\uE33A', 
                     u'\uE73A', 
                     u'\uEBBA', 
                     u'\uEBBB', 
                     u'\uE737', 
                      ],
             'codes': [ 
                     'EFE0', 
                     'EFE1', 
                     'EFE2', 
                     'EFE3', 
                     'EFE4', 
                     'EFE5', 
                     'EFE6', 
                     'EFE7', 
                     'EBB0', 
                     'EBB1', 
                     'E044', 
                     'E444', 
                     'E077', 
                     'E477', 
                     'EBB2', 
                     'E0F0', 
                     'E4F0', 
                     'EBB3', 
                     'EBB4', 
                     'E116', 
                     'E516', 
                     'E153', 
                     'E553', 
                     'EBB5', 
                     'EBB6', 
                     'E259', 
                     'E659', 
                     'EFE8', 
                     'EFE9', 
                     'EBB9', 
                     'EBAF', 
                     'E2E2', 
                     'E6E2', 
                     'E33A', 
                     'E73A', 
                     'EBBA', 
                     'EBBB', 
                     'E737', 
                      ],
             }, 
         'Runic': {
             'names': [ 
                     'fMedrun', 
                     'mMedrun', 
                      ],
             'chars': [ 
                     u'\u16A0', 
                     u'\u16D8', 
                      ],
             'codes': [ 
                     '16A0', 
                     '16D8', 
                      ],
             }, 
         'Number_forms': {
             'names': [ 
                     'romnumCDlig', 
                     'romnumDDlig', 
                     'romnumDDdbllig', 
                     'CONbase', 
                     'conbase', 
                      ],
             'chars': [ 
                     u'\u2180', 
                     u'\u2181', 
                     u'\u2182', 
                     u'\u2183', 
                     u'\u2184', 
                      ],
             'codes': [ 
                     '2180', 
                     '2181', 
                     '2182', 
                     '2183', 
                     '2184', 
                      ],
             }, 
         'Characters_with_diaeresis_and_macron': {
             'names': [ 
                     'eumlmacr', 
                      ],
             'chars': [ 
                     u'\uE4CD', 
                      ],
             'codes': [ 
                     'E4CD', 
                      ],
             }, 
         'Miscellaneous_Technical': {
             'names': [ 
                     'metrshort', 
                     'metrshortlong', 
                     'metrlongshort', 
                     'metrdblshortlong', 
                      ],
             'chars': [ 
                     u'\u23D1', 
                     u'\u23D2', 
                     u'\u23D3', 
                     u'\u23D4', 
                      ],
             'codes': [ 
                     '23D1', 
                     '23D2', 
                     '23D3', 
                     '23D4', 
                      ],
             }, 
         'Characters_with_dot_below': {
             'names': [ 
                     'AAligdotbl', 
                     'aaligdotbl', 
                     'AEligdotbl', 
                     'aeligdotbl', 
                     'AOligdotbl', 
                     'aoligdotbl', 
                     'AUligdotbl', 
                     'auligdotbl', 
                     'AVligdotbl', 
                     'avligdotbl', 
                     'AYligdotbl', 
                     'ayligdotbl', 
                     'bscapdotbl', 
                     'Cdotbl', 
                     'cdotbl', 
                     'dscapdotbl', 
                     'ETHdotbl', 
                     'ethdotbl', 
                     'Fdotbl', 
                     'fdotbl', 
                     'Finsdotbl', 
                     'finsdotbl', 
                     'Gdotbl', 
                     'gdotbl', 
                     'gscapdotbl', 
                     'Jdotbl', 
                     'jdotbl', 
                     'lscapdotbl', 
                     'mscapdotbl', 
                     'nscapdotbl', 
                     'Oslashdotbl', 
                     'oslashdotbl', 
                     'OOligdotbl', 
                     'ooligdotbl', 
                     'Pdotbl', 
                     'pdotbl', 
                     'Qdotbl', 
                     'qdotbl', 
                     'rscapdotbl', 
                     'rrotdotbl', 
                     'sscapdotbl', 
                     'slongdotbl', 
                     'tscapdotbl', 
                     'Vinsdotbl', 
                     'vinsdotbl', 
                     'THORNdotbl', 
                     'thorndotbl', 
                      ],
             'chars': [ 
                     u'\uEFF2', 
                     u'\uEFF3', 
                     u'\uE036', 
                     u'\uE436', 
                     u'\uEFF4', 
                     u'\uEFF5', 
                     u'\uEFF6', 
                     u'\uEFF7', 
                     u'\uEFF8', 
                     u'\uEFF9', 
                     u'\uEFFA', 
                     u'\uEFFB', 
                     u'\uEF25', 
                     u'\uE066', 
                     u'\uE466', 
                     u'\uEF26', 
                     u'\uE08F', 
                     u'\uE48F', 
                     u'\uE0EE', 
                     u'\uE4EE', 
                     u'\uE3E5', 
                     u'\uE7E5', 
                     u'\uE101', 
                     u'\uE501', 
                     u'\uEF27', 
                     u'\uE151', 
                     u'\uE551', 
                     u'\uEF28', 
                     u'\uEF29', 
                     u'\uEF2A', 
                     u'\uEBE0', 
                     u'\uEBE1', 
                     u'\uEFFC', 
                     u'\uEFFD', 
                     u'\uE26D', 
                     u'\uE66D', 
                     u'\uE288', 
                     u'\uE688', 
                     u'\uEF2B', 
                     u'\uE7C1', 
                     u'\uEF2C', 
                     u'\uE7C2', 
                     u'\uEF2D', 
                     u'\uE3E6', 
                     u'\uE7E6', 
                     u'\uE39F', 
                     u'\uE79F', 
                      ],
             'codes': [ 
                     'EFF2', 
                     'EFF3', 
                     'E036', 
                     'E436', 
                     'EFF4', 
                     'EFF5', 
                     'EFF6', 
                     'EFF7', 
                     'EFF8', 
                     'EFF9', 
                     'EFFA', 
                     'EFFB', 
                     'EF25', 
                     'E066', 
                     'E466', 
                     'EF26', 
                     'E08F', 
                     'E48F', 
                     'E0EE', 
                     'E4EE', 
                     'E3E5', 
                     'E7E5', 
                     'E101', 
                     'E501', 
                     'EF27', 
                     'E151', 
                     'E551', 
                     'EF28', 
                     'EF29', 
                     'EF2A', 
                     'EBE0', 
                     'EBE1', 
                     'EFFC', 
                     'EFFD', 
                     'E26D', 
                     'E66D', 
                     'E288', 
                     'E688', 
                     'EF2B', 
                     'E7C1', 
                     'EF2C', 
                     'E7C2', 
                     'EF2D', 
                     'E3E6', 
                     'E7E6', 
                     'E39F', 
                     'E79F', 
                      ],
             }, 
         'Characters_with_ogonek_and_curl_above_reversed_ogonek': {
             'names': [ 
                     'Eogoncurl', 
                     'eogoncurl', 
                     'Oogoncurl', 
                     'oogoncurl', 
                      ],
             'chars': [ 
                     u'\uEBF2', 
                     u'\uEBF3', 
                     u'\uE24F', 
                     u'\uE64F', 
                      ],
             'codes': [ 
                     'EBF2', 
                     'EBF3', 
                     'E24F', 
                     'E64F', 
                      ],
             }, 
         'Punctuation_marks': {
             'names': [ 
                     'hidot', 
                     'posit', 
                     'ductsimpl', 
                     'punctvers', 
                     'colelevposit', 
                     'colmidcomposit', 
                     'bidotscomposit', 
                     'tridotscomposit', 
                     'punctelev', 
                     'punctelevdiag', 
                     'punctelevhiback', 
                     'punctelevhack', 
                     'punctflex', 
                     'punctexclam', 
                     'punctinter', 
                     'punctintertilde', 
                     'punctinterlemn', 
                     'wavylin', 
                     'medcom', 
                     'parag', 
                     'renvoi', 
                     'virgsusp', 
                     'virgmin', 
                      ],
             'chars': [ 
                     u'\uF1F8', 
                     u'\uF1E2', 
                     u'\uF1E3', 
                     u'\uF1EA', 
                     u'\uF1E4', 
                     u'\uF1E5', 
                     u'\uF1F2', 
                     u'\uF1E6', 
                     u'\uF161', 
                     u'\uF1F0', 
                     u'\uF1FA', 
                     u'\uF1FB', 
                     u'\uF1F5', 
                     u'\uF1E7', 
                     u'\uF160', 
                     u'\uF1E8', 
                     u'\uF1F1', 
                     u'\uF1F9', 
                     u'\uF1E0', 
                     u'\uF1E1', 
                     u'\uF1EC', 
                     u'\uF1F4', 
                     u'\uF1F7', 
                      ],
             'codes': [ 
                     'F1F8', 
                     'F1E2', 
                     'F1E3', 
                     'F1EA', 
                     'F1E4', 
                     'F1E5', 
                     'F1F2', 
                     'F1E6', 
                     'F161', 
                     'F1F0', 
                     'F1FA', 
                     'F1FB', 
                     'F1F5', 
                     'F1E7', 
                     'F160', 
                     'F1E8', 
                     'F1F1', 
                     'F1F9', 
                     'F1E0', 
                     'F1E1', 
                     'F1EC', 
                     'F1F4', 
                     'F1F7', 
                      ],
             }, 
         'Supplemental_Punctuation': {
             'names': [ 
                     'luhsqbNT', 
                     'luslst', 
                     'ruslst', 
                     'dbloblhyph', 
                     'rlslst', 
                     'llslst', 
                     'verbarql', 
                     'verbarqr', 
                     'luhsqb', 
                     'ruhsqb', 
                     'llhsqb', 
                     'rlhsqb', 
                     'lUbrack', 
                     'rUbrack', 
                     'ldblpar', 
                     'rdblpar', 
                     'tridotsdownw', 
                     'tridotsupw', 
                     'quaddots', 
                     'fivedots', 
                     'punctpercont', 
                      ],
             'chars': [ 
                     u'\u2E00', 
                     u'\u2E0C', 
                     u'\u2E0D', 
                     u'\u2E17', 
                     u'\u2E1C', 
                     u'\u2E1D', 
                     u'\u2E20', 
                     u'\u2E21', 
                     u'\u2E22', 
                     u'\u2E23', 
                     u'\u2E24', 
                     u'\u2E25', 
                     u'\u2E26', 
                     u'\u2E27', 
                     u'\u2E28', 
                     u'\u2E29', 
                     u'\u2E2A', 
                     u'\u2E2B', 
                     u'\u2E2C', 
                     u'\u2E2D', 
                     u'\u2E2E', 
                      ],
             'codes': [ 
                     '2E00', 
                     '2E0C', 
                     '2E0D', 
                     '2E17', 
                     '2E1C', 
                     '2E1D', 
                     '2E20', 
                     '2E21', 
                     '2E22', 
                     '2E23', 
                     '2E24', 
                     '2E25', 
                     '2E26', 
                     '2E27', 
                     '2E28', 
                     '2E29', 
                     '2E2A', 
                     '2E2B', 
                     '2E2C', 
                     '2E2D', 
                     '2E2E', 
                      ],
             }, 
         'Mathematical_Operators': {
             'names': [ 
                     'minus', 
                     'infin', 
                     'logand', 
                     'tridotupw', 
                     'tridotdw', 
                     'quaddot', 
                     'est', 
                     'esse', 
                     'notequals', 
                     'dipledot', 
                      ],
             'chars': [ 
                     u'\u2212', 
                     u'\u221E', 
                     u'\u2227', 
                     u'\u2234', 
                     u'\u2235', 
                     u'\u2237', 
                     u'\u223B', 
                     u'\u2248', 
                     u'\u2260', 
                     u'\u22D7', 
                      ],
             'codes': [ 
                     '2212', 
                     '221E', 
                     '2227', 
                     '2234', 
                     '2235', 
                     '2237', 
                     '223B', 
                     '2248', 
                     '2260', 
                     '22D7', 
                      ],
             }, 
         'Characters_with_macron_and_breve': {
             'names': [ 
                     'Amacrbreve', 
                     'amacrbreve', 
                     'AEligmacrbreve', 
                     'aeligmacrbreve', 
                     'Emacrbreve', 
                     'emacrbreve', 
                     'Imacrbreve', 
                     'imacrbreve', 
                     'Omacrbreve', 
                     'omacrbreve', 
                     'OEligmacrbreve', 
                     'oeligmacrbreve', 
                     'Oslashmacrbreve', 
                     'oslashmacrbreve', 
                     'Umacrbreve', 
                     'umacrbreve', 
                     'Ymacrbreve', 
                     'ymacrbreve', 
                      ],
             'chars': [ 
                     u'\uE010', 
                     u'\uE410', 
                     u'\uE03D', 
                     u'\uE43D', 
                     u'\uE0B7', 
                     u'\uE4B7', 
                     u'\uE137', 
                     u'\uE537', 
                     u'\uE21B', 
                     u'\uE61B', 
                     u'\uE260', 
                     u'\uE660', 
                     u'\uE253', 
                     u'\uE653', 
                     u'\uE30B', 
                     u'\uE70B', 
                     u'\uE375', 
                     u'\uE775', 
                      ],
             'codes': [ 
                     'E010', 
                     'E410', 
                     'E03D', 
                     'E43D', 
                     'E0B7', 
                     'E4B7', 
                     'E137', 
                     'E537', 
                     'E21B', 
                     'E61B', 
                     'E260', 
                     'E660', 
                     'E253', 
                     'E653', 
                     'E30B', 
                     'E70B', 
                     'E375', 
                     'E775', 
                      ],
             }, 
         }
  
  
  
  
  
