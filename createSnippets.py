#!/usr/bin/python
# -*- coding: utf-8 -*-
import xml.etree.cElementTree as et
import sys
from PIL import Image
import os,codecs,string
import argparse
from menota import MenotaEntities
import re

def ConvertWithMenota(string):
  pattern = re.compile(r'(__\w+__)')
  for f in  re.findall(pattern,string):
    # convert
    fsplit = string.split('_')
    utext =  fsplit[2]+getattr(MenotaEntities,fsplit[3])
    print "convert",f,"to",utext
    string = string.replace(f,utext,1)
    
  return string

def Generate(input_xml,output_dir,image_dir):
  counter = {}
  wcounter = 0
  lcounter = 0
  ofile = {}
  nbPage = 0
  
  for subdir in ["Lines","Words","Chars"]:
    sub_output = os.path.join(output_dir,subdir)
    if not os.path.exists(sub_output):
      os.mkdir(sub_output)

  for event, elem in et.iterparse(input_xml):
     if elem.tag == "SinglePage":
        fileName = os.path.join(image_dir,os.path.basename(elem.get('FileName')))
        if not os.path.exists(fileName):
            print fileName,"does not exists"
            continue
        nbPage+=1
        #if nbPage>2:continue
        im1 = Image.open(fileName)
        #print fileName
        for line in elem.getiterator("Line"):
          lineAnnot =  ConvertWithMenota(line.get("Value"))
          lineTitle = os.path.basename(fileName)
          # extract line image and save it
          if  (line.get("Left") and line.get("Top") and line.get("Right") and line.get("Bottom")):
             lbox = ( int(line.get("Left")),int(line.get("Top")),  int(line.get("Right")),  int(line.get("Bottom")))
             lregion = im1.crop(lbox)
             lSaveName = os.path.join(output_dir,"Lines/%s.png" % (str(lcounter)))
             lLinkName = "Lines/%s.png" % (str(lcounter))
             lregion.save(lSaveName)
             lcounter+=1
            
          for l in line.getiterator("Word"):
              wordAnnot =  ConvertWithMenota(l.get("Value"))
                  
              # extract word image and save it
              if not (l.get("Left") and l.get("Top") and l.get("Right") and l.get("Bottom")):
                  continue
              wbox = ( int(l.get("Left")),int(l.get("Top")),  int(l.get("Right")),  int(l.get("Bottom")))
              wregion = im1.crop(wbox)
              wSaveName = os.path.join(output_dir,"Words/%s.png" % (str(wcounter)))
              wLinkName = "Words/%s.png" % (str(wcounter))
              wregion.save(wSaveName)
              wcounter+=1

              for w in l.getiterator("Field"):
                 annot = w.get("Value")
                 if annot=='.': annot='dot'
                 if annot=='?': annot='question_mark'
                 if annot=="'": annot='apostrophe'
                 if annot==u'\ua75b': annot="r_rotunda"
                 if annot==u'\ua77a': annot= "d_insular"
                 if annot==u'ſ': annot="s_long"
                 unicode_annot = ConvertWithMenota(annot)

                 path = os.path.join(output_dir,"Chars",annot)
                 if not os.path.exists(path):
                     os.mkdir(path)
                 if not counter.has_key(annot):
                     counter[annot]=0
                     ofile[annot]=codecs.open(os.path.join(output_dir,"Chars",annot,"list.html"),'w','utf8')

                 #if counter[annot]==1: 
                 #   continue

                 # extract character image and save it       
                 box = ( int(w.get("Left")),int(w.get("Top")),  int(w.get("Right")),  int(w.get("Bottom")))
                 if box[0]==box[2]: # pathological case
                    continue
                 region = im1.crop(box)
                 saveName = "%s/%s.png" % (path,str(counter[annot]))
                 region.save(saveName)

                 # add link in character page
                 linkName = "Chars/%s/%s.png" % (annot,str(counter[annot]))
                 ofile[annot].write("""<li><img src="%s" data-image-url="%s" data-word-annot="%s" data-image-url2="%s" data-line-annot="%s"  title="%s" class="preview" rel="popover"/></li>\n""" % (linkName,wLinkName,wordAnnot,lLinkName,lineAnnot, lineTitle))

                 #print annot,saveName

                 counter[annot]+=1
        elem.clear()      

  for k in ofile.keys():
      ofile[k].write("""<script>
      $(function (){$('.preview').popover(
      {    'trigger':'hover',
            'placement':'bottom',
          'html':true,
          'content':function(){return $(this).data('wordAnnot')+"<br/><img src='"+$(this).data('imageUrl')+"'> <br/><hr/>"+$(this).data('lineAnnot')+"<br/> <img src='"+$(this).data('imageUrl2')+"'>";}
      });
      });
      </script>
      """)
      ofile[k].close()

  
  lower = codecs.open(os.path.join(output_dir,"lower.html"),'w','utf8')
  upper = codecs.open(os.path.join(output_dir,"upper.html"),'w','utf8')
  conjonctions =  codecs.open(os.path.join(output_dir,"conjonctions.html"),'w','utf8')
  ponctuations =  codecs.open(os.path.join(output_dir,"ponctuations.html"),'w','utf8')
  combined =  codecs.open(os.path.join(output_dir,"combined.html"),'w','utf8')

  keys = counter.keys()
  keys.sort()
  for k in keys:
    if k == "dot":
        ponctuations.write("""<li><a  href="javascript:location.href='collage.html?chars=%s'">%s (%d)</a></li>\n"""%(k,".",counter[k]))
        continue

    out = None
    if "CONJ" in k or "ELIS" in k or "LIG" in k:
       out = conjonctions
    if k in ["'",",",":","?","´","apostrophe","question_mark"]:
       out = ponctuations
    if k in string.uppercase:
       out = upper
    if k in string.lowercase:
       out = lower
    if k in u"é":
       out = lower
    if k in ["r_rotunda","d_insular","s_long"]:
       out = lower
    if not out:
       print "no out", k
       out = combined

    unicode_k = ConvertWithMenota(k)
    out.write("""<li><a  href="javascript:location.href='collage.html?chars=%s'">%s <span class="badge">%d</span></a></li>\n"""%(k,unicode_k,counter[k]))



if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Create snippets and html pages for character segmentation display')
  parser.add_argument('input_xml',
                   help='The XML file path in format DocumentList contaning the annotations')
  parser.add_argument('--output_dir',
                   help='target directory')
  parser.add_argument('--image_dir',
                   help='directory of the full images')
  

  args = parser.parse_args()
  Generate(args.input_xml,args.output_dir,args.image_dir)
